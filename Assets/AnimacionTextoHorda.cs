﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimacionTextoHorda : MonoBehaviour {

    public float tiempo;
    public AnimationsPlaylist lista;

    private IEnumerator CargarCartel()
    {
        lista.PlayAnimation("entrada", 0, 1, 1);
        yield return new WaitForSeconds(tiempo);
        lista.PlayAnimation("salida", 0, 1, 1);
    }

    public void NumeroHorda(string texto, string hordasTotal)
    {
        StartCoroutine(CargarCartel());
        GetComponentsInChildren<Text>()[1].text = texto  + "/" + hordasTotal;
    }

    public void SubirAntes()
    {
        lista.PlayAnimation("salida", 0, 1, 1);
    }


}
