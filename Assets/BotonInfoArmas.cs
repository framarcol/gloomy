﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BotonInfoArmas : MonoBehaviour {

    private void Update()
    {
        if(GetComponentInParent<SpriteRenderer>().sprite == null)
        {
            GetComponent<Image>().color = new Color(1,1,1,0);
        }
        else
        {
            GetComponent<Image>().color = new Color(1, 1, 1, 1);
        }
    }
}
