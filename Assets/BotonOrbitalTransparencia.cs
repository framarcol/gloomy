﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BotonOrbitalTransparencia : MonoBehaviour {

    private void OnEnable()
    {
        if (GetComponent<Button>().interactable)
        {
            GetComponentInChildren<Text>().color = new Color(GetComponentInChildren<Text>().color.r, GetComponentInChildren<Text>().color.g, GetComponentInChildren<Text>().color.b, 1);
            GetComponentsInChildren<Image>()[1].color = new Color(1, 1, 1, 1);
        }
        else{
            GetComponentInChildren<Text>().color = new Color(GetComponentInChildren<Text>().color.r, GetComponentInChildren<Text>().color.g, GetComponentInChildren<Text>().color.b, 0.5f);
            GetComponentsInChildren<Image>()[1].color = new Color(1, 1, 1, 0.5f);
        }
    }
}
