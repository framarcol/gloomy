﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Caravana : MonoBehaviour {

    public bool moverIzquierda;
    public bool moverDerecha;
    public float Velocidad;
    public float frecuenciaMin, frecuenciaMax;
    public int numReapariciones;
    public bool iniciado;
    int vecesQueLlevo;
    Vector2 posicionInicial;
    Quaternion rotacionInicial;
    private void Start()
    {
        vecesQueLlevo = 0;
        rotacionInicial = transform.localRotation;
        posicionInicial = transform.localPosition;
        StartCoroutine(ArrancarBuitre());

        if (moverDerecha)
        {
            transform.localRotation = new Quaternion(0, 0, 0, 0);
        }
        else
        {
            transform.localRotation = rotacionInicial;
        }

    }

    void IniciarBuitre()
    {
        StartCoroutine(ArrancarBuitre());
    }

    IEnumerator ArrancarBuitre()
    {
        float numero = Random.Range(frecuenciaMin, frecuenciaMax);
        yield return new WaitForSeconds(numero);
        transform.localPosition = posicionInicial;
        vecesQueLlevo++;
        if (vecesQueLlevo < numReapariciones)
            IniciarBuitre();
    }


    public void BuitreVerdeAccion()
    {

        iniciado = true;
    }

    void Update()
    {

        if (iniciado)
        {

            transform.Translate(Vector3.left * Time.deltaTime * (Velocidad / 2));

        }
    }
}
