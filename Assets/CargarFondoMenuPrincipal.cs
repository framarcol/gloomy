﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CargarFondoMenuPrincipal : MonoBehaviour {

    public Sprite[] fondosFases;
    public GameObject PantallaSelectorFases;

	void Update () {

        if(!PantallaSelectorFases.activeSelf)
        {
            GetComponent<SpriteRenderer>().sprite = fondosFases[0];
        }
        else
        {
          //  Debug.Log(BotonesMenu.mundoSeleccionado);
            GetComponent<SpriteRenderer>().sprite = fondosFases[BotonesMenu.mundoSeleccionado];
        }
		
	}


}
