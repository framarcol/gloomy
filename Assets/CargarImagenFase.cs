﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CargarImagenFase : MonoBehaviour {

    public Sprite[] ImagenMundo;
    public Sprite[] frontal;

    public bool esFrontal;

    private void OnEnable()
    {
        if(BotonesMenu.mundoSeleccionado != 0)
        {
            if(!esFrontal)
            GetComponent<Image>().sprite = ImagenMundo[BotonesMenu.mundoSeleccionado - 1];
            else GetComponent<Image>().sprite = frontal[BotonesMenu.mundoSeleccionado - 1];
        }
      
    }
}
