﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CompruebaIdioma : MonoBehaviour {

    public Toggle spanish, chinese;

	// Use this for initialization
	public void Opcion () {

        if (spanish.isOn)
        {
            PlayerPrefs.SetString(SaveGame.idioma, "Spanish");
        }
        else if(chinese.isOn)
        {
            PlayerPrefs.SetString(SaveGame.idioma, "Chinese");
        }
        else
        {
            PlayerPrefs.SetString(SaveGame.idioma, "English");
        }

       // Debug.Log(PlayerPrefs.GetString(SaveGame.idioma));

        SceneManager.LoadScene(0);
        
	}
}
