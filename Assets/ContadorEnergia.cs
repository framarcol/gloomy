﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContadorEnergia : MonoBehaviour {

    public bool soloCantidad;

    private void Update()
    {
        if (!soloCantidad)
            GetComponent<Text>().text = PlayerPrefs.GetInt(SaveGame.energia).ToString() + "/3";
        else GetComponent<Text>().text = PlayerPrefs.GetInt(SaveGame.energia).ToString();
    }

  
}
