﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContadorEnergiaRestart : MonoBehaviour {


    void OnEnable()
    {
        if(PlayerPrefs.GetInt(SaveGame.nivelesDesbloquedos[BotonesMenu.mundoSeleccionado-1]) >= BotonesMenu.nivelMundoSeleccionado)
        {
            GetComponent<Text>().text = "0";
        }
        else
        {
            GetComponent<Text>().text = "1";
        }
    }

}
