﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorBotonSelector : MonoBehaviour {

    public static Vector2 touchPositionSelector;
    public Transform CanvasArmas;

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)   //Code for touchscreen
        {
            foreach (Touch toque in Input.touches)
            {
                if (toque.phase == TouchPhase.Ended)
                {
                    touchPositionSelector = Camera.main.ScreenToWorldPoint(toque.position);
                    ScreenTouched(Camera.main.ScreenToWorldPoint(toque.position));
                }

                if (toque.phase == TouchPhase.Moved)
                {
                    touchPositionSelector = Camera.main.ScreenToWorldPoint(toque.position);
                    ScreenTouched(Camera.main.ScreenToWorldPoint(toque.position));
                }

                if (toque.phase == TouchPhase.Stationary)
                {
                    touchPositionSelector = Camera.main.ScreenToWorldPoint(toque.position);
                    ScreenTouched(Camera.main.ScreenToWorldPoint(toque.position));
                }
            }
        }
        else
        {
            //No touchscreen detected
            if (Input.GetMouseButton(0))
            {
                touchPositionSelector = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                ScreenTouched(Camera.main.ScreenToWorldPoint(Input.mousePosition));
            }
        }


    }

    private void ScreenTouched(Vector2 screenTouchedPosition)
    {

            RaycastHit2D hit = Physics2D.Raycast(screenTouchedPosition, Vector2.zero, 0f);



            if (hit)
            {
             //   Debug.Log(hit.transform.name);
                switch (hit.transform.name)
                {
                    case "btnDerecha":

                    CanvasArmas.position = new Vector2(CanvasArmas.position.x - 10, CanvasArmas.position.y);
                        break;
                    case "btnIzquierda" :
                    CanvasArmas.position = new Vector2(CanvasArmas.position.x + 10, CanvasArmas.position.y);

                    break;
                }
            }           
    }
}
