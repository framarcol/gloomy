﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorPosicion : MonoBehaviour {

    Vector3 tempVector3;
    Rigidbody2D myRigidbody2D;
    int entero;
    bool iniciado;
    bool creado;
    bool dentroAtrezzo, zombieAdjuntado;
    bool porton;
  //  GameObject[] enePrefab;
    GameObject enemigos;
    public bool soyYo;
    private void Awake()
    {
       myRigidbody2D = gameObject.GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        enemigos = GameObject.FindGameObjectWithTag("Enemigos");
    }

    public void IniciarPersonaje(bool esPorton)
    {
        string[] pos2;
        pos2 = gameObject.name.Split('s');
        entero = int.Parse(pos2[1]);
      //  Debug.Log(entero);
        entero = GetComponentInParent<GeneradorEnemigosV3>().choices[entero];
        
        iniciado = true;
        porton = esPorton;
    }

    void MetodoVelocidad(float velocidad)
    {
        if (!GetComponentInParent<GeneradorEnemigosV3>().haciaDerecha)
            tempVector3 = transform.TransformDirection(Vector3.right) * -1 * velocidad;
        else tempVector3 = transform.TransformDirection(Vector3.right) * velocidad;
        myRigidbody2D.velocity = new Vector2(tempVector3.x, myRigidbody2D.velocity.y);
    }

    private void Update()
    {
       /* if (zombieAdjuntado)
        {
            if(GetComponentsInChildren<GenericEnemyControl>().Length == 0 && GetComponentsInChildren<ParacaEnemyControl>().Length == 0 &&
                GetComponentsInChildren<VoladorEnemyControl>().Length == 0)
            {
                gameObject.SetActive(false);
            }
        }*/

        if(iniciado)
        {
            switch (entero)
            {
                case 0:
                    MetodoVelocidad(1);              
                    break;
                case 1:
                    MetodoVelocidad(2.5f);          
                    break;
                case 2:
                    MetodoVelocidad(0.6f);
                    break;
                case 3:
                    MetodoVelocidad(1);
                    break;
                case 4:
                    MetodoVelocidad(1);
                    break;
                case 5:
                    MetodoVelocidad(1.9f);
                    break;
                case 6:
                    MetodoVelocidad(3);
                    break;
                case 8:
                    MetodoVelocidad(0.35f);
                    break;
                case 9:
                    MetodoVelocidad(1);             
                    break;
                case 10:
                    MetodoVelocidad(1);           
                    break;
                case 11:
                    MetodoVelocidad(1.3f);
                    break;
                case 12:
                    transform.Translate(Vector3.left * Time.deltaTime * (2 / 2));
                    break;
                case 13:
                    transform.Translate(Vector3.left * Time.deltaTime * (5 / 2));
                    break;
                case 14:
                    transform.Translate(Vector3.left * Time.deltaTime * (7 / 2));
                    break;
                case 15:
                    transform.Translate(Vector3.left * Time.deltaTime * (2 / 2));
                    break;
                case 16:
                    transform.Translate(Vector3.left * Time.deltaTime * (5 / 2));
                    break;
                case 17:
                    transform.Translate(Vector3.left * Time.deltaTime * (2 / 2));
                    break;
                case 18:
                    transform.Translate(Vector3.left * Time.deltaTime * (6 / 2));
                    break;
                case 19:
                    transform.Translate(Vector3.left * Time.deltaTime * (2 / 2));
                    break;
                case 20:
                    dentroAtrezzo = true;
                    break;
            }
        }
        else
        {
            myRigidbody2D.velocity = new Vector2(0,0);
        }

        if (dentroAtrezzo)
        {
            if (iniciado)
            {
                AdjuntarZombie();
                dentroAtrezzo = false;
            }
        }
    }

    void MetodoIniciarZombie(int idEnemigo)
    {
        if (enemigos.GetComponentsInChildren<GenericEnemyControl>().Length != 0)
        {
            for (int i = 0; i < enemigos.GetComponentsInChildren<GenericEnemyControl>().Length; i++)
            {
                if (enemigos.GetComponentsInChildren<GenericEnemyControl>()[i].idEnemigo == idEnemigo)
                {
                    GenericEnemyControl enem0 = enemigos.GetComponentsInChildren<GenericEnemyControl>()[i];
                    enem0.transform.parent = transform;
                    enem0.transform.position = transform.position;
                    if (!porton)
                        enem0.IniciarPersonaje();
                    else enem0.IniciarPersonajePorton();
                    zombieAdjuntado = true;
                    iniciado = false;
                    break;
                }
                else
                {
                    if (enemigos.GetComponentsInChildren<GenericEnemyControl>().Length == i + 1)
                    {
                        CrearZombie();
                    }
                }
            }
        }
        else
        {
            CrearZombie();
        }
    }

    void MetodoIniciarVagoneta(int idVagoneta)
    {
        if(enemigos.GetComponentsInChildren<GenericEnemyControl>().Length != 0)
        {
            for (int i = 0; i < enemigos.GetComponentsInChildren<VagonetaControlador>().Length; i++)
            {
                if (enemigos.GetComponentsInChildren<VagonetaControlador>()[i].idVagoneta == idVagoneta)
                {
                    VagonetaControlador enem10 = enemigos.GetComponentsInChildren<VagonetaControlador>()[i];
                    enem10.transform.parent = transform;
                    enem10.transform.position = transform.position;
                    enem10.IniciarVagoneta();
                    enem10.GetComponent<MoverVagoneta>().IniciarNiebla();
                    zombieAdjuntado = true;
                    iniciado = false;
                    break;
                }
                else
                {
                    if (enemigos.GetComponentsInChildren<VagonetaControlador>().Length == i + 1)
                    {
                        CrearZombie();
                    }
                }
            }
        }
        else
        {
            CrearZombie();
        }
      
    }

    void AdjuntarZombie()
    {
        if (!zombieAdjuntado)
        {
            switch (entero)
            {
                case 0:
                    MetodoIniciarZombie(1);
                    break;
                case 1:
                    MetodoIniciarZombie(6);
                    break;
                case 2:
                    MetodoIniciarZombie(4);
                    break;
                case 3:
                    MetodoIniciarZombie(2);
                    break;
                case 4:
                    MetodoIniciarZombie(3);
                    break;
                case 5:
                    MetodoIniciarZombie(7);
                    break;
                case 6:
                    MetodoIniciarZombie(5);
                    break;
                case 8:
                    MetodoIniciarZombie(0);
                    break;
                case 9:
                    if (enemigos.GetComponentsInChildren<VoladorEnemyControl>().Length != 0)
                    {
                        VoladorEnemyControl enem9 = enemigos.GetComponentInChildren<VoladorEnemyControl>();
                        enem9.transform.parent = transform;
                        enem9.transform.position = transform.position;
                        enem9.IniciarPersonaje();
                        zombieAdjuntado = true;
                        iniciado = false;
                    }
                    else
                    {
                        CrearZombie();
                    }
                    break;
                case 10:
                    MetodoIniciarZombie(3);
                    break;
                case 11:
                    if (enemigos.GetComponentsInChildren<ParacaEnemyControl>().Length != 0)
                    {
                        ParacaEnemyControl enem11 = enemigos.GetComponentInChildren<ParacaEnemyControl>();
                        enem11.transform.parent = transform;
                        enem11.transform.position = transform.position;
                        enem11.IniciarPersonaje();
                        zombieAdjuntado = true;
                        StartCoroutine(IniciadoDes());
                    }
                    else
                    {
                        CrearZombie();
                    }
                    break;
                case 12:
                    MetodoIniciarVagoneta(entero);
                    break;
                case 13:
                    MetodoIniciarVagoneta(entero);
                    break;
                case 14:
                    MetodoIniciarVagoneta(entero);
                    break;
                case 15:
                    MetodoIniciarVagoneta(entero);
                    break;
                case 16:
                    MetodoIniciarVagoneta(entero);
                    break;
                case 17:
                    MetodoIniciarVagoneta(entero);
                    break;
                case 18:
                    MetodoIniciarVagoneta(entero);
                    break;
                case 19:
                    MetodoIniciarVagoneta(entero);
                    break;
                case 20:
                    GetComponentInChildren<MoverNiebla>().IniciarNiebla();
                    zombieAdjuntado = true;
                    iniciado = false;
                    break;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if(!zombieAdjuntado)
        {
            if (collision.gameObject.name == "Atrezzo40")
            {   
                if (iniciado) AdjuntarZombie();
                else dentroAtrezzo = true;
            }
        }
       
    }

    void MetodoCrearZombie(string nombre)
    {
        GameObject objeto0 = Instantiate((GameObject)Resources.Load(nombre), transform);
        GenericEnemyControl enem0 = objeto0.GetComponentInChildren<GenericEnemyControl>();
        enem0.transform.parent = transform;
        enem0.transform.position = transform.position;
        if (!porton)
            enem0.IniciarPersonaje();
        else enem0.IniciarPersonajePorton();
        zombieAdjuntado = true;
        iniciado = false;
    }

    void MetodoCrearVagoneta(string nombre)
    {
        GameObject objeto12 = Instantiate((GameObject)Resources.Load(nombre), transform);
        VagonetaControlador enem12 = objeto12.GetComponent<VagonetaControlador>();
        enem12.transform.parent = transform;
        enem12.transform.position = transform.position;
        enem12.IniciarVagoneta();
        enem12.GetComponent<MoverVagoneta>().IniciarNiebla();
        zombieAdjuntado = true;
        iniciado = false;
    }

    void CrearZombie()
    {
        switch (entero)
        {
            case 0:
                MetodoCrearZombie("EnemyStandard");
                break;
            case 1:
                MetodoCrearZombie("EnemyMidget");
                break;
            case 2:
                MetodoCrearZombie("EnemyMuscle");
                break;
            case 3:
                MetodoCrearZombie("EnemySkellington");
                break;
            case 4:
                MetodoCrearZombie("EnemyStandardHalfZombie");
                break;
            case 5:
                MetodoCrearZombie("EnemyLoca");
                break;
            case 6:
                MetodoCrearZombie("EnemyCabeza1");
                break;
            case 8:
                MetodoCrearZombie("EnemyColossus");
                break;
            case 9:
                GameObject objeto9 = Instantiate((GameObject)Resources.Load("EnemyDragon"), transform);
                VoladorEnemyControl enem9 = objeto9.GetComponentInChildren<VoladorEnemyControl>();
                enem9.transform.parent = transform;
                enem9.transform.position = transform.position;
                if (!porton)
                    enem9.IniciarPersonaje();
                else enem9.IniciarPersonajePorton();
                zombieAdjuntado = true;
                iniciado = false;
                break;
            case 10:
                MetodoCrearZombie("EnemyStandardHalfZombie");
                break;
            case 11:
                GameObject objeto11 = Instantiate((GameObject)Resources.Load("EnemyParachute"), transform);
                ParacaEnemyControl enem11 = objeto11.GetComponentInChildren<ParacaEnemyControl>();
                enem11.transform.parent = transform;
                enem11.transform.position = transform.position;
                if (!porton)
                    enem11.IniciarPersonaje();
                else enem11.IniciarPersonajePorton();
                zombieAdjuntado = true;
                StartCoroutine(IniciadoDes());
                break;
            case 12:
                Debug.Log("d");
                MetodoCrearVagoneta("WagonDynamiteS2");
                break;
            case 13:
                MetodoCrearVagoneta("WagonDynamiteS5");
                break;
            case 14:
                MetodoCrearVagoneta("WagonDynamiteS7");
                break;
            case 15:
                MetodoCrearVagoneta("WagonCoalS2");
                break;
            case 16:
                MetodoCrearVagoneta("WagonCoalS5");
                break;
            case 17:
                MetodoCrearVagoneta("WagonGoldS2");
                break;
            case 18:
                MetodoCrearVagoneta("WagonGoldS6");
                iniciado = false;
                break;
            case 19:
                MetodoCrearVagoneta("RandomwagonS2");
                break;
            case 20:
                GetComponentInChildren<MoverNiebla>().IniciarNiebla();
                zombieAdjuntado = true;
                iniciado = false;
                break;

        }
    }

    IEnumerator IniciadoDes()
    {
        yield return new WaitForSeconds(0f);
        iniciado = false;
    }
}
