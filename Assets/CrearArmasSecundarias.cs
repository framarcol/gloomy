﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrearArmasSecundarias : MonoBehaviour {

    MiarmaControla armas;


    private void Start()
    {
        armas = GameObject.FindGameObjectWithTag("ArmaTag").GetComponentInChildren<MiarmaControla>();
    }

    public void IniciarBarricada(int id)
    {
        armas.Barricada(id);
    }

    public void IniciarMina(int id)
    {
        armas.Mina(id);
    }

    public void IniciarPincho(int id)
    {
        armas.Pincho(id);
    }

}
