﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CuentaAtras : MonoBehaviour {

    System.DateTime hora2;
    public bool subido;
    // Use this for initialization
    void OnEnable () {

      /*   if(PlayerPrefs.GetInt(SaveGame.energia) < 3)
        {
            System.DateTime hora = System.DateTime.Parse(PlayerPrefs.GetString(SaveGame.horaObjectivo));

            hora2 = System.DateTime.Parse(PlayerPrefs.GetString(SaveGame.ultimaHora));

   //          Debug.Log(System.DateTime.Compare(hora, hora2));

        }*/
    }

    private void Update()
    {
        System.DateTime horaActual = System.DateTime.Now;
#if UNITY_EDITOR
      //  PlayerPrefs.SetString(SaveGame.horaObjectivo, horaActual.ToString());
#endif
        hora2 = System.DateTime.Parse(PlayerPrefs.GetString(SaveGame.horaObjectivo));

        if (PlayerPrefs.GetInt(SaveGame.energia) < 3 && PlayerPrefs.GetString(SaveGame.ultimaHora) != "1")
        {
            if((int)hora2.Subtract(horaActual).Seconds >= 10)
            GetComponent<Text>().text = (int)hora2.Subtract(horaActual).TotalMinutes + ":" + (int)hora2.Subtract(horaActual).Seconds;
            else
            {
                GetComponent<Text>().text = (int)hora2.Subtract(horaActual).TotalMinutes + ":0" + (int)hora2.Subtract(horaActual).Seconds;
            }

            if (hora2.Subtract(horaActual).TotalDays < 0) if (PlayerPrefs.GetInt(SaveGame.energia) < 3) PlayerPrefs.SetInt(SaveGame.energia, 3);

            if((int)hora2.Subtract(horaActual).TotalMinutes == 0 && (int)hora2.Subtract(horaActual).Seconds <= 1)
            {
                if(PlayerPrefs.GetInt(SaveGame.energia) < 3)
                {
                    if((int)hora2.Subtract(horaActual).TotalMinutes< -15)
                    {
                        PlayerPrefs.SetInt(SaveGame.energia, 3);
                    }     
                    else if((int)hora2.Subtract(horaActual).TotalMinutes < -10)
                    {
                        if(PlayerPrefs.GetInt(SaveGame.energia) >= 1)
                        {
                            PlayerPrefs.SetInt(SaveGame.energia, 3);
                        }                     
                        else
                        {
                            PlayerPrefs.SetInt(SaveGame.energia, PlayerPrefs.GetInt(SaveGame.energia) + 2);
                        }
                    }
                    else
                    {
                        subido = true;
                        PlayerPrefs.SetInt(SaveGame.energia, PlayerPrefs.GetInt(SaveGame.energia) + 1);
                    }                  

                    System.DateTime hora = System.DateTime.Parse(PlayerPrefs.GetString(SaveGame.horaObjectivo));

                    PlayerPrefs.SetString(SaveGame.ultimaHora, hora.ToString());

                    hora2 = hora.AddMinutes(30);

                    PlayerPrefs.SetString(SaveGame.horaObjectivo, hora2.ToString());
                }
       
            }
        }
        else
        {
            GetComponent<Text>().text = "";
        }

       // if (PlayerPrefs.GetInt(SaveGame.energia) == 5) GetComponent<Text>().text = "";
    }
}
