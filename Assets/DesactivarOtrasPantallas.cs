﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesactivarOtrasPantallas : MonoBehaviour {

   public  GameObject[] objeto1;

     
	// Use this for initialization
	void OnEnable () {
		for(int i = 0; i < objeto1.Length; i++)
        {
            objeto1[i].SetActive(false);
        }
	}
}
