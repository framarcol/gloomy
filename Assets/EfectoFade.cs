﻿using System.Collections;
using System.Collections.Generic;
using Tapdaq;
using UnityEngine;
using UnityEngine.UI;

public class EfectoFade : MonoBehaviour {

    bool isInTransition;
    float transition;
    bool isShowing;
    float duration;

    public Image EvitarPulsar;

    private void Start()
    {
        AdManager.LoadRewardedVideo("oro");
    }

    private void OnEnable()
    {
        EvitarPulsar.raycastTarget = true;
        StartCoroutine(DeshabilitarPulsacion());
    }

    IEnumerator DeshabilitarPulsacion()
    {
        yield return new WaitForSeconds(1);
        EvitarPulsar.raycastTarget = false;
    }

    private void Update()
    {
        if (!isInTransition)
            return;

        transition += (isShowing) ? Time.deltaTime * (1 / duration) : -Time.deltaTime * (1 / duration);
        GetComponent<CanvasGroup>().alpha = transition;

        if (transition > 1 || transition < 0)
        {
            isInTransition = false;
        }
    }

    public void Fade(bool showing, float duration)
    {
        isShowing = showing;
        isInTransition = true;
        this.duration = duration;
        transition = (isShowing) ? 0 : 1;
    }
}
