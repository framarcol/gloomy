﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergySystem : MonoBehaviour {

	// Use this for initialization
	void Start () {

#if UNITY_EDITOR
       // PlayerPrefs.DeleteAll();
#endif

        if (!PlayerPrefs.HasKey(SaveGame.ultimaHora))
        {
            PlayerPrefs.SetString(SaveGame.ultimaHora, "1");
            PlayerPrefs.SetInt(SaveGame.energia, 5);
        }
    }
}
