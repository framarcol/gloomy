﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscudoDesaparicion : MonoBehaviour {

    TowerControl controlTorre;

    private void Start()
    {
        controlTorre = GetComponentInParent<TowerControl>();
    }


    // Update is called once per frame
    void Update () {

        if (controlTorre.energy <= 0) gameObject.SetActive(false);
		
	}
}
