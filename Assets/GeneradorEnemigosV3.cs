﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorEnemigosV3 : MonoBehaviour {


    [HideInInspector]
    public int[] choices;
    Transform[] posiciones;
    public bool haciaDerecha;
    public bool omitirHorda;
    [HideInInspector]
    public bool activado;

    GeneradorEnemigosV3()
    {
        choices = new int[30];
    }

    public int NumeroPosiciones()
    { 
        return GetComponentsInChildren<ControladorPosicion>().Length;
    }



    public void Activar()
    {
        posiciones = GetComponentsInChildren<Transform>();

        if (haciaDerecha)
        {
            for (int i = 0; i < posiciones.Length; i++)
            {
                posiciones[i].position = new Vector2(posiciones[i].position.x * -1, posiciones[i].position.y);
            }
        }

    }

    public void Desactivar()
    {
        if (GetComponentsInChildren<GenericEnemyControl>().Length == 0 && GetComponentsInChildren<ParacaEnemyControl>().Length == 0 && GetComponentsInChildren<VoladorEnemyControl>().Length == 0)
            gameObject.SetActive(false);
    }

    public void ActivarSimple()
    {
        activado = true;
        posiciones = GetComponentsInChildren<Transform>();

        if (haciaDerecha)
        {
            for (int i = 0; i < posiciones.Length; i++)
            {
                posiciones[i].position = new Vector2(posiciones[i].position.x * -1, posiciones[i].position.y);
            }
        }
    }

    void Awake()
    {
        if (GetComponentInParent<GeneradorHordas>() != null)
        {
            if (!GetComponentInParent<GeneradorHordas>().HordasGeneradas && !GetComponentInParent<GeneradorHordas>().procedural)
            {
                IniciarHorda();
            }
        }
        else
        {
            IniciarHorda();
        }
    }

    void IniciarHorda()
    {
        // Debug.Log("aquisi;");
        activado = true;
        posiciones = GetComponentsInChildren<Transform>();

        if (haciaDerecha)
        {
            for (int i = 0; i < posiciones.Length; i++)
            {
                posiciones[i].position = new Vector2(posiciones[i].position.x * -1, posiciones[i].position.y);
            }
        }

        for (int i = 0; i < posiciones.Length-1; i++)
        {
          //  Instantiate(enemigo[0], posiciones[i + 1]);
        }
    }

    public bool GetHaciaDerecha()
    {
        return haciaDerecha;
    }
}
