﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorHordasPorton : MonoBehaviour {

    float[] tiempoHorda;
    GeneradorEnemigosV2[] hordas;
    GeneradorEnemigosV3[] hordasV3;
    int horda;
    bool PestanaGameOverMostrada;
    public bool hordasIniciadas;
    GameObject infoJuego;
    GeneradorHordas generadorHordas;
    public bool noHayEnemigos;
    public bool HordasGeneradas;
    public bool procedural;

    public bool V3;

    public AnimationsPlaylist porton;

  //  bool frontal;

    public void IniciarHordas()
    {
        porton.PlayAnimation("porton", 0, 1, 1);
        StartCoroutine(PortonActivado());
        porton.GetComponentInChildren<BoxCollider2D>().enabled = true;
        hordasIniciadas = false;
        horda = 0;
        if (!V3)
        {
            hordas = GetComponentsInChildren<GeneradorEnemigosV2>();
        }
        else
        {
            hordasV3 = GetComponentsInChildren<GeneradorEnemigosV3>();
        }
        generadorHordas = GameObject.FindGameObjectWithTag("GeneradorHordas").GetComponent<GeneradorHordas>();
        infoJuego = GameObject.FindGameObjectWithTag("GameController");

        if (!V3)
        {
            tiempoHorda = new float[hordas.Length];

            for (int i = 0; i < hordas.Length; i++)
            {
                string nombreHorda = hordas[i].gameObject.name;
                tiempoHorda[i] = float.Parse(nombreHorda.Substring(nombreHorda.Length - 2));
            }
        }
        else
        {
            tiempoHorda = new float[hordasV3.Length];

            for (int i = 0; i < hordasV3.Length; i++)
            {
                string nombreHorda = hordasV3[i].gameObject.name;
                tiempoHorda[i] = float.Parse(nombreHorda.Substring(nombreHorda.Length - 2));
            }
        }

       


        StartCoroutine(GenerarHordas());

    }

    IEnumerator PortonActivado()
    {
        yield return new WaitForSeconds(0.5f);
        porton.GetComponentInChildren<BoxCollider2D>().enabled = false;
    }

    private void Update()
    {
        if (!V3)
        {
            if (hordasIniciadas)
            {
                if (GetComponentsInChildren<GenericEnemyControl>().Length + GetComponentsInChildren<ParacaEnemyControl>().Length + GetComponentsInChildren<VoladorEnemyControl>().Length == 0)
                {
                    noHayEnemigos = true;

                    if (generadorHordas.noHayEnemigos)
                    {
                        if (GameObject.FindGameObjectWithTag("ProtaTag").GetComponent<ProtaControl>().estoyMuerto)
                        {
                            TouchControl touch = GameObject.FindGameObjectWithTag("GameController").GetComponent<TouchControl>();
                            touch.pauseStatus = true;
                            infoJuego.GetComponent<MenusEmergentes>().MostrarPausa();
                        }
                        else
                        {

                            TouchControl touch = GameObject.FindGameObjectWithTag("GameController").GetComponent<TouchControl>();
                            touch.pauseStatus = true;
                            infoJuego.GetComponent<MenusEmergentes>().MostrarPausa2();

                            GameObject.FindGameObjectWithTag("tower").GetComponent<TowerControl>().DeslizarTorre();

                            GameObject.FindGameObjectWithTag("ProtaTag").GetComponent<ProtaControl>().DeslizarPersonaje();

                        }
                    }
                }
            }
        }
        else
        {
            if (hordasIniciadas)
            {
                if (GetComponentsInChildren<ControladorPosicion>().Length == 0)
                {
                    noHayEnemigos = true;

                    if (generadorHordas.noHayEnemigos)
                    {
                        if (GameObject.FindGameObjectWithTag("ProtaTag").GetComponent<ProtaControl>().estoyMuerto)
                        {
                            TouchControl touch = GameObject.FindGameObjectWithTag("GameController").GetComponent<TouchControl>();
                            touch.pauseStatus = true;
                            infoJuego.GetComponent<MenusEmergentes>().MostrarPausa();
                        }
                        else
                        {

                            TouchControl touch = GameObject.FindGameObjectWithTag("GameController").GetComponent<TouchControl>();
                            touch.pauseStatus = true;
                            infoJuego.GetComponent<MenusEmergentes>().MostrarPausa2();

                            GameObject.FindGameObjectWithTag("tower").GetComponent<TowerControl>().DeslizarTorre();

                            GameObject.FindGameObjectWithTag("ProtaTag").GetComponent<ProtaControl>().DeslizarPersonaje();

                        }
                    }
                }
            }
        }
      
    }

    IEnumerator GenerarHordas()
    {
        if (!V3)
        {
            if (horda < hordas.Length)
            {
                yield return new WaitForSeconds(tiempoHorda[horda]);

                hordasIniciadas = true;

                for (int i = 0; i < hordas[horda].GetComponentsInChildren<GenericEnemyControl>().Length; i++)
                {
                    hordas[horda].GetComponentsInChildren<GenericEnemyControl>()[i].IniciarPersonajePorton();
                }
                for (int i = 0; i < hordas[horda].GetComponentsInChildren<ParacaEnemyControl>().Length; i++)
                {
                    hordas[horda].GetComponentsInChildren<ParacaEnemyControl>()[i].IniciarPersonajePorton();
                }
                for (int i = 0; i < hordas[horda].GetComponentsInChildren<VoladorEnemyControl>().Length; i++)
                {
                    hordas[horda].GetComponentsInChildren<VoladorEnemyControl>()[i].IniciarPersonajePorton();
                }
                horda++;
                StartCoroutine(GenerarHordas());
            }
            else
            {
                StopAllCoroutines();
            }
        }
        else
        {
            if (horda < hordasV3.Length)
            {
                yield return new WaitForSeconds(tiempoHorda[horda]);

                hordasIniciadas = true;

                for (int i = 0; i < hordasV3[horda].GetComponentsInChildren<ControladorPosicion>().Length; i++)
                {
                    hordasV3[horda].GetComponentsInChildren<ControladorPosicion>()[i].IniciarPersonaje(true);
                }
                horda++;
                StartCoroutine(GenerarHordas());
            }
            else
            {
                StopAllCoroutines();
            }
        }
      

    }
}
