﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tapdaq;
using UnityEngine.UI;

public class GestorAnuncios : MonoBehaviour {

    public Button botonAnuncioOrbital, botonAnuncioRecursos, botonRecursos, botonOroOrbital, botonDiamantesOrbital;

    public Button botonOrbital;

    public GameObject PanelRecursos, PanelOrbital;

    bool esRecursos;

    private void Awake()
    {
        esRecursos = false;
        botonAnuncioRecursos.interactable = false;
        botonAnuncioOrbital.interactable = false;
        botonOrbital.interactable = false;
    }

    public void ActivarPanelOrbital()
    {
   
        PanelRecursos.SetActive(false);
        PanelOrbital.SetActive(true);
    }

    public void DesactivarPanelOrbital()
    {
        PanelOrbital.SetActive(false);
    }

    public void ActivarPanelRecursos()
    {
        
        PanelOrbital.SetActive(false);
        PanelRecursos.SetActive(true);
    }

    public void DesactivarPanelRecursos()
    {
        PanelRecursos.SetActive(false);
    }


    private void OnEnable()
    {
        AdManager.LoadRewardedVideo("diamantes");
        if (PlayerPrefs.GetInt(SaveGame.oro) < 5000)
        {
            botonOroOrbital.interactable = false;
        }
        else
        {
            botonOroOrbital.interactable = true;
        }

        if (PlayerPrefs.GetInt(SaveGame.diamantes) < 1)
        {
            botonDiamantesOrbital.interactable = false;
        }
        else
        {
            botonDiamantesOrbital.interactable = true;
        }

        PlayerPrefs.SetInt(SaveGame.orbital, 0);

        if (PlayerPrefs.GetInt(SaveGame.orbital) == 0)
        {
            botonOrbital.interactable = true;
        }


        TDCallbacks.AdAvailable += OnAdAvailable;
        TDCallbacks.RewardVideoValidated += OnRewardVideoValidated;
    }

    private void OnDisable()
    {
        TDCallbacks.AdAvailable -= OnAdAvailable;
        TDCallbacks.RewardVideoValidated -= OnRewardVideoValidated;
    }

    public void TappedReward()
    {
        esRecursos = false;
        if (AdManager.IsRewardedVideoReady("diamantes"))
        {
            AdManager.ShowRewardVideo("diamantes");
        }
    }

    public void TappedRewardRecursos()
    {
        esRecursos = true;
       if (AdManager.IsRewardedVideoReady("diamantes"))
        {
            AdManager.ShowRewardVideo("diamantes");
        }
    }

    public void PremioOrbital()
    {
        PlayerPrefs.SetInt(SaveGame.orbital, 1);
        botonOrbital.interactable = false;
        DesactivarPanelOrbital();
        AdManager.LoadRewardedVideo("diamantes");
    }

    public void PremioRecursos()
    {
        ControladorSelectorArmas.totalPuntos += 100;
        botonRecursos.interactable = false;
        DesactivarPanelRecursos();
        AdManager.LoadRewardedVideo("diamantes");
    }

    public void ComprarOrbital()
    {
        PlayerPrefs.SetInt(SaveGame.oro, PlayerPrefs.GetInt(SaveGame.oro) - 5000);
        PremioOrbital();
    }

    public void ComprarOrbitalDiamantes()
    {
        PlayerPrefs.SetInt(SaveGame.diamantes, PlayerPrefs.GetInt(SaveGame.diamantes) - 1);
        PremioOrbital();
    }

    private void OnRewardVideoValidated(TDVideoReward videoReward)
    {
        if (videoReward.RewardValid)
        {

            if (!esRecursos)
            {
                PremioOrbital();
            }
            else
            {
                PremioRecursos();
            }
        }
    }

    private void OnAdAvailable(TDAdEvent e)
    {
        if (e.adType == "REWARD_AD" && e.tag == "diamantes")
        {
            botonAnuncioOrbital.interactable = true;
        }
        else
        {
            botonAnuncioOrbital.interactable = false;
        }

        if (e.adType == "REWARD_AD" && e.tag == "diamantes")
        {
            botonAnuncioRecursos.interactable = true;
        }
        else
        {
            botonAnuncioRecursos.interactable = false;
        }
    }
}
