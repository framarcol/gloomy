﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Tapdaq;

public class GestorAnunciosMenuPrincipal : MonoBehaviour {

    public Button botonMasRecursos, botonMasRecursos2, botonVerAnuncio, botonVerAnuncioPremio, botonParaVerAnuncio, botonParaVerAnuncio2, botonParaVerAnuncio3, botonParaVerAnuncio4;

    public GameObject panelEnergia, panelPremio, panelRecompensa;

    public Image imgOro, imgDiamante;
    public Text txtDinero;
    public Text txtWait, txtWaitPremio;

    bool esRecursos;

	void Start()
    {
        esRecursos = false;
        botonVerAnuncio.interactable = false;
        botonVerAnuncioPremio.interactable = false;

        if (PlayerPrefs.GetInt(SaveGame.energia) != 5)
        {
            botonMasRecursos.interactable = true;
            botonMasRecursos2.interactable = true;
        }
        else
        {
            botonMasRecursos.interactable = false;
            botonMasRecursos2.interactable = false;

        }
    }

    private void Update()
    {
        if (PlayerPrefs.HasKey(SaveGame.horaObjetivoAnuncio))
        {
            System.DateTime fechaActual = System.DateTime.Now;

            System.DateTime fechaDestino = System.DateTime.Parse(PlayerPrefs.GetString(SaveGame.horaObjetivoAnuncio));

             if (System.DateTime.Compare(fechaActual, fechaDestino) == -1)
              {
                  botonParaVerAnuncio.interactable = false;
                  botonParaVerAnuncio2.interactable = false;
                  botonParaVerAnuncio3.interactable = false;
                  botonParaVerAnuncio4.interactable = false;
            }
              else
              {
                  botonParaVerAnuncio.interactable = true;
                  botonParaVerAnuncio2.interactable = true;
                  botonParaVerAnuncio3.interactable = true;
                  botonParaVerAnuncio4.interactable = true;
            }
        }
    }

    public void ActivarPanelRecompensa()
    {
        panelRecompensa.SetActive(true);
    }

    public void DesactivarPanelRecompensa()
    {
        panelRecompensa.SetActive(false);
    }

    public void ActivarPanelPremio()
    {
       if(!botonVerAnuncio.interactable)
        {
            AdManager.LoadRewardedVideo("oro");
        }
        panelRecompensa.SetActive(false);
        panelEnergia.SetActive(false);
        panelPremio.SetActive(true);
    }

    public void DesactivarPanelPremio()
    {
        panelPremio.SetActive(false);
    }

    public void ActivarPanelEnergia()
    {
        if (!botonVerAnuncio.interactable)
        {
            AdManager.LoadRewardedVideo("oro");
        }
        panelRecompensa.SetActive(false);
        panelPremio.SetActive(false);
        panelEnergia.SetActive(true);
    }

    public void TappedReward()
    {
        esRecursos = false;
        if (AdManager.IsRewardedVideoReady("oro"))
        {
            AdManager.ShowRewardVideo("oro");
        }
    }

    public void TappedRewardOro()
    {
        esRecursos = true;
        if (AdManager.IsRewardedVideoReady("oro"))
        {
            AdManager.ShowRewardVideo("oro");
        }
    }

    private void OnEnable()
    {
       TDCallbacks.AdAvailable += OnAdAvailable;
       TDCallbacks.RewardVideoValidated += OnRewardVideoValidated;
    }

    private void OnDisable()
    {
      TDCallbacks.AdAvailable -= OnAdAvailable;
      TDCallbacks.RewardVideoValidated -= OnRewardVideoValidated;
    }

    public void PremioDinero()
    {
        AdManager.LoadRewardedVideo("oro");
        DesactivarPanelPremio();
        ActivarPanelRecompensa();
        int[] limitePosibilidad = new int[IniciarJuego.shopData.Getcofre(4).posibilidades];

        int posibilidadAcumulada = 0;

        int numeroAzaroso = Random.Range(1, 101);
        int opcionelegida = 0;

        for (int i = 0; i < IniciarJuego.shopData.Getcofre(4).posibilidades; i++)
        {
            limitePosibilidad[i] = posibilidadAcumulada + IniciarJuego.shopData.Getcofre(4).porcentaje[i];
            posibilidadAcumulada = limitePosibilidad[i];

            if (i != 0)
            {
                if (limitePosibilidad[i] >= numeroAzaroso && limitePosibilidad[i - 1] < numeroAzaroso)
                {
                    opcionelegida = i;
                }
            }
            else
            {

                if (limitePosibilidad[i] >= numeroAzaroso)
                {
                    opcionelegida = i;
                }

            }

        }

        ShopData.CofreItem cofre = IniciarJuego.shopData.Getcofre(4);

     //   Debug.Log(opcionelegida);

        switch (cofre.tipoPosible[(opcionelegida * 10) + 0])
        {
            case ShopData.TipoPosibilidad.DIAMANTES:
                int numRandom = Random.Range(cofre.diamantesMin[opcionelegida], cofre.DiamantesMax[opcionelegida] + 1);
                PlayerPrefs.SetInt(SaveGame.diamantes, PlayerPrefs.GetInt(SaveGame.diamantes) + numRandom);
                imgDiamante.color = new Color(1, 1, 1, 1);
                imgOro.color = new Color(1, 1, 1, 0);
                txtDinero.text = numRandom.ToString();
                break;
            case ShopData.TipoPosibilidad.ORO:
                int numRandom2 = Random.Range(cofre.oroMin[opcionelegida], cofre.oroMax[opcionelegida] + 1);
                PlayerPrefs.SetInt(SaveGame.oro, PlayerPrefs.GetInt(SaveGame.oro) + numRandom2);
                imgDiamante.color = new Color(1, 1, 1, 0);
                imgOro.color = new Color(1, 1, 1, 1);
                txtDinero.text = numRandom2.ToString();
                break;
        }

        if (!PlayerPrefs.HasKey(SaveGame.horaObjetivoAnuncio))
        {
            System.DateTime fecha = System.DateTime.Now;

            fecha = fecha.AddDays(1);

            fecha = System.DateTime.Parse(fecha.Month.ToString() + "/" + fecha.Day.ToString() + "/" + fecha.Year.ToString() + " 12:00:01 AM");

            PlayerPrefs.SetString(SaveGame.horaObjetivoAnuncio, fecha.ToString());

        }
        else
        {
            System.DateTime fechaActual = System.DateTime.Now;
  
            System.DateTime fechaDestino = System.DateTime.Parse(PlayerPrefs.GetString(SaveGame.horaObjetivoAnuncio));

            if (System.DateTime.Compare(fechaActual, fechaDestino) > -1)
            {
                fechaActual = fechaActual.AddDays(1);
                fechaActual = System.DateTime.Parse(fechaActual.Month.ToString() + "/" + fechaActual.Day.ToString() + "/" + fechaActual.Year.ToString() + " 12:00:01 AM");
                PlayerPrefs.SetString(SaveGame.horaObjetivoAnuncio, fechaActual.ToString());
            }
        }
    }

    private void OnRewardVideoValidated(TDVideoReward videoReward)
    {
        if (!esRecursos && videoReward.RewardValid)
        {
            AdManager.LoadRewardedVideo("oro");
            PlayerPrefs.SetInt(SaveGame.energia, 5);
            DesactivarPanelEnergia();
            botonMasRecursos.interactable = false;
            botonMasRecursos2.interactable = false;

        }
        else if(videoReward.RewardValid)
        {
            PremioDinero();
        }
    }

    private void OnAdAvailable(TDAdEvent e)
    {
        if (e.adType == "REWARD_AD" && e.tag == "oro")
        {
            txtWait.text = "";
            botonVerAnuncio.interactable = true;
        }
        else
        {
            txtWait.text =  SeleccionaIdiomas.gameText[9,15];
        }

        if (e.adType == "REWARD_AD" && e.tag == "oro")
        {
            txtWaitPremio.text = "";
            botonVerAnuncioPremio.interactable = true;
        }
        else
        {
            txtWaitPremio.text = SeleccionaIdiomas.gameText[9, 15];
        }
    }

    public void DesactivarPanelEnergia()
    {
        panelEnergia.SetActive(false);
    }
}
