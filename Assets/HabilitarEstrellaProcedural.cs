﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HabilitarEstrellaProcedural : MonoBehaviour {

    public int idNivel, numEstrella;
	// Use this for initialization
	void OnEnable () {

        if (PlayerPrefs.GetInt(SaveGame.estrelllas[2, idNivel]) >= numEstrella){

            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }

	}
	
}
