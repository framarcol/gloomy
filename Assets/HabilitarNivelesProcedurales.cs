﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HabilitarNivelesProcedurales : MonoBehaviour {

    public Button[] Tumbas;
    bool entrado;
	
	void OnEnable () {

        for(int i = 0; i < Tumbas.Length; i++)
        {
            Tumbas[i].interactable = false;
        }

#if UNITY_EDITOR
        PlayerPrefs.SetInt(SaveGame.nivelesDesbloquedos[1], 25);
#endif

        for(int i = 1; i < IniciarJuego.shopData.GetNumNivelesMundo(0); i++)
        {
            if (IniciarJuego.shopData.GetMundo(i, 1).ProceduralDesbloqueable)
            {       
                if(PlayerPrefs.GetInt(SaveGame.nivelesDesbloquedos[0]) >= i)
                {
                    entrado = true;
                    Tumbas[IniciarJuego.shopData.GetMundo(i, 1).idNivelDes-1].interactable = true;
                }
            }
        }

        for (int i = 1; i < IniciarJuego.shopData.GetNumNivelesMundo(1); i++)
        {
            if (IniciarJuego.shopData.GetMundo(i, 2).ProceduralDesbloqueable)
            {
                if (PlayerPrefs.GetInt(SaveGame.nivelesDesbloquedos[1]) >= i)
                {
                    Tumbas[IniciarJuego.shopData.GetMundo(i, 2).idNivelDes - 1].interactable = true;
                }
            }
        }

        // PlayerPrefs.DeleteKey(SaveGame.diaActual);

        if (entrado)
        {
            if (!PlayerPrefs.HasKey(SaveGame.diaActual))
            {
                System.DateTime fecha = System.DateTime.Now;

                fecha = fecha.AddDays(1);

                fecha = System.DateTime.Parse(fecha.Month.ToString() + "/" + fecha.Day.ToString() + "/" + fecha.Year.ToString() + " 12:00:01 AM");

                PlayerPrefs.SetString(SaveGame.diaActual, fecha.ToString());

            }
            else
            {
                System.DateTime fechaActual = System.DateTime.Now;
                //  fechaActual = fechaActual.AddDays(3);
                System.DateTime fechaDestino = System.DateTime.Parse(PlayerPrefs.GetString(SaveGame.diaActual));

                if (System.DateTime.Compare(fechaActual, fechaDestino) > -1)
                {
                    // Debug.Log(System.DateTime.Compare(fechaActual, fechaDestino));
                    PlayerPrefs.SetInt(SaveGame.estrelllas[2, 0], 0);
                    PlayerPrefs.SetInt(SaveGame.estrelllas[2, 1], 0);
                    PlayerPrefs.SetInt(SaveGame.estrelllas[2, 2], 0);
                    PlayerPrefs.SetInt(SaveGame.nivelesDesbloquedos[2], 0);
                    fechaActual = fechaActual.AddDays(1);

                    fechaActual = System.DateTime.Parse(fechaActual.Month.ToString() + "/" + fechaActual.Day.ToString() + "/" + fechaActual.Year.ToString() + " 12:00:01 AM");

                    PlayerPrefs.SetString(SaveGame.diaActual, fechaDestino.ToString());
                }
            }
        }

#if UNITY_EDITOR
    /*    for (int i = 0; i < Tumbas.Length; i++)
        {
            Tumbas[i].interactable = true;
        }*/
#endif

    }
	

}
