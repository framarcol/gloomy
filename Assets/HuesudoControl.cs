﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HuesudoControl : MonoBehaviour {

    public float tiempoRevivir;
    public int vecesRevivir;
    public int veces;

    private void Awake()
    {
        veces = 0;
    }

    public void Resucitar(bool reventado)
    {
        if (veces < vecesRevivir && !reventado)
        {
            //GetComponentInChildren<BoxCollider2D>().enabled = false;
            StartCoroutine(ResurgirZombie());
        }
        else
        {
            GetComponent<GenericEnemyControl>().MetodoDying();
            GetComponent<GenericEnemyControl>().MetodoDying2();
        }

    }

    IEnumerator ResurgirZombie()
    {
     
        yield return new WaitForSeconds(tiempoRevivir);
        GetComponent<GenericEnemyControl>().AnimacionHuesudo();
        yield return new WaitForSeconds(2);
        GetComponent<GenericEnemyControl>().ResetEnemy();
        GetComponentInChildren<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        GetComponentInChildren<BoxCollider2D>().enabled = true;
        veces++;
      //  yield return new WaitForSeconds(2);
      //  

    }
}
