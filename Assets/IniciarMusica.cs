﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IniciarMusica : MonoBehaviour {


    public bool esperar;

    public int id;
    // Use this for initialization

    public void Iniciar()
    {
        if(!GetComponent<AudioSource>().isPlaying)
        GetComponent<AudioSource>().Play();
    }

    void Start () {

        if(id != -1)
        {
            if (!esperar)
                GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[id]);
            else GetComponent<AudioSource>().clip = IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[id];
        }
     
        else
        {
            int random = Random.Range(1, 3);
            switch (BotonesMenu.mundoSeleccionado)
            {
                case 0:
                    if(random == 1)
                    GetComponent<AudioSource>().clip = IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[8];
                    else
                        GetComponent<AudioSource>().clip = IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[16];
                    GetComponent<AudioSource>().Play();
                    break;
                case 1:
                    if (random == 1)
                        GetComponent<AudioSource>().clip = IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[9];
                    else
                        GetComponent<AudioSource>().clip = IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[16];
                    GetComponent<AudioSource>().Play();
                    break;
                default:
                    GetComponent<AudioSource>().clip = IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[8];
                    GetComponent<AudioSource>().Play();
                    break;

            }
        }
		
	}
}
