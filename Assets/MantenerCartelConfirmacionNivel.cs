﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MantenerCartelConfirmacionNivel : MonoBehaviour {

    public bool activarAnimación;

    private void OnEnable()
    {
        if (activarAnimación)
        {
            GetComponent<AnimationsPlaylist>().PlayAnimation("entrada", 0, 1, 1);
        }
    }

    public void Desactivar()
    {
        gameObject.SetActive(false);
    }
}
