﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MantenerLayer : MonoBehaviour {

    public int id;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if(id == 0)
            {
            if(GetComponent<SpriteRenderer>() != null)
            GetComponent<SpriteRenderer>().sortingOrder = 1000;
            else
            {
                GetComponent<ParticleSystemRenderer>().sortingOrder = 1000;
            }
        }
       
        else
        {
            GetComponent<SpriteRenderer>().sortingOrder = 999;
        }
	}
}
