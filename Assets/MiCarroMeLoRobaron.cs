﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiCarroMeLoRobaron : MonoBehaviour {

    public int numVeces;
    public float[] tiempo;
    public float[] posicionesX, poscionesY;
    public float[] reapariciones;
    public GameObject coche;

    public Sprite[] spritesCoche;

    Vector2[] origen;

    float[] timer;
    bool[] activarUnaVez, activarMovimiento;

    int veces;

	// Use this for initialization
	void Start () {

        timer = new float[8];
        activarUnaVez = new bool[8];
        activarMovimiento = new bool[8];
        origen = new Vector2[8];

        coche.GetComponent<SpriteRenderer>().sprite = spritesCoche[0];
        coche.GetComponent<SpriteRenderer>().sortingOrder = 0;

        origen[0] = coche.transform.position;

        for (int i = 0; i < posicionesX.Length; i++)
        {
            if (posicionesX[i] == 0)
                posicionesX[i] = coche.transform.position.x;
        }

        for (int i = 1; i < origen.Length && i < posicionesX.Length; i++)
        {
            origen[i] = new Vector2(posicionesX[i - 1], poscionesY[i - 1]);
        }

        activarMovimiento[0] = true;
		
	}

    void ResetearValores()
    {
        coche.GetComponent<SpriteRenderer>().sprite = spritesCoche[0];
        coche.GetComponent<SpriteRenderer>().sortingOrder = 0;

        for (int i = 0; i < timer.Length; i++)
        {
            timer[i] = 0;
            activarUnaVez[i] = false;
        }
    }

    void Subir(int idMovimiento)
    {
        Vector2 destino;

        destino = new Vector2(posicionesX[idMovimiento], poscionesY[idMovimiento]);

        Vector2 distancia = destino - origen[idMovimiento];

        timer[idMovimiento] += Time.deltaTime;

        float porcentaje = timer[idMovimiento] / tiempo[idMovimiento];

        if (timer[idMovimiento] < tiempo[idMovimiento])
        {
            activarUnaVez[idMovimiento] = false;
            coche.transform.position = distancia * porcentaje + origen[idMovimiento];
        }
        else
        {
            activarMovimiento[idMovimiento] = false;
            if (!activarUnaVez[idMovimiento])
            {
                activarUnaVez[idMovimiento] = true;

                switch (idMovimiento)
                {
                    case 0:
                        coche.GetComponent<SpriteRenderer>().sprite = spritesCoche[1];          
                        break;
                    case 1:
                        coche.GetComponent<SpriteRenderer>().sortingOrder = 1;
                        break;
                    case 2:
                        coche.GetComponent<SpriteRenderer>().sprite = spritesCoche[2];
                        break;
                    case 3:
                        coche.GetComponent<SpriteRenderer>().sortingOrder = 2;
                        break;
                }
                if(idMovimiento < posicionesX.Length-1)
                activarMovimiento[idMovimiento+1] = true;
                else
                {
                    if(veces < numVeces-1)
                    {
                        StartCoroutine(Resetar());
                    }
                }
            }

        }
    }

    private void Update()
    {
        for(int i = 0; i < posicionesX.Length; i++) if (activarMovimiento[i]) Subir(i);
    }

    IEnumerator Resetar()
    {
        yield return new WaitForSeconds(reapariciones[veces]);
        veces++;
        ResetearValores();
        activarMovimiento[0] = true;

    }

}
