﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonedasEstado : MonoBehaviour {

    int monedasDiamantes, monedasOro;
    public Text oro, diamantes;

    private void Update()
    {
        monedasDiamantes = PlayerPrefs.GetInt(SaveGame.diamantes);
        monedasOro = PlayerPrefs.GetInt(SaveGame.oro);
        oro.text = monedasOro.ToString();
        diamantes.text = monedasDiamantes.ToString();

    }
}
