﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MostrarEnergia : MonoBehaviour {

	
	// Update is called once per frame
	void OnEnable () {

        GetComponent<Text>().text = PlayerPrefs.GetInt(SaveGame.energia).ToString() + "/5";
	}
}
