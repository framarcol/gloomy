﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MostrarHorda : MonoBehaviour {

    public Text textoHorda;

    private void OnEnable()
    {
        GetComponent<Text>().text = SeleccionaIdiomas.gameText[1,8]+ textoHorda.text;
    }
}
