﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MostrarOro : MonoBehaviour {
    private void OnEnable()
    {
        GetComponent<Text>().text = PlayerPrefs.GetInt(SaveGame.oro).ToString();
    }
}
