﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverVagoneta : MonoBehaviour {

    public bool moverIzquierda;
    public bool moverDerecha;
    public float Velocidad;


    public bool iniciado;

    public void IniciarNiebla()
    {
        iniciado = true;
        if (GetComponent<VagonetaControlador>() != null) GetComponent<VagonetaControlador>().IniciarVagoneta();
    }

    void Update()
    {

        if (iniciado)
        {
            if (moverIzquierda)
            {
                transform.Translate(Vector3.left * Time.deltaTime * (Velocidad / 2));
            }

            if (moverDerecha)
            {
                transform.Translate(Vector3.right * Time.deltaTime * (Velocidad / 2));
            }
        }
    }
}
