﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoVagonetaLoadScreen : MonoBehaviour {

    Vector3 origen;

    float timer;
    public float Velocidad;

    bool activarSubida;

    private void Start()
    {
        origen = GetComponent<Transform>().localPosition;
        activarSubida = true;   
    }

    private void Update()
    {
        if (activarSubida)
        {
            Subir();
        }
       
    }

    void Subir()
    {
        transform.Translate(Vector3.left * Velocidad);
        //Vector3 destino = new Vector3(origen.x*-1, 33, 0);  //posicionObjetivo;

        //Vector3 distancia = destino - origen;

        //timer += Time.deltaTime;

        //float porcentaje = timer / Velocidad;

        //porcentaje = Mathf.Sin(porcentaje * Mathf.PI * 0.5f);

        //if (timer < Velocidad) GetComponent<Transform>().localPosition = distancia * porcentaje + origen;
        //else activarSubida = false;
    }
}
