﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicaScroll : MonoBehaviour {

    bool esPLay;

	// Use this for initialization
	void Start () {
		
        GetComponent<AudioSource>().clip = IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[14];
    }
	
	// Update is called once per frame
	void Update () {

      //  Debug.Log(GetComponent<ScrollRect>().velocity);

         if((GetComponent<ScrollRect>().velocity.x > 4 || GetComponent<ScrollRect>().velocity.x < -4) && !esPLay)
        {
            esPLay = true;
            GetComponent<AudioSource>().Play();
        }
        else if((GetComponent<ScrollRect>().velocity.x < 4 && GetComponent<ScrollRect>().velocity.x > -4))
        {
            esPLay = false;
        }

    }
}
