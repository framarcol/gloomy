﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nessy : MonoBehaviour {

    public bool moverIzquierda;
    public bool moverDerecha;
    public float Velocidad;
    public float frecuenciaMin, frecuenciaMax;
    public int numReapariciones;
    public bool iniciado;
    int vecesQueLlevo;
    Vector2 posicionInicial;
    Quaternion rotacionInicial;

    public AnimationsPlaylist animasao;
    
    private void Start()
    {
        vecesQueLlevo = 0;
        rotacionInicial = transform.localRotation;
        posicionInicial = transform.localPosition;
        StartCoroutine(ArrancarBuitre());

        if (moverDerecha)
        {
            transform.localRotation = new Quaternion(0, 0, 0, 0);
        }
        else
        {
            transform.localRotation = rotacionInicial;
        }

    }

    void IniciarBuitre()
    {
        StartCoroutine(ArrancarBuitre());
    }

    IEnumerator ArrancarBuitre()
    {
        float numero = Random.Range(frecuenciaMin, frecuenciaMax);
        animasao.PlayAnimation("ReposoDia", 0, 1, 1);
        yield return new WaitForSeconds(2f);
        animasao.PlayAnimation("EmergerDia", 0, 1, 1);
        yield return new WaitForSeconds(0.3f);
        animasao.PlayAnimation("NadarDia", 0, -1, 1);
        yield return new WaitForSeconds(4);
        animasao.PlayAnimation("SumergirDia", 0, 1, 1);
        yield return new WaitForSeconds(numero - 7.3f);

        transform.localPosition = posicionInicial;
        vecesQueLlevo++;
        if (vecesQueLlevo < numReapariciones)
            IniciarBuitre();
    }


    public void BuitreVerdeAccion()
    {

        iniciado = true;
    }

    void Update()
    {

        if (iniciado)
        {
            transform.Translate(Vector3.right * Time.deltaTime * (Velocidad / 2));
        }
    }
}
