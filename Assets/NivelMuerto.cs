﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NivelMuerto : MonoBehaviour {

    public int oroActual, oroFinalConteo;

    ControladorRecompensas oroRecomp;

    public GameObject pantallaNegra;
    public GameObject nivelPausado;

    public Text textoOro;

    bool moverArmasAbajo;
    GameObject objetoArmas;

    private void Awake()
    {
        objetoArmas = GameObject.FindGameObjectWithTag("ArmasPrefab");
        oroRecomp = GameObject.FindGameObjectWithTag("GameController").GetComponent<ControladorRecompensas>();
    }

    void OnEnable () {

        //    if (GameObject.FindGameObjectWithTag("ProtaTag").GetComponent<ProtaInfo>().frontal) Time.timeScale = 0;
        nivelPausado.SetActive(false);
        moverArmasAbajo = true;
        GameObject.FindGameObjectWithTag("Interfaz").GetComponentInChildren<PausaCantTouch>().GetComponent<Button>().gameObject.SetActive(false);

        pantallaNegra.SetActive(true);
   
        if(oroRecomp.OroFinal() != 0)
        {
            oroFinalConteo = oroRecomp.OroFinal() - oroRecomp.oroInicio;
            StartCoroutine(EfectoSuma());
        }
        else
        {
            textoOro.text = "0";
        }


    }

    private void Update()
    {
        if (moverArmasAbajo)
        {
            objetoArmas.transform.Translate(Vector3.down * Time.deltaTime * (7));
        }

        if (oroActual <= oroFinalConteo)
        {
            StartCoroutine(EfectoSuma());
            textoOro.text = oroActual.ToString();
        }
    }

    public IEnumerator EfectoSuma()
    {
        yield return new WaitForSeconds(0.01f);
        if (oroFinalConteo - oroActual >= 10)
        {
            oroActual += 10;
        }
        else
        {
            Time.timeScale = 0;
            oroActual += oroFinalConteo - oroActual;
        }
    }

}
