﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PantallaCargaLoading : MonoBehaviour {

    public Sprite[] sprites;

	// Use this for initialization
	void Awake () {
        GetComponent<Image>().sprite = sprites[Random.Range(0, sprites.Length)];		
	}
}
