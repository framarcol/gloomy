﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PaqueteRecompensaContador : MonoBehaviour {

    public int idNivel;
    System.DateTime horaLimite;
    private void Update()
    {
        System.DateTime horaActual = System.DateTime.Now;
        if (idNivel != 10)
        {
            if(PlayerPrefs.GetString(SaveGame.diaActual) != "")
            horaLimite = System.DateTime.Parse(PlayerPrefs.GetString(SaveGame.diaActual));
        }
        else
        {
            if(PlayerPrefs.GetString(SaveGame.horaObjetivoAnuncio) != "")
            horaLimite = System.DateTime.Parse(PlayerPrefs.GetString(SaveGame.horaObjetivoAnuncio));
        }    

        string texto = "";

        if ((int)horaLimite.Subtract(horaActual).Hours >= 10)
            texto += (int)horaLimite.Subtract(horaActual).Hours;
        else
        {
            texto += "0" + (int)horaLimite.Subtract(horaActual).Hours;
        }

        if ((int)horaLimite.Subtract(horaActual).Minutes >= 10)
            texto += ":" +(int)horaLimite.Subtract(horaActual).Minutes;
        else
        {
            texto += ":0" + (int)horaLimite.Subtract(horaActual).Minutes;
        }

        if ((int)horaLimite.Subtract(horaActual).Seconds >= 10)
            texto += ":" + (int)horaLimite.Subtract(horaActual).Seconds;
        else
        {
            texto += ":0" + (int)horaLimite.Subtract(horaActual).Seconds;
        }

#if UNITY_EDITOR
 //       PlayerPrefs.SetInt(SaveGame.estrelllas[2, idNivel], 0);
#endif

        if(idNivel != 10)
        {
            if (PlayerPrefs.GetInt(SaveGame.estrelllas[2, idNivel]) == 0)
            {
                GetComponentInChildren<Text>().text = "";
                GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);

            }
            else
            {
                GetComponentInChildren<Text>().text = texto;
                GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.2f);
            }
        }
        else
        {
            if(PlayerPrefs.HasKey(SaveGame.horaObjetivoAnuncio)){
                if (System.DateTime.Compare(horaActual, horaLimite) != -1)
                {
                    GetComponentInChildren<Text>().text = "";
                }
                else
                {
                    GetComponentInChildren<Text>().text = texto;
                }
            }
            else
            {
                GetComponentInChildren<Text>().text = "";
            }
           
        }
      
 

    }
}
