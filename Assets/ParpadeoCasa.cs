﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParpadeoCasa : MonoBehaviour {

    public float encender, apagar;

    private void Start()
    {
        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
        StartCoroutine(Parpadeo());
    }

    void Repetir()
    {
        StartCoroutine(Parpadeo());
    }

    IEnumerator Parpadeo()
    {
        yield return new WaitForSeconds(encender);
        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        yield return new WaitForSeconds(apagar);
        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
        StopCoroutine(Parpadeo());
        Repetir();
        
    }
}
