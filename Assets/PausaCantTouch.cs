﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PausaCantTouch : MonoBehaviour {

    public GameObject noTocar;
    public GameObject armagedon;
	public void BtnPausar()
    {
        armagedon.SetActive(false);
        noTocar.SetActive(true);
    }

    public void BtnReanudar()
    {
        noTocar.SetActive(false);
        if (PlayerPrefs.GetInt(SaveGame.orbital) == 1)
        {
            armagedon.SetActive(true);
        }
           

    }
}
