﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PausaMusica : MonoBehaviour {

    private void OnApplicationFocus(bool focus)
    {
        if (!focus)
        {
            GetComponent<AudioSource>().Pause();
        }
        else
        {
            GetComponent<AudioSource>().UnPause();
        }
    }
}
