﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PausaPantallaNegra : MonoBehaviour {


    public GameObject pantallaGranada;
    public GameObject nivelMuerto;
    public AudioSource audio;
    private void OnEnable()
    {
        nivelMuerto.SetActive(false);
        pantallaGranada.SetActive(true);
        audio.Pause();

    }

    private void OnDisable()
    {
        pantallaGranada.SetActive(false);
        audio.UnPause();
    }

}
