﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PremiosCofre : MonoBehaviour {


    public int idCofre;
    public int idTrofeo;
    public int numDiamantes;
    public int numOro;
    public int numFragmento;
    int subPosibilidad;
    public SpriteRenderer[] fragmentosFinal;
    public Sprite[] fragmentos;


    public int fragmentoFinal;
    bool activado;
    /*
     * IDS:
     * 
     * Oro: 1
     * Diamantes: 2
     * FragmentoLanzallamas: 3
     * FragmentoLaser: 4
     * FragmentoRPG: 5
     * FragmentoUzi: 6
     * 
     */

    // Use this for initialization
    void Start () {

        if(idCofre != 0)
        {
            subPosibilidad = 0;

            int[] limitePosibilidad = new int[IniciarJuego.shopData.Getcofre(idCofre).posibilidades];

            int posibilidadAcumulada = 0;

            int numeroAzaroso = Random.Range(1, 101);
            int opcionelegida = 0;

#if UNITY_EDITOR
            numeroAzaroso = 61;
#endif

    //        Debug.Log("Azar: " +numeroAzaroso);

            for (int i = 0; i < IniciarJuego.shopData.Getcofre(idCofre).posibilidades; i++)
            {
                limitePosibilidad[i] = posibilidadAcumulada + IniciarJuego.shopData.Getcofre(idCofre).porcentaje[i];
                posibilidadAcumulada = limitePosibilidad[i];

                if (i != 0)
                {
                    //    Debug.Log("Limite: " + limitePosibilidad[i]);
                    if (limitePosibilidad[i] >= numeroAzaroso && limitePosibilidad[i - 1] < numeroAzaroso)
                    {
                     //   Debug.Log("entrado");
                        opcionelegida = i;
                    }       
                }
                else {

                    if (limitePosibilidad[i] >= numeroAzaroso)
                    {
                      //  Debug.Log("entrado");
                        opcionelegida = i;
                    }

                }

            }

            MetodoPremio(opcionelegida, subPosibilidad);
        }
		
	}

    void MetodoPremio(int opcionelegida, int subPosibilidad)
    {
      //  Debug.Log((opcionelegida+1).ToString());

        int ancho = 10;
        switch (IniciarJuego.shopData.Getcofre(idCofre).tipoPosible[(opcionelegida*ancho)+subPosibilidad])
        {
            case ShopData.TipoPosibilidad.FRAGMENTO:

                int idFragmento = IniciarJuego.shopData.Getcofre(idCofre).idArmaFragmento[(opcionelegida * ancho) + subPosibilidad];

#if UNITY_EDITOR
         //      PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[idFragmento], 0);
#endif

                if (PlayerPrefs.GetInt(SaveGame.armasDesbloqueadas[idFragmento]) == 0)
                {
                    int numFragmentoActual = PlayerPrefs.GetInt(SaveGame.fragmentosDesbloqueados[idFragmento]);
                    int numeroRandom = Random.Range(IniciarJuego.shopData.Getcofre(idCofre).numFragmentosMIN[(opcionelegida * ancho) + subPosibilidad], IniciarJuego.shopData.Getcofre(idCofre).numFragmentosMAX[(opcionelegida * ancho) + subPosibilidad] + 1);
                    numFragmento = numeroRandom;
                    PlayerPrefs.SetInt(SaveGame.fragmentosDesbloqueados[idFragmento], numFragmentoActual + numeroRandom);

                    if (numFragmentoActual + numeroRandom >= IniciarJuego.shopData.GetArma(idFragmento).NumPiezas)
                    {
                        numFragmento = 0;
                        fragmentosFinal[idFragmento].sprite = fragmentos[idFragmento];
                        activado = true;
                        fragmentosFinal[idFragmento].GetComponentsInChildren<Text>()[1].text =  SeleccionaIdiomas.gameText[9,18];
                        fragmentosFinal[idFragmento].GetComponentsInChildren<Text>()[1].fontSize = 72;
                        PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[idFragmento], 1);
                    }

                    switch (idFragmento)
                    {
                        case 6:
                            idTrofeo = 6;
                            break;
                        case 5:
                            idTrofeo = 5;
                            break;
                        case 7:
                            idTrofeo = 3;
                            break;
                        case 8:
                            idTrofeo = 4;
                            break;
                    }
                }
                else
                {
                    subPosibilidad++;
                    MetodoPremio(opcionelegida, subPosibilidad);
                }              
                break;
            case ShopData.TipoPosibilidad.ORO:
                numOro = Random.Range(IniciarJuego.shopData.Getcofre(idCofre).oroMin[opcionelegida], IniciarJuego.shopData.Getcofre(idCofre).oroMax[opcionelegida] + 1);
                PlayerPrefs.SetInt(SaveGame.oro, PlayerPrefs.GetInt(SaveGame.oro) + numOro);
                idTrofeo = 1;
                break;
            case ShopData.TipoPosibilidad.DIAMANTES:
                numDiamantes = Random.Range(IniciarJuego.shopData.Getcofre(idCofre).diamantesMin[opcionelegida], IniciarJuego.shopData.Getcofre(idCofre).DiamantesMax[opcionelegida]+1);
                PlayerPrefs.SetInt(SaveGame.diamantes, PlayerPrefs.GetInt(SaveGame.diamantes) + numDiamantes);
                MenusEmergentes.diamantes = PlayerPrefs.GetInt(SaveGame.diamantes);
                idTrofeo = 2;
                break;
        }
    }


}
