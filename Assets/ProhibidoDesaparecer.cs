﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProhibidoDesaparecer : MonoBehaviour {

    bool isInTransition;
    float transition;
    bool isShowing;
    float duration;
    public bool Secundaria;
    public AudioSource audio;
    private void OnEnable()
    {
        //  Debug.Log("Gola");
        if(!Secundaria)
        audio.PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[15]);
        Fade(false, 1);
    }

    private void Update()
    {
        if (!isInTransition)
            return;

        transition += (isShowing) ? Time.deltaTime * (1 / duration) : -Time.deltaTime * (1 / duration);
        GetComponent<SpriteRenderer>().color = new Color(1,1,1, transition);

        if (transition > 1 || transition < 0)
        {
            isInTransition = false;
            if(!Secundaria)gameObject.SetActive(false);
        }
    }

    public void Fade(bool showing, float duration)
    {
        isShowing = showing;
        isInTransition = true;
        this.duration = duration;
        transition = (isShowing) ? 0 : 1;
    }
}
