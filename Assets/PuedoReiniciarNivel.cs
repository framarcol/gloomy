﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PuedoReiniciarNivel : MonoBehaviour {

	void OnEnable()
    {
        if (PlayerPrefs.GetInt(SaveGame.nivelesDesbloquedos[BotonesMenu.mundoSeleccionado - 1]) > BotonesMenu.nivelMundoSeleccionado)
        {

        }
        else
        {
            if (PlayerPrefs.GetInt(SaveGame.energia) <= 0) GetComponent<Button>().interactable = false;
            else GetComponent<Button>().interactable = true;
        }
    }
}
