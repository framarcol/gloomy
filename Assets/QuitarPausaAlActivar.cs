﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitarPausaAlActivar : MonoBehaviour {


    void OnEnable()
    {
        if (GameObject.FindGameObjectWithTag("Interfaz")!=null)
        GameObject.FindGameObjectWithTag("Interfaz").SetActive(false);
    }
}
