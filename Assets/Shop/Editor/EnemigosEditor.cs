﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GeneradorEnemigosV3))]
public class EnemigosEditor : Editor {

       string[] _choices = new[] { "Standard", "Midget", "Muscle", "Skellington", "Standard Half", "Loca", "Cabeza1",
           "Cabeza2", "Colossus", "Dragon", "MedioZombie", "Parachute", "DynamiteS2", "DynamiteS5", "DynamiteS7", "CoalS2", "CoalS5",
           "GoldS2", "GoldS6", "RandomwagonS2", "Niebla" };
       int _choiceIndex = 0;

    public override void OnInspectorGUI()
    {
          DrawDefaultInspector();
          GeneradorEnemigosV3 myScript = (GeneradorEnemigosV3)target;

          int posiciones = myScript.NumeroPosiciones();

          for(int i = 0; i < posiciones; i++)
          {
              GUILayout.BeginHorizontal();
              GUILayout.Label("Posición "+ (i).ToString());

              myScript.choices[i] = EditorGUILayout.Popup(myScript.choices[i], _choices);

              GUILayout.EndHorizontal();
          }
          // Save the changes back to the object
          EditorUtility.SetDirty(target);
    }
}
