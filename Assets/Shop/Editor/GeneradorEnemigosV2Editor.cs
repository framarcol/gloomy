﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GeneradorHordas))]
public class GeneradorEnemigosV2Editor : Editor {

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GeneradorHordas myScript = (GeneradorHordas)target;
        if (GUILayout.Button("Crear Hordas"))
        {
            myScript.CrearHordas();
        }

        if (GUILayout.Button("Eliminar Hordas"))
        {
            myScript.EliminarHordas();
        }

       // for(int i = 0; i < myScript.pr)

    }
}
