﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(ShopData))]
public class ShopDataEditor : Editor {

    ShopData shopData;

    GUIStyle headerStyle;
    GUIStyle boxHeaderStyle;

    string[] _choices = new[] { "Standard", "Midget", "Muscle", "Skellington", "Standard Half", "Loca", "Cabeza1",
           "Cabeza2", "Colossus", "Dragon", "MedioZombie", "Parachute", "DynamiteS2", "DynamiteS5", "DynamiteS7", "CoalS2", "CoalS5",
           "GoldS2", "GoldS6", "RandomwagonS2", "Niebla" };

    public override void OnInspectorGUI() {

        shopData = (ShopData)target;

        CreateStyles();

        EditorGUILayout.LabelField("GLOOMY Z", headerStyle, GUILayout.Height(34f));
        EditorGUILayout.Space();

        PopulateStandardItemCategories();
        PopulateUpgradeItemCategories();
        PopulateExtraItemCategories();

        DrawStandardItemList();

        //   DrawSetItemList();

        DrawUpgradeItemList();


        ShopData.UpgradeItem upgrade =  shopData.upgradeItems[0].upgradeItemList[0];

        //    Debug.Log(upgrade.itemId);

        DrawExtraList();

        DrawSoundItem(upgrade);

        //  DrawVirtualMoneyItemList();

        EditorUtility.SetDirty(shopData);
        Undo.RecordObject(shopData, "Shop modified");

    }

    private void PopulateStandardItemCategories() {

        if (shopData.standardShopItems.Count != 0) return;

        Array values = Enum.GetValues(typeof(ShopData.ShopStandardCategory));
        int count = values.Length;

        for (int i = 0; i < count; i++) {

            shopData.standardShopItems.Add(

                new ShopData.StandardItemsCategorized((ShopData.ShopStandardCategory)values.GetValue(i))
                
            );
            shopData.foldoutList.Add(true);

        }
        
    }

    private void PopulateExtraItemCategories()
    {
        if (shopData.extrasItems.Count != 0) return;

        Array values2 = Enum.GetValues(typeof(ShopData.ExtrasCategory));
        int count = values2.Length;

        for (int i = 0; i < count; i++)
        {

            shopData.extrasItems.Add(

                new ShopData.ExtrasItemCategorized((ShopData.ExtrasCategory)values2.GetValue(i))

            );
            shopData.foldoutList3.Add(true);

        }

    }

    private void PopulateUpgradeItemCategories()
    {
        if (shopData.upgradeItems.Count != 0) return;

        Array values2 = Enum.GetValues(typeof(ShopData.ShopWorldCategory));
        int count = values2.Length;

        for (int i = 0; i < count; i++)
        {

            shopData.upgradeItems.Add(

                new ShopData.MundosItemsCategorized((ShopData.ShopWorldCategory)values2.GetValue(i))

            );
            shopData.foldoutList2.Add(true);

        }

    }

    private void DrawStandardItemList() {

        if(GUILayout.Button("Armas", boxHeaderStyle)) {

            shopData.displayStandardItems = !shopData.displayStandardItems;

        }

        if (!shopData.displayStandardItems) return;

        EditorGUI.indentLevel++;

        foreach (ShopData.StandardItemsCategorized itemsCategorized in shopData.standardShopItems) {

            ShopData.ShopStandardCategory category = itemsCategorized.category;

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            shopData.foldoutList[(int)category] = EditorGUILayout.Foldout(shopData.foldoutList[(int)category], shopData.readableCategoryNames[(int)category]);
            if (GUILayout.Button("Añadir", GUILayout.MaxWidth(100f))) {

                itemsCategorized.standardItemList.Add(new ShopData.StandardShopItem());

                shopData.selectedCurrency.Add(0);

            }
            EditorGUILayout.EndHorizontal();

            if (shopData.foldoutList[(int)category]) {

                for (int i = 0; i < itemsCategorized.standardItemList.Count; i++) {

                    ShopData.StandardShopItem standardItem = itemsCategorized.standardItemList[i];
                    DrawStandardItem(itemsCategorized, standardItem);

                }

            }

        }

        EditorGUI.indentLevel--;

        EditorGUILayout.Space();

    }

    private void DrawStandardItem(ShopData.StandardItemsCategorized itemsCategorized, ShopData.StandardShopItem standardItem) {

      //  int index = itemsCategorized.standardItemList.IndexOf(standardItem);
      

        EditorGUILayout.Space();

        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Space(30);

            {
                EditorGUILayout.BeginVertical("Box");

                {
                    EditorGUILayout.BeginHorizontal();
                    
                    standardItem.hidden = GUILayout.Toggle(standardItem.hidden, "", GUILayout.Width(7f));

                    if (standardItem.hidden) GUI.enabled = false;

                    if (standardItem.itemThumbnail.Length != 6) standardItem.itemThumbnail = new Sprite[6];
                    if (standardItem.itemThumbnail2.Length != 6) standardItem.itemThumbnail2 = new Sprite[6];

                    for(int i = 0; i < 6; i++)
                    {
                        {
                            EditorGUILayout.BeginVertical();

                            standardItem.itemThumbnail[i] = (Sprite)EditorGUILayout.ObjectField(standardItem.itemThumbnail[i], typeof(Sprite), false, GUILayout.Width(45f), GUILayout.Height(45f));
                            standardItem.itemThumbnail2[i] = (Sprite)EditorGUILayout.ObjectField(standardItem.itemThumbnail2[i], typeof(Sprite), false, GUILayout.Width(45f), GUILayout.Height(45f));

                            EditorGUILayout.EndVertical();

                        }
                    }

                    EditorGUILayout.BeginVertical();

                    standardItem.itemHigh = (Sprite)EditorGUILayout.ObjectField(standardItem.itemHigh, typeof(Sprite), false, GUILayout.Width(45f), GUILayout.Height(45f));

                    EditorGUILayout.EndVertical();

                    {
                        EditorGUILayout.BeginVertical();

                        EditorGUILayout.LabelField("ID");
                        standardItem.itemId = EditorGUILayout.IntField(standardItem.itemId, GUILayout.MaxWidth(80f));

                        EditorGUILayout.LabelField("Name");
                        standardItem.itemName = EditorGUILayout.TextField(standardItem.itemName, GUILayout.MaxWidth(150f));

                        EditorGUILayout.EndVertical();
                    }

          //          GUILayout.FlexibleSpace();

                    if (standardItem.hidden)
                        EditorGUILayout.LabelField("Hidden", GUILayout.Width(60f));

                    else if (GUILayout.Button("X", GUILayout.Width(50f))) {

                        itemsCategorized.standardItemList.Remove(standardItem);
                        shopData.selectedCurrency.RemoveAt(shopData.selectedCurrency.Count-1);
                        return;

                    }

                    EditorGUILayout.EndHorizontal();
                }

                standardItem.itemDescription = EditorGUILayout.TextArea(standardItem.itemDescription, GUILayout.Height(50f));              

                standardItem.hasUnlockEvent = EditorGUILayout.ToggleLeft("Desbloqueable", standardItem.hasUnlockEvent);
                if (standardItem.hasUnlockEvent) {

                    {
                        EditorGUILayout.BeginHorizontal();
                        EditorGUI.indentLevel++;

                        standardItem.unlockItemEvent.specialLevel = EditorGUILayout.ToggleLeft("Special", standardItem.unlockItemEvent.specialLevel, GUILayout.Width(100f));
                        standardItem.unlockItemEvent.levelId = EditorGUILayout.IntField("Level Id", standardItem.unlockItemEvent.levelId);

                        EditorGUI.indentLevel--;
                        EditorGUILayout.EndHorizontal();
                    }

                }

                {

                    // Mejoras

                    float medidas = 45f;

                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Recursos", GUILayout.Width(100f));

                    standardItem.Recursos = EditorGUILayout.IntField(standardItem.Recursos, GUILayout.MaxWidth(100f));

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Num. Piezas", GUILayout.Width(100f));

                    standardItem.NumPiezas = EditorGUILayout.IntField(standardItem.NumPiezas, GUILayout.MaxWidth(100f));

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Nivel Max.", GUILayout.Width(100f));

                    standardItem.MaxLevel = EditorGUILayout.IntField(standardItem.MaxLevel, GUILayout.MaxWidth(100f));

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField(" ", GUILayout.Width(100f));

                    EditorGUILayout.LabelField("0", GUILayout.Width(medidas));

                    EditorGUILayout.LabelField("1", GUILayout.Width(medidas));

                    EditorGUILayout.LabelField("2", GUILayout.Width(medidas));

                    EditorGUILayout.LabelField("3", GUILayout.Width(medidas));

                    EditorGUILayout.LabelField("4", GUILayout.Width(medidas));

                    EditorGUILayout.LabelField("5", GUILayout.Width(medidas));

                    EditorGUILayout.EndHorizontal();  

                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Daño", GUILayout.Width(100f));


                    for (int i = 0; i < standardItem.Dano.Length; i++)
                    {
                        standardItem.Dano[i] = EditorGUILayout.IntField(standardItem.Dano[i], GUILayout.MaxWidth(medidas));
                    }

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Cadencia", GUILayout.Width(100f));

                    for (int i = 0; i < standardItem.Cadencia.Length; i++)
                    {
                        standardItem.Cadencia[i] = EditorGUILayout.FloatField(standardItem.Cadencia[i], GUILayout.MaxWidth(medidas));
                    }

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Alcance", GUILayout.Width(100f));

                    for (int i = 0; i < standardItem.Capacidad.Length; i++)
                    {
                        standardItem.Alcance[i] = EditorGUILayout.FloatField(standardItem.Alcance[i], GUILayout.MaxWidth(medidas));
                    }

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Capacidad", GUILayout.Width(100f));

                    for(int i = 0; i < standardItem.Capacidad.Length; i++)
                    {
                        standardItem.Capacidad[i] = EditorGUILayout.IntField(standardItem.Capacidad[i], GUILayout.MaxWidth(medidas));
                    }

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("T. Recarga", GUILayout.Width(100f));

                    for (int i = 0; i < standardItem.TiempoRecarga.Length; i++)
                    {
                        standardItem.TiempoRecarga[i] = EditorGUILayout.FloatField(standardItem.TiempoRecarga[i], GUILayout.MaxWidth(medidas));
                    }

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Ratio", GUILayout.Width(100f));

                    for (int i = 0; i < standardItem.Ratio.Length; i++)
                    {
                        standardItem.Ratio[i] = EditorGUILayout.FloatField(standardItem.Ratio[i], GUILayout.MaxWidth(medidas));
                    }

                    EditorGUILayout.EndHorizontal();
                   

                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Precio", GUILayout.Width(100f));

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Diamantes", GUILayout.Width(100f));

                    for (int i = 0; i < standardItem.Diamantes.Length; i++)
                    {
                        standardItem.Diamantes[i] = EditorGUILayout.IntField(standardItem.Diamantes[i], GUILayout.MaxWidth(medidas));
                    }

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Oro", GUILayout.Width(100f));

                    for (int i = 0; i < standardItem.Oro.Length; i++)
                    {
                        standardItem.Oro[i] = EditorGUILayout.IntField(standardItem.Oro[i], GUILayout.MaxWidth(medidas));
                    }

                    EditorGUILayout.EndHorizontal();
                }

                EditorGUI.indentLevel++;

                for(int i = 0; i < standardItem.costPacks.Count; i++){

                    {
                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Space(30);

                        GUI.backgroundColor = new Color(0.65f, 0.65f, 1, 1);
                        {
                            EditorGUILayout.BeginVertical(GUI.skin.button);
                            GUI.backgroundColor = Color.white;

                            {
                                EditorGUILayout.BeginHorizontal();


                                if (standardItem.onSale) {

                                    standardItem.costPacks[i].gotDiscount = EditorGUILayout.Toggle(standardItem.costPacks[i].gotDiscount, GUILayout.Width(60f));

                                } else {

                                    EditorGUILayout.LabelField("", GUILayout.Width(60f));

                                }

                                standardItem.costPacks[i].cost = EditorGUILayout.IntField(shopData.readableCurrencyNames[(int)standardItem.costPacks[i].currency], standardItem.costPacks[i].cost);
                                //standardItem.itemCost[currency] = EditorGUILayout.IntField(shopData.readableCurrencyNames[(int)currency], standardItem.itemCost[currency]);
                                if (standardItem.costPacks[i].cost < 0) standardItem.costPacks[i].cost = 0;

                                if (GUILayout.Button("x", GUILayout.MaxWidth(40f))) {
                                    standardItem.costPacks.Remove(standardItem.costPacks[i]);
                                    //standardItem.itemCost.Remove(currency);
                                    //standardItem.discountEnable.Remove(currency);
                                }

                                EditorGUILayout.EndHorizontal();
                            }

                            EditorGUILayout.EndVertical();
                        }

                        EditorGUILayout.EndHorizontal();
                    }

                }

                EditorGUI.indentLevel--;

                {
                    EditorGUILayout.BeginHorizontal();

                    if (standardItem.costPacks.Count > 0) standardItem.onSale = GUILayout.Toggle(standardItem.onSale, "On Sale", GUI.skin.button, GUILayout.MaxWidth(75f));
                    else standardItem.onSale = false;

                    if (standardItem.onSale) {
                        EditorGUIUtility.labelWidth = 50f;
                        standardItem.salePercentage = EditorGUILayout.Slider("%", standardItem.salePercentage, 0f, 1f, GUILayout.Width(200f));
                        EditorGUIUtility.labelWidth = 0;
                        GUILayout.FlexibleSpace();
                    }

                    EditorGUILayout.EndHorizontal();
                }

                EditorGUILayout.EndVertical();
            }

            EditorGUILayout.EndHorizontal();
        }

        if (standardItem.hidden) GUI.enabled = true;

    }

    private void DrawUpgradeItemList() {

        GUILayout.Space(20f);

        {
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Extras", boxHeaderStyle)) {

                shopData.displayVirtualMoneyPacks = !shopData.displayVirtualMoneyPacks;
            }

            if (shopData.displayVirtualMoneyPacks) return;

            GUILayout.FlexibleSpace();

            EditorGUILayout.EndHorizontal();
        }

        foreach (ShopData.MundosItemsCategorized itemsCategorized in shopData.upgradeItems)
        {

            ShopData.ShopWorldCategory category2 = itemsCategorized.category;

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();

            shopData.foldoutList2[(int)category2] = EditorGUILayout.Foldout(shopData.foldoutList2[(int)category2], shopData.nombresCategoriaLejibles[(int)category2]);

            if (GUILayout.Button("Añadir", GUILayout.MaxWidth(100f)))
            {

                itemsCategorized.upgradeItemList.Add(new ShopData.UpgradeItem());

                shopData.selectedCurrency2.Add(0);

            }

            EditorGUILayout.EndHorizontal();

            if (shopData.foldoutList2[(int)category2])
            {

                for (int i = 0; i < itemsCategorized.upgradeItemList.Count; i++)
                {
                    
                    ShopData.UpgradeItem upgrade = itemsCategorized.upgradeItemList[i];
                    upgrade.itemId = i+1;
                    DrawUpgradeItem(itemsCategorized, upgrade);                  

                }

            }

        }

    }

    private void DrawExtraList()
    {
        GUILayout.Space(20f);

        {
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Extras", boxHeaderStyle))
            {

                shopData.displayExtras = !shopData.displayExtras;
            }

            if (shopData.displayExtras) return;

            GUILayout.FlexibleSpace();

            EditorGUILayout.EndHorizontal();
        }

        foreach (ShopData.ExtrasItemCategorized itemsCategorized in shopData.extrasItems)
        {
            ShopData.ExtrasCategory category2 = itemsCategorized.category;

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();

            shopData.foldoutList3[(int)category2] = EditorGUILayout.Foldout(shopData.foldoutList3[(int)category2], shopData.nombreExtras[(int)category2]);

            if (GUILayout.Button("Añadir", GUILayout.MaxWidth(100f)))
            {
                itemsCategorized.upgradeItemList.Add(new ShopData.CofreItem());
            }

            EditorGUILayout.EndHorizontal();

            if (shopData.foldoutList3[(int)category2])
            {
                for (int i = 0; i < itemsCategorized.upgradeItemList.Count; i++)
                {
                    ShopData.CofreItem upgrade = itemsCategorized.upgradeItemList[i];
                    upgrade.itemId = i + 1;
                    DrawCofreItem(itemsCategorized, upgrade);

                }

            }

        }

    }

    private void DrawSoundItem(ShopData.UpgradeItem upgradeItem)
    {

        GUILayout.Space(20f);

        {
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Sonidos", boxHeaderStyle)) shopData.displaySets = !shopData.displaySets;
            if (shopData.displaySets) return;

            GUILayout.FlexibleSpace();

            EditorGUILayout.EndHorizontal();
        }

        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Space(30);

            {
                EditorGUILayout.BeginVertical("Box");

                {
                    EditorGUILayout.BeginHorizontal();

                    {
                        EditorGUILayout.BeginVertical();

                        EditorGUILayout.BeginHorizontal();

                        EditorGUILayout.LabelField("Sonidos", GUILayout.Width(60f));

                        EditorGUILayout.EndHorizontal();

                        string[] nombres = new string[] { "Fondo menu", "D. Pistola", "D. Rifle", "D. Escopeta", "D. FrancoTirador",
                        "Explosion Granada", "Mando Elevables", "Seleccionar Mundo" ,"Musica mundo 1", "Musica mundo 2", "Boton Play", "Pulsar botones", "pulsar back",
                        "Boton Delante Detras", "Scroll tumbas", "No tienes aun el arma", "Play inventario escogido", "Titulo Gloomy", "Añadir Arma",
                        "Añadir Recurso", "Restar Recurso", "TrasicionEntrePantallas", "Dar moneda", "Fanfarria Armas", "Game Over", "Fanfarria Victoria", "back Menu",
                            "Clin Estrella", "foco", "D. Gatling", "Final Gatling", "Premio Cofre", "D. Uzi", "D. Laser", "Combo"};

                        for(int i = 0; i < upgradeItem.audios.Length && i < nombres.Length; i++)
                        {
                            EditorGUILayout.BeginHorizontal();

                            EditorGUILayout.LabelField(nombres[i] + "(" + i + ")", GUILayout.Width(150f));

                            upgradeItem.audios[i] = (AudioClip)EditorGUILayout.ObjectField(upgradeItem.audios[i], typeof(AudioClip), true);

                            EditorGUILayout.EndHorizontal();
                        }


                    }
                    EditorGUILayout.EndHorizontal();
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndHorizontal();
        }


    }

    private void DrawCofreItem(ShopData.ExtrasItemCategorized itemsCategorized, ShopData.CofreItem cofreitem)
    {
        int index = itemsCategorized.upgradeItemList.IndexOf(cofreitem);

        GUILayout.Space(10);

        {
            EditorGUILayout.BeginVertical("Box");

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField("ID:", GUILayout.Width(30f));
            EditorGUILayout.LabelField(cofreitem.itemId.ToString(), GUILayout.MaxWidth(40f));

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField("Descripción:", GUILayout.Width(120f));
            cofreitem.Descripcion = EditorGUILayout.TextArea(cofreitem.Descripcion, GUILayout.Height(30f));

            EditorGUILayout.EndHorizontal();

            GUILayout.Space(20);

            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Añadir Opción", GUILayout.Width(120f)))
            {
                if (cofreitem.posibilidades < 40)
                    cofreitem.posibilidades++;
            }

            if (GUILayout.Button("Eliminar Opción", GUILayout.Width(120f)))
            {
                if (cofreitem.posibilidades >= 0)
                cofreitem.posibilidades--;

            }

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField("Opciones:", GUILayout.Width(120f));

            EditorGUILayout.EndHorizontal();

            GUILayout.Space(20);

            for (int i = 0; i < cofreitem.posibilidades; i++)
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.LabelField("Posibilidad "+(i+1).ToString(), GUILayout.Width(120f));

                if (GUILayout.Button("+", GUILayout.Width(40f)))
                {
                    if (cofreitem.NumOpciones[i] <= 40)
                        cofreitem.NumOpciones[i]++;
                }

                if (GUILayout.Button("-", GUILayout.Width(40f)))
                {
                    if (cofreitem.NumOpciones[i] >= 0)
                        cofreitem.NumOpciones[i]--;
                }

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.LabelField("%", GUILayout.Width(30));

                cofreitem.porcentaje[i] = EditorGUILayout.IntField(cofreitem.porcentaje[i], GUILayout.MaxWidth(40f));

                EditorGUILayout.EndHorizontal();

                GUILayout.Space(10);

                int ancho = 10;

                for (int j = 0; j < cofreitem.NumOpciones[i]; j++)
                {
                    EditorGUILayout.BeginHorizontal();

                    cofreitem.tipoPosible[(i * ancho) + j] = (ShopData.TipoPosibilidad)EditorGUILayout.EnumPopup(cofreitem.tipoPosible[(i * ancho) + j], GUILayout.MaxWidth(100));

                    switch (cofreitem.tipoPosible[(i * ancho) + j])
                    {
                        case ShopData.TipoPosibilidad.ORO:
                            EditorGUILayout.LabelField("MIN: ", GUILayout.Width(30));
                            cofreitem.oroMin[i] = EditorGUILayout.IntField(cofreitem.oroMin[i], GUILayout.MaxWidth(40f));
                            EditorGUILayout.LabelField("MAX: ", GUILayout.Width(30));
                            cofreitem.oroMax[i] = EditorGUILayout.IntField(cofreitem.oroMax[i], GUILayout.MaxWidth(40f));
                            break;
                        case ShopData.TipoPosibilidad.DIAMANTES:
                            EditorGUILayout.LabelField("MIN: ", GUILayout.Width(30));
                            cofreitem.diamantesMin[i] = EditorGUILayout.IntField(cofreitem.diamantesMin[i], GUILayout.MaxWidth(40f));
                            EditorGUILayout.LabelField("MAX: ", GUILayout.Width(30));
                            cofreitem.DiamantesMax[i] = EditorGUILayout.IntField(cofreitem.DiamantesMax[i], GUILayout.MaxWidth(40f));
                            break;
                        case ShopData.TipoPosibilidad.FRAGMENTO:
                            EditorGUILayout.LabelField("ID ARMA: ", GUILayout.Width(80));
                            cofreitem.idArmaFragmento[(i*ancho)+j] = EditorGUILayout.IntField(cofreitem.idArmaFragmento[(i * ancho) + j], GUILayout.MaxWidth(40f));
                            EditorGUILayout.LabelField("MIN: ", GUILayout.Width(80));
                            cofreitem.numFragmentosMIN[(i * ancho)+j] = EditorGUILayout.IntField(cofreitem.numFragmentosMIN[(i * ancho) + j], GUILayout.MaxWidth(40f));
                            EditorGUILayout.LabelField("MAX: ", GUILayout.Width(80));
                            cofreitem.numFragmentosMAX[(i * ancho)+j] = EditorGUILayout.IntField(cofreitem.numFragmentosMAX[(i * ancho) + j], GUILayout.MaxWidth(40f));
                            break;
                    }


                    EditorGUILayout.EndHorizontal();
                }
               
            }


            EditorGUILayout.EndVertical();
        }
    }


    private void DrawUpgradeItem(ShopData.MundosItemsCategorized itemsCategorized, ShopData.UpgradeItem upgradeItem) {

        int index = itemsCategorized.upgradeItemList.IndexOf(upgradeItem);

        EditorGUILayout.Space();

        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Space(30);

            {
                EditorGUILayout.BeginVertical("Box");

                {
                    EditorGUILayout.BeginHorizontal();

                    {
                        EditorGUILayout.BeginVertical();

                        EditorGUILayout.BeginHorizontal();

                        EditorGUILayout.LabelField("Nivel", GUILayout.Width(60f));
                        upgradeItem.itemId = EditorGUILayout.IntField(upgradeItem.itemId, GUILayout.MaxWidth(40f));

                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();

                        EditorGUILayout.LabelField("Escena", GUILayout.Width(60f));
                        upgradeItem.escena = EditorGUILayout.IntField(upgradeItem.escena, GUILayout.MaxWidth(40f));

                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();

                        EditorGUILayout.LabelField("Tiempo", GUILayout.Width(60f));
                        upgradeItem.tiempoEscena = EditorGUILayout.IntField(upgradeItem.tiempoEscena, GUILayout.MaxWidth(40f));

                        EditorGUILayout.EndHorizontal();

                        if (upgradeItem.frontal == false)
                        {
                            EditorGUILayout.BeginHorizontal();

                            EditorGUILayout.LabelField("% Torre", GUILayout.Width(60f));
                            upgradeItem.PorcentajeEstrellaDos = EditorGUILayout.IntField(upgradeItem.PorcentajeEstrellaDos, GUILayout.MaxWidth(40f));

                            EditorGUILayout.EndHorizontal();
                        }
                        else
                        {
                            EditorGUILayout.BeginHorizontal();

                            EditorGUILayout.LabelField("Victimas 1", GUILayout.Width(70f));
                            upgradeItem.Victimas[0] = EditorGUILayout.IntField(upgradeItem.Victimas[0], GUILayout.MaxWidth(40f));

                            EditorGUILayout.EndHorizontal();

                            EditorGUILayout.BeginHorizontal();

                            EditorGUILayout.LabelField("Victimas 2", GUILayout.Width(70f));
                            upgradeItem.Victimas[1] = EditorGUILayout.IntField(upgradeItem.Victimas[1], GUILayout.MaxWidth(40f));

                            EditorGUILayout.EndHorizontal();

                            EditorGUILayout.BeginHorizontal();

                            EditorGUILayout.LabelField("Victimas 3", GUILayout.Width(70f));
                            upgradeItem.Victimas[2] = EditorGUILayout.IntField(upgradeItem.Victimas[2], GUILayout.MaxWidth(40f));

                            EditorGUILayout.EndHorizontal();
                        }


                        EditorGUILayout.EndVertical();
                    }

                    if (GUILayout.Button("+", GUILayout.Width(50f)))
                    {

                        itemsCategorized.upgradeItemList.Insert(index+1, new ShopData.UpgradeItem());
                    //    shopData.selectedCurrency2.RemoveAt(shopData.selectedCurrency2.Count - 1);
                        return;

                    }

                    if (GUILayout.Button("X", GUILayout.Width(50f))) {

                        itemsCategorized.upgradeItemList.Remove(upgradeItem);
                        shopData.selectedCurrency2.RemoveAt(shopData.selectedCurrency2.Count - 1);
                        return;

                    }

                    EditorGUILayout.EndHorizontal();
                }

                upgradeItem.itemDescription = EditorGUILayout.TextArea(upgradeItem.itemDescription, GUILayout.Height(50f));
                
                {

                    float medidas = 45f;

                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Num Tips", GUILayout.Width(100f));

                    upgradeItem.numTips = EditorGUILayout.IntField(upgradeItem.numTips, GUILayout.MaxWidth(100f));

                    EditorGUILayout.EndHorizontal();

                    if(upgradeItem.numTips != 0)
                    {
                        EditorGUILayout.BeginHorizontal();

                        EditorGUILayout.LabelField(" ", GUILayout.Width(100f));

                        EditorGUILayout.LabelField("0", GUILayout.Width(medidas));

                        EditorGUILayout.LabelField("1", GUILayout.Width(medidas));

                        EditorGUILayout.LabelField("2", GUILayout.Width(medidas));

                        EditorGUILayout.LabelField("3", GUILayout.Width(medidas));

                        EditorGUILayout.LabelField("4", GUILayout.Width(medidas));

                        EditorGUILayout.LabelField("5", GUILayout.Width(medidas));

                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();

                        EditorGUILayout.LabelField("Tips", GUILayout.Width(100f));

                        for (int i = 0; i < upgradeItem.numTips; i++)
                        {
                            upgradeItem.idTips[i] = EditorGUILayout.IntField(upgradeItem.idTips[i], GUILayout.MaxWidth(medidas));
                        }

                        EditorGUILayout.EndHorizontal();
                    }

                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Nivel Frontal", GUILayout.Width(150f));
                    upgradeItem.frontal = GUILayout.Toggle(upgradeItem.frontal, "", GUILayout.Width(15f));

                    EditorGUILayout.EndHorizontal();

                    if(upgradeItem.frontal == false)
                    {
                        EditorGUILayout.BeginHorizontal();

                        EditorGUILayout.LabelField("Puntos", GUILayout.Width(150f));
                        upgradeItem.PuntosNivel = EditorGUILayout.IntField(upgradeItem.PuntosNivel, GUILayout.Width(50f));

                        EditorGUILayout.EndHorizontal();
                    }
              
                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Recompensa Oro", GUILayout.Width(150f));
                    upgradeItem.RecompensaOro = EditorGUILayout.IntField(upgradeItem.RecompensaOro, GUILayout.Width(50f));

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Arma Desbloqueable", GUILayout.Width(150f));
                    upgradeItem.ArmaDesbloqueable = GUILayout.Toggle(upgradeItem.ArmaDesbloqueable, "", GUILayout.Width(15f));

                    if(upgradeItem.ArmaDesbloqueable == true)
                    {
                        EditorGUILayout.LabelField("ID Arma", GUILayout.Width(150f));
                        upgradeItem.idArmaDes = EditorGUILayout.IntField(upgradeItem.idArmaDes, GUILayout.Width(50f));
                    }

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Procedural Desbloqueable", GUILayout.Width(150f));
                    upgradeItem.ProceduralDesbloqueable = GUILayout.Toggle(upgradeItem.ProceduralDesbloqueable, "", GUILayout.Width(15f));

                    if (upgradeItem.ProceduralDesbloqueable == true)
                    {
                        EditorGUILayout.LabelField("Nivel", GUILayout.Width(150f));
                        upgradeItem.idNivelDes = EditorGUILayout.IntField(upgradeItem.idNivelDes, GUILayout.Width(50f));
                    }

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Armas Obligatorias", GUILayout.Width(150f));
                    upgradeItem.ArmasObligatorias = GUILayout.Toggle(upgradeItem.ArmasObligatorias, "", GUILayout.Width(15f));

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    if (upgradeItem.ArmasObligatorias == true)
                    {
                        EditorGUILayout.LabelField("Cantidad", GUILayout.Width(150f));
                        upgradeItem.NumArmasObligatorias = EditorGUILayout.IntField(upgradeItem.NumArmasObligatorias, GUILayout.Width(50f));              
                    }

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    if (upgradeItem.ArmasObligatorias == true)
                    {
                        EditorGUILayout.LabelField("C. Secundarias", GUILayout.Width(150f));
                        upgradeItem.numArmasSecundariasObligatorias = EditorGUILayout.IntField(upgradeItem.numArmasSecundariasObligatorias, GUILayout.Width(50f));
                    }

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Armas Bloqueadas", GUILayout.Width(150f));
                    upgradeItem.armasBloq = GUILayout.Toggle(upgradeItem.armasBloq, "", GUILayout.Width(15f));

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    if (upgradeItem.armasBloq == true)
                    {
                        EditorGUILayout.LabelField("C. Bloqueadas", GUILayout.Width(150f));
                        upgradeItem.numArmasBloq = EditorGUILayout.IntField(upgradeItem.numArmasBloq, GUILayout.Width(50f));
                    }

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    if (upgradeItem.armasBloq == true)
                    {
                        for (int i = 0; i < upgradeItem.numArmasBloq; i++)
                        {
                            EditorGUILayout.BeginVertical();

                            EditorGUILayout.LabelField("ID " + (i + 1).ToString(), GUILayout.Width(45f));
                            upgradeItem.idArmasBloq[i] = EditorGUILayout.IntField(upgradeItem.idArmasBloq[i], GUILayout.Width(45f));

                            EditorGUILayout.EndVertical();
                        }
                    }

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    if (upgradeItem.ArmasObligatorias == true)
                    {
                        EditorGUILayout.LabelField("Tutorial", GUILayout.Width(150f));
                        upgradeItem.esTutorial = EditorGUILayout.Toggle(upgradeItem.esTutorial, GUILayout.Width(50f));
                    }

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    if (upgradeItem.ArmaDesbloqueable == false && upgradeItem.idLlave == 0)
                    {
                        EditorGUILayout.LabelField("Cofre (B:1 P:2 O:3)", GUILayout.Width(150f));
                        upgradeItem.idcofre = EditorGUILayout.IntField(upgradeItem.idcofre, GUILayout.Width(50f));
                    }

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    if (upgradeItem.ArmaDesbloqueable == false)
                    {
                        EditorGUILayout.LabelField("Llave (Nivel W3)", GUILayout.Width(150f));
                        upgradeItem.idLlave = EditorGUILayout.IntField(upgradeItem.idLlave, GUILayout.Width(50f));
                    }

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    if(upgradeItem.IdArmas == null)
                    upgradeItem.IdArmas = new int[80];

                    if (upgradeItem.ArmasObligatorias == true)
                    {

                        for (int i = 0; i < upgradeItem.NumArmasObligatorias; i++)
                        {
                            EditorGUILayout.BeginVertical();

                            EditorGUILayout.LabelField("ID " + (i + 1).ToString(), GUILayout.Width(45f));
                            upgradeItem.IdArmas[i] = EditorGUILayout.IntField(upgradeItem.IdArmas[i], GUILayout.Width(45f));

                            EditorGUILayout.EndVertical();
                        }
                    }

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    if (upgradeItem.IdArmasSecundarias == null)
                    upgradeItem.IdArmasSecundarias = new int[80];
                    if(upgradeItem.IdArmasSecundariasCantidad == null)
                    upgradeItem.IdArmasSecundariasCantidad = new int[80];

                    if (upgradeItem.numArmasSecundariasObligatorias > 0)
                    {

                        for (int i = 0; i < upgradeItem.numArmasSecundariasObligatorias; i++)
                        {
                            EditorGUILayout.BeginVertical();

                            EditorGUILayout.LabelField("ID " + (i + 1).ToString(), GUILayout.Width(45f));
                            upgradeItem.IdArmasSecundarias[i] = EditorGUILayout.IntField(upgradeItem.IdArmasSecundarias[i], GUILayout.Width(45f));
                            EditorGUILayout.LabelField("C.: " + (i + 1).ToString(), GUILayout.Width(45f));
                            upgradeItem.IdArmasSecundariasCantidad[i] = EditorGUILayout.IntField(upgradeItem.IdArmasSecundariasCantidad[i], GUILayout.Width(45f));

                            EditorGUILayout.EndVertical();
                        }
                    }

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();

                    if(upgradeItem.idEnemigo.Length == 0)
                    {
                        upgradeItem.idEnemigo = new int[20];
                        upgradeItem.cantidadEnemigos = new int[20];
                    }

                    EditorGUILayout.LabelField("Max. Zombies Pantalla", GUILayout.Width(150f));

                    if (GUILayout.Button("+", GUILayout.MaxWidth(40f)))
                    {
                        upgradeItem.numTipoEnemigos++;
                    }

                    if (GUILayout.Button("-", GUILayout.MaxWidth(40f)))
                    {
                        upgradeItem.numTipoEnemigos--;
                    }                  

                    EditorGUILayout.EndHorizontal();

                    for (int i = 0; i < upgradeItem.numTipoEnemigos; i++)
                    {
                        GUILayout.BeginHorizontal();
                        GUILayout.Label("Tipo " + (i).ToString());

                        upgradeItem.idEnemigo[i] = EditorGUILayout.Popup(upgradeItem.idEnemigo[i], _choices);

                        GUILayout.Label("Cantidad: ");

                        upgradeItem.cantidadEnemigos[i] = EditorGUILayout.IntField(upgradeItem.cantidadEnemigos[i], GUILayout.Width(50f));

                        GUILayout.EndHorizontal();
                    }
                }

                EditorGUI.indentLevel++;

                for (int i = 0; i < upgradeItem.costPacks.Count; i++) {

                    {
                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Space(30);

                        GUI.backgroundColor = new Color(0.65f, 0.65f, 1, 1);
                        {
                            EditorGUILayout.BeginVertical(GUI.skin.button);
                            GUI.backgroundColor = Color.white;

                            {
                                EditorGUILayout.BeginHorizontal();


                                if (upgradeItem.onSale) {

                                    upgradeItem.costPacks[i].gotDiscount = EditorGUILayout.Toggle(upgradeItem.costPacks[i].gotDiscount, GUILayout.Width(60f));
                                    //standardItem.discountEnable[currency] = EditorGUILayout.Toggle(standardItem.discountEnable[currency], GUILayout.Width(60f));

                                } else {

                                    EditorGUILayout.LabelField("", GUILayout.Width(60f));

                                }

                                upgradeItem.costPacks[i].cost = EditorGUILayout.IntField(shopData.readableCurrencyNames[(int)upgradeItem.costPacks[i].currency], upgradeItem.costPacks[i].cost);
                                //standardItem.itemCost[currency] = EditorGUILayout.IntField(shopData.readableCurrencyNames[(int)currency], standardItem.itemCost[currency]);
                                if (upgradeItem.costPacks[i].cost < 0) upgradeItem.costPacks[i].cost = 0;

                                if (GUILayout.Button("x", GUILayout.MaxWidth(40f))) {
                                    upgradeItem.costPacks.Remove(upgradeItem.costPacks[i]);
                                    //standardItem.itemCost.Remove(currency);
                                    //standardItem.discountEnable.Remove(currency);
                                }

                                EditorGUILayout.EndHorizontal();
                            }

                            EditorGUILayout.EndVertical();
                        }

                        EditorGUILayout.EndHorizontal();
                    }

                }

                EditorGUI.indentLevel--;

                {
                    EditorGUILayout.BeginHorizontal();

                    if (upgradeItem.costPacks.Count > 0) upgradeItem.onSale = GUILayout.Toggle(upgradeItem.onSale, "On Sale", GUI.skin.button, GUILayout.MaxWidth(75f));
                    else upgradeItem.onSale = false;

                    if (upgradeItem.onSale) {
                        EditorGUIUtility.labelWidth = 50f;
                        upgradeItem.salePercentage = EditorGUILayout.Slider("%", upgradeItem.salePercentage, 0f, 1f, GUILayout.Width(200f));
                        EditorGUIUtility.labelWidth = 0;
                        GUILayout.FlexibleSpace();
                    }

                    EditorGUILayout.EndHorizontal();
                }

                EditorGUILayout.EndVertical();
            }

            EditorGUILayout.EndHorizontal();
        }

       

        if (upgradeItem.hidden) GUI.enabled = true;

    }

    private void CreateStyles() {

        if (headerStyle == null) {
            
            headerStyle = new GUIStyle(GUI.skin.label);

            headerStyle.fontSize = 30;
            headerStyle.fontStyle = FontStyle.Bold;

        }

        if(boxHeaderStyle == null) {

            boxHeaderStyle = new GUIStyle(GUI.skin.box);

            boxHeaderStyle.fontStyle = FontStyle.Bold;
            boxHeaderStyle.normal.textColor = new Color(0.7f,0.7f,0.7f);

            boxHeaderStyle.hover.background = boxHeaderStyle.normal.background;
            boxHeaderStyle.hover.textColor = new Color(0.9f, 0.9f, 0.9f);

        }
        
    }

}
