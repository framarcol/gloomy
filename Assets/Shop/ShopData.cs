﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class ShopData : ScriptableObject {

    [Serializable]
    public enum TipoPosibilidad { ORO = 0, DIAMANTES = 1, FRAGMENTO = 3 }

    [Serializable]
    public enum ShopCurrency { DIAMANTES = 0, ORO = 1}
    public List<string> readableCurrencyNames = new List<string> { "Diamantes", "Oro"};

    [Serializable]
	public enum ShopStandardCategory { ARMAS = 0, ARMAS_SECUNDARIAS = 1/*, UPGRADES = 3 */}
    public List<string> readableCategoryNames = new List<string>{ "Principales" , "Secundarias"/*, "Upgrades"*/ };

    [Serializable]
    public enum ShopWorldCategory { MUNDO1 = 0, MUNDO2 = 1, MUNDO3 = 2, MUNDO4 = 3}
    public List<string> nombresCategoriaLejibles = new List<string> { "Mundo 1", "Mundo 2", "Mundo 3", "Mundo 4"};

    [Serializable]
    public enum ExtrasCategory { COFRES = 0 }
    public List<string> nombreExtras = new List<string> { "Cofres"};

    [Serializable]
    public class UnlockItemEvent{

        public bool specialLevel;

        public int levelId;

        public UnlockItemEvent() {

            specialLevel = false;
            levelId = -1;

        }

    }

    [Serializable]
    public class CostPack {

        public ShopCurrency currency;

        public int cost;

        public bool gotDiscount;

        public CostPack(ShopCurrency currency) {

            this.currency = currency;
            cost = 0;
            gotDiscount = false;

        }

    }

    [Serializable]
    public class ShopItem {

        public bool hidden;

        public int itemId;

        public string itemName;

        public string itemDescription;

        public int Recursos;
        public int MaxLevel;
        public int NumPiezas;

        public float[] Alcance = new float [6];
        public int[] Capacidad = new int[6];
        public float[] Ratio = new float[6];
        public float[] Cadencia = new float[6];
        public int[] Dano = new int[6];
        public float[] TiempoRecarga = new float[6];
        public int[] Diamantes = new int[6];
        public int[] Oro = new int[6];
        public int[] Victimas = new int[3];
        public int[] idTips;
        public int[] idArmasBloq = new int[10];
        public int idcofre = 0;
        public int idLlave = 0;
        public int numTips;

        public bool armasBloq;
        public int numArmasBloq;

        public Sprite[] itemThumbnail;
        public Sprite[] itemThumbnail2;
        public Sprite itemHigh;

        public GameObject itemPresentation;
        public Material itemOverrideMaterial;
        public AudioClip[] audios;

        public int numTipoEnemigos;
        public int[] idEnemigo;
        public int[] cantidadEnemigos;

        public List<CostPack> costPacks;
        
        public Dictionary<ShopCurrency, int> itemCost;
        public Dictionary<ShopCurrency, bool> discountEnable;
        
        public bool onSale;
        public float salePercentage;

        public ShopItem() {

            hidden = false;
            itemId = 0;
            itemName = "";
            itemDescription = "Descripcion";
            itemThumbnail = new Sprite[6];
            itemThumbnail2 = new Sprite[6];
            
            

            Alcance = new float[6];
            Capacidad = new int[6];
            Ratio = new float[6];
            Cadencia = new float[6];
            Dano = new int[6];
            TiempoRecarga = new float[6];
            Victimas = new int[3];
            audios = new AudioClip[50];
            idEnemigo = new int[20];
            cantidadEnemigos = new int[20];
            idTips = new int[10];

            itemPresentation = null;
            itemOverrideMaterial = null;

            costPacks = new List<CostPack>();

            itemCost = new Dictionary<ShopCurrency, int>();
            discountEnable = new Dictionary<ShopCurrency, bool>();

            onSale = false;
            salePercentage = 0f;

        }
        
    }

    [Serializable]
    public class SoundItem
    {

        AudioClip[] audios;

        public SoundItem()
        {
            audios = new AudioClip[40];
        }

    }

    [Serializable]
    public class CofreItem
    {
        public TipoPosibilidad[] tipoPosible;
        public int itemId;
        public int[] numFragmentosMIN;
        public int[] numFragmentosMAX;
        public int posibilidades;
        public int[] NumOpciones;
        public string Descripcion;
        public string nombreCofre;
        public int[] porcentaje;
        public int[] oroMin, oroMax;
        public int[] diamantesMin, DiamantesMax;
        public int[] idArmaFragmento;
        public int ancho;

        public CofreItem()
        {
            ancho = 10;
            tipoPosible = new TipoPosibilidad[200];
            numFragmentosMIN = new int[200];
            numFragmentosMAX = new int[200];
            idArmaFragmento = new int[200];
            oroMax = new int[200];
            oroMin = new int[200];
            diamantesMin = new int[200];
            DiamantesMax = new int[200];
            porcentaje = new int[200];
            NumOpciones = new int[200];
        }

    }

    [Serializable]
    public class StandardShopItem : ShopItem {

        public bool hasLowpolyVersion;
        public GameObject lowpolyItemPresentation;
        public Material lowpolyItemOverrideMaterial;

        public bool hasUnlockEvent;
        public UnlockItemEvent unlockItemEvent;

        public StandardShopItem() : base() {

            hasLowpolyVersion = false;
            lowpolyItemPresentation = null;
            lowpolyItemOverrideMaterial = null;

            hasUnlockEvent = false;
            unlockItemEvent = new UnlockItemEvent();

        }

    }

    [Serializable]
    public class StandardItemsCategorized {

        public ShopStandardCategory category;

        public List<StandardShopItem> standardItemList;

        public StandardItemsCategorized(ShopStandardCategory category) {

            this.category = category;
            standardItemList = new List<StandardShopItem>();


        }

    }

    [Serializable]
    public class ExtrasItemCategorized
    {
        public ExtrasCategory category;

        public List<CofreItem> upgradeItemList;

        public ExtrasItemCategorized(ExtrasCategory category)
        {

            this.category = category;
            upgradeItemList = new List<CofreItem>();


        }
    }

    [Serializable]
    public class MundosItemsCategorized
    {
        public ShopWorldCategory category;

        public List<UpgradeItem> upgradeItemList;

        public MundosItemsCategorized(ShopWorldCategory category)
        {

            this.category = category;
            upgradeItemList = new List<UpgradeItem>();


        }
    }

    [Serializable]
    public class ShopItemSet : ShopItem {

        public List<int> linkedItemIds;

        public ShopItemSet() : base() {

            linkedItemIds = new List<int>();

        }

    }

    [Serializable]
    public class UpgradeItem : ShopItem {

     //   public enum UpgradeType { LIFE = 0, MAX_AMMO = 1, WORLD = 2 }
       // public UpgradeType updgradeType;

        public int extraLife;

        public int maxAmmo;

        public int PuntosNivel;
        public bool ArmaDesbloqueable;
        public bool ProceduralDesbloqueable;
        public bool ArmasObligatorias;
        public int NumArmasObligatorias;
        public int numArmasSecundariasObligatorias;
        public int[] IdArmas;
        public int[] IdArmasSecundarias;
        public int[] IdArmasSecundariasCantidad;
        public int idArmaDes;
        public int idNivelDes;
        public bool esTutorial;
        public int RecompensaOro;
        public int escena;
        public int tiempoEscena;
        public int PorcentajeEstrellaDos;
        public bool frontal;

        public int worldIdToUnlock;
        public Sprite worldBackground;

        public UpgradeItem() : base() {

            extraLife = 0;
            maxAmmo = 0;
            worldIdToUnlock = -1;
            worldBackground = null;
        }

    }

    [Serializable]
    public class VirtualMoneyItem : ShopItem {

        public ShopCurrency targetCurrency;

        public int virtualCurrencyReward;

        public decimal realMoneyCost;

        public VirtualMoneyItem() : base() {

            targetCurrency = ShopCurrency.DIAMANTES;
            virtualCurrencyReward = 0;
            realMoneyCost = 0.00m;

        }

    }

    //Standard items (skins)
    //public Dictionary<ShopStandardCategory, List<StandardShopItem>> standardShopItems = new Dictionary<ShopStandardCategory, List<StandardShopItem>>();
    public List<StandardItemsCategorized> standardShopItems = new List<StandardItemsCategorized>();
    public bool displayStandardItems;

    //Set of skins
    public List<ShopItemSet> shopItemSets = new List<ShopItemSet>();
    public bool displaySets;

    //Upgrades
    public List<MundosItemsCategorized> upgradeItems = new List<MundosItemsCategorized>();
    public List<ExtrasItemCategorized> extrasItems = new List<ExtrasItemCategorized>();
    public bool displayUpgrades;
    public bool displayExtras;

    //Packs of virtual currency (diamonds)
    public List<VirtualMoneyItem> virtualMoneyPacks = new List<VirtualMoneyItem>();
    public bool displayVirtualMoneyPacks;

    //public ShopStandardCategory categoryOptions;
    public List<bool> foldoutList = new List<bool>();
    public List<bool> foldoutList2 = new List<bool>();
    public List<bool> foldoutList3 = new List<bool>();
    public List<int> selectedCurrency = new List<int>();
    public List<int> selectedCurrency2 = new List<int>();

    public bool ContainsCurrency(List<CostPack> packs, ShopCurrency currency) {

        for(int i = 0; i < packs.Count; i++) {

            if (packs[i].currency == currency) return true;

        }

        return false;

    }

    public StandardShopItem GetArma(int itemId) {

        for(int i = 0; i < standardShopItems.Count; i++) {

            if(standardShopItems[i].category == ShopStandardCategory.ARMAS ||standardShopItems[i].category == ShopStandardCategory.ARMAS_SECUNDARIAS) {

                for(int j = 0; j < standardShopItems[i].standardItemList.Count; j++) {

                    if(standardShopItems[i].standardItemList[j].itemId == itemId) {

                        return standardShopItems[i].standardItemList[j];

                    }

                }

            }

        }

        return null;

    }

    public CofreItem Getcofre(int idCofre)
    {
        return extrasItems[0].upgradeItemList[idCofre-1];
    }

    public int GetNumeroArmas(int tipo)
    {
        ShopStandardCategory categoria = ShopStandardCategory.ARMAS;
        switch (tipo)
        {
            case 1:
                categoria = ShopStandardCategory.ARMAS;
                break;
            case 2:
                categoria = ShopStandardCategory.ARMAS_SECUNDARIAS;
                break;
        }

        for (int i = 0; i < standardShopItems.Count; i++)
        {

            if (standardShopItems[i].category == categoria)
            {

                return standardShopItems[i].standardItemList.Count;

            }

        }

        return 0;
    }

    public int GetNumeroArmas()
    {
        return standardShopItems.Count;
    }

    public int GetNivelByEscena(int escena)
    {
        for (int i = 0; i < upgradeItems.Count; i++)
        {

                for (int j = 0; j < upgradeItems[i].upgradeItemList.Count; j++)
                {

                    if (upgradeItems[i].upgradeItemList[j].escena == escena)
                    {

                        return upgradeItems[i].upgradeItemList[j].itemId;

                    }

                }

        }

        return 0;
    }

    public int GetMundoByEscena(int escena)
    {
        for (int i = 0; i < upgradeItems.Count; i++)
        {

            for (int j = 0; j < upgradeItems[i].upgradeItemList.Count; j++)
            {

                if (upgradeItems[i].upgradeItemList[j].escena == escena)
                {

                    return i + 1;

                }

            }

        }

        return 0;
    }

    public int GetNumNivelesMundo(int idmundo)
    {
        for (int i = 0; i < upgradeItems.Count; i++)
        {
            if(i == idmundo)
            {
                return upgradeItems[i].upgradeItemList.Count;
            }    

        }

        return 0;
    }

    public int GetEscenaByNivel(int nivel)
    {
        for (int i = 0; i < upgradeItems.Count; i++)
        {

            for (int j = 0; j < upgradeItems[i].upgradeItemList.Count; j++)
            {

                if (upgradeItems[i].upgradeItemList[j].itemId == nivel)
                {

                    return upgradeItems[i].upgradeItemList[j].escena;

                }

            }

        }

        return 0;
    }

    public int[] GetArmaNivelDesbloqueado(int idArma)
    {
        int[] tabla = new int[2];

        for (int i = 0; i < upgradeItems.Count; i++)
        {
            for (int j = 0; j < upgradeItems[i].upgradeItemList.Count; j++)
            {
                if (upgradeItems[i].upgradeItemList[j].ArmaDesbloqueable)
                {

                    if(upgradeItems[i].upgradeItemList[j].idArmaDes == idArma)
                    {
                        
                        tabla[0] = j;
                        tabla[1] = i;
                        return tabla;
                    }        

                }
            }
        }
        return tabla;
    }

    public StandardShopItem GetSecundaria(int itemId) {

        for (int i = 0; i < standardShopItems.Count; i++) {

            if (standardShopItems[i].category == ShopStandardCategory.ARMAS_SECUNDARIAS) {

                for (int j = 0; j < standardShopItems[i].standardItemList.Count; j++) {

                    if (standardShopItems[i].standardItemList[j].itemId == itemId) {

                        return standardShopItems[i].standardItemList[j];

                    }

                }

            }

        }

        return null;

    }

    public UpgradeItem GetMundo(int itemId, int mundo) {

        /* for (int i = 0; i < upgradeItems.Count; i++) {

             if (upgradeItems[i].upgradeItemList[i].itemId == itemId && upgradeItems[i].Mundo == worldId) return upgradeItems[i];

         }*/
       // Debug.Log(mundo);
        ShopWorldCategory categoria = ShopWorldCategory.MUNDO1;

        switch (mundo)
        {
            case 1:
                categoria = ShopWorldCategory.MUNDO1;
                break;
            case 2:
                categoria = ShopWorldCategory.MUNDO2;
                break;
            case 3:
                categoria = ShopWorldCategory.MUNDO3;
                break;
        }

        for (int i = 0; i < upgradeItems.Count; i++)
        {

            if (upgradeItems[i].category == categoria)
            {

                for (int j = 0; j < upgradeItems[i].upgradeItemList.Count; j++)
                {

                    if (upgradeItems[i].upgradeItemList[j].itemId == itemId)
                    {

                        return upgradeItems[i].upgradeItemList[j];

                    }

                }

            }

        }

        return null;

    }

    public int GetCountWorld(int idWorld)
    {

        ShopWorldCategory categoria = ShopWorldCategory.MUNDO1;

        switch (idWorld)
        {
            case 1:
                categoria = ShopWorldCategory.MUNDO1;
                break;
            case 2:
                categoria = ShopWorldCategory.MUNDO2;
                break;
            case 3:
                categoria = ShopWorldCategory.MUNDO3;
                break;
        }

        for (int i = 0; i < upgradeItems.Count; i++)
        {

            if (upgradeItems[i].category == categoria)
            {

                return upgradeItems[i].upgradeItemList.Count;

            }

        }

        return 0;
    }

}
