﻿using DragonBones;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Superviviente : MonoBehaviour {

    public GameObject explosion;
    public UnityArmatureComponent armadura;

    public void Muerte(float tiempo)
    {      
        StartCoroutine(EsperarMuerte(tiempo));     
    }

    IEnumerator EsperarMuerte(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        Explode();
    }

    protected void Explode()
    {
        GetComponentInChildren<BoxCollider2D>().enabled = false;
        Rigidbody2D myRigidbody2D = GetComponent<Rigidbody2D>();
        armadura.armature.animation.timeScale = 0f;
        myRigidbody2D.velocity = Vector2.zero;
        myRigidbody2D.isKinematic = true;
        if (explosion != null)
        {
            explosion.transform.localEulerAngles = Vector3.forward * Random.Range(-180f, 180f);
            explosion.SetActive(true);
        }

        Rigidbody2D tempRigidbody;
        for (int i = 0; i < armadura.transform.childCount; i++)
        {
            tempRigidbody = armadura.transform.GetChild(i).gameObject.AddComponent<Rigidbody2D>();

            if (tempRigidbody != null)
            {
                tempRigidbody.mass = 200f;
                tempRigidbody.AddForce((armadura.transform.GetChild(i).position - transform.position) * 400, ForceMode2D.Impulse);

            }

        }
        StartCoroutine(EsperarMuerteFinal());
    }

    IEnumerator EsperarMuerteFinal()
    {
        yield return new WaitForSeconds(1);
        gameObject.SetActive(false);
     //   Destroy(gameObject);

    }
}
