﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TipsManager : MonoBehaviour {

    private void Awake()
    {
        ShopData.UpgradeItem minivel = IniciarJuego.shopData.GetMundo(BotonesMenu.nivelMundoSeleccionado, BotonesMenu.mundoSeleccionado);
#if UNITY_EDITOR
        PlayerPrefs.SetInt(SaveGame.nivelesDesbloquedos[BotonesMenu.mundoSeleccionado-1], 25);
#endif
        if(PlayerPrefs.GetInt(SaveGame.nivelesDesbloquedos[BotonesMenu.mundoSeleccionado-1]) >= BotonesMenu.nivelMundoSeleccionado)
        {
            GetComponent<Text>().text = SeleccionaIdiomas.gameText[15, minivel.idTips[Random.Range(0, minivel.numTips)]];
        }
        else
        {
            GetComponent<Text>().text = SeleccionaIdiomas.gameText[15, minivel.idTips[0]];
        }


    }

}
