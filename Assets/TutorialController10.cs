﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialController10 : MonoBehaviour {

   public  AnimationsPlaylist cartel, cartel3;
    public GameObject mensajeTutorial, mensajeTutorial2;
    public GameObject flechasTutorial;
    public GameObject cantTouchFullScreen;
    public GameObject btnPausa;
    bool unaVez;

   public BoxCollider2D arma1, arma2, arma3;


    IEnumerator Start () {

        arma1.enabled = false;
        arma3.enabled = false;
        arma2.enabled = false;
        btnPausa.SetActive(false);
        yield return new WaitForSeconds(2.1f);
        mensajeTutorial.SetActive(true);
        arma2.enabled = true;
 
        cartel.PlayAnimation("entrada", 0, 1, 1);
        yield return new WaitForSeconds(0.9f);
        Time.timeScale = 0;

    }

    private void Update()
    {

        for(int i = 0; i < MiarmaControla.posicionado.Length; i++)
        {
            if (MiarmaControla.posicionado[i])
            {
                ReanudarJuego2();
            }
        }       
    }

    public void ReanudarJuego()
    {
        cantTouchFullScreen.SetActive(true);
        mensajeTutorial.SetActive(false);
        flechasTutorial.SetActive(true);
        mensajeTutorial2.SetActive(true);

    }

    public void ReanudarJuego2()
    {
        if (!unaVez)
        {
            arma3.enabled = true;
            arma1.enabled = true;
            btnPausa.SetActive(true);
            unaVez = true;
            Time.timeScale = 1;
            flechasTutorial.SetActive(false);
            mensajeTutorial2.SetActive(false);
            StartCoroutine(MensajeTutorial3());
        }

    }

    IEnumerator MensajeTutorial3()
    {
        cantTouchFullScreen.SetActive(false);
        cartel3.PlayAnimation("entrada", 0, 1, 1);
        yield return new WaitForSeconds(4);
        cartel3.PlayAnimation("salida", 0, 1, 1);
    }
	
	
}
