﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialController11 : MonoBehaviour {

    public GameObject mensajeTutorial;
    public AnimationsPlaylist animacion;
    public Text txtCartel, txtTap;
    public GameObject CantTouchFullScreen;
    bool activado, activado2;

   public BoxCollider2D[] armas;

    public GameObject btnPausa;

	// Use this for initialization
	IEnumerator Start () {
        for (int i = 0; i < armas.Length; i++)
            armas[i].enabled = false;
        btnPausa.SetActive(false);
        txtCartel.text = SeleccionaIdiomas.gameText[13, 1];
        animacion.PlayAnimation("entrada", 0, 1, 1);
        yield return new WaitForSeconds(0.8f);
        txtTap.text = SeleccionaIdiomas.gameText[2, 2];
        Time.timeScale = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (!activado && Input.touchCount > 0)
        {
            activado = true;
            txtCartel.text = SeleccionaIdiomas.gameText[13, 2];
            Time.timeScale = 1;
            StartCoroutine(Esperar());
        }
        else
        {
            if (!activado && Input.GetMouseButtonDown(0))
            {
                activado = true;
                txtCartel.text = SeleccionaIdiomas.gameText[13, 2];
                Time.timeScale = 1;
                StartCoroutine(Esperar());
            }
        }

        if (activado2 && Input.touchCount > 0)
        {
            activado2 = false;
            //   btnPausa.gameObject.SetActive(true);
            txtCartel.text = SeleccionaIdiomas.gameText[13, 3];
            txtTap.text = "";
            Time.timeScale = 1;
            StartCoroutine(SalidaCartel());
        }
        else
        {
            if (activado2 && Input.GetMouseButtonDown(0))
            {
                activado2 = false;
                //   btnPausa.gameObject.SetActive(true);
                txtCartel.text = SeleccionaIdiomas.gameText[13, 3];
                txtTap.text = "";
                Time.timeScale = 1;
                StartCoroutine(SalidaCartel());
            }
        }
    }

    IEnumerator Esperar()
    {
        yield return new WaitForSeconds(0.2f);
        Time.timeScale = 0;
        activado2 = true;
    }

    IEnumerator SalidaCartel()
    {
        yield return new WaitForSeconds(4);
        for (int i = 0; i < armas.Length; i++)
            armas[i].enabled = true;
        CantTouchFullScreen.SetActive(false);
        btnPausa.SetActive(true);
        animacion.PlayAnimation("salida", 0, 1, 1);
    }
}
