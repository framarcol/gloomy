﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialController5 : MonoBehaviour {

    public GameObject mensajeSinPausa, mensajeTutorial, mensajeTutorial2;
    public GameObject notocar;
    public float tiempoSegundoMensaje;
    public float tiempoEsperaMensajeUno;
    public Button boton;
    public GameObject btnPausa;
    bool activado;
    bool unaVez;
   public bool tutorial5Nivel11;

    public AnimationsPlaylist animTuto2;

    public GameObject wait;

	// Use this for initialization

	IEnumerator Start () {

        btnPausa.SetActive(false);
        yield return new WaitForSeconds(tiempoEsperaMensajeUno);
        mensajeSinPausa.SetActive(false);
        yield return new WaitForSeconds(tiempoSegundoMensaje - 0.7f);
        btnPausa.SetActive(false);
        mensajeTutorial.SetActive(true);
        boton.interactable = false;
        yield return new WaitForSeconds(0.7f);
        notocar.SetActive(false);
        boton.interactable = true;
        Time.timeScale = 0f;

	}

    private void Update()
    {
        if (notocar.activeSelf)
        {
            wait.SetActive(true);
        }
        else wait.SetActive(false);
        if (!unaVez && activado && Input.touchCount > 0)
        {
            unaVez = true;
            StartCoroutine(DesactivarCartel2());
        }
        else
        {
            if (!unaVez && activado && Input.GetMouseButtonDown(0))
            {
                unaVez = true;
                StartCoroutine(DesactivarCartel2());
            }
        }
    }

    public void DesactivarMensaje()
    {
        Time.timeScale = 1f;
        if (!tutorial5Nivel11) btnPausa.SetActive(false);
        else btnPausa.SetActive(true);
        mensajeTutorial.SetActive(false);
      if(!tutorial5Nivel11)  StartCoroutine(Mensaje2());
    }

    IEnumerator Mensaje2()
    {
        btnPausa.SetActive(true);
        yield return new WaitForSeconds(4);
        btnPausa.SetActive(false);
        mensajeTutorial2.SetActive(true);
        animTuto2.PlayAnimation("entrada", 0, 1, 1);
        yield return new WaitForSeconds(1);
        Time.timeScale = 0;
        if(!tutorial5Nivel11) activado = true;
    }

    IEnumerator DesactivarCartel2()
    {
        Time.timeScale = 1;
        btnPausa.SetActive(true);
        animTuto2.PlayAnimation("salida", 0, 1, 1);
        yield return new WaitForSeconds(0.5f);
        mensajeTutorial2.SetActive(false);

    }
	
}
