﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialController6 : MonoBehaviour {

    public GameObject[] mensajes;
    public float tPrimerMensaje, tSegundoMensaje, tTercerMensaje;
    public float tiempoEsperaMensaje;
    public AnimationsPlaylist[] listas;

	IEnumerator Start () {

        yield return new WaitForSeconds(tPrimerMensaje);
        mensajes[0].SetActive(true);
        yield return new WaitForSeconds(tiempoEsperaMensaje);
        mensajes[0].SetActive(false);
        yield return new WaitForSeconds(tSegundoMensaje);
        listas[0].PlayAnimation("entrada", 0, 1, 1);
        yield return new WaitForSeconds(tiempoEsperaMensaje);
        mensajes[1].SetActive(false);
        yield return new WaitForSeconds(tTercerMensaje);
        listas[1].PlayAnimation("entrada", 0, 1, 1);
        yield return new WaitForSeconds(tiempoEsperaMensaje);
        mensajes[2].SetActive(false);


    }
}
