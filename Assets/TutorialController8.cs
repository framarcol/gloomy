﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialController8 : MonoBehaviour {

    public Button btnPausa;
    public AnimationsPlaylist cartel;
    public GameObject sombra1, sombra2, Mensajetutorial;
    bool activado, activado2, activado3;
    public Text txtCartel;
    ProtaControl miarma;
    public BoxCollider2D CantTouchThisFull;

	// Use this for initialization
	IEnumerator Start () {
        miarma = GameObject.FindGameObjectWithTag("ProtaTag").GetComponent<ProtaControl>();
        miarma.disparable = false;
        CantTouchThisFull.enabled = false;
        yield return new WaitForSeconds(2);
        Mensajetutorial.SetActive(true);
        txtCartel.text = SeleccionaIdiomas.gameText[11, 1];
        sombra1.SetActive(true);
        btnPausa.gameObject.SetActive(false);
        cartel.PlayAnimation("entrada", 0, 1, 1);
        yield return new WaitForSeconds(1);
        Time.timeScale = 0;
        activado = true;

    }
	
	// Update is called once per frame
	void Update () {

        if (activado && Input.touchCount > 0)
        {
            activado = false;
            txtCartel.text = SeleccionaIdiomas.gameText[11, 2];
            sombra1.SetActive(false);
            sombra2.SetActive(true);
            StartCoroutine(Espera1());
          }
        else
        {
            if (activado && Input.GetMouseButtonDown(0))
            {
                activado = false;
                txtCartel.text = SeleccionaIdiomas.gameText[11, 2];
                sombra1.SetActive(false);
                sombra2.SetActive(true);
                StartCoroutine(Espera1());
            }
        }

        if (activado2 && Input.touchCount > 0)
        {
            activado2 = false;
            txtCartel.text = SeleccionaIdiomas.gameText[11, 3];
            sombra2.SetActive(false);
            StartCoroutine(Espera2());
        }
        else
        {
            if (activado2 && Input.GetMouseButtonDown(0))
            {
                activado2 = false;
                txtCartel.text = SeleccionaIdiomas.gameText[11, 3];
                sombra1.SetActive(true);
                sombra2.SetActive(false);
                StartCoroutine(Espera2());
            }
        }

        if (activado3 && Input.touchCount > 0)
        {
            activado3 = false;
            StartCoroutine(Salir());
        }
        else
        {
            if (activado3 && Input.GetMouseButtonDown(0))
            {
                activado3 = false;
                StartCoroutine(Salir());
            }
        }
    }

    IEnumerator Espera1()
    {
        Time.timeScale = 1;
        yield return new WaitForSeconds(0.2f);
        Time.timeScale = 0;
        activado2 = true;
    }

    IEnumerator Espera2()
    {
        Time.timeScale = 1;
        yield return new WaitForSeconds(0.2f);
        Time.timeScale = 0;
        activado3 = true;
    }

    IEnumerator Salir()
    {
        CantTouchThisFull.enabled = true;
        Time.timeScale = 1;
        cartel.PlayAnimation("salida", 0, 1, 1);
        yield return new WaitForSeconds(0.5f);
        Mensajetutorial.SetActive(false);
        btnPausa.gameObject.SetActive(true);
        miarma.disparable = true;
    }
}
