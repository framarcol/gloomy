﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorialcontroller7 : MonoBehaviour {

    public AnimationsPlaylist cartel;
    public Text txtCartel, txtTap;
    public GameObject MensajeTutorial;
    bool activado, activado2;
    public Button btnPausa;
    public GameObject cantTouchFullScreen;

    public GameObject wait;

    IEnumerator Start()
    {
        //GameObject.FindGameObjectWithTag("ProtaTag").GetComponentInChildren<ProtaControl>().DeshabilitarDisparo();
        cantTouchFullScreen.SetActive(true);
        yield return new WaitForSeconds(6);
        cantTouchFullScreen.SetActive(false);
        //GameObject.FindGameObjectWithTag("ProtaTag").GetComponentInChildren<ProtaControl>().HabilitarDisparo();
        MensajeTutorial.SetActive(true);
        cartel.PlayAnimation("entrada", 0, 1, 1);
        btnPausa.gameObject.SetActive(false);
        txtCartel.text = SeleccionaIdiomas.gameText[10, 1];
        yield return new WaitForSeconds(1);
        Time.timeScale = 0;
        activado = true;
    }

    private void Update()
    {
        if (cantTouchFullScreen.activeSelf)
        {
            wait.SetActive(true);
        }
        else
        {
            wait.SetActive(false);
        }
        if(activado && Input.touchCount > 0)
        {
            activado = false;
            txtCartel.text = SeleccionaIdiomas.gameText[10, 2];
            StartCoroutine(Esperar());
        }
        else
        {
            if (activado && Input.GetMouseButtonDown(0))
            {
                activado = false;
                txtCartel.text = SeleccionaIdiomas.gameText[10, 2];
                StartCoroutine(Esperar());
            }
        }

        if (activado2 && Input.touchCount > 0)
        {
            activado2 = false;
            btnPausa.gameObject.SetActive(true);
            txtTap.text = "";
            StartCoroutine(SalidaCartel());
        }
        else
        {
            if (activado2 && Input.GetMouseButtonDown(0))
            {
                activado2 = false;
                btnPausa.gameObject.SetActive(true);
                txtTap.text = "";
                StartCoroutine(SalidaCartel());
            }
        }

    }

    IEnumerator Esperar()
    {
        Time.timeScale = 1;
        yield return new WaitForSeconds(0.2f);
        Time.timeScale = 0;
        activado2 = true;
    }

    IEnumerator SalidaCartel()
    {
        Time.timeScale = 1;
        cartel.PlayAnimation("salida", 0, 1, 1);
        yield return new WaitForSeconds(0.5f);
        MensajeTutorial.SetActive(false);
    }



}
