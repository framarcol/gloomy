﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VagonetaControlador : MonoBehaviour {

    InteractableObject objetoInteractivo;
    public GameObject explosion;
    public GameObject explosionFinal;
    bool iniciado2;
    public bool vagonetaIniciada;
    public int oro;
    public int danoTorre;
    public GameObject OroUp;
    public GameObject partiOro, partiOro2, partiOro3;
    public GameObject partiCarbon, partiCarbon2, partiCarbon3;
    public bool aleatorio;
    public bool disparable;
    public int idVagoneta;
    int oroInicial;

    //Variables GeneradorV3

    private void OnCollisionEnter2D(Collision2D collision)
    {
       if (collision.gameObject.name == "tower")
        {
            switch (objetoInteractivo.reactorType)
            {
                case InteractableObject.ReactorType.dinamita:
                    GetComponent<BoxCollider2D>().enabled = false;
                    GetComponent<MoverVagoneta>().iniciado = false;
                    if(objetoInteractivo.energy != -1) GetComponent<AnimationsPlaylist>().PlayAnimation("dynamite_explosion", 0, 1, 1);
                    GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePosition;
                    GetComponent<Rigidbody2D>().freezeRotation = true;
                    explosion.SetActive(true);
                    explosionFinal.SetActive(true);
                    GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[5]);
                    StartCoroutine(Desaparecer());
                    collision.gameObject.GetComponent<TowerControl>().ShootReactor(danoTorre);
                //    collision.gameObject.GetComponent<TowerControl>().energy -= danoTorre;
                    break;
                case InteractableObject.ReactorType.oro:
                    GetComponent<BoxCollider2D>().enabled = false;
                    GetComponent<MoverVagoneta>().iniciado = false;
                    GameObject objetovida = Instantiate(OroUp, transform.position, new Quaternion(0, 0, 0, 0), GameObject.FindGameObjectWithTag("InterfazW").transform);
                    objetovida.GetComponent<AnimacionOro>().EfectoOro(oro, transform.position);
                    if (objetoInteractivo.energy != -1) GetComponent<AnimationsPlaylist>().PlayAnimation("empty_caida", 0, 1, 1);
                    partiOro3.SetActive(true);
                    GameObject.FindGameObjectWithTag("GameController").GetComponent<ControladorRecompensas>().SumarOro(oro);
                    GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePosition;
                    GetComponent<Rigidbody2D>().freezeRotation = true;
                    StartCoroutine(Desaparecer());
                    break;
                case InteractableObject.ReactorType.carbon:
                    GetComponent<BoxCollider2D>().enabled = false;
                    GetComponent<MoverVagoneta>().iniciado = false;
                    if (objetoInteractivo.energy != -1) GetComponent<AnimationsPlaylist>().PlayAnimation("empty_caida", 0, 1, 1);
                    partiCarbon3.SetActive(true);
                    GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePosition;
                    GetComponent<Rigidbody2D>().freezeRotation = true;
                    StartCoroutine(Desaparecer());
                    break;
            }
        }
    }

  

    private void Start()
    {
        objetoInteractivo = GetComponent<InteractableObject>();
        oroInicial = oro;
        if (aleatorio)
        {
            int numero = Random.Range(1, 4);

            switch (numero)
            {
                case 1:
                    objetoInteractivo.reactorType = InteractableObject.ReactorType.carbon;
                    break;
                case 2:
                    objetoInteractivo.reactorType = InteractableObject.ReactorType.oro;
                    break;
                case 3:
                    objetoInteractivo.reactorType = InteractableObject.ReactorType.dinamita;
                    break;
            }

        }

        if(vagonetaIniciada)IniciarVagoneta();     

    }


    public void IniciarVagoneta()
    {
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;

        if(objetoInteractivo == null)
        {
            objetoInteractivo = GetComponent<InteractableObject>();
        }
        switch (objetoInteractivo.reactorType)
        {
            case InteractableObject.ReactorType.dinamita:
                GetComponent<AnimationsPlaylist>().PlayAnimation("dynamite", 0, 10, 1);
                break;
            case InteractableObject.ReactorType.oro:
                GetComponent<AnimationsPlaylist>().PlayAnimation("gold_big", 0, 10, 1);
                break;
            case InteractableObject.ReactorType.carbon:
                GetComponent<AnimationsPlaylist>().PlayAnimation("carbon_big", 0, 10, 1);
                break;
        }

        vagonetaIniciada = true;
    }


    private void Update()
    {
        if (vagonetaIniciada)
        {
            switch (objetoInteractivo.reactorType)
            {
                case InteractableObject.ReactorType.dinamita:
                    if (objetoInteractivo.energy == 0)
                    {
                        if (!iniciado2)
                        {
                            iniciado2 = true;
                            objetoInteractivo.energy = -1;
                            GetComponent<MoverVagoneta>().iniciado = false;
                            explosion.SetActive(true);
                            GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[5]);
                            explosionFinal.SetActive(true);
                            StartCoroutine(Desaparecer());
                        }

                    }
                    break;
                case InteractableObject.ReactorType.oro:
                    if (objetoInteractivo.energy == 0)
                    {        
                        if (!iniciado2)
                        {
                            objetoInteractivo.energy = -1;
                            GetComponent<MoverVagoneta>().iniciado = false;
                            iniciado2 = true;
                            GetComponent<AnimationsPlaylist>().PlayAnimation("empty_caida", 0, 1, 1);
                            StartCoroutine(Desaparecer());
                        }
                    }
                    break;
                case InteractableObject.ReactorType.carbon:
                    if (objetoInteractivo.energy == 0)
                    {                   
                        if (!iniciado2)
                        {
                            objetoInteractivo.energy = -1;
                            GetComponent<MoverVagoneta>().iniciado = false;
                            iniciado2 = true;
                            GetComponent<AnimationsPlaylist>().PlayAnimation("empty_caida", 0, 1, 1);
                            StartCoroutine(Desaparecer());
                        }

                    }
                    break;
            }
        }  
       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        for (int i = 1; i < 20; i++)
        {
            if (collision.gameObject.name == "Atrezzo" + i)
            {
                disparable = false;
            }
        }
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        for (int i = 1; i < 20; i++)
        {
            if (collision.gameObject.name == "Atrezzo" + i)
            {
                disparable = true;
            }
        }
    }

    public void SacarOro(int id)
    {
        if(id == 1)
        partiOro.SetActive(true);
        if (id == 2)
        partiOro2.SetActive(true);
    }

    public void SacarCarbon(int id)
    {
        if (id == 1)
            partiCarbon.SetActive(true);
        if (id == 2)
            partiCarbon2.SetActive(true);
    }

    IEnumerator Desaparecer()
    {
        if(objetoInteractivo.reactorType == InteractableObject.ReactorType.dinamita) GetComponent<AnimationsPlaylist>().PlayAnimation("dynamite_explosion", 0, 1, 1);
        yield return new WaitForSeconds(3f);
        if(!GameObject.FindGameObjectWithTag("GeneradorHordas").GetComponent<GeneradorHordas>().V3)
        Destroy(this.gameObject);
        else
        {
            GetComponentInParent<ControladorPosicion>().gameObject.SetActive(false);
            if(objetoInteractivo != null)
            objetoInteractivo.energy = 20;
            else
            {
                objetoInteractivo = GetComponent<InteractableObject>();
                objetoInteractivo.energy = 20;
            }
            oro = oroInicial;
            vagonetaIniciada = false;
            iniciado2 = false;
            partiCarbon.SetActive(false);
            partiCarbon2.SetActive(false);
            partiCarbon3.SetActive(false);
            partiOro.SetActive(false);
            partiOro2.SetActive(false);
            partiOro3.SetActive(false);
            explosion.SetActive(false);
            explosionFinal.SetActive(false);
            GetComponent<BoxCollider2D>().enabled = true;
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            transform.parent = GameObject.FindGameObjectWithTag("Enemigos").transform;
            transform.position = new Vector2(18, 6);
        }
    }

}
