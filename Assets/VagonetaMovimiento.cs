﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VagonetaMovimiento : MonoBehaviour {

    public bool moverIzquierda;
    public bool moverDerecha;
    public float Velocidad;


    bool iniciado;

    public void IniciarVagoneta()
    {
        iniciado = true;
    }

    void Update()
    {
        if (iniciado)
        {
            if (moverIzquierda)
            {
                transform.Translate(Vector3.left * Time.deltaTime * (Velocidad / 2));
            }

            if (moverDerecha)
            {
                transform.Translate(Vector3.right * Time.deltaTime * (Velocidad / 2));
            }
        }
    }
}
