﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictimasController : MonoBehaviour {

    //public GameObject[] victimas;

   // public float posicionDestinoObjeto1/*,posicionMinina*/;
    public float tiempoObjetoPequeno;
    public float tiempoObjetoGrandeCentro;
    public float tiempoObjetoGrandeSalida;
    public float EsperaCentro;

    float timer;

    Vector3 origen;

    public int idObjeto;

    public int victimasSalvadas;

    public RectTransform objetoMover, objetoMover2;

    public float posicioLlegadaX /*posicionMinina2*/;
    public float /*posicionMinima2Y*/ posicioLlegadaY;

    float timer2;

    Vector3 origen2, origen4, origen5;

    public float[] telefericoX, telefericoY;

   // public float posicionObjeto2SalidaX, /*posicionObjeto2SalidaY*/ /*posicionMinina3*/;

    float timer3, timer4, timer5;

    Vector3 origen3;

    bool activarMovimiento, activarMovimiento2, activarMovimiento3, activarMovimiento4, activarMovimiento5;

    bool activarUnaVez, activarUnaVez2, activarUnaVez4, activarUnaVez5;
    GameObject infoJuego;

   // AnimationsPlaylist animacionObjetoGrande;

    private void Start()
    {
      //  Debug.Log(Time.time);
        Empezar(true);
    }

    void Empezar(bool comienzo)
    {
        activarUnaVez = false;
        activarUnaVez2 = false;
        activarUnaVez4 = false;
        activarUnaVez5 = false;
        timer = 0;
        timer2 = 0;
        timer3 = 0;
        timer4 = 0;
        timer5 = 0;
        if (!comienzo)
        {
            objetoMover.position = origen;
            objetoMover2.position = origen2;
        }
        infoJuego = GameObject.FindGameObjectWithTag("GameController");
     
        origen = objetoMover.position;

        origen2 = objetoMover2.position;
        origen3 = new Vector3(posicioLlegadaX, posicioLlegadaY, objetoMover2.position.z);
        if(telefericoX != null)
        {
            if(telefericoX.Length > 0)
            {
                origen4 = new Vector2(telefericoX[0], telefericoY[0]);
                origen5 = new Vector2(telefericoX[1], telefericoY[1]);
            }
        }
        activarMovimiento = true;
    }

    IEnumerator EsperarAterrizaje()
    {
        yield return new WaitForSeconds(tiempoObjetoGrandeCentro-1.5f);
        if (idObjeto == 1) objetoMover2.GetComponentInChildren<AnimationsPlaylist>().PlayAnimation("aterrizar", 0, 1, 1);
        yield return new WaitForSeconds(1.1f);
        if (idObjeto == 1) objetoMover2.GetComponentInChildren<AnimationsPlaylist>().PlayAnimation("reposo", 0, 20, 2);

    }

    private void Update()
    {
        if(idObjeto == 1)
        {
            //if(!objetoMover2.GetComponentInChildren<AnimationsPlaylist>().IsPlayingAnimation())
            //objetoMover2.GetComponentInChildren<AnimationsPlaylist>().PlayAnimation("reposo", 0, 20, 1);
        }

        if (activarMovimiento)
        {
            Subir();
        }

        if (activarMovimiento2)
        {
            Subir2();
        }

        if (activarMovimiento3)
        {
            Subir3();
        }

        if (activarMovimiento4)
        {
            Subir4();
        }

        if (activarMovimiento5)
        {
            Subir5();
        }

        for (int i = 0; i < GetComponentsInChildren<BoxCollider2D>().Length; i++)
        {
            if(i !=0)
            {
                GetComponentsInChildren<BoxCollider2D>()[i].enabled = false;
            }
            else
            {
                GetComponentsInChildren<BoxCollider2D>()[i].enabled = true;
            }
        }

        if(GetComponentsInChildren<Superviviente>().Length == 0)
        {
            if(victimasSalvadas == 0)
            infoJuego.GetComponent<MenusEmergentes>().MostrarPausa();
            else infoJuego.GetComponent<MenusEmergentes>().MostrarPausa2();
        }

    }

    void Subir4()
    {
        Vector3 destino;

        destino = new Vector3(telefericoX[1], telefericoY[1], objetoMover.position.z);

        Vector3 distancia = destino - origen4;

        timer4 += Time.deltaTime;

        float porcentaje = timer4 / tiempoObjetoPequeno;

    //    porcentaje = Mathf.Sin(porcentaje * Mathf.PI * 0.5f);

        if (timer4 < tiempoObjetoPequeno)
        {
            activarUnaVez5 = false;
            objetoMover.position = distancia * porcentaje + origen4;
        }
        else
        {
            activarMovimiento4 = false;
            if (!activarUnaVez4)
            {
                activarUnaVez4 = true;
                activarMovimiento5 = true;
            }

        }
    }

    void Subir5()
    {
        Vector3 destino;

        if (idObjeto != 3)
            destino = new Vector3((origen.x) * -1, objetoMover.position.y, objetoMover.position.z);
        else
        {
            destino = new Vector3(telefericoX[2], telefericoY[2], objetoMover.position.z);
        }

        Vector3 distancia = destino - origen5;

        timer5 += Time.deltaTime;

        float porcentaje = timer5 / tiempoObjetoPequeno;

      //  porcentaje = Mathf.Sin(porcentaje * Mathf.PI * 0.5f);

        if (timer5 < tiempoObjetoPequeno)
        {
            activarUnaVez5 = false;
            objetoMover.position = distancia * porcentaje + origen5;
        }
        else
        {
            activarMovimiento5 = false;
            if (!activarUnaVez5)
            {
                activarUnaVez5 = true;
                activarMovimiento2 = true;
            }

        }
    }

    void Subir()
    {
        Vector3 destino;

        if(idObjeto !=3)
        destino = new Vector3((origen.x)*-1, objetoMover.position.y, objetoMover.position.z);
        else
        {
            destino = new Vector3(telefericoX[0], telefericoY[0], objetoMover.position.z);
        }

        Vector3 distancia = destino - origen;

        timer += Time.deltaTime;

        float porcentaje = timer / tiempoObjetoPequeno;

        if(idObjeto!=3)
        porcentaje = Mathf.Sin(porcentaje * Mathf.PI * 0.5f);

        if (timer < tiempoObjetoPequeno) {
            activarUnaVez = false;
            objetoMover.position = distancia * porcentaje + origen;
        } 
        else
        {
            activarMovimiento = false;
            if (!activarUnaVez)
            {
                activarUnaVez = true;
                if(idObjeto != 3)
                activarMovimiento2 = true;
                else
                {
                    activarMovimiento4 = true;
                }
                if (idObjeto == 1)
                {
                    objetoMover2.GetComponentInChildren<AnimationsPlaylist>().PlayAnimation("reposo_atras", 0, 80, 8);
                    StartCoroutine(EsperarAterrizaje());
                }
   
            }
     
        }
        
    }

    void Subir2()
    {
        Vector3 destino2 = new Vector3(posicioLlegadaX, posicioLlegadaY, objetoMover2.position.z);  //posicionObjetivo;

        Vector3 distancia2 = destino2 - origen2;

        timer2 += Time.deltaTime;

        float porcentaje2 = timer2 / tiempoObjetoGrandeCentro;

        porcentaje2 = Mathf.Sin(porcentaje2 * Mathf.PI * 0.5f);

        if (timer2 < tiempoObjetoGrandeCentro) {

            objetoMover2.position = distancia2 * porcentaje2 + origen2;

        } 
        else
        {
            activarMovimiento2 = false;
            if (!activarUnaVez2)
            {
                activarUnaVez2 = true;
                GetComponentsInChildren<Superviviente>()[GetComponentsInChildren<Superviviente>().Length-1].gameObject.SetActive(false);    
                victimasSalvadas++;

                StartCoroutine(EsperaCarga());
            }

        }

    }

    IEnumerator EsperaCarga()
    {
        if (idObjeto == 2) objetoMover2.GetComponentInChildren<AnimationsPlaylist>().StopAnimation();

        yield return new WaitForSeconds(EsperaCentro);

        if (idObjeto == 1) objetoMover2.GetComponentInChildren<AnimationsPlaylist>().PlayAnimation("despegar", 0, 1, 1);
        if (idObjeto == 2) objetoMover2.GetComponentInChildren<AnimationsPlaylist>().PlayAnimation("tren",0,50,1);

        activarMovimiento3 = true;
        if (idObjeto == 1) yield return new WaitForSeconds(1.1f);
        if (idObjeto == 1) objetoMover2.GetComponentInChildren<AnimationsPlaylist>().PlayAnimation("reposo_alante", 0, 50, 8);
    }

    void Subir3()
    {
        Vector3 destino3;

        if (idObjeto != 3)
        destino3 = new Vector3((origen2.x)*-1, origen2.y, objetoMover2.position.z);  //posicionObjetivo;
        else destino3 = new Vector3((origen2.x) * -1, posicioLlegadaY, objetoMover2.position.z);

        Vector3 distancia3 = destino3 - origen3;

        timer3 += Time.deltaTime;

        float porcentaje3 = timer3 / tiempoObjetoGrandeSalida;

        porcentaje3 = Mathf.Sin(porcentaje3 * Mathf.PI * 0.5f);

        if (timer3 < tiempoObjetoGrandeSalida)
        {
            objetoMover2.position = distancia3 * porcentaje3 + origen3;
        }
        else
        {
            if (GetComponentsInChildren<BoxCollider2D>().Length != 0)
            {
                Empezar(false);
            }
             
            activarMovimiento3 = false;
        }

    }


}
