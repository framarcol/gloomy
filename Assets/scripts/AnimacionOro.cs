﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimacionOro : MonoBehaviour {

    float Velocidad;
    bool activarSubida;

    public float objetivo;
    public bool esGranada;

    float timer;
    Vector3 posicionObjetivo;
    Vector3 origen;

    public void EfectoOro(int cantidad, Vector2 comienzo)
    {
        GetComponentInChildren<Text>().text = "+" + cantidad.ToString();
        GetComponentInChildren<Text>().color = Color.yellow;
       // Debug.Log(transform.position.x);
        origen = new Vector2(comienzo.x, comienzo.y + 1);
        posicionObjetivo = new Vector3(comienzo.x, comienzo.y + objetivo + 1, 0);
        Velocidad = 0.5f;
        activarSubida = true;
        if(!esGranada)
        GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[22]);
        else
        {
            GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[34]);
        }
    }

    private void Update()
    {
        if (activarSubida) Subir();

        if (activarSubida)
        if (GetComponent<Transform>().localRotation != new Quaternion(0, 0, 0, 0)) GetComponent<Transform>().localRotation = GetComponent<Transform>().localRotation;
     
    }

    void Subir()
    {
        Vector3 destino = posicionObjetivo;

        Vector3 distancia = destino - origen;

        timer += Time.deltaTime;

        float porcentaje = timer / Velocidad;

        porcentaje = Mathf.Sin(porcentaje * Mathf.PI * 0.5f);

        if (timer < Velocidad) transform.localPosition = distancia * porcentaje + origen;
        else
        {
            StartCoroutine(Destruir());
            activarSubida = false;
        }
      
    }

    IEnumerator Destruir()
    {
        yield return new WaitForSeconds(1f);
        Destroy(this.gameObject);
    }

}
