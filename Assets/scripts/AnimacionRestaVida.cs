﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimacionRestaVida : MonoBehaviour {

    float Velocidad;
    bool activarSubida;

    public float objetivo;

    float timer;
    Vector3 posicionObjetivo;
    Vector3 origen;

    public void EfectoVida(int cantidad)
    {
        GetComponentInChildren<Text>().text = cantidad.ToString();
     //   GetComponentInChildren<Text>().color = Color.red;
        origen = transform.position;

        posicionObjetivo = new Vector3(transform.position.x + Random.Range(-0.3f, 0.3f), transform.position.y + objetivo + Random.Range(0, 0.6f), 0);
        Velocidad = 1;
        activarSubida = true;
    }

    private void Update()
    {
        if (activarSubida) Subir();

        if (activarSubida)
        if (GetComponent<Transform>().localRotation != new Quaternion(0, 0, 0, 0)) GetComponent<Transform>().localRotation = GetComponent<Transform>().localRotation;

    }

    void Subir()
    {
        Vector3 destino = posicionObjetivo;

        Vector3 distancia = destino - origen;

        timer += Time.deltaTime;

        float porcentaje = timer / Velocidad;

        porcentaje = Mathf.Sin(porcentaje * Mathf.PI * 0.5f);

        if (timer < Velocidad)
        {
            transform.localPosition = distancia * porcentaje + origen;
        }
        else
        {
            Destroy(this.gameObject);
            activarSubida = false;
        }
    }
}
