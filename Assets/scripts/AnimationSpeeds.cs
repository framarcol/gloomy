﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSpeeds : MonoBehaviour {

    public float andar;
    public float correr;
    public float reposo;
    public float atacar;
    public float escalar;
    public float comer;
    public float reposo_andar;
    public float andar_reposo;
    public float reposo_escalar;
    public float escalar_reposo;
    public float reposo_correr;
    public float correr_reposo;
    public float recibir_impacto;
    public float recibir_impacto_escalar;
    public float reposo_atacar;
}
