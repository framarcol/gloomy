﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DragonBones;
using System;

public class AnimationsPlaylist : MonoBehaviour {
    
    public class AnimationNode
    {
        public string animation;
        public int times;
        public float speed;
        public float fadeInTime;
               

        public AnimationNode (string anAnimation, float anFadeInTime, int anTimes, float anSpeed)
        {
            animation = anAnimation;
            fadeInTime = anFadeInTime;
            times = anTimes;
            speed = anSpeed;
        }
    }


    public UnityArmatureComponent armatureClass;
    DragonBones.Animation animationClass;
    DragonBones.AnimationState animationState;
    [Space(10)]
    public string currentAnimation;
    List<AnimationNode> animationPlayList = new List<AnimationNode>();

    //EVENTS********************************************************
    public enum DragonBonesEventType { frameEvent, loopComplete};
    public delegate void EndPlaylistDelegate();
    public event EndPlaylistDelegate EndPlaylist;
    public delegate void DragonBonesEventDelegate(DragonBonesEventType type, EventObject eventObject);
    public event DragonBonesEventDelegate DragonBonesEvent;

    // Use this for initialization
    void Start () {

		if (armatureClass != null)
        {
            animationClass = armatureClass.armature.animation;
           // if (animationClass != null) Debug.Log("G");
            armatureClass.armature.eventDispatcher.AddEventListener(EventObject.FRAME_EVENT, DragonBonesEventHandler);
            armatureClass.armature.eventDispatcher.AddEventListener(EventObject.LOOP_COMPLETE, DragonBonesEventHandler);
        }
    }

    // Update is called once per frame
    void Update () {
        if ((animationPlayList != null) && (animationPlayList.Count > 0))
        {
		    if (!animationClass.isPlaying)
            {
                animationPlayList.RemoveAt(0);
                if (animationPlayList.Count > 0)
                {
                    _playAnimation(animationPlayList[0].animation, animationPlayList[0].fadeInTime,
                                    animationPlayList[0].times, animationPlayList[0].speed);
                } else
                {
                    enabled = false;
                    if (EndPlaylist != null)
                    {
                        EndPlaylist();
                    }
                }
            }
        }
        else
        {
            enabled = false;
        }
    }

    private void _playAnimation (string paAnimation, float paFadeInTime, int paTimes, float paSpeed)
    //Reproduce un elemento y activa el script para que update espere al final de la reproducción, borre el primer elemento
    //e invoque esta función de uevo si queda algún elemento en la lista
    {
     //   Debug.Log(paAnimation);
        if (paFadeInTime > 0.0f)
        {
            if(animationClass == null)
            {
                animationClass = armatureClass.armature.animation;
            }
           animationState = animationClass.FadeIn(paAnimation, paFadeInTime, paTimes, 0, null, AnimationFadeOutMode.SameLayerAndGroup, true);
        } else
        {
            animationClass = armatureClass.armature.animation;
            animationState = animationClass.Play(paAnimation, paTimes); 
        }

        animationState.timeScale = paSpeed;
        currentAnimation = paAnimation;
        enabled = true;
    }

    //****************************************************************************************************************************
    //  Public methods
    //****************************************************************************************************************************
    public void PlayAnimation (string paAnimation, float paFadeInTime, int paTimes, float paSpeed)
    //Todos los elementos deben añadirse a la lista, aunque sólo sea un único elemento
    {
        AnimationNode paNewNode = new AnimationNode(paAnimation, paFadeInTime, paTimes, paSpeed);
        animationPlayList.Clear();
        animationPlayList.Add(paNewNode);
        _playAnimation(paAnimation, paFadeInTime, paTimes, paSpeed);
    }

    public void PlayAnimationList (List<AnimationNode> lista)
    {
        if (animationPlayList != null)
        {
            animationPlayList.Clear();
        }
        animationPlayList = lista;
        _playAnimation(animationPlayList[0].animation, animationPlayList[0].fadeInTime, animationPlayList[0].times, animationPlayList[0].speed);
    }

    public void PlayNextAnimation ()
    {
        animationClass.Stop();
    }

    public void StopAnimation()
    {
        animationClass.Stop();
    }

    private void DragonBonesEventHandler(string type, EventObject eventObject)
    {
         switch (type)
        {
            case "frameEvent":
                if (DragonBonesEvent != null)
                {
                    DragonBonesEvent(DragonBonesEventType.frameEvent, eventObject);
                }
                break;
            case "loopComplete":
                if (DragonBonesEvent != null)
                {
                    DragonBonesEvent(DragonBonesEventType.loopComplete, eventObject);
                }
                break;
        }
        //throw new NotImplementedException();
    }

    public bool IsPlayingAnimation ()
    {
        return animationClass.isPlaying;
    }
}
