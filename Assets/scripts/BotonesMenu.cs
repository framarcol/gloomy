﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BotonesMenu : MonoBehaviour {

    public GameObject[] pantallas;
    int[] NumAnterior;
    int contador;
    public static int mundoSeleccionado;
    public static int nivelMundoSeleccionado;
    public static int idDEstino;
    public Sprite[] estrellas;

    public GameObject[] Fases;
  //  public GameObject btnFlechaDer, btnFlechaIzq;
    int indiceFases;

    public AudioClip western;
    public AudioClip slaidin;

    public GameObject botonInicial;

    public Sprite[] numeros;

    int idPantallaFinal;

    public GameObject popapConfirmacionNivel;
    public GameObject cartelInfoPiezas;
    public GameObject cartelInfoDesafios;
    public GameObject cartelCreditos;
    public GameObject cartelInfoWorld;
    public GameObject cartelInfoCoins;

    /**
     * 1: PantallaOpciones
     * 2: PantallaPrincipal
     * 3: PantallaSelectorMundos
     * 4: PantallaSelectorFases
     * 5: PantallaTienda   
     * 6: PantallaSelectorProcedural
     * 7: PantallaCompra
     **/

    private void OnApplicationFocus(bool focus)
    {
        if (focus)
        {
            Time.timeScale = 1f;
        }
        else
        {
            Time.timeScale = 0f;
        }
    }

    public void AbrirCartelCreditos()
    {
        cartelCreditos.SetActive(true);
    }

    public void AbrirCartelInfoWorld()
    {
        cartelInfoWorld.SetActive(true);
    }

    public void AbrirCartelInfoCoins()
    {
        cartelInfoCoins.SetActive(true);
    }

    public void AbrirCartelInfoDesafios()
    {
        cartelInfoDesafios.SetActive(true);
    }

    public void AbriCartelInfoPiezas()
    {
        cartelInfoPiezas.SetActive(true);
    }

    private void Start()
    {
        StartCoroutine(EsperarPlayPrincipal());
        indiceFases = 0;
        NumAnterior = new int[30];
        contador = 0;
        NumAnterior[0] = 2;

        if (idDEstino != 0)
        {
            if (idDEstino != 5)
                CambiarPantalla(idDEstino);
            else
            {
                CambiarPantalla(3);
                CambiarPantalla(5);
            }

            if (idDEstino == 3)
            {
                if(mundoSeleccionado != 3)
                {
                    CambiarPantallaNormal(4);
                    CargarFases(mundoSeleccionado);

                }
                else
                {
                    CambiarPantallaNormal(6);
                }

            }

            idDEstino = 0;
        }

    }

    IEnumerator EsperarPlayPrincipal()
    {
        botonInicial.SetActive(false);
        yield return new WaitForSeconds(1.8f);
        botonInicial.SetActive(true);
    }

    public void CambiarPantallaFade(int idDestinoFinal)
    {
        int origen = NumAnterior[contador]-1;
        contador++;
        NumAnterior[contador] = idDestinoFinal;
     //   Debug.Log(origen);

        pantallas[origen].GetComponent<EfectoFade>().Fade(false, 1.25f);
        pantallas[idDestinoFinal - 1].SetActive(true);
        
        pantallas[idDestinoFinal - 1].GetComponent<EfectoFade>().Fade(true, 1.25f);
    }

    public void CambiarPantalla(int idPantalla)
    {
        if (idPantalla == 6)
        {
            indiceFases = 0;
            mundoSeleccionado = 3;
        }

        GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[21]);
        contador++;

        if (idPantalla != 4)
        {
            for (int i = 0; i < pantallas.Length; i++)
            {
                pantallas[i].gameObject.SetActive(false);
            }
            pantallas[idPantalla - 1].gameObject.SetActive(true);
        }
        else
        {
            StartCoroutine(SaltoPantalla(idPantalla));
        }

        NumAnterior[contador] = idPantalla;
    }

    public void CambiarPantallaNormal(int idPantalla)
    {
        if (idPantalla == 6)
        {
            indiceFases = 0;
            mundoSeleccionado = 3;
        }
    
        GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[21]);
        contador++;

        for (int i = 0; i < pantallas.Length; i++)
        {
            pantallas[i].gameObject.SetActive(false);
        }
        pantallas[idPantalla - 1].gameObject.SetActive(true);

        NumAnterior[contador] = idPantalla;
    }

    IEnumerator SaltoPantalla(int idPantalla)
    {
        yield return new WaitForSeconds(1.3f);

        for (int i = 0; i < pantallas.Length; i++)
        {
            pantallas[i].gameObject.SetActive(false);
        }

        pantallas[idPantalla - 1].gameObject.SetActive(true);
    }

    private void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
            BotonVolver();
    }

    public void BotonVolver()
    {
        if(contador > 0)
        {
            contador--;

            for (int i = 0; i < pantallas.Length; i++)
            {
                if (pantallas[i].GetComponent<CanvasGroup>() != null) pantallas[i].GetComponent<CanvasGroup>().alpha = 1;
                pantallas[i].gameObject.SetActive(false);
            }

            pantallas[NumAnterior[contador] - 1].gameObject.SetActive(true);
        }
        else
        {
            Application.Quit();
        }
       
    }

    public void CargarFases(int idMundo)
    {
        // Debug.Log("mundo cargado:" + idMundo);

        switch (idMundo)
        {
            case 1:
                GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[7]);
                break;
            default:
                GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[7]);
                break;
              
        }

        mundoSeleccionado = idMundo;

        indiceFases = 0;

        Cargador(0);
    }

    public void PaginaSiguiente()
    {
        indiceFases += 10;

        Cargador(indiceFases);
    }

    public void Cargador(int indice)
    {
        for (int i = 0; i < Fases.Length; i++) Fases[i].SetActive(false);

        for (int i = indiceFases, j = 0; i < IniciarJuego.shopData.GetCountWorld(mundoSeleccionado) && i < indiceFases+40; i++, j++)
        {
            Fases[j].SetActive(true);

            if (i + 1 < 10)
            {
                Fases[j].GetComponentsInChildren<Image>()[6].sprite = numeros[0];
                Fases[j].GetComponentsInChildren<Image>()[5].sprite = numeros[i+1];
            }
            else if (i + 1 < 20)
            {
                Fases[j].GetComponentsInChildren<Image>()[6].sprite = numeros[1];
                Fases[j].GetComponentsInChildren<Image>()[5].sprite = numeros[i + 1 - 10];
            }
            else if (i + 1 < 30)
            {
                Fases[j].GetComponentsInChildren<Image>()[6].sprite = numeros[2];
                Fases[j].GetComponentsInChildren<Image>()[5].sprite = numeros[i + 1 - 20];
            }
            else
            {
               Fases[j].GetComponentsInChildren<Image>()[6].sprite = numeros[3];
               Fases[j].GetComponentsInChildren<Image>()[5].sprite = numeros[i + 1 - 30];
            }

            switch (BotonesMenu.mundoSeleccionado)
            {
                case 1:
                    Fases[j].GetComponentsInChildren<Image>()[6].color = new Color(1, 0.85f, 0, 1);
                    Fases[j].GetComponentsInChildren<Image>()[5].color = new Color(1, 0.85f, 0, 1);
                    break;
                case 2:
                    Fases[j].GetComponentsInChildren<Image>()[6].color = new Color(1, 1, 1, 1);
                    Fases[j].GetComponentsInChildren<Image>()[5].color = new Color(1, 1, 1, 1);
                    break;
                case 3:
                    Fases[j].GetComponentsInChildren<Image>()[6].color = new Color(1, 1, 1, 1);
                    Fases[j].GetComponentsInChildren<Image>()[5].color = new Color(1, 1, 1, 1);
                    break;
            }

            if(PlayerPrefs.GetInt((SaveGame.estrelllas[mundoSeleccionado-1,j]))>0) PlayerPrefs.SetInt(SaveGame.nivelesDesbloquedos[mundoSeleccionado-1],j+1);

            if (i > PlayerPrefs.GetInt(SaveGame.nivelesDesbloquedos[mundoSeleccionado - 1]) & i != 0)
            {
                switch (BotonesMenu.mundoSeleccionado) {

                    case 1:
                        Fases[j].GetComponentsInChildren<Image>()[4].sprite = estrellas[2];
                        break;
                    case 2:
                        Fases[j].GetComponentsInChildren<Image>()[4].sprite = estrellas[3];
                        break;
                    case 3:
                        Fases[j].GetComponentsInChildren<Image>()[4].sprite = estrellas[4];
                        break;


                }
             
                Fases[j].GetComponentsInChildren<Image>()[1].sprite = estrellas[0];
                Fases[j].GetComponentsInChildren<Image>()[2].sprite = estrellas[0];
                Fases[j].GetComponentsInChildren<Image>()[3].sprite = estrellas[0];
                Fases[j].GetComponent<Button>().interactable = false;

            }
            else
            {
                Fases[j].GetComponent<Button>().interactable = true;
                int numEstrellas = PlayerPrefs.GetInt(SaveGame.estrelllas[mundoSeleccionado - 1, i]);
                Fases[j].GetComponentsInChildren<Image>()[4].sprite = estrellas[0];
                if (numEstrellas > 0)
                Fases[j].GetComponentsInChildren<Image>()[1].sprite = estrellas[1];
                else Fases[j].GetComponentsInChildren<Image>()[1].sprite = estrellas[0];
                if (numEstrellas > 1)
                    Fases[j].GetComponentsInChildren<Image>()[2].sprite = estrellas[1];
                else Fases[j].GetComponentsInChildren<Image>()[2].sprite = estrellas[0];
                if (numEstrellas > 2)
                    Fases[j].GetComponentsInChildren<Image>()[3].sprite = estrellas[1];
                else Fases[j].GetComponentsInChildren<Image>()[3].sprite = estrellas[0];

            }
        }

#if UNITY_EDITOR
        for (int i = 0; i < Fases.Length; i++) Fases[i].GetComponent<Button>().interactable = true;
#endif

        //   if (indiceFases+40 <= IniciarJuego.shopData.GetCountWorld(mundoSeleccionado)) btnFlechaDer.SetActive(true);
        //   else btnFlechaDer.SetActive(false);

        // if (indiceFases == 0) btnFlechaIzq.SetActive(false);
        // else btnFlechaIzq.SetActive(true);
    }

    public void PaguinaAnterior()
    {
        indiceFases -= 10;

        Cargador(indiceFases);
    }

    public void IniciarNivel(int fase)
    {
        nivelMundoSeleccionado = fase + indiceFases;

        ShopData.UpgradeItem nivel = IniciarJuego.shopData.GetMundo(nivelMundoSeleccionado, mundoSeleccionado);

        loadSC.nextScene = nivel.escena;

        popapConfirmacionNivel.SetActive(true);
        if (PlayerPrefs.GetInt(SaveGame.energia) <= 0) popapConfirmacionNivel.GetComponentInChildren<Button>().interactable = false;
        else popapConfirmacionNivel.GetComponentInChildren<Button>().interactable = true;
    }

    public void DesactivarPanel()
    {
        popapConfirmacionNivel.SetActive(false);
    }

    public void IniciarNivel2()
    {
        //Resta
        PlayerPrefs.SetInt(SaveGame.energia, PlayerPrefs.GetInt(SaveGame.energia) - 1);
        if (PlayerPrefs.GetInt(SaveGame.energia) < 3)
        {
            System.DateTime hora = System.DateTime.Now;
            System.DateTime hora2 = System.DateTime.Now.AddMinutes(30);

            if (PlayerPrefs.HasKey(SaveGame.horaObjectivo))
            {
                System.DateTime hora3 = System.DateTime.Parse(PlayerPrefs.GetString(SaveGame.horaObjectivo));

                if (System.DateTime.Compare(hora, hora3) == 1)
                {
                    PlayerPrefs.SetString(SaveGame.ultimaHora, hora.ToString());
                    PlayerPrefs.SetString(SaveGame.horaObjectivo, hora2.ToString());
                }
            }
            else
            {
                PlayerPrefs.SetString(SaveGame.ultimaHora, hora.ToString());
                PlayerPrefs.SetString(SaveGame.horaObjectivo, hora2.ToString());
            }
        }
        LoadSCSImple.esInicio = true;
        LoadSCSImple.esMenu = false;

        SceneManager.LoadScene("LoadScreen");
    }

}
