﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CargaIdioma : MonoBehaviour {

    public int columna, fila;

    void OnEnable () {

        GetComponent<Text>().text = SeleccionaIdiomas.gameText[columna, fila];

    }
	
}
