﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CargarFasesHabilitadas : MonoBehaviour {

    private void OnEnable()
    {
#if UNITY_EDITOR
        //PlayerPrefs.SetInt(SaveGame.nivelesDesbloquedos[BotonesMenu.mundoSeleccionado], test);
#endif
        if(BotonesMenu.nivelMundoSeleccionado == 0)
        {
            transform.localPosition = new Vector2(PlayerPrefs.GetInt(SaveGame.nivelesDesbloquedos[BotonesMenu.mundoSeleccionado-1]) * -519, transform.position.y);
        }
        else
        {
            transform.localPosition = new Vector2(BotonesMenu.nivelMundoSeleccionado * -519, transform.position.y);
            BotonesMenu.nivelMundoSeleccionado = 0;

        }
    }

    }