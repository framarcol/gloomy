﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CargarMundosHabilitados : MonoBehaviour {


    public Text[] estrellasConseguidas, estrellasTotal;
    public Text txtEnergia;
    public GameObject Bloqueado, desbloqueado;

    public Button botonMundo2, botonMundo3;

    private void OnEnable()
    {
#if UNITY_EDITOR
    //    PlayerPrefs.SetInt(SaveGame.nivelesDesbloquedos[0], 0);
    //    PlayerPrefs.SetInt(SaveGame.energia, 5);
#endif
        if (PlayerPrefs.GetInt(SaveGame.nivelesDesbloquedos[0]) == IniciarJuego.shopData.GetNumNivelesMundo(0)) botonMundo2.interactable = true;
        else botonMundo2.interactable = false;

        bool entrado = false;
        int nivel=0;
        for (int i = 1; i < IniciarJuego.shopData.GetNumNivelesMundo(0); i++)
        {
            if (IniciarJuego.shopData.GetMundo(i, 1).ProceduralDesbloqueable)
            {
                if (PlayerPrefs.GetInt(SaveGame.nivelesDesbloquedos[0]) >= i)
                {
                    nivel = i;
                    entrado = true;
                    break;
                }
            }
        }

#if UNITY_EDITOR
        entrado = false;
#endif

        if (entrado)
        {
           botonMundo3.interactable = true;
            Bloqueado.SetActive(false);
            desbloqueado.SetActive(true);
        }
        else
        {
            botonMundo3.interactable = false;
            Bloqueado.SetActive(true);
            desbloqueado.SetActive(false);
            txtEnergia.text = SeleccionaIdiomas.gameText[0,19]+ ": 1 \n " + SeleccionaIdiomas.gameText[0,18] + ":" + 15;
        }
  

        int[] estrellas = new int[3];

        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < PlayerPrefs.GetInt(SaveGame.nivelesDesbloquedos[i]); j++)
            {
                estrellas[i] += PlayerPrefs.GetInt(SaveGame.estrelllas[i, j]);
            }

            estrellasConseguidas[i].text = estrellas[i].ToString();
            estrellasTotal[i].text = (IniciarJuego.shopData.GetCountWorld(i) * 3).ToString();
        }
    }
}
