﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CargarTrozoImagen : MonoBehaviour {

    public Sprite[] spritesTrozacos;

	// Use this for initialization
	void Awake () {

        GetComponent<Image>().sprite = spritesTrozacos[Random.Range(0, spritesTrozacos.Length - 1)];
        StartCoroutine(PararImagen());
		
	}

    IEnumerator PararImagen()
    {
        yield return new WaitForSeconds(2.5f);
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePosition;
        GetComponent<Rigidbody2D>().gravityScale *= 2;
    }

}
