﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlIndiVidualArma : MonoBehaviour {

    Image temporizador;
    Image selector;
    public int idArma;

	void Start () {
        temporizador = GetComponentsInChildren<Image>()[0];
        selector = GetComponentsInChildren<Image>()[1];

        temporizador.color = new Color(0, 0, 0, 0);
        selector.color = new Color(0, 0, 0, 0);

	}

    public void RecargarArmaAntes2()
    {
        GameObject miGameOb = GameObject.FindGameObjectWithTag("ArmaTag");

        miGameOb.GetComponent<MiarmaControla>().RecargarArmaAntes(idArma);
    }

}
