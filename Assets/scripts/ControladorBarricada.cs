﻿using DragonBones;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorBarricada : MonoBehaviour {


    InteractableObject vidaBarricada;
    public Text vida;
    public float Velocidad;
    bool activarSubida;

    float timer;
    Vector3  posicionObjetivo;
    Vector3 origen;
    int posicionBarricada;
    bool[] ejecutado;
    private AnimationsPlaylist animationPlayList;
    float energiaInicial;
    public float altura;

    void Awake () {
        ejecutado = new bool[4];
        vidaBarricada = GetComponent<InteractableObject>();
        vida.text = vidaBarricada.energy.ToString();
        energiaInicial = vidaBarricada.energy;

        animationPlayList = transform.GetComponent<AnimationsPlaylist>();
        animationPlayList.DragonBonesEvent += DragonBonesEventListener;

    }

    private void DragonBonesEventListener(AnimationsPlaylist.DragonBonesEventType type, EventObject eventObject)
    {
      //  Debug.Log("Escuchando");
    }


    public void Emerger(int posicion)
    {
        posicionBarricada = posicion;
        posicionObjetivo = new Vector3(GetComponent<RectTransform>().position.x, altura, 0);
        origen = new Vector3(GetComponent<RectTransform>().position.x, -4, 0);
        if (IniciarJuego.shopData.GetArma(12) != null)
            Velocidad = IniciarJuego.shopData.GetArma(12).TiempoRecarga[PlayerPrefs.GetInt(SaveGame.nivelesArmas[12])];
        else Velocidad = 2;
        if (Velocidad == 0) Velocidad = 2;
        vidaBarricada.energy = IniciarJuego.shopData.GetArma(12).Capacidad[PlayerPrefs.GetInt(SaveGame.nivelesArmas[12])];
        if (vidaBarricada.energy == 0) vidaBarricada.energy = 40;
        activarSubida = true;

    }

    public int RecibirDano(int dano)
    {
       // Debug.Log(dano);
        vidaBarricada.energy -= dano;

        return vidaBarricada.energy;
    }

    void Subir()
    {
        Vector3 destino = posicionObjetivo;

        Vector3 distancia = destino - origen;

        timer += Time.deltaTime;

        float porcentaje = timer / Velocidad;

        porcentaje = Mathf.Sin(porcentaje * Mathf.PI * 0.5f);

        if (timer < Velocidad) transform.localPosition = distancia * porcentaje + origen;
        else activarSubida = false;
    }


	
	// Update is called once per frame
	void Update () {

        if (activarSubida)
        {
            GetComponent<BoxCollider2D>().enabled = false;
            vida.text = "";
            Subir();
        }
        else
        {
            GetComponent<BoxCollider2D>().enabled = true;
            vida.text = vidaBarricada.energy.ToString();
        }
 
        if (vidaBarricada.energy <= 0)
        {
            Debug.Log("kj");
            GetComponent<BoxCollider2D>().enabled = false;
            MiarmaControla.posicionado[posicionBarricada] = false;
            MiarmaControla.Subposicion[posicionBarricada * 2] = false;
            MiarmaControla.Subposicion[(posicionBarricada * 2) + 1] = false;
            vida.text = "";
            if (!ejecutado[2])
            {
                if(!ejecutado[0])
                animationPlayList.PlayAnimation("barricada0-3", 0, 1, 1);
                else if(!ejecutado[1]) animationPlayList.PlayAnimation("barricada1-3", 0, 1, 1);
                else animationPlayList.PlayAnimation("barricada2-3", 0, 1, 1);
                ejecutado[2] = true;
            }         
            StartCoroutine(Destruir());
         
        }

        if(vidaBarricada.energy / energiaInicial < 0.8f && vidaBarricada.energy / energiaInicial >= 0.4f)
        {
            if (!ejecutado[0])
            {
                GetComponent<AnimationsPlaylist>().PlayAnimation("barricada0-1", 0, 1, 1);
                ejecutado[0] = true;
            }
           
        }

        if (vidaBarricada.energy / energiaInicial < 0.4f)
        {
            if (!ejecutado[1])
            {
                if (!ejecutado[0]) animationPlayList.PlayAnimation("barricada0-2", 0, 1, 1);
                else animationPlayList.PlayAnimation("barricada1-2", 0, 1, 1);
                ejecutado[1] = true;
            }

        }


    }

    public IEnumerator Destruir()
    {
        yield return new WaitForSeconds(1.2f);
        Destroy(this.gameObject);
    }
}
