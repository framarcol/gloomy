﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorInterfaz2 : MonoBehaviour {


    public GameObject botonExplosion;

    MenusEmergentes controladorJuego;
   // public GameObject pantallaNegra;
    public bool pausado;

    public GameObject explosiones;
    public int segundosEntreExplosiones;
    public int numExplosiones;


    int contador;

    int explosionesQueLlevo;

    private void Start()
    {
        explosionesQueLlevo = 1;
        if (PlayerPrefs.GetInt(SaveGame.orbital) == 1)
        {
            botonExplosion.SetActive(true);
        }

        else
        {
            botonExplosion.SetActive(false);
        }

#if UNITY_EDITOR
        botonExplosion.SetActive(true);
#endif

    }

    public void Bombardear()
    {
        botonExplosion.SetActive(false);
#if !UNITY_EDITOR
       
        PlayerPrefs.SetInt(SaveGame.orbital, 0);
#endif
        GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[5]);
        GameObject.FindGameObjectWithTag("GeneradorHordas").GetComponent<GeneradorHordas>().EliminarEnemigosActivos();
        explosiones.SetActive(true);

        if(explosionesQueLlevo < numExplosiones)
        {
            StartCoroutine(EsperarNuevaExplosion());
        }

    }

    public IEnumerator EsperarNuevaExplosion()
    {
        yield return new WaitForSeconds(segundosEntreExplosiones);
        explosionesQueLlevo++;
        explosiones.SetActive(false);
        Bombardear();
    }

    private void OnApplicationFocus(bool focus)
    {
        if (!focus)
        {
            PausarJ();
        }
    }


    public void PausarJ()
    {
        contador += 1;
        //Debug.Log(pausado+contador.ToString());
        if (!pausado)
        {
            pausado = true;
            controladorJuego = GameObject.FindGameObjectWithTag("GameController").GetComponent<MenusEmergentes>();
            Time.timeScale = 0f;
            TouchControl touch = GameObject.FindGameObjectWithTag("GameController").GetComponent<TouchControl>();
            touch.pauseStatus = true;
            controladorJuego.Pausar();
        }
        else
        {
            Desactivar();
            pausado = false;
        }

    }

    public void Desactivar()
    {
        //  pantallaNegra.SetActive(false);
        Time.timeScale = 1f;
        TouchControl touch = GameObject.FindGameObjectWithTag("GameController").GetComponent<TouchControl>();
        GameObject.FindGameObjectWithTag("MenuPausa").GetComponent<botones>().Confirmaciones();
        StartCoroutine(NoTocar());
        touch.pauseStatus = false;
        MenusEmergentes.activar = true;
    }

    IEnumerator NoTocar()
    {
        PausaCantTouch pausa = GameObject.FindGameObjectWithTag("Interfaz").GetComponentInChildren<PausaCantTouch>();
        yield return new WaitForSeconds(0.5f);
        if(pausa != null)
        pausa.BtnReanudar();
    }
}
