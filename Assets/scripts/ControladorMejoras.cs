﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorMejoras : MonoBehaviour {

    public AnimationsPlaylist[] imgArmas;
    public GameObject[] Mejoras;
    public GameObject[] tresEstrellas;
    public GameObject[] cincoEstrellas;
    public GameObject[] desbloqueados;
    public GameObject[] bloqueados;
    public GameObject[] precio;
    public ParticleSystem[] particulas;
    public Image[] estrellasCincoEstrellas;
    public Image[] estrellasTresEstrellas;
    public AudioClip fanfarria;
    public Sprite relleno;
    public Sprite vacio;
    public Sprite[] fragmentos;
    public SpriteRenderer[] fragmentosAsignados;
    public GameObject panelConfirmacion;

    // Elementos de navegacion
    public GameObject Flechaizquierda, Flechaderecha;
    public GameObject[] armas;

    public Text diamantes, oro;
    public int monedasOro, monedasDiamantes;
    int indice;
    int idDecidida;
    List<int> Mostrado;
    int valorOro, valorDiamantes, valorActual;

    bool[] inicio;

    public Text[] txtNivel;
    public Text txtConfirmar, txtOroConfirmar, txtDiamantesConfirmar;

    public Text txtWeapons, txtConsumableTools;
    Color rojo, blanco;
    public GameObject interrogacionT, interrogacionW, panelInfoCoins;

    private void Start()
    {
        ShopData.UpgradeItem[] minivel = new ShopData.UpgradeItem[2];
#if UNITY_EDITOR
        PlayerPrefs.SetInt(SaveGame.nivelesDesbloquedos[0], 2);
#endif
        for (int i = 0; i < minivel.Length; i++)
        {
            if(PlayerPrefs.GetInt(SaveGame.nivelesDesbloquedos[i]) > 0)
            {
                minivel[i] = IniciarJuego.shopData.GetMundo(PlayerPrefs.GetInt(SaveGame.nivelesDesbloquedos[i]), i);

                if (minivel[i].ArmaDesbloqueable)
                {
                    if (minivel[i].idArmaDes < 11) interrogacionW.SetActive(true);
                    else interrogacionT.SetActive(true);
                }
            }
           
        }

        rojo = txtWeapons.color;
        blanco = txtConsumableTools.color;
        inicio = new bool[3];
        indice = 1;
#if UNITY_EDITOR
        PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[1], 1);
        PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[2], 1);
        PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[3], 1);
        PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[4], 0);
        PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[5], 0);
        PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[6], 0);
        PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[7], 0);
        PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[8], 0);
        PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[11], 1);
        PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[12], 1);
        PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[13], 1);
        PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[14], 1);

        PlayerPrefs.SetInt(SaveGame.diamantes, 40);
        PlayerPrefs.SetInt(SaveGame.oro, 200000);

           PlayerPrefs.SetInt(SaveGame.nivelesArmas[1], 4);
           PlayerPrefs.SetInt(SaveGame.nivelesArmas[2], 2);
           PlayerPrefs.SetInt(SaveGame.nivelesArmas[3], 3);
           PlayerPrefs.SetInt(SaveGame.nivelesArmas[4], 3);
           PlayerPrefs.SetInt(SaveGame.nivelesArmas[5], 4);
        PlayerPrefs.SetInt(SaveGame.nivelesArmas[6], 0);
        PlayerPrefs.SetInt(SaveGame.nivelesArmas[11], 0);
        PlayerPrefs.SetInt(SaveGame.nivelesArmas[12], 0);
        PlayerPrefs.SetInt(SaveGame.nivelesArmas[13], 0);
        PlayerPrefs.SetInt(SaveGame.nivelesArmas[14], 0);
    
#endif
        CargaIdArmas(indice);

    }

    public void ReiniciarUpgrades()
    {
        for (int i = 0; i < SaveGame.nivelesArmas.Length; i++)
        PlayerPrefs.SetInt(SaveGame.nivelesArmas[i], 0);
    }

    public void CambiaUpgrades(bool esSecundaria)
    {
        GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[13]);

        if (!esSecundaria)
        {
            indice = 1;
            txtWeapons.color = rojo;
            txtConsumableTools.color = blanco;
            for (int i = 0; i < inicio.Length; i++) inicio[i] = false;
        }
        else
        {
            indice = 11;
            txtWeapons.color = blanco;
            txtConsumableTools.color = rojo;
            for (int i = 0; i < inicio.Length; i++) inicio[i] = false;
        }
        
        CargaIdArmas(indice);
    }

    public void CargaIdArmas(int comienzoIndice)
    {
        for (int j = 0, i = 0 ; j < 3; i++)
        {
            if(comienzoIndice + i != 8)
            {
                if (IniciarJuego.shopData.GetArma(comienzoIndice + i) != null)
                {
                    armas[j].SetActive(true);
                    CargarArma(IniciarJuego.shopData.GetArma(comienzoIndice + i), j);
                    j++;
                }
                else
                {
                    armas[j].SetActive(false);
                    break;
                }
            }
       
        }

        if (IniciarJuego.shopData.GetArma(comienzoIndice +3) == null) Flechaderecha.SetActive(false);
        else Flechaderecha.SetActive(true);

        if (comienzoIndice == 1 || comienzoIndice == 11) Flechaizquierda.SetActive(false);
        else Flechaizquierda.SetActive(true);

    }

    public void CargaIdArmasAnuncio()
    {
        for (int j = 0, i = 0; j < 3; i++)
        {
            if (IniciarJuego.shopData.GetArma(indice + i) != null)
            {
                armas[j].SetActive(true);
                CargarArma(IniciarJuego.shopData.GetArma(indice + i), j);
                j++;
            }
            else
            {
                armas[j].SetActive(false);
                break;
            }
        }

        if (IniciarJuego.shopData.GetArma(indice + 3) == null) Flechaderecha.SetActive(false);
        else Flechaderecha.SetActive(true);

        if (indice == 1 || indice == 11) Flechaizquierda.SetActive(false);
        else Flechaizquierda.SetActive(true);

    }

    public void BtnFLechaDerecha()
    {
        GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[13]);
        for (int i = 0; i < armas.Length; i++) armas[i].SetActive(false);
        indice += 3;
        for (int i = 0; i < inicio.Length; i++) inicio[i] = false;
        CargaIdArmas(indice);
    }

    public void BtnFLechaIzquierda()
    {
        for (int i = 0; i < inicio.Length; i++) inicio[i] = false;
        GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[13]);
        indice -= 3;
        CargaIdArmas(indice);

    }

    IEnumerator EjecutarParticulas()
    {
        yield return new WaitForSeconds(0.1f);
        particulas[(idDecidida * 2)].Play();
        yield return new WaitForSeconds(1);
        particulas[(idDecidida * 2)+1].Play();

    }

    public void BtnComprarMejora()
    {
        int valorAnadido = 0;
    //    if (indice + idDecidida > 6) valorAnadido += 4;

        int valorActual = PlayerPrefs.GetInt(SaveGame.nivelesArmas[indice + idDecidida + valorAnadido]);

        imgArmas[idDecidida].PlayAnimation("Arma" + (indice + idDecidida + valorAnadido).ToString() + "Cambio" + (valorActual+1).ToString() +  (valorActual+2).ToString(), 0, 1, 1);

        GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[23]);

        StartCoroutine(EjecutarParticulas());

        monedasOro = PlayerPrefs.GetInt(SaveGame.oro);
        monedasDiamantes = PlayerPrefs.GetInt(SaveGame.diamantes);

        if (valorActual <= IniciarJuego.shopData.GetArma(indice + idDecidida + valorAnadido).MaxLevel)
        {
            if (monedasOro >= valorOro && monedasDiamantes >= valorDiamantes)
            {
             //   Debug.Log("D");
                PlayerPrefs.SetInt(SaveGame.diamantes, monedasDiamantes - valorDiamantes);
                PlayerPrefs.SetInt(SaveGame.oro, monedasOro - valorOro);
              //  Debug.Log(indice + idDecidida + valorAnadido);
                PlayerPrefs.SetInt(SaveGame.nivelesArmas[indice + idDecidida + valorAnadido], valorActual + 1);
                CargaIdArmas(indice);
            }
        }

        panelConfirmacion.SetActive(false);
    }

    public void ActivarPanel(int idBoton)
    {
        int valorAnadido = 0;
//        if (indice + idBoton > 6) valorAnadido += 4;

        valorActual = PlayerPrefs.GetInt(SaveGame.nivelesArmas[indice + idBoton + valorAnadido]);

        valorOro = IniciarJuego.shopData.GetArma(indice + idBoton + valorAnadido).Oro[valorActual + 1];
        valorDiamantes = IniciarJuego.shopData.GetArma(indice + idBoton + valorAnadido).Diamantes[valorActual + 1];

        if(valorOro <= PlayerPrefs.GetInt(SaveGame.oro) && valorDiamantes <= PlayerPrefs.GetInt(SaveGame.diamantes))
        {
            idDecidida = idBoton;

            txtConfirmar.text = SeleccionaIdiomas.gameText[1,9] + " " +  (valorActual + 1).ToString() + " " + SeleccionaIdiomas.gameText[1, 10];
            txtOroConfirmar.text = valorOro.ToString();
            txtDiamantesConfirmar.text = valorDiamantes.ToString();

            panelConfirmacion.SetActive(true);
        }
        else
        {
            panelInfoCoins.SetActive(true);
        }

    }

    public void DesactivarPanel()
    {
        panelConfirmacion.SetActive(false);
    }

    public void ActivarAnimasao(int pos)
    {
        imgArmas[pos].PlayAnimation("Arma" + (indice + idDecidida).ToString() + "Cambio12", 0, 1, 1);
    }

    public void CargarArma(ShopData.StandardShopItem arma, int pos)
    {
        ShopData.StandardShopItem armafinal = arma;

    //    Debug.Log(pistola.itemId);

        if (!inicio[pos])
        {
            inicio[pos] = true;
            imgArmas[pos].PlayAnimation("Reposo" + arma.itemId.ToString() + PlayerPrefs.GetInt(SaveGame.nivelesArmas[arma.itemId]).ToString(), 0, 1, 1);

        }

        if (armafinal.MaxLevel == 5)
        {
            tresEstrellas[pos].SetActive(false);
            cincoEstrellas[pos].SetActive(true);

            int valorNivelArma = PlayerPrefs.GetInt(SaveGame.nivelesArmas[armafinal.itemId]);

            for (int i = pos*5; i < pos*5+valorNivelArma; i++) estrellasCincoEstrellas[i].sprite = relleno;

            for (int i = pos * 5 + valorNivelArma; i < pos*5+5; i++) estrellasCincoEstrellas[i].sprite = vacio;
        }
        else
        {
            tresEstrellas[pos].SetActive(true);
            cincoEstrellas[pos].SetActive(false);

            int valorNivelArma = PlayerPrefs.GetInt(SaveGame.nivelesArmas[armafinal.itemId]);

            for (int i = pos * 3; i < pos * 3 + valorNivelArma; i++) estrellasTresEstrellas[i].sprite = relleno;

            for (int i = pos * 3 + valorNivelArma; i < pos * 3 + 3; i++) estrellasTresEstrellas[i].sprite = vacio;
        }

        if (PlayerPrefs.GetInt(SaveGame.armasDesbloqueadas[armafinal.itemId]) == 1)
        {
            desbloqueados[pos].SetActive(true);
            fragmentosAsignados[pos].sprite = null;
            bloqueados[pos].SetActive(false);
        }
        else
        {
            desbloqueados[pos].SetActive(false);
            bloqueados[pos].SetActive(true);
            if (IniciarJuego.shopData.GetArma(armafinal.itemId).NumPiezas == 0)
            {
                bloqueados[pos].GetComponentInChildren<Text>().text = SeleccionaIdiomas.gameText[0,19] + ": " + (IniciarJuego.shopData.GetArmaNivelDesbloqueado(armafinal.itemId)[1] + 1) + "\n\n" + SeleccionaIdiomas.gameText[0, 18] + ": " + (IniciarJuego.shopData.GetArmaNivelDesbloqueado(armafinal.itemId)[0] + 1);
                fragmentosAsignados[pos].sprite = null;
            }
            else
            {
                bloqueados[pos].GetComponentInChildren<Text>().text = PlayerPrefs.GetInt(SaveGame.fragmentosDesbloqueados[armafinal.itemId]) + "/" + IniciarJuego.shopData.GetArma(armafinal.itemId).NumPiezas;
                fragmentosAsignados[pos].sprite = fragmentos[armafinal.itemId];
            }
        }

        int nivelArma = PlayerPrefs.GetInt(SaveGame.nivelesArmas[armafinal.itemId]);

        if(nivelArma != armafinal.MaxLevel)
        txtNivel[pos].text = SeleccionaIdiomas.gameText[1,2] + " " + nivelArma.ToString() + " > " + (nivelArma + 1).ToString();
        else txtNivel[pos].text = SeleccionaIdiomas.gameText[1,11];

        if (nivelArma < armafinal.MaxLevel)
        {
            precio[pos].SetActive(true);
            precio[pos].GetComponentsInChildren<Text>()[0].text = armafinal.Diamantes[nivelArma + 1].ToString();
            precio[pos].GetComponentsInChildren<Text>()[1].text = armafinal.Oro[nivelArma + 1].ToString();

            if (armafinal.Oro[nivelArma + 1] > PlayerPrefs.GetInt(SaveGame.oro)) precio[pos].GetComponentsInChildren<Text>()[1].color = Color.red;
            else precio[pos].GetComponentsInChildren<Text>()[1].color = Color.white;

            if (armafinal.Diamantes[nivelArma + 1] > PlayerPrefs.GetInt(SaveGame.diamantes)) precio[pos].GetComponentsInChildren<Text>()[0].color = Color.red;
            else precio[pos].GetComponentsInChildren<Text>()[0].color = Color.white;

        }
        else
        {
            precio[pos].SetActive(false);
        }


        Mejoras[pos*5].GetComponentsInChildren<Text>()[1].text = armafinal.Dano[nivelArma].ToString();

        Color miColor = new Color(0.074f, 1, 0, 1);

        if(nivelArma < armafinal.MaxLevel)
        {

            Mejoras[pos * 5].GetComponentsInChildren<Text>()[3].text = armafinal.Dano[nivelArma + 1].ToString();
            
            if (armafinal.Dano[nivelArma + 1] > armafinal.Dano[nivelArma]) Mejoras[pos * 5].GetComponentsInChildren<Text>()[3].color = miColor;
            else Mejoras[pos * 5].GetComponentsInChildren<Text>()[3].color = Mejoras[pos * 5 + 1].GetComponentsInChildren<Text>()[1].color;

            if(armafinal.itemId < 11)
            Mejoras[pos * 5 + 1].GetComponentsInChildren<Text>()[3].text = armafinal.Alcance[nivelArma + 1].ToString();
            else Mejoras[pos * 5 + 1].GetComponentsInChildren<Text>()[3].text = armafinal.Ratio[nivelArma + 1].ToString();

            if (armafinal.Alcance[nivelArma + 1] > armafinal.Alcance[nivelArma])
            {
                Mejoras[pos * 5 + 1].GetComponentsInChildren<Text>()[3].color = new Color(1, 1, 1, 1);
            }
            else Mejoras[pos * 5 + 1].GetComponentsInChildren<Text>()[3].color = Mejoras[pos * 5 + 1].GetComponentsInChildren<Text>()[1].color;

            Mejoras[pos * 5 + 2].GetComponentsInChildren<Text>()[3].text = armafinal.Cadencia[nivelArma + 1].ToString();
            if (armafinal.Cadencia[nivelArma + 1] < armafinal.Cadencia[nivelArma]) Mejoras[pos * 5 + 2].GetComponentsInChildren<Text>()[3].color = miColor;
            else Mejoras[pos * 5 + 2].GetComponentsInChildren<Text>()[3].color = Mejoras[pos * 5 + 1].GetComponentsInChildren<Text>()[1].color;

            Mejoras[pos * 5 + 3].GetComponentsInChildren<Text>()[3].text = armafinal.Capacidad[nivelArma + 1].ToString();
            if (armafinal.Capacidad[nivelArma + 1] > armafinal.Capacidad[nivelArma]) Mejoras[pos * 5 + 3].GetComponentsInChildren<Text>()[3].color = miColor;
            else Mejoras[pos * 5 + 3].GetComponentsInChildren<Text>()[3].color = Mejoras[pos * 5 + 1].GetComponentsInChildren<Text>()[1].color;

            Mejoras[pos * 5 + 4].GetComponentsInChildren<Text>()[3].text = armafinal.TiempoRecarga[nivelArma + 1].ToString();
            if (armafinal.TiempoRecarga[nivelArma + 1] < armafinal.TiempoRecarga[nivelArma]) Mejoras[pos * 5 + 4].GetComponentsInChildren<Text>()[3].color = miColor;
            else Mejoras[pos * 5 + 4].GetComponentsInChildren<Text>()[3].color = Mejoras[pos * 5 + 1].GetComponentsInChildren<Text>()[1].color;

            if(armafinal.Dano[nivelArma] == 0)
            {
                Mejoras[pos * 5].SetActive(false);
            }
            else
            {
                Mejoras[pos * 5].SetActive(true);
            }

            if (armafinal.Alcance[nivelArma] == 0)
            {
                Mejoras[pos * 5 + 1].SetActive(false);
            }
            else
            {
                Mejoras[pos * 5 + 1].SetActive(true);
            }

            if(armafinal.itemId == 11 || armafinal.itemId == 13)
            {
                Mejoras[pos * 5 + 1].SetActive(true);
            }

            if (armafinal.Cadencia[nivelArma] == 0)
            {
                Mejoras[pos * 5 + 2].SetActive(false);
            }
            else
            {
                Mejoras[pos * 5 + 2].SetActive(true);
            }

            if (armafinal.Capacidad[nivelArma] == 0 || armafinal.itemId == 11 || armafinal.itemId == 13)
            {
                Mejoras[pos * 5 + 3].SetActive(false);
            }
            else
            {
                Mejoras[pos * 5 + 3].SetActive(true);
            }

            if (armafinal.TiempoRecarga[nivelArma] == 0 || armafinal.itemId == 11 || armafinal.itemId == 13)
            {
                Mejoras[pos * 5 + 4].SetActive(false);
            }
            else
            {
                Mejoras[pos * 5 + 4].SetActive(true);
            }

            if(armafinal.itemId == 12)
            {
                Mejoras[pos * 5 + 4].GetComponentsInChildren<Text>()[0].text = SeleccionaIdiomas.gameText[0, 17];
                Mejoras[pos * 5 + 3].GetComponentsInChildren<Text>()[0].text = SeleccionaIdiomas.gameText[0, 16];
                Mejoras[pos * 5 + 4].transform.localPosition = new Vector3(Mejoras[pos * 5 + 4].transform.localPosition.x, -150, 0);
                Mejoras[pos * 5 + 3].transform.localPosition = new Vector3(Mejoras[pos * 5 + 3].transform.localPosition.x, -100, 0);

            }
            else if (armafinal.itemId == 11)
            {
                Mejoras[pos * 5 + 1].GetComponentsInChildren<Text>()[0].text = SeleccionaIdiomas.gameText[0, 15];
                Mejoras[pos * 5 + 3].transform.localPosition = new Vector3(Mejoras[pos * 5 + 3].transform.localPosition.x, -100, 0);
            }
            else if (armafinal.itemId == 14)
            {
                //   Mejoras[pos * 5 + 2].transform.localPosition = new Vector3(Mejoras[pos * 5 + 2].transform.localPosition.x, -150, 0);
                //    Mejoras[pos * 5 + 4].transform.localPosition = new Vector3(Mejoras[pos * 5 + 4].transform.localPosition.x, 200, 0);
                Mejoras[pos * 5 + 3].GetComponentsInChildren<Text>()[0].text = SeleccionaIdiomas.gameText[0, 22];
                Mejoras[pos * 5 + 3].transform.localPosition = new Vector3(Mejoras[pos * 5 + 3].transform.localPosition.x, -150, 0);

            }
            else if (armafinal.itemId == 13)
            {
                Mejoras[pos * 5 + 3].transform.localPosition = new Vector3(Mejoras[pos * 5 + 3].transform.localPosition.x, -250, 0);
                Mejoras[pos * 5 + 4].transform.localPosition = new Vector3(Mejoras[pos * 5 + 4].transform.localPosition.x, -200, 0);
            }
            else
            {
                Mejoras[pos * 5 + 4].GetComponentsInChildren<Text>()[0].text = SeleccionaIdiomas.gameText[0, 14];
                Mejoras[pos * 5 + 3].GetComponentsInChildren<Text>()[0].text = SeleccionaIdiomas.gameText[0, 13];
                Mejoras[pos * 5 + 2].GetComponentsInChildren<Text>()[0].text = SeleccionaIdiomas.gameText[0, 12];
                Mejoras[pos * 5 + 1].GetComponentsInChildren<Text>()[0].text = SeleccionaIdiomas.gameText[0, 11];
                Mejoras[pos * 5].GetComponentsInChildren<Text>()[0].text = SeleccionaIdiomas.gameText[0, 10];
                Mejoras[pos * 5 + 4].transform.localPosition = new Vector3(Mejoras[pos * 5 + 4].transform.localPosition.x, -250, 0);
                Mejoras[pos * 5 + 3].transform.localPosition = new Vector3(Mejoras[pos * 5 + 3].transform.localPosition.x, -300, 0);
            }

            for(int i = 0; i < 5; i++)
            {
                Mejoras[pos * 5 + i].GetComponentsInChildren<Text>()[2].text = ">";
            }

        }
        else
        {
            for (int i = 0; i < 5; i++)
            {
                Mejoras[pos * 5 + i].GetComponentsInChildren<Text>()[2].text = "";
                Mejoras[pos * 5 + i].GetComponentsInChildren<Text>()[3].text = "";

                Mejoras[pos * 5 + i].transform.localPosition = new Vector2(-27, Mejoras[pos * 5 + i].transform.localPosition.y);
            }

            Mejoras[pos * 5 + 4].GetComponentsInChildren<Text>()[0].text = SeleccionaIdiomas.gameText[0, 14];
            Mejoras[pos * 5 + 3].GetComponentsInChildren<Text>()[0].text = SeleccionaIdiomas.gameText[0, 13];
            Mejoras[pos * 5 + 2].GetComponentsInChildren<Text>()[0].text = SeleccionaIdiomas.gameText[0, 12];
            Mejoras[pos * 5 + 1].GetComponentsInChildren<Text>()[0].text = SeleccionaIdiomas.gameText[0, 11];
            Mejoras[pos * 5].GetComponentsInChildren<Text>()[0].text = SeleccionaIdiomas.gameText[0, 10];
            Mejoras[pos * 5 + 4].transform.localPosition = new Vector3(Mejoras[pos * 5 + 4].transform.localPosition.x, -250, 0);
            Mejoras[pos * 5 + 3].transform.localPosition = new Vector3(Mejoras[pos * 5 + 3].transform.localPosition.x, -300, 0);

        }

        if(armafinal.itemId <11) Mejoras[pos*5+1].GetComponentsInChildren<Text>()[1].text = armafinal.Alcance[nivelArma].ToString();
        else Mejoras[pos * 5 + 1].GetComponentsInChildren<Text>()[1].text = armafinal.Ratio[nivelArma].ToString();
        Mejoras[pos*5+2].GetComponentsInChildren<Text>()[1].text = armafinal.Cadencia[nivelArma].ToString();
        Mejoras[pos*5+3].GetComponentsInChildren<Text>()[1].text = armafinal.Capacidad[nivelArma].ToString();
        Mejoras[pos*5+4].GetComponentsInChildren<Text>()[1].text = armafinal.TiempoRecarga[nivelArma].ToString();

    }

    void Awake()
    {
        if(IniciarJuego.shopData == null)
        {
            IniciarJuego.shopData = Resources.Load("Shopdata") as ShopData;
        }
 
    }

}
