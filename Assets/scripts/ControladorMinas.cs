﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorMinas : MonoBehaviour {

    public float Velocidad;
    bool activarSubida;

    float timer;
    Vector3 posicionObjetivo;
    Vector3 origen;
    int posicionMina, subPosicionMina;
    public GameObject explosion;
    public float altura;


    public void Emerger(int posicion, int subposicion)
    {
        posicionMina = posicion;
        subPosicionMina = subposicion;
        posicionObjetivo = new Vector3(GetComponent<RectTransform>().position.x, altura, 0);
        origen = new Vector3(GetComponent<RectTransform>().position.x, -4, 0);
        activarSubida = true;
    }

    public void Explotar()
    {
        Destroy(GetComponent<BoxCollider2D>());
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        transform.eulerAngles = Vector3.forward * Random.Range(-45f, 45f);
        transform.Find("Sprite").gameObject.SetActive(false);
        transform.Find("Explosion").gameObject.SetActive(true);
        GetComponentInChildren<ControladorMinas>().enabled = false;
        explosion.SetActive(true);
        MiarmaControla.Subposicion[subPosicionMina] = false;
        if(MiarmaControla.Subposicion[posicionMina * 2] == false && MiarmaControla.Subposicion[(posicionMina*2)+1] == false)
        MiarmaControla.posicionado[posicionMina] = false;
    }

    void Subir()
    {
        Vector3 destino = posicionObjetivo;

        Vector3 distancia = destino - origen;

        timer += Time.deltaTime;

        float porcentaje = timer / Velocidad;

        porcentaje = Mathf.Sin(porcentaje * Mathf.PI * 0.5f);

        if (timer < Velocidad) transform.localPosition = distancia * porcentaje + origen;
        else activarSubida = false;
    }



    // Update is called once per frame
    void Update()
    {
        if (activarSubida)
        {
            GetComponent<BoxCollider2D>().enabled = false;
            Subir();
        }
        else GetComponent<BoxCollider2D>().enabled = true;
    }
}
