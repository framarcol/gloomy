﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorMundos : MonoBehaviour {

    public GameObject[] Desbloqueados;
    public GameObject[] Bloqueados;

    public static int[] NumLevelsMundo;
   // public int[] Niveles;

    private void Start()
    {
        if(IniciarJuego.shopData == null)
        IniciarJuego.Iniciar();

        NumLevelsMundo = new int[IniciarJuego.shopData.nombresCategoriaLejibles.Count];

        for(int i = 0; i < IniciarJuego.shopData.upgradeItems.Count; i++)
        {
            NumLevelsMundo[i] = IniciarJuego.shopData.GetCountWorld(i-1);
        }

        for (int i = 0; i < SaveGame.numMundos-1; i++)
        {
            if(i == 0)
            {
                Bloqueados[i].SetActive(false);
                Desbloqueados[i].SetActive(true);
            }
            else
            {
                if(PlayerPrefs.GetInt(SaveGame.nivelesDesbloquedos[i-1]) == NumLevelsMundo[i])
                {
                    Bloqueados[i].SetActive(false);
                    Desbloqueados[i].SetActive(true);
                }
                else
                {
                    Bloqueados[i].SetActive(true);
                    Desbloqueados[i].SetActive(false);
                }
            }
        }

    }

}
