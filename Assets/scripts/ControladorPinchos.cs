﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorPinchos : MonoBehaviour {

    public float Velocidad;
    bool activarSubida;

    float timer;
    Vector3 posicionObjetivo;
    Vector3 origen;
    Vector3 vacio;
    int posicionBarricada;
    public float altura;
    //public float segundosDestruccion;
  
    public bool activarBajada;


    public void Emerger(int posicion)
    {
        posicionBarricada = posicion;
        posicionObjetivo = new Vector3(GetComponent<RectTransform>().position.x, altura, 0);
        origen = new Vector3(GetComponent<RectTransform>().position.x, -4, 0);
        activarSubida = true;
    }


    void Subir()
    {
        Vector3 destino = posicionObjetivo;

        Vector3 distancia = destino - origen;

        timer += Time.deltaTime;

        float porcentaje = timer / Velocidad;

        porcentaje = Mathf.Sin(porcentaje * Mathf.PI * 0.5f);

        if (timer < Velocidad) transform.position = distancia * porcentaje + origen;
        else
        {
            timer = 0;
            vacio = origen;
            origen = posicionObjetivo;
            posicionObjetivo = vacio;
            StartCoroutine(PrepararBajada());
            activarSubida = false;
        }
     
    }

    IEnumerator PrepararBajada()
    {
        yield return new WaitForSeconds(IniciarJuego.shopData.GetArma(14).Capacidad[PlayerPrefs.GetInt(SaveGame.nivelesArmas[14])]);
        activarBajada = true;
    }

    void Bajar()
    {
        Vector3 destino = posicionObjetivo;

        Vector3 distancia = destino - origen;

        timer += Time.deltaTime;

        float porcentaje = timer / Velocidad;

        porcentaje = Mathf.Sin(porcentaje * Mathf.PI * 0.5f);

        if (timer < Velocidad) transform.localPosition = distancia * porcentaje + origen;
        else
        {
            MiarmaControla.posicionado[posicionBarricada] = false;
            MiarmaControla.Subposicion[posicionBarricada * 2] = false;
            MiarmaControla.Subposicion[(posicionBarricada * 2) + 1] = false;
            Destroy(this.gameObject);
            activarBajada = false;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (activarBajada)
        {
            GetComponent<BoxCollider2D>().enabled = false;
            Bajar();
        }

        if (activarSubida)
        {
            GetComponent<BoxCollider2D>().enabled = false;
            Subir();
        }
        else
        {
            GetComponent<BoxCollider2D>().enabled = true;
        }
    }
}
