﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorRecompensas : MonoBehaviour {

    public int oroInicio;
    public int oroFinal;

    private void Start()
    {
        oroInicio = PlayerPrefs.GetInt(SaveGame.oro);
    }

    public void SumarOro(int cantidad)
    {
        oroFinal = PlayerPrefs.GetInt(SaveGame.oro) + cantidad;
        PlayerPrefs.SetInt(SaveGame.oro, oroFinal);        
    }

    public int OroFinal()
    {
        return oroFinal;
    }

/*    public void GuardarOroJuego()
    {
        int oroFinal = PlayerPrefs.GetInt(SaveGame.oro) + oroPartida;
        PlayerPrefs.SetInt(SaveGame.oro, oroFinal);
    }*/

}
