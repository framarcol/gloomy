﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorSenalBarricada : MonoBehaviour {

	void Update () {

        for(int i = 0, j=0; i < GetComponentsInChildren<Image>().Length; i+=2, j++)
        {
            if (MiarmaControla.posicionado[j])
            {
                GetComponentsInChildren<Image>()[i].color = new Color (GetComponentsInChildren<Image>()[i].color.r, GetComponentsInChildren<Image>()[i].color.g, GetComponentsInChildren<Image>()[i].color.b, 0);
            }
            else
            {
                GetComponentsInChildren<Image>()[i].color = new Color(GetComponentsInChildren<Image>()[i].color.r, GetComponentsInChildren<Image>()[i].color.g, GetComponentsInChildren<Image>()[i].color.b, 1);
            }
        }		
	}
}
