﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorSenalMinas : MonoBehaviour {

    void Update()
    {
        for (int i = 0, j = 0; i < GetComponentsInChildren<Image>().Length; i += 4, j+=2)
        {
            if (MiarmaControla.Subposicion[j]) GetComponentsInChildren<Image>()[i].color = new Color(1, 1, 1, 0);
            else  GetComponentsInChildren<Image>()[i].color = new Color(1, 1, 1, 1);          

            if (MiarmaControla.Subposicion[j+1]) GetComponentsInChildren<Image>()[i + 1].color = new Color(1, 1, 1, 0);
            else GetComponentsInChildren<Image>()[i + 1].color = new Color(1, 1, 1, 1);

        }
    }
}
