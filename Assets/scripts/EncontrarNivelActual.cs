﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EncontrarNivelActual : MonoBehaviour {

    public GameObject estrella1, estrella2, estrella3;

	void Start () {

        int nivel = IniciarJuego.shopData.GetNivelByEscena(SceneManager.GetActiveScene().buildIndex);

        GetComponent<Text>().text = SeleccionaIdiomas.gameText[1,2] + ": " + nivel.ToString() + "  /  " + SeleccionaIdiomas.gameText[1,3] + ": " + BotonesMenu.mundoSeleccionado;

        if(PlayerPrefs.GetInt(SaveGame.estrelllas[BotonesMenu.mundoSeleccionado-1, nivel-1] )> 0){
            estrella1.SetActive(true);
        }
        if (PlayerPrefs.GetInt(SaveGame.estrelllas[BotonesMenu.mundoSeleccionado-1, nivel-1]) > 1)
        {
            estrella2.SetActive(true);
        }

        if (PlayerPrefs.GetInt(SaveGame.estrelllas[BotonesMenu.mundoSeleccionado-1, nivel-1]) > 2)
        {
            estrella3.SetActive(true);
        }

    }
	
}
