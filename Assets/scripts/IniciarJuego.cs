﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tapdaq;
using UnityEngine.UI;

public class IniciarJuego : MonoBehaviour {

    public static ShopData shopData = null;
    public static bool zPLayVersion;
    public GameObject uno, dos;
    public Text dameTExto;

    public bool zVersion;

    private IEnumerator Start()
    {
       yield return new WaitForSeconds(2);
       zPLayVersion = false;
        AdManager.Init();
       // AdManager.InitWithConsent(TDStatus.TRUE);
    }

    private void OnEnable()
    {
        TDCallbacks.TapdaqConfigLoaded += OnTapdaqConfigLoaded;
        TDCallbacks.TapdaqConfigFailedToLoad += OnTapdaqConfigFailToLoad;
    }

    private void OnDisable()
    {
        TDCallbacks.TapdaqConfigLoaded -= OnTapdaqConfigLoaded;
        TDCallbacks.TapdaqConfigFailedToLoad -= OnTapdaqConfigFailToLoad;
    }

    private void OnTapdaqConfigLoaded()
    {
    }

    private void OnTapdaqConfigFailToLoad(TDAdError error)
    {
      
    }

#if UNITY_ANDROID
    void OnApplicationPause(bool pauseStatus)
    {
        AdManager.OnApplicationPause(pauseStatus);
    }
#endif

    void Awake()
    {    
        shopData = Resources.Load("Shopdata") as ShopData;
    }

    public static void Iniciar()
    {
        shopData = Resources.Load("Shopdata") as ShopData;
    }
}
