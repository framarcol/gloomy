﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovimientoFlecha : MonoBehaviour {

    Transform plataforma;
    Vector3 posicionOriginal;
    Vector3 moverEjes;
    public float distancia;
    public float Velocidad;
    public bool ejeY;
    public bool ejeX;

    private void Start()
    {
        if (ejeX)
        {
            moverEjes = new Vector3(1, 0, 0);
        }

        if (ejeY)
        {
            moverEjes = new Vector3(0, 1, 0);
        }

        if (ejeX && ejeY)
        {
            moverEjes = new Vector3(1, 1, 0);
        }

        plataforma = GetComponent<Transform>();
        posicionOriginal = plataforma.position;

    }

    private void Update()
    {
       plataforma.position = posicionOriginal + moverEjes * Mathf.PingPong(Time.time * Velocidad, distancia);

    }

}
