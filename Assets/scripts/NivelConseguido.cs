﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NivelConseguido : MonoBehaviour {

    public GameObject pantallaNegra;
    public GameObject[] estrellas;
    public Text textoOro;
    float porcentajeFinal;
    int estrellasConseguidas;
    public bool test;
    public float correccionIzquierda;

    public float velParte2;
    float timerParte2;
    bool activarSubidaParte2;

    bool sumaFinalizada;

    public AudioClip starwin, foco;

    //Elementos nuevo cartel
    public GameObject IluminacionDiamante, IluminacionOro, focos, oroConseguido, diamantesConseguidos;
    public Text txtDiamantesConseguidos, txtDiamantesTotal, txtOroConseguido, txtMundo, txtNivel;

    public GameObject botonMenu, botonSiguiente, botonReiniciar;

    public SpriteRenderer armaDesbloqueada;

    int sumaDiamantes;

    int animacionBajada, animacionBajada2;

    public AnimationsPlaylist listaAnimacion, listaAnimacion2;

    public Transform vectorParte1;

    public Transform vectorParte2;
    Vector3 origen;

    public Text textoVictoria;

    int oroActual, oroFinalConteo, oroActual2, oroFinalConteo2;

    int nivelActual;

    ControladorRecompensas oroRecomp;

    float esperarCartelArmas;

    bool parpadeoIniciado, conteoIniciado;

    ShopData.UpgradeItem miNivel;

    bool esperaBoton, esperaBoton2, esperaBoton3, esperaBoton4, esperaBoton5;

    public GameObject[] cofres;
    public GameObject[] llave;
    public GameObject[] trofeos;
    bool moverArmasAbajo;
    GameObject objetoArmas;
    ProtaControl prota;
    public ParticleSystem[] cofresParty;

    public AnimationsPlaylist[] cofresAnim;

    private void Awake()
    {
       objetoArmas = GameObject.FindGameObjectWithTag("ArmasPrefab");

    }


    void Start () {

        GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[25]);

        prota = GameObject.FindGameObjectWithTag("ProtaTag").GetComponent<ProtaControl>();

#if UNITY_EDITOR
        PlayerPrefs.SetInt(SaveGame.nivelesDesbloquedos[0], 1);
        PlayerPrefs.SetInt(SaveGame.nivelesDesbloquedos[1], 1);
#endif
        moverArmasAbajo = true;
        //GameObject.FindGameObjectWithTag("ArmasPrefab").

        pantallaNegra.gameObject.SetActive(true);

        miNivel = IniciarJuego.shopData.GetMundo(IniciarJuego.shopData.GetNivelByEscena(SceneManager.GetActiveScene().buildIndex), BotonesMenu.mundoSeleccionado);

        oroRecomp = GameObject.FindGameObjectWithTag("GameController").GetComponent<ControladorRecompensas>();

        oroActual = 1000000;

        GameObject.FindGameObjectWithTag("Interfaz").SetActive(false);

        nivelActual = IniciarJuego.shopData.GetNivelByEscena(SceneManager.GetActiveScene().buildIndex);

        origen = vectorParte2.localPosition;

        estrellasConseguidas = 0;

        bool frontal = IniciarJuego.shopData.GetMundo(BotonesMenu.nivelMundoSeleccionado, BotonesMenu.mundoSeleccionado).frontal;

        TowerControl torre = null;

        for (int i = 0; i < estrellas.Length; i++)
        {
            estrellas[i].SetActive(false);
        }

        //  estrellas[0].SetActive(true);
        esperarCartelArmas = 3.5f;

        GetComponentsInParent<AudioSource>()[1].volume = 0.5f;


        float porcentajeRestante =100;

        if (!frontal)
        {
            torre = GameObject.FindGameObjectWithTag("tower").GetComponent<TowerControl>();
            porcentajeRestante = (torre.energy / torre.energiaInicial) * 100;

            if (IniciarJuego.shopData.GetMundo(IniciarJuego.shopData.GetNivelByEscena(SceneManager.GetActiveScene().buildIndex), BotonesMenu.mundoSeleccionado) != null)
            {
                porcentajeFinal = miNivel.PorcentajeEstrellaDos;
                estrellasConseguidas = 1;
            }
            else
            {
                porcentajeFinal = 80;
                estrellasConseguidas = 1;
            }

            if (porcentajeRestante == 100)
            {
                //     estrellas[1].SetActive(true);
                //     estrellas[2].SetActive(true);
                estrellasConseguidas = 3;
            }
            else if (porcentajeRestante > porcentajeFinal)
            {
                //   estrellas[1].SetActive(true);
                estrellasConseguidas = 2;
            }
        }
        else
        {
            int[] victimasFinal = miNivel.Victimas;            
            VictimasController controladorVictimas = GameObject.FindGameObjectWithTag("VictimasController").GetComponent<VictimasController>();

            if (victimasFinal[0] <= controladorVictimas.victimasSalvadas)
            {
                estrellasConseguidas = 1;
            }

            if (victimasFinal[1] <= controladorVictimas.victimasSalvadas)
            {
                estrellasConseguidas = 2;
            }

            if (victimasFinal[2] <= controladorVictimas.victimasSalvadas)
            {
                estrellasConseguidas = 3;
            }


        }

#if UNITY_EDITOR
      //  estrellasConseguidas = 2;
#endif

        switch (estrellasConseguidas)
        {
            case 1:
                textoVictoria.text = SeleccionaIdiomas.gameText[9, 9];
                break;
            case 2:
                textoVictoria.text = SeleccionaIdiomas.gameText[9, 8];
                break;
            case 3:
                textoVictoria.text = SeleccionaIdiomas.gameText[9, 7];
                break;
        }

        animacionBajada = 1;
        animacionBajada2 = 2;

        listaAnimacion.PlayAnimation("AnimationBajar"+ animacionBajada, 0,1,1);
    
        StartCoroutine(EsperarPrimerCartel());

        int nivelesPasados = PlayerPrefs.GetInt(SaveGame.nivelesDesbloquedos[BotonesMenu.mundoSeleccionado-1]);

#if UNITY_EDITOR
        nivelesPasados = 0;
#endif

        if (nivelesPasados < miNivel.itemId)
        {
            esperarCartelArmas = 1;
        }

    }

    IEnumerator EsperarCartelArma()
    {
        listaAnimacion2.PlayAnimation("AnimationBajar" + animacionBajada2, 0, 1, 1);

        yield return new WaitForSeconds(2.5f);

        listaAnimacion2.PlayAnimation("AnimationSubir" + animacionBajada2, 0, 1, 2.5f);

        cofres[miNivel.idcofre].GetComponent<SpriteRenderer>().sortingOrder = 999;
        cofres[miNivel.idcofre].SetActive(true);


        textoVictoria.text = "";

       // Debug.Log(miNivel.idcofre);

        switch (miNivel.idcofre)
        {
            case 1:
                if (!esperaBoton) esperaBoton = true;
                break;
            case 2:
                if (!esperaBoton5) esperaBoton5 = true;
                break;
            case 3:
                if (!esperaBoton4) esperaBoton4 = true;
                break;
            case 0:
                if (!esperaBoton) esperaBoton = true;
                break;
        }

       
    }

    IEnumerator AnimacionesCartel()
    {    
        yield return new WaitForSeconds(esperarCartelArmas);
   
        estrellas[0].SetActive(true);
        GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[27]);

        int diamantes = PlayerPrefs.GetInt(SaveGame.diamantes);

        diamantesConseguidos.SetActive(true);

        if (IniciarJuego.shopData.GetNivelByEscena(SceneManager.GetActiveScene().buildIndex) != 0)
            oroRecomp.SumarOro(IniciarJuego.shopData.GetMundo(IniciarJuego.shopData.GetNivelByEscena(SceneManager.GetActiveScene().buildIndex), BotonesMenu.mundoSeleccionado).RecompensaOro);
        else
        {
            oroRecomp.SumarOro(500);
        }

        if (PlayerPrefs.GetInt(SaveGame.nivelesDesbloquedos[BotonesMenu.mundoSeleccionado - 1]) < nivelActual)
        {
            PlayerPrefs.SetInt(SaveGame.nivelesDesbloquedos[BotonesMenu.mundoSeleccionado - 1], nivelActual);
        }

        textoOro.text = oroRecomp.oroInicio.ToString();

        oroActual2 = 0;
        oroFinalConteo2 = oroRecomp.OroFinal() - oroRecomp.oroInicio;

        oroConseguido.SetActive(true);

        if (sumaDiamantes > 0)
        {
            txtDiamantesConseguidos.text = "1";
        }       

        float tiempoEspera = 0.6f;

        if (estrellasConseguidas > 1)
        {
            yield return new WaitForSeconds(tiempoEspera);
            if (sumaDiamantes > 1)
            {
                diamantes = PlayerPrefs.GetInt(SaveGame.diamantes) + 2;

                txtDiamantesConseguidos.text = "2";
            }

            estrellas[1].SetActive(true);
            GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[27]);
        }

        //Energia Conseguida
        if (!GameObject.FindGameObjectWithTag("MenuPausa").GetComponentInChildren<CuentaAtras>().subido)
            PlayerPrefs.SetInt(SaveGame.energia, PlayerPrefs.GetInt(SaveGame.energia) + 1);



        if (estrellasConseguidas > 2)
        {
            yield return new WaitForSeconds(tiempoEspera);

            if (sumaDiamantes > 2)
            {
                diamantes = PlayerPrefs.GetInt(SaveGame.diamantes) + 3;

                txtDiamantesConseguidos.text = "3";
            }              

            estrellas[2].SetActive(true);
            GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[27]);
        }

        diamantes = MenusEmergentes.diamantes;

        txtDiamantesTotal.text = diamantes.ToString();

        parpadeoIniciado = true;
        oroActual = oroRecomp.oroInicio;
        oroFinalConteo = oroRecomp.OroFinal();

        if (sumaDiamantes > 0)
        {
            yield return new WaitForSeconds(tiempoEspera);
            diamantes = MenusEmergentes.diamantes + 1;
            IluminacionDiamante.SetActive(true);
            txtDiamantesTotal.text = diamantes.ToString();
            yield return new WaitForSeconds(0.1f);
            if(sumaDiamantes > 1)
            IluminacionDiamante.SetActive(false);
    
        }

        if (estrellasConseguidas > 1)
        {
            yield return new WaitForSeconds(tiempoEspera);

            if (sumaDiamantes > 1)
            {
                diamantes = MenusEmergentes.diamantes + 2;
            }      

            IluminacionDiamante.SetActive(true);
            txtDiamantesTotal.text = diamantes.ToString();
            yield return new WaitForSeconds(0.1f);
            if(sumaDiamantes > 2)
            IluminacionDiamante.SetActive(false);
        }

        if (estrellasConseguidas > 2)
        {
            yield return new WaitForSeconds(tiempoEspera);

            if (sumaDiamantes > 2)
                diamantes = MenusEmergentes.diamantes + 3;

            IluminacionDiamante.SetActive(true);
            txtDiamantesTotal.text = diamantes.ToString();
        }

        yield return new WaitForSeconds(tiempoEspera);

        txtMundo.text = SeleccionaIdiomas.gameText[1,3] + ": " + BotonesMenu.mundoSeleccionado.ToString();
        txtNivel.text = SeleccionaIdiomas.gameText[1,2] + ": " + BotonesMenu.nivelMundoSeleccionado.ToString();

        GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[28]);
        focos.SetActive(true);

        botonMenu.SetActive(true);
        if (BotonesMenu.nivelMundoSeleccionado == 25 || BotonesMenu.mundoSeleccionado == 3) botonSiguiente.SetActive(false);
        else botonSiguiente.SetActive(true);
        botonReiniciar.SetActive(true);


    }

    IEnumerator EsperarPrimerCartel() {

        yield return new WaitForSeconds(2.2f);

        int nivelesPasados = PlayerPrefs.GetInt(SaveGame.nivelesDesbloquedos[BotonesMenu.mundoSeleccionado - 1]);

#if UNITY_EDITOR
        nivelesPasados = 0;
#endif

        if (nivelesPasados < miNivel.itemId)
        {
            StartCoroutine(EsperarCartelArma());
        }
        else
        {
            listaAnimacion.PlayAnimation("AnimationSubir2", 0, 1, 2.5f);
            CargarDatosSegundoCartel();
            StartCoroutine(AnimacionesCartel());
        }


    }

    void CargarDatosSegundoCartel()
    {
        activarSubidaParte2 = true;
       
     //   nivelActual = IniciarJuego.shopData.GetNivelByEscena(SceneManager.GetActiveScene().buildIndex);

        int estrellasQueTengo;

#if UNITY_EDITOR
     //  PlayerPrefs.SetInt(SaveGame.estrelllas[BotonesMenu.mundoSeleccionado - 1, nivelActual - 1], 0);
#endif

        if (SaveGame.estrelllas != null)
        {
            estrellasQueTengo = PlayerPrefs.GetInt(SaveGame.estrelllas[BotonesMenu.mundoSeleccionado - 1, nivelActual - 1]);
        }
        else
        {
            estrellasQueTengo = 2;
        }

        if (SaveGame.estrelllas != null)
        {
            if(estrellasQueTengo < estrellasConseguidas)
            PlayerPrefs.SetInt(SaveGame.estrelllas[BotonesMenu.mundoSeleccionado - 1, nivelActual - 1], estrellasConseguidas);
        }

        sumaDiamantes = 0;

        sumaDiamantes = estrellasConseguidas - estrellasQueTengo;

        if (sumaDiamantes <= 0) sumaDiamantes = 0;

        txtDiamantesConseguidos.text = sumaDiamantes.ToString();

        PlayerPrefs.SetInt(SaveGame.diamantes, PlayerPrefs.GetInt(SaveGame.diamantes) + sumaDiamantes);

        ShopData.UpgradeItem nivel = IniciarJuego.shopData.GetMundo(nivelActual, BotonesMenu.mundoSeleccionado);

        if (nivel != null)
        {
            if (nivel.ArmaDesbloqueable == true)
            {
                PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[nivel.idArmaDes], 1);
            }
        }
    }

    void MetodoAparecerNumero()
    {
        esperaBoton2 = false;

        for (int i = 0; i < cofres.Length; i++) cofres[i].SetActive(false);

        switch (miNivel.idcofre)
        {
            case 0:
                if (miNivel.ArmaDesbloqueable)
                {
                    armaDesbloqueada.color = new Color(1, 1, 1, 1);
                    armaDesbloqueada.sprite = IniciarJuego.shopData.GetArma(IniciarJuego.shopData.GetMundo(nivelActual, BotonesMenu.mundoSeleccionado).idArmaDes).itemHigh;



                }
                else
                {
                    llave[miNivel.idLlave - 1].SetActive(true);
                }
                break;
            default:

                int id = cofres[miNivel.idcofre].GetComponent<PremiosCofre>().idTrofeo;

                trofeos[id].SetActive(true);

                switch (id)
                {
                    case 1:
                        trofeos[id].GetComponentInChildren<Text>().text = cofres[miNivel.idcofre].GetComponent<PremiosCofre>().numOro.ToString();
                        break;
                    case 2:
                        trofeos[id].GetComponentInChildren<Text>().text = cofres[miNivel.idcofre].GetComponent<PremiosCofre>().numDiamantes.ToString();
                        break;
                    default:
                        if(cofres[miNivel.idcofre].GetComponent<PremiosCofre>().numFragmento != 0)
                        trofeos[id].GetComponentInChildren<Text>().text = cofres[miNivel.idcofre].GetComponent<PremiosCofre>().numFragmento.ToString();
                        else
                        {
                            trofeos[id].GetComponentInChildren<Text>().text = "";
                        }
                        break;

                }
                break;
        }

        StartCoroutine(EsperaBoton3());
    }

    private void Update()
    {
        prota.disparable = false;
        if (moverArmasAbajo)
        {
            objetoArmas.transform.Translate(Vector3.down * Time.deltaTime * (7));
        }

        if (Input.touchCount > 0 && esperaBoton)   //Code for touchscreen
        {
            esperaBoton = false;
            cofresAnim[miNivel.idcofre].PlayAnimation("Tap3", 0, 1, 1);
            cofresParty[miNivel.idcofre].Play();
            StartCoroutine(EsperaBoton2());
        }
        else
        {       
            if (Input.GetMouseButton(0) && esperaBoton)
            {
                esperaBoton = false;
                cofresAnim[miNivel.idcofre].PlayAnimation("Tap3", 0, 1, 1);
                cofresParty[miNivel.idcofre].Play();
                StartCoroutine(EsperaBoton2());

            }
        }

        if (Input.touchCount > 0 && esperaBoton4)   //Code for touchscreen
        {
            esperaBoton4 = false;
            cofresAnim[miNivel.idcofre].PlayAnimation("Tap1", 0, 1, 1);
            StartCoroutine(EsperaBoton5());
        }
        else
        {
            if (Input.GetMouseButton(0) && esperaBoton4)
            {
                esperaBoton4 = false;
                cofresAnim[miNivel.idcofre].PlayAnimation("Tap1", 0, 1, 1);
                StartCoroutine(EsperaBoton5());

            }
        }

        if (Input.touchCount > 0 && esperaBoton5)   //Code for touchscreen
        {
            esperaBoton5 = false;
            cofresAnim[miNivel.idcofre].PlayAnimation("Tap2", 0, 1, 1);
            StartCoroutine(EsperaBoton());
        }
        else
        {
            if (Input.GetMouseButton(0) && esperaBoton5)
            {
                esperaBoton5 = false;
                cofresAnim[miNivel.idcofre].PlayAnimation("Tap2", 0, 1, 1);
                StartCoroutine(EsperaBoton());

            }
        }

        if (Input.touchCount > 0 && esperaBoton3)   //Code for touchscreen
        {
            esperaBoton3 = false;
            listaAnimacion.PlayAnimation("AnimationSubir" + animacionBajada, 0, 1, 2.5f);
            StartCoroutine(AnimacionesCartel());
            CargarDatosSegundoCartel();
        }
        else
        {

            if (Input.GetMouseButton(0) && esperaBoton3)
            {
                esperaBoton3 = false;
                listaAnimacion.PlayAnimation("AnimationSubir" + animacionBajada, 0, 1, 2.5f);
                StartCoroutine(AnimacionesCartel());
                CargarDatosSegundoCartel();
            }

        }

        if (esperaBoton2)   //Code for touchscreen
        {
            GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[31]);
            MetodoAparecerNumero();  
        }

        if (activarSubidaParte2)
        {
            Subir();
        }

        if (parpadeoIniciado)
        {
            parpadeoIniciado = false;
            StartCoroutine(ParpadeoOro());
        }

        if(oroActual <= oroFinalConteo)
        {
            sumaFinalizada = false;
            StartCoroutine(EfectoSuma());
            textoOro.text = oroActual.ToString();
        }

        if(oroActual2 <= oroFinalConteo2)
        {
            StartCoroutine(EfectoSuma2());
            txtOroConseguido.text = oroActual2.ToString();
        }
    }

    IEnumerator EsperaBoton2()
    {
        yield return new WaitForSeconds(1.5f);
        esperaBoton2 = true;
    }

    IEnumerator EsperaBoton3()
    {
        yield return new WaitForSeconds(1);
        esperaBoton3 = true;
    }

    IEnumerator EsperaBoton5()
    {
        yield return new WaitForSeconds(0.5f);
        esperaBoton5 = true;
    }

    IEnumerator EsperaBoton()
    {
        yield return new WaitForSeconds(0.7f);
        esperaBoton = true;
    }


    void Subir()
    {
        Vector3 destino = new Vector3(-12, 0, 0);  //posicionObjetivo;

        Vector3 distancia = destino - origen;

        timerParte2 += Time.deltaTime;

        float porcentaje = timerParte2 / velParte2;

        porcentaje = Mathf.Sin(porcentaje * Mathf.PI * 0.5f);

        if (timerParte2 < velParte2) vectorParte2.localPosition = distancia * porcentaje + origen;
        else activarSubidaParte2 = false;
    }

    public IEnumerator EfectoSuma()
    {
        yield return new WaitForSeconds(0.01f);
        if(oroFinalConteo - oroActual >= 10)
        {
            oroActual += 10;
        }
        else
        {
            sumaFinalizada = true;
            IluminacionOro.SetActive(true);
            oroActual += oroFinalConteo - oroActual;
        }
    }

    public IEnumerator ParpadeoOro()
    {
        while(!sumaFinalizada)
        {
            if(!IluminacionOro.activeSelf)
            GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[22]);
            IluminacionOro.SetActive(true);
            yield return new WaitForSeconds(0.2f);
            IluminacionOro.SetActive(false);
            yield return new WaitForSeconds(0.2f);
        }
    }

    public IEnumerator EfectoSuma2()
    {
        yield return new WaitForSeconds(0.01f);

        if (oroFinalConteo2 - oroActual2 >= 10)
        {
            oroActual2 += 10;
        }
        else
        {
            oroActual2 += oroFinalConteo2 - oroActual2;
        }
    }


}
