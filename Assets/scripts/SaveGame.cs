﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveGame : MonoBehaviour {

    public static string[] nivelesArmas;
    public static string[] armasDesbloqueadas;
    public static string[] nivelesDesbloquedos;
    public static string[] fragmentosDesbloqueados;
    public static string[,] estrelllas;
    public static int numMundos;
    public static string oro;
    public static string diamantes;
    public static string idioma;
    public static string ultimaHora;
    public static string horaObjectivo;
    public static string horaObjetivoAnuncio;
    public static string energia;
    public static string diaActual;
    public static string orbital;

    private void Start()
    {
        Iniciar();
    }

    public static void Iniciar()
    {
        nivelesArmas = new string[20];

        for (int i = 0; i < nivelesArmas.Length; i++)
        {
            nivelesArmas[i] = "StatsArmas" + i;
        }

        fragmentosDesbloqueados = new string[20];

        for (int i = 0; i < fragmentosDesbloqueados.Length; i++)
        {
            fragmentosDesbloqueados[i] = "TipoFragmento" + i;
        }

        armasDesbloqueadas = new string[20];

        for (int i = 0; i < armasDesbloqueadas.Length; i++)
        {
            armasDesbloqueadas[i] = "DesArma" + i;
        }

        numMundos = 3;

        nivelesDesbloquedos = new string[numMundos];

        for(int i =0; i < numMundos; i++)
        {
            nivelesDesbloquedos[i] = "StatsMundo" + i;
        }

        estrelllas = new string[4, 40];

        for(int i = 0; i < numMundos; i++)
        {
            for(int j = 0; j < 40; j++)
            {
                estrelllas[i, j] = "EstrellasMundo" + i + "Nivel" + j;
            }
        }

        oro = "oro2";
        diamantes = "diamantes2";
        idioma = "IdiomaJuego";
        ultimaHora = "UltimaConexionJugador";
        horaObjectivo = "horafinalizacion";
        diaActual = "diaDelJuego";
        orbital = "bombardeoOrbital";
        horaObjetivoAnuncio = "VerAnuncioAldia";
        energia = "EnergySystem";

    }

}
