﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptArmaPrefab : MonoBehaviour {

    GameObject miarma;
	// Use this for initialization
	void Start () {

        miarma = GameObject.FindGameObjectWithTag("ArmaTag");

	}

    public void RecargarAntes(int idArma)
    {
        idArma--;
        miarma.GetComponent<MiarmaControla>().RecargarArmaAntes(idArma);
    }
	
	
}
