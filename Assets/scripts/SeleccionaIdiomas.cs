﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SeleccionaIdiomas : MonoBehaviour {

    public static string[,] gameText = new string[100, 1000];
    //public static string idiomaDefecto = "English";

    void Awake() {

       if (Application.systemLanguage.ToString() != "Chinese" && Application.systemLanguage.ToString() != "English"
            && Application.systemLanguage.ToString() != "Spanish" && !PlayerPrefs.HasKey(SaveGame.idioma))
        {
            SceneManager.LoadSceneAsync("Idioma", LoadSceneMode.Single);
        }
        ActualizaIdioma();

#if UNITY_EDITOR
        PlayerPrefs.DeleteKey(SaveGame.idioma);
#endif

    }

    public static void ActualizaIdioma()
    {
        TextAsset lector;

          if (PlayerPrefs.HasKey(SaveGame.idioma))
          {
              if (PlayerPrefs.GetString(SaveGame.idioma) == "Spanish" 
                  || PlayerPrefs.GetString(SaveGame.idioma) == "English"
                  || PlayerPrefs.GetString(SaveGame.idioma) == "Chinese")
              {
                  lector = Resources.Load("Idiomas/" + PlayerPrefs.GetString(SaveGame.idioma)) as TextAsset;
              }
              else
              {
                  lector = Resources.Load("Idiomas/" + "English") as TextAsset;
              }

          }
          else
          {
            lector = Resources.Load("Idiomas/" + "English") as TextAsset;
            string idioma = Application.systemLanguage.ToString();

            PlayerPrefs.SetString(SaveGame.idioma, idioma);

            if (idioma == "Spanish" || idioma == "English" || idioma == "Chinese")
            {
                lector = Resources.Load("Idiomas/" + idioma) as TextAsset;
            }
            else
            {
                lector = Resources.Load("Idiomas/" + "English") as TextAsset;
            }
            }

        string[] guardaBloques = lector.text.Split('{');

        for (int i = 0; i < guardaBloques.Length; i++)
        {
            string[] guardarPalabras = guardaBloques[i].Split('\n');

            for (int j = 1; j < guardarPalabras.Length - 1; j++)
            {
                gameText[i, j-1] = guardarPalabras[j];
            }
            
        }

    }

    }
