﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialControler : MonoBehaviour {


    public GameObject mensajeTutorial;
    public Text textoTutorial;
    public float segundosMensaje;
    MiarmaControla controladorArmas;
    bool segundoMensaje;
    //bool tiempoSegundoMensajeCumplido;
    GameObject generador;
    public GameObject imagen1, imagen2;
    public GameObject botonContinue;
    public bool desactivarPrimerMensaje;
    bool segundoMensajeMostrado;
    public bool esTuto2;
    public AnimationsPlaylist playListTutorial;
    public GameObject txtTap;

    GameObject btnPausa;

    public GameObject wait;

    public Image imgSelect1;


    void Awake()
    {
        btnPausa = GameObject.FindGameObjectWithTag("Interfaz").GetComponentInChildren<Button>().gameObject;
        controladorArmas = GameObject.FindGameObjectWithTag("ArmaTag").GetComponent<MiarmaControla>();
    }

    IEnumerator Start()
    {
        if (esTuto2) imgSelect1.raycastTarget = false;
        btnPausa.SetActive(false);
        txtTap.SetActive(false);
        if(!esTuto2)playListTutorial.PlayAnimation("entrada", 0, 1, 1);
        yield return new WaitForSeconds(1f);
        txtTap.SetActive(true);
       if(!esTuto2) Time.timeScale = 0f;
    }

    public void MostrarMensaje(int numMensaje)
    {
        btnPausa.SetActive(false);
        mensajeTutorial.SetActive(true);
        if (!esTuto2)
        textoTutorial.text = SeleccionaIdiomas.gameText[2,2];
        else textoTutorial.text = SeleccionaIdiomas.gameText[3, 1];
  
    }

    private void Update()
    {
     //   Debug.Log(Time.timeScale);
        if (!segundoMensajeMostrado)
        {
            controladorArmas.habilitado = false;
        }

        if (!controladorArmas.habilitado)
        {
            wait.SetActive(true);
        }
        else
        {
            wait.SetActive(false);
        }


#if UNITY_EDITOR || UNITY_STANDALONE
        if (!segundoMensaje)
        {
            if (Input.GetMouseButtonDown(0) || desactivarPrimerMensaje)
            {
                //Debug.Log("Entro");
                Time.timeScale = 1;
                segundoMensaje = true;
                mensajeTutorial.SetActive(false);
                // generador.SetActive(true);
                btnPausa.SetActive(true);
                StartCoroutine(PausarJuego());
            }
        }
#else
        if (!segundoMensaje)
        {
            if (Input.touchCount > 0 || desactivarPrimerMensaje)
            {
                Debug.Log("Entro");
                Time.timeScale = 1;
                segundoMensaje = true;
                mensajeTutorial.SetActive(false);
                //generador.SetActive(true);
                StartCoroutine(PausarJuego());
            }
        }
#endif
    }

    IEnumerator HabilitarArma()
    {
        yield return new WaitForSeconds(0.2f);
        controladorArmas.habilitado = true;
        StopCoroutine(HabilitarArma());
    }

    public void BotonContinuar()
    {
        btnPausa.SetActive(true);
        Time.timeScale = 1;
        mensajeTutorial.SetActive(false);
        botonContinue.SetActive(false);
    }

    

    IEnumerator PausarJuego()
    {
        yield return new WaitForSeconds(segundosMensaje);
        btnPausa.SetActive(false);
        imagen1.SetActive(false);
        botonContinue.GetComponent<Button>().interactable = false;
        if (!esTuto2)playListTutorial.PlayAnimation("entrada", 0, 1, 1);
        MostrarMensaje(1);
        imagen2.SetActive(true);
        yield return new WaitForSeconds(0.7f);
        botonContinue.GetComponent<Button>().interactable = true;
        segundoMensajeMostrado = true;
    //    tiempoSegundoMensajeCumplido = true;
        controladorArmas.habilitado = true;
        if(esTuto2)imgSelect1.raycastTarget = true;
        Time.timeScale = 0f;
    }



}
