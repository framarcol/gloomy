﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialControler2 : MonoBehaviour {

    public float segundos;
    public float segundosSegundoMensaje;
    public GameObject MensajesSinPausa;
    public Text textoTutorial;

	IEnumerator Start () {

        yield return new WaitForSeconds(segundos);
        MensajesSinPausa.SetActive(true);
        textoTutorial.text = SeleccionaIdiomas.gameText[3,2];
        yield return new WaitForSeconds(4f);
        MensajesSinPausa.SetActive(false);
        yield return new WaitForSeconds(segundosSegundoMensaje);
        MensajesSinPausa.SetActive(true);
        textoTutorial.text = SeleccionaIdiomas.gameText[3, 3];
        yield return new WaitForSeconds(6f);
        MensajesSinPausa.SetActive(false);


    }
	
}
