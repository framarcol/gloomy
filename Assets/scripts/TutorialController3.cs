﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialController3 : MonoBehaviour {

    public Text txtTextoSeleccionado, txtTextoSeleccionado2, txtTextoSeleccionado3;
    public GameObject mensajeTutorial;
    public GameObject mensajeTutorial2;
    public GameObject mensajeTutorial3;
    public BoxCollider2D botonArma2, botonArma1;
    MiarmaControla controladorArmas;
    ProtaInfo prota;
    public float retrasoMensaje;
    bool unavez, desArmas;
    public Button boton1, boton2, boton3;
    public GameObject btnPausa;
    public Image imgSelect2, imgSelect1;
    bool unaVez2;
    public AnimationsPlaylist animMensaje1, animMensaje2;
    bool unaVez3;

    public GameObject wait;

	IEnumerator Start () {

        imgSelect2.raycastTarget = false;
        imgSelect1.raycastTarget = false;
        botonArma2.enabled = false;
        Time.timeScale = 1f;
        controladorArmas = GameObject.FindGameObjectWithTag("ArmaTag").GetComponent<MiarmaControla>();
        prota = GameObject.FindGameObjectWithTag("ProtaTag").GetComponent<ProtaInfo>();
        desArmas = true;
        yield return new WaitForSeconds(1.3f);
        btnPausa.SetActive(false);
        mensajeTutorial.SetActive(true);
        animMensaje1.PlayAnimation("entrada", 0, 1, 1);
        boton1.interactable = false;
        txtTextoSeleccionado.text = SeleccionaIdiomas.gameText[4, 1];
        yield return new WaitForSeconds(0.7f);
        boton1.interactable = true;
        botonArma2.enabled = true;
        botonArma1.enabled = false;
        desArmas = false;
        Time.timeScale = 0f;
	}

    public void ReanudarJuego()
    {
        Time.timeScale = 1f;
        btnPausa.SetActive(true);
        mensajeTutorial.SetActive(false);
        imgSelect2.raycastTarget = true;
        StartCoroutine(MensajeDos());        
    }

    private void Update()
    {
        if (controladorArmas.currentWeapon == MiarmaControla.WeaponTypes._wtRifle)
        {
            if (!unaVez3)
            {
                unaVez3 = true;
                ReanudarJuego();
            }
         
        }
     

        if (prota.Ammo[2] == prota.Capacidad[2] - 1) ReanudarJuego3();

        if (desArmas) controladorArmas.habilitado = false;

        if (!controladorArmas.habilitado) wait.SetActive(true);
        else  wait.SetActive(false);


        if (prota.Ammo[2] <= 29 && prota.Ammo[2] > 27)
        {
            Time.timeScale = 1f;
            btnPausa.SetActive(true);
            mensajeTutorial2.SetActive(false);
        }

        if(prota.Ammo[2] == 0)
        {
            if (!unavez)
            {
                Time.timeScale = 0f;
                txtTextoSeleccionado3.text = SeleccionaIdiomas.gameText[4, 3];
                btnPausa.SetActive(false);
                mensajeTutorial3.SetActive(true);
                unavez = true;
            }

        }
    }

    public void ReanudarJuego2()
    {
        Time.timeScale = 1f;
        btnPausa.SetActive(true);
        mensajeTutorial3.SetActive(false);
        imgSelect1.raycastTarget = true;
    }

    public void ReanudarJuego3()
    {
        if (!unaVez2)
        {
            unaVez2 = true;
            Time.timeScale = 1f;
            btnPausa.SetActive(true);
            mensajeTutorial2.SetActive(false);
        }


    }

    IEnumerator MensajeDos()
    {
        desArmas = true;
        yield return new WaitForSeconds(retrasoMensaje - 0.7f);
        btnPausa.SetActive(false);
        mensajeTutorial2.SetActive(true);
        animMensaje2.PlayAnimation("entrada", 0, 1, 1);
        boton2.interactable = false;
        txtTextoSeleccionado2.text = SeleccionaIdiomas.gameText[4, 2];
        yield return new WaitForSeconds(0.7f);
        boton2.interactable = true;
        botonArma1.enabled = true;
        desArmas = false;
        controladorArmas.habilitado = true;
        Time.timeScale = 0f;

    }

}
