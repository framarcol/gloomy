﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialController4 : MonoBehaviour {

    public float retraso;
    public GameObject mensaje;
    bool desArmas;
    public Button boton;
    public GameObject btnPausa;
    ProtaInfo prota;
    bool unaVez;

    public GameObject wait;

    private void Awake()
    {

        prota = GameObject.FindGameObjectWithTag("ProtaTag").GetComponent<ProtaInfo>();

    }

    IEnumerator Start () {
       // controladorArmas = GameObject.FindGameObjectWithTag("ArmaTag").GetComponent<MiarmaControla>();

        desArmas = true;

        yield return new WaitForSeconds(retraso - 0.7f);

        desArmas = false;
     //   controladorArmas.habilitado = true;
        btnPausa.SetActive(false);
        mensaje.SetActive(true);
        GetComponentInChildren<AnimationsPlaylist>().PlayAnimation("entrada", 0, 1, 1);
        // Time.timeScale = 0f;
        //  boton.interactable = false;
        yield return new WaitForSeconds(4f);
        GetComponentInChildren<AnimationsPlaylist>().PlayAnimation("salida", 0, 1,1);
        yield return new WaitForSeconds(0.7f);
        //    boton.interactable = true;
        if (!unaVez)
        {
            unaVez = true;
            Time.timeScale = 1f;
            btnPausa.SetActive(true);
            mensaje.SetActive(false);
        }
    }

    private void Update()
    {
        if (prota.Ammo[1] == prota.Capacidad[1] - 1) Reanudar();
        if (prota.Ammo[2] == prota.Capacidad[2] - 1) Reanudar();
    }

    public void Reanudar()
    {
       

    }
	
}
