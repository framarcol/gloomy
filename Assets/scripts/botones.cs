﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class botones : MonoBehaviour {

    //   public GameObject horditas;
    public GameObject FakeLoadScreen;
    public GameObject CantTouchThis;

    public GameObject ConfirmacionRestart, ConfirmacionMenu;


    private IEnumerator Start()
    {
        yield return new WaitForSeconds(0.5f);
        CantTouchThis.SetActive(false);
    }

    public void OnGUI()
    {
      //  GUI.Button(new Rect(10, 10, 150, 100), PlayerPrefs.GetInt(SaveGame.diamantes).ToString());
    }
    private void Update()
    {
      //  Debug.Log(BotonesMenu.mundoSeleccionado);
    //    Debug.Log(BotonesMenu.nivelMundoSeleccionado);
    }

    void Restar()
    {
        if (PlayerPrefs.GetInt(SaveGame.nivelesDesbloquedos[BotonesMenu.mundoSeleccionado - 1]) >= BotonesMenu.nivelMundoSeleccionado)
        {

        }
        else
        {
            //Resta
            PlayerPrefs.SetInt(SaveGame.energia, PlayerPrefs.GetInt(SaveGame.energia) - 1);
            if (PlayerPrefs.GetInt(SaveGame.energia) < 3)
            {
                System.DateTime hora = System.DateTime.Now;
                System.DateTime hora2 = System.DateTime.Now.AddMinutes(30);

                if (PlayerPrefs.HasKey(SaveGame.horaObjectivo))
                {
                    System.DateTime hora3 = System.DateTime.Parse(PlayerPrefs.GetString(SaveGame.horaObjectivo));

                    if (System.DateTime.Compare(hora, hora3) == 1)
                    {
                        PlayerPrefs.SetString(SaveGame.ultimaHora, hora.ToString());
                        PlayerPrefs.SetString(SaveGame.horaObjectivo, hora2.ToString());
                    }
                }
                else
                {
                    PlayerPrefs.SetString(SaveGame.ultimaHora, hora.ToString());
                    PlayerPrefs.SetString(SaveGame.horaObjectivo, hora2.ToString());
                }
            }
        }
    }

    public void CargarEscena(bool restando)
    {
        FakeLoadScreen.SetActive(true);
        Time.timeScale = 1f;

        if (!restando)
        {
            Restar();
        }
        else
        {
            PlayerPrefs.SetInt(SaveGame.energia, PlayerPrefs.GetInt(SaveGame.energia) - 1);
            if (PlayerPrefs.GetInt(SaveGame.energia) < 3)
            {
                System.DateTime hora = System.DateTime.Now;
                PlayerPrefs.SetString(SaveGame.ultimaHora, hora.ToString());

                System.DateTime hora2 = System.DateTime.Now.AddMinutes(30);
                PlayerPrefs.SetString(SaveGame.horaObjectivo, hora2.ToString());
            }
        } 

        ShopData.UpgradeItem nivel = IniciarJuego.shopData.GetMundo(BotonesMenu.nivelMundoSeleccionado, BotonesMenu.mundoSeleccionado);

        if (!nivel.frontal)
            SceneManager.LoadSceneAsync("LoadScreenArmas");
        else SceneManager.LoadSceneAsync("LoadScreenArmasFrontal");
    }

    public void CargarEscenaMismasArmas(bool restando)
    {
        Time.timeScale = 1f;

        if (!restando)
        {
            Restar();
        }
        else
        {
            PlayerPrefs.SetInt(SaveGame.energia, PlayerPrefs.GetInt(SaveGame.energia) - 1);
            if (PlayerPrefs.GetInt(SaveGame.energia) < 3)
            {
                System.DateTime hora = System.DateTime.Now;
                PlayerPrefs.SetString(SaveGame.ultimaHora, hora.ToString());

                System.DateTime hora2 = System.DateTime.Now.AddMinutes(30);
                PlayerPrefs.SetString(SaveGame.horaObjectivo, hora2.ToString());
            }
        }


        //    ShopData.UpgradeItem nivel = IniciarJuego.shopData.GetMundo(BotonesMenu.nivelMundoSeleccionado, BotonesMenu.mundoSeleccionado);
        FakeLoadScreen.SetActive(true);
        SceneManager.LoadSceneAsync("LoadScreen");

        /*   if (!nivel.frontal)
               SceneManager.LoadSceneAsync("LoadScreen");
           else SceneManager.LoadSceneAsync("LoadScreenArmasFrontal");*/
    }

    public void MenuPrincipal()
    {
        PlayerPrefs.SetInt(SaveGame.orbital, 0);
        Time.timeScale = 1f;
        FakeLoadScreen.SetActive(true);
        BotonesMenu.idDEstino = 3;
        LoadSCSImple.esMenu = true;
        SceneManager.LoadScene("LoadScreen");
    }

    public void MenuArmas()
    {
        Time.timeScale = 1f;
        FakeLoadScreen.SetActive(true);
        BotonesMenu.idDEstino = 5;
        LoadSCSImple.esMenu = true;
        SceneManager.LoadScene("LoadScreen");
    }

    public void SiguienteNivel()
    {
        PlayerPrefs.SetInt(SaveGame.orbital, 0);
        //Resta
        PlayerPrefs.SetInt(SaveGame.energia, PlayerPrefs.GetInt(SaveGame.energia) - 1);
        if (PlayerPrefs.GetInt(SaveGame.energia) < 3)
        {
            System.DateTime hora = System.DateTime.Now;
            System.DateTime hora2 = System.DateTime.Now.AddMinutes(30);

            if (PlayerPrefs.HasKey(SaveGame.horaObjectivo))
            {
                System.DateTime hora3 = System.DateTime.Parse(PlayerPrefs.GetString(SaveGame.horaObjectivo));

                if (System.DateTime.Compare(hora, hora3) == 1)
                {
                    PlayerPrefs.SetString(SaveGame.ultimaHora, hora.ToString());
                    PlayerPrefs.SetString(SaveGame.horaObjectivo, hora2.ToString());
                }
            }
            else
            {
                PlayerPrefs.SetString(SaveGame.ultimaHora, hora.ToString());
                PlayerPrefs.SetString(SaveGame.horaObjectivo, hora2.ToString());
            }
        }

        FakeLoadScreen.SetActive(true);
        int escena = SceneManager.GetActiveScene().buildIndex;
        int nivel = IniciarJuego.shopData.GetNivelByEscena(escena);

        BotonesMenu.nivelMundoSeleccionado = nivel + 1;

        loadSC.nextScene = IniciarJuego.shopData.GetMundo(BotonesMenu.nivelMundoSeleccionado, BotonesMenu.mundoSeleccionado).escena;

        LoadSCSImple.esInicio = true;
        LoadSCSImple.esMenu = false;
        SceneManager.LoadScene("LoadScreen");
    }

    public void Confirmaciones()
    {
        ConfirmacionMenu.SetActive(false);
        ConfirmacionRestart.SetActive(false);
    }

    public void Desactivar()
    {
        GameObject.FindGameObjectWithTag("Interfaz").GetComponent<ControladorInterfaz2>().pausado = false;
        Confirmaciones();
        Time.timeScale = 1f;
        TouchControl touch = GameObject.FindGameObjectWithTag("GameController").GetComponent<TouchControl>();
        StartCoroutine(NoTocar());
        touch.pauseStatus = false;
        MenusEmergentes.activar = true;
    }

    IEnumerator NoTocar()
    {
        yield return new WaitForSeconds(0.5f);
        PausaCantTouch pausa = GameObject.FindGameObjectWithTag("Interfaz").GetComponentInChildren<PausaCantTouch>();
        if(pausa != null)
        pausa.BtnReanudar();
    }
}
