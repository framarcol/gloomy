﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeControl : MonoBehaviour {

    //public bool fadeState;
    Color color;
    Color colorOrig, colorDest;
    float fadingPercent;
    float seconds;
    bool enabledAfterFade;

    // Use this for initialization
    void Start () {
        fadingPercent = 1.0f;
        color = Color.black;
        this.GetComponent<Image>().color = color;
        enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log(this.GetComponent<Image>().color + "   Fading Percent: " + fadingPercent);
		if (fadingPercent < 1.0f)
        {
            fadingPercent += Time.unscaledDeltaTime / seconds;
            color = Color.Lerp(colorOrig, colorDest, fadingPercent);
            this.GetComponent<Image>().color = color;
        }
        else
        {
            fadingPercent = 1.0f;   //Avoiding values greater than 1.0f
            color = colorDest;
            this.GetComponent<Image>().color = color;
            enabled = false;
            gameObject.SetActive(enabledAfterFade);
        }
	}

    public void FadeOn (float fadeSeconds)
    {
        colorOrig = Color.black;
        colorDest = Color.clear;
        this.enabledAfterFade = false;
        _fade(fadeSeconds);
    }

    public void FadeOff(float fadeSeconds)
    {
        colorOrig = Color.clear;
        colorDest = Color.black;
        this.enabledAfterFade = true;
        _fade(fadeSeconds);
    }

    public void FadeToColor (float fadeSeconds, Color newColor, bool enableAfterFade)
    {
        colorOrig = this.GetComponent<Image>().color;
        colorDest = newColor;
        //Debug.Log(colorOrig + "    " + colorDest);
        this.enabledAfterFade = enableAfterFade;
        _fade(fadeSeconds);
    }

    private void _fade (float fadeSeconds)  //just to avoid repeat code
    {
        seconds = fadeSeconds;
        fadingPercent = 1.0f - fadingPercent;   //this way, if percent wasn't zero, it holds previous alpha
        enabled = true;
        gameObject.SetActive(true);
    }
}
