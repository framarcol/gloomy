﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ejecutarAnimacionBoton : MonoBehaviour {

    BotonesMenu objeto;
    public bool activarAlEntrar;
    List<string> nombreAnimaciones;
    public int cambioPantalla;
    public float segundosAnimacion;
    AnimationsPlaylist listaAnimaciones;
    public AudioClip play, generico, slaidin;

    public int numAnims;

    private void Start()
    {
        numAnims = 0;
        objeto = GameObject.FindGameObjectWithTag("ControladorPrincipalMenu").GetComponent<BotonesMenu>();
        listaAnimaciones = GetComponent<AnimationsPlaylist>();
        if(GetComponentInChildren<DragonBones.UnityArmatureComponent>() != null)
        if (GetComponentInChildren<DragonBones.UnityArmatureComponent>().animation != null)
            nombreAnimaciones = GetComponentInChildren<DragonBones.UnityArmatureComponent>().animation.animationNames;
    }

    private void OnEnable()
    {
        if (activarAlEntrar)
        {
            if (GetComponentInChildren<DragonBones.UnityArmatureComponent>().animation != null)
                nombreAnimaciones = GetComponentInChildren<DragonBones.UnityArmatureComponent>().animation.animationNames;

            if(listaAnimaciones == null)
            {
                listaAnimaciones = GetComponent<AnimationsPlaylist>();
            }

            GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[17]);

            listaAnimaciones.PlayAnimation(nombreAnimaciones[Random.Range(0,nombreAnimaciones.Count-1)], 0, 1, 1);
        }
        else
        {
            if(GetComponentInChildren<DragonBones.UnityArmatureComponent>() != null)
            {
                if (GetComponentInChildren<DragonBones.UnityArmatureComponent>().animation != null)
                {
                    nombreAnimaciones = GetComponentInChildren<DragonBones.UnityArmatureComponent>().animation.animationNames;
                    int contador = nombreAnimaciones.Count - 1;
                    string nombreFinal = nombreAnimaciones[contador];
                    if (listaAnimaciones != null)
                        listaAnimaciones.PlayAnimation(nombreFinal, 0, 1, 1);
                }
            }
            
        }
    }

    public void EjecutarAnimacion()
    {
        StartCoroutine(Animar());
    }

    public void EjecutarAnimacionVolver()
    {
        GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[12]);
        StartCoroutine(AnimarVolver());
    }


    IEnumerator Animar()
    {
        if (cambioPantalla == 3)
        {
            numAnims = 1;
            GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[10]);
        }
        else
        {
            GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[21]);
        }

        if (GetComponentInChildren<DragonBones.UnityArmatureComponent>().animation != null)
        {
            nombreAnimaciones = GetComponentInChildren<DragonBones.UnityArmatureComponent>().animation.animationNames;
        }

        string animacionFinal = nombreAnimaciones[Random.Range(0, nombreAnimaciones.Count - 1 - numAnims)];

        if(animacionFinal == "reposo")
        {
            animacionFinal = nombreAnimaciones[1];
        }

        listaAnimaciones.PlayAnimation(animacionFinal, 0, 1, 1);

        yield return new WaitForSeconds(segundosAnimacion);

        objeto.CambiarPantalla(cambioPantalla);
    }

    IEnumerator AnimarVolver()
    {
        GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[21]);

        if (GetComponentInChildren<DragonBones.UnityArmatureComponent>().animation != null)
        {
            nombreAnimaciones = GetComponentInChildren<DragonBones.UnityArmatureComponent>().animation.animationNames;
        }

        listaAnimaciones.PlayAnimation(nombreAnimaciones[Random.Range(0, nombreAnimaciones.Count - 1)], 0, 1, 1);

        yield return new WaitForSeconds(segundosAnimacion);

        objeto.BotonVolver();
    }

}
