﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorEnemigosV2 : MonoBehaviour {

    public GameObject[] enemigo;
    Transform[] posiciones;
    public bool haciaDerecha;
    public bool omitirHorda;
    [HideInInspector]
    public bool activado;


    public void Activar()
    {
        posiciones = GetComponentsInChildren<Transform>();

        if (haciaDerecha)
        {
            for (int i = 0; i < posiciones.Length; i++)
            {
                posiciones[i].position = new Vector2(posiciones[i].position.x * -1, posiciones[i].position.y);
            }
        }    

    }

    public void Desactivar()
    {
        if(GetComponentsInChildren<GenericEnemyControl>().Length == 0 && GetComponentsInChildren<ParacaEnemyControl>().Length == 0 && GetComponentsInChildren<VoladorEnemyControl>().Length == 0)
        gameObject.SetActive(false);
    }

    public void ActivarSimple()
    {
        activado = true;
        posiciones = GetComponentsInChildren<Transform>();

        if (haciaDerecha)
        {
            for (int i = 0; i < posiciones.Length; i++)
            {
                posiciones[i].position = new Vector2(posiciones[i].position.x * -1, posiciones[i].position.y);
            }
        }

        for (int i = 0; i < enemigo.Length; i++)
        {
            Instantiate(enemigo[i], posiciones[i + 1]);
        }
    }

    void Awake()
    {
        if(GetComponentInParent<GeneradorHordas>() != null)
        {
            if (!GetComponentInParent<GeneradorHordas>().HordasGeneradas && !GetComponentInParent<GeneradorHordas>().procedural)
            {
                IniciarHorda();
            }
        }
        else
        {
            IniciarHorda();
        }
    }

    void IniciarHorda()
    {
        // Debug.Log("aquisi;");
        activado = true;
        posiciones = GetComponentsInChildren<Transform>();

        if (haciaDerecha)
        {
            for (int i = 0; i < posiciones.Length; i++)
            {
                posiciones[i].position = new Vector2(posiciones[i].position.x * -1, posiciones[i].position.y);
            }
        }

        for (int i = 0; i < enemigo.Length; i++)
        {
            Instantiate(enemigo[i], posiciones[i + 1]);
        }
    }
    
    public bool GetHaciaDerecha()
    {
        return haciaDerecha;
    }
}
