﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GeneradorHordas : MonoBehaviour {


    float[] tiempoHorda;
    int[] cantidadHordas;
    GeneradorEnemigosV2[] hordas;
    GeneradorEnemigosV3[] hordasV3;
    int horda;
    bool PestanaGameOverMostrada;
    bool hordasIniciadas;
    GameObject infoJuego;
    public int numHordas;
    public int numHordasFinal;
    bool frontal;
    public GameObject hordaText;
    GeneradorHordasPorton generador;
    public bool HordasGeneradas;
    public bool procedural;
    public bool noHayEnemigos;
    bool finalizado;
    public bool V3;

    void CrearZombiesCarga()
    {
        string[] nombres = new string[] { "EnemyStandard", "EnemyMidget", "EnemyMuscle", "EnemySkellington", "",
                        "EnemyLoca", "EnemyCabeza1", "" ,"EnemyColossus", "EnemyDragon", "EnemyStandardHalfZombie",
            "EnemyParachute", "WagonDynamiteS2",
                        "WagonDynamiteS5", "WagonDynamiteS7", "WagonCoalS2", "WagonCoalS5", "WagonGoldS2", "WagonGoldS6", "RandomwagonS2"};

        GameObject enemis = GameObject.FindGameObjectWithTag("Enemigos");

        ShopData.UpgradeItem miNivel = IniciarJuego.shopData.GetMundo(BotonesMenu.nivelMundoSeleccionado, BotonesMenu.mundoSeleccionado);

        int[] contadorEnemigos = new int[20];

        for (int i = 0; i < miNivel.numTipoEnemigos; i++)
        {
            switch (miNivel.idEnemigo[i])
            {
                case 0:
                    for (int j = 0; j < enemis.GetComponentsInChildren<GenericEnemyControl>().Length; j++)
                        if (enemis.GetComponentsInChildren<GenericEnemyControl>()[j].idEnemigo == 1) contadorEnemigos[0]++;
                    break;
                case 1:
                    for (int j = 0; j < enemis.GetComponentsInChildren<GenericEnemyControl>().Length; j++)
                        if (enemis.GetComponentsInChildren<GenericEnemyControl>()[j].idEnemigo == 6) contadorEnemigos[1]++;
                    break;
                case 2:
                    for (int j = 0; j < enemis.GetComponentsInChildren<GenericEnemyControl>().Length; j++)
                        if (enemis.GetComponentsInChildren<GenericEnemyControl>()[j].idEnemigo == 4) contadorEnemigos[2]++;
                    break;
                case 3:
                    for (int j = 0; j < enemis.GetComponentsInChildren<GenericEnemyControl>().Length; j++)
                        if (enemis.GetComponentsInChildren<GenericEnemyControl>()[j].idEnemigo == 2) contadorEnemigos[3]++;
                    break;
                case 5:
                    for (int j = 0; j < enemis.GetComponentsInChildren<GenericEnemyControl>().Length; j++)
                        if (enemis.GetComponentsInChildren<GenericEnemyControl>()[j].idEnemigo == 7) contadorEnemigos[5]++;
                    break;
                case 6:
                    for (int j = 0; j < enemis.GetComponentsInChildren<GenericEnemyControl>().Length; j++)
                        if (enemis.GetComponentsInChildren<GenericEnemyControl>()[j].idEnemigo == 5) contadorEnemigos[6]++;
                    break;
                case 8:
                    for (int j = 0; j < enemis.GetComponentsInChildren<GenericEnemyControl>().Length; j++)
                        if (enemis.GetComponentsInChildren<GenericEnemyControl>()[j].idEnemigo == 0) contadorEnemigos[8]++;
                    break;
                case 9:
                    contadorEnemigos[9] = enemis.GetComponentsInChildren<VoladorEnemyControl>().Length;
                    break;
                case 10:
                    for (int j = 0; j < enemis.GetComponentsInChildren<GenericEnemyControl>().Length; j++)
                        if (enemis.GetComponentsInChildren<GenericEnemyControl>()[j].idEnemigo == 3) contadorEnemigos[10]++;
                    break;
                case 11:
                    contadorEnemigos[11] = enemis.GetComponentsInChildren<ParacaEnemyControl>().Length;
                    break;
                case 12:
                    for (int j = 0; j < enemis.GetComponentsInChildren<VagonetaControlador>().Length; j++)
                        if (enemis.GetComponentsInChildren<VagonetaControlador>()[j].idVagoneta == 12) contadorEnemigos[12]++;
                    break;
                case 13:
                    for (int j = 0; j < enemis.GetComponentsInChildren<VagonetaControlador>().Length; j++)
                        if (enemis.GetComponentsInChildren<VagonetaControlador>()[j].idVagoneta == 13) contadorEnemigos[13]++;
                    break;
                case 14:
                    for (int j = 0; j < enemis.GetComponentsInChildren<VagonetaControlador>().Length; j++)
                        if (enemis.GetComponentsInChildren<VagonetaControlador>()[j].idVagoneta == 14) contadorEnemigos[14]++;
                    break;
                case 15:
                    for (int j = 0; j < enemis.GetComponentsInChildren<VagonetaControlador>().Length; j++)
                        if (enemis.GetComponentsInChildren<VagonetaControlador>()[j].idVagoneta == 15) contadorEnemigos[15]++;
                    break;
                case 16:
                    for (int j = 0; j < enemis.GetComponentsInChildren<VagonetaControlador>().Length; j++)
                        if (enemis.GetComponentsInChildren<VagonetaControlador>()[j].idVagoneta == 16) contadorEnemigos[16]++;
                    break;
                case 17:
                    for (int j = 0; j < enemis.GetComponentsInChildren<VagonetaControlador>().Length; j++)
                        if (enemis.GetComponentsInChildren<VagonetaControlador>()[j].idVagoneta == 17) contadorEnemigos[17]++;
                    break;
                case 18:
                    for (int j = 0; j < enemis.GetComponentsInChildren<VagonetaControlador>().Length; j++)
                        if (enemis.GetComponentsInChildren<VagonetaControlador>()[j].idVagoneta == 18) contadorEnemigos[18]++;
                    break;
                case 19:
                    for (int j = 0; j < enemis.GetComponentsInChildren<VagonetaControlador>().Length; j++)
                        if (enemis.GetComponentsInChildren<VagonetaControlador>()[j].idVagoneta == 19) contadorEnemigos[19]++;
                    break;
            }

        }

        for (int i = 0; i < miNivel.numTipoEnemigos; i++)
        {
            for (int j = contadorEnemigos[miNivel.idEnemigo[i]]; j < miNivel.cantidadEnemigos[i]; j++)
            {
                GameObject objeto = Instantiate((GameObject)Resources.Load(nombres[miNivel.idEnemigo[i]]), enemis.transform);
                objeto.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            }
        }
    }

    public void EliminarEnemigosActivos()
    {
        int hordaActual = horda;
      

        for(int j = hordaActual; j >= 0; j--)
        {
            if (j < hordasV3.Length)
            {          
                for (int i = 0; i < hordasV3[j].GetComponentsInChildren<GenericEnemyControl>().Length; i++)
                {
                    hordasV3[j].GetComponentsInChildren<GenericEnemyControl>()[i].MatarPersonaje();
                }
                for (int i = 0; i < hordasV3[j].GetComponentsInChildren<ParacaEnemyControl>().Length; i++)
                {
                    hordasV3[j].GetComponentsInChildren<ParacaEnemyControl>()[i].MatarPersonaje();
                }
                for (int i = 0; i < hordasV3[j].GetComponentsInChildren<VoladorEnemyControl>().Length; i++)
                {
                    hordasV3[j].GetComponentsInChildren<VoladorEnemyControl>()[i].MatarPersonaje();
                }
            }
        }

        
    }

    /*private void OnGUI()
    {
        //   GUI.Label(new Rect(10, 10, 100, 20), GetComponentsInChildren<GenericEnemyControl>().Length.ToString());

        GUI.Label(new Rect(10, 40, 100, 20), "E: " + GameObject.FindGameObjectWithTag("Enemigos").GetComponentsInChildren<GenericEnemyControl>().Length.ToString());

        int f = 60;

        for(int i = 0; i < GameObject.FindGameObjectWithTag("Enemigos").GetComponentsInChildren<GenericEnemyControl>().Length; i++)
        {
            GUI.Label(new Rect(80, f + (i * 10), 100, 20), "E: " + GameObject.FindGameObjectWithTag("Enemigos").GetComponentsInChildren<GenericEnemyControl>()[i].transform.position);

        }

    }*/

    private void Awake()
    {
        numHordasFinal = 0;
#if !UNITY_EDITOR
        CrearZombiesCarga();
#endif

    }

    void Start () {

        if(GameObject.FindGameObjectWithTag("GeneradorHordasPorton") != null)
        generador = GameObject.FindGameObjectWithTag("GeneradorHordasPorton").GetComponent<GeneradorHordasPorton>();

        if (IniciarJuego.shopData != null)
        {
            frontal = IniciarJuego.shopData.GetMundo(BotonesMenu.nivelMundoSeleccionado, BotonesMenu.mundoSeleccionado).frontal;
        }

        hordasIniciadas = false;
        horda = 0;
        hordasV3 = GetComponentsInChildren<GeneradorEnemigosV3>();

        infoJuego = GameObject.FindGameObjectWithTag("GameController");

        tiempoHorda = new float[hordasV3.Length];
        cantidadHordas = new int[hordasV3.Length];

        int hordaActual = 0;

        string letraActual = "";
            for (int i = 0; i < hordasV3.Length; i++)
            {
                string nombreHorda = hordasV3[i].gameObject.name;

                tiempoHorda[i] = float.Parse(nombreHorda.Substring(nombreHorda.Length - 2));

                if (procedural)
                {
                    if (i == 0)
                    {
                        letraActual = nombreHorda.Substring(nombreHorda.Length - 6);
                        letraActual = letraActual.Remove(1);
                        cantidadHordas[hordaActual]++;
                    }
                    else
                    {
                        string letra = nombreHorda.Substring(nombreHorda.Length - 6);
                        letra = letra.Remove(1);

                        if (letra == letraActual)
                        {
                            cantidadHordas[hordaActual]++;
                        }
                        else
                        {
                            hordaActual++;
                            letraActual = letra;
                            cantidadHordas[hordaActual]++;
                        }

                    }
                }
            }       

        if (procedural)
        {
            int numero = 0;

            for (int i = 0; i < cantidadHordas.Length; i++)
            {
                if (cantidadHordas[i] > 0)
                {
                    int aleatorio = numero + Random.Range(0, cantidadHordas[i]);
#if UNITY_EDITOR
                    aleatorio = numero + 0;
#endif
                    GetComponentsInChildren<GeneradorEnemigosV3>()[aleatorio].ActivarSimple();
                    numero += cantidadHordas[i];
                }
            }
                hordasV3 = GetComponentsInChildren<GeneradorEnemigosV3>();
        }

        StartCoroutine(GenerarHordasV3());
       
		
	}

    public void CrearHordas()
    {
        for(int i = 0; i < GetComponentsInChildren<GeneradorEnemigosV2>().Length; i++)
        {
           GetComponentsInChildren<GeneradorEnemigosV2>()[i].ActivarSimple();
        }
    }

    public void EliminarHordas()
    {
        while(GetComponentsInChildren<GenericEnemyControl>().Length != 0)
        {
            DestroyImmediate(GetComponentsInChildren<GenericEnemyControl>()[0].gameObject);
        }

        while (GetComponentsInChildren<ParacaEnemyControl>().Length != 0)
        {
            DestroyImmediate(GetComponentsInChildren<ParacaEnemyControl>()[0].gameObject);
        }
    }

    public void CheckHordas()
    {
        if (!hordasIniciadas)
        {
            if(!V3)
            StartCoroutine(GenerarHordas2());
        }
    }

    public void MostrarMenu()
    {
        if (GameObject.FindGameObjectWithTag("ProtaTag").GetComponent<ProtaControl>().estoyMuerto)
        {
            TouchControl touch = GameObject.FindGameObjectWithTag("GameController").GetComponent<TouchControl>();
            touch.pauseStatus = true;
            infoJuego.GetComponent<MenusEmergentes>().MostrarPausa();
        }
        else
        {
            TouchControl touch = GameObject.FindGameObjectWithTag("GameController").GetComponent<TouchControl>();
            touch.pauseStatus = true;
            infoJuego.GetComponent<MenusEmergentes>().MostrarPausa2();

            GameObject.FindGameObjectWithTag("tower").GetComponent<TowerControl>().DeslizarTorre();

            GameObject.FindGameObjectWithTag("ProtaTag").GetComponent<ProtaControl>().DeslizarPersonaje();

        }
    }

    public void FinalizarJuego()
    {
        if (!frontal)
        {
            if (!finalizado)
            {
                noHayEnemigos = true;
                finalizado = true;

                if (generador == null)
                {
                    MostrarMenu();
                }
                else
                {
                    if (generador.noHayEnemigos || !GameObject.FindGameObjectWithTag("GeneradorHordasPorton").GetComponent<GeneradorHordasPorton>().hordasIniciadas)
                    {
                        MostrarMenu();
                    }
                }
            }  
        }
    }

    private void Update()
    {
        if (!frontal)
        {
            if (!V3)
            {
                if (GetComponentsInChildren<GenericEnemyControl>().Length + GetComponentsInChildren<ParacaEnemyControl>().Length + GetComponentsInChildren<VoladorEnemyControl>().Length == 0)
                {
                    FinalizarJuego();
                }
            }
            else
            {
                if(GetComponentsInChildren<MoverNiebla>().Length == 0)
                {
                    if (GetComponentsInChildren<ControladorPosicion>().Length == 0)
                    {
                        FinalizarJuego();
                    }
                }
                else
                {
                    
                    if(GetComponentsInChildren<ControladorPosicion>().Length == GetComponentsInChildren<MoverNiebla>().Length)
                    {
                        FinalizarJuego();
                    }
                }
               
            }
        }
     }

    IEnumerator GenerarHordas()
    {
        if (!GameObject.FindGameObjectWithTag("ProtaTag").GetComponent<ProtaControl>().estoyMuerto)
        {
            if (horda < hordas.Length)
            {
                if (hordas[horda].activado)
                {
                    //GetComponentsInChildren<GeneradorEnemigosV2>()[horda].ActivarSimple(tiempoHorda[horda]);
                    yield return new WaitForSeconds(tiempoHorda[horda]);
                    if (!GetComponentsInChildren<GeneradorEnemigosV2>()[horda].omitirHorda)
                    {
                        //Debug.Log(GetComponentsInChildren<GeneradorEnemigosV2>()[horda].gameObject.name);
                        GameObject objetovida = Instantiate(hordaText, new Vector2(0, 0), new Quaternion(0, 0, 0, 0), GameObject.FindGameObjectWithTag("InterfazW").transform);
                        objetovida.GetComponent<AnimacionTextoHorda>().NumeroHorda((numHordasFinal + 1).ToString(), numHordas.ToString());
                        numHordasFinal++;
                    }

                    hordasIniciadas = true;

                    for (int i = 0; i < hordas[horda].GetComponentsInChildren<GenericEnemyControl>().Length; i++)
                    {
                        hordas[horda].GetComponentsInChildren<GenericEnemyControl>()[i].IniciarPersonaje();
                    }
                    for (int i = 0; i < hordas[horda].GetComponentsInChildren<ParacaEnemyControl>().Length; i++)
                    {
                        hordas[horda].GetComponentsInChildren<ParacaEnemyControl>()[i].IniciarPersonaje();
                    }
                    for (int i = 0; i < hordas[horda].GetComponentsInChildren<VoladorEnemyControl>().Length; i++)
                    {
                        hordas[horda].GetComponentsInChildren<VoladorEnemyControl>()[i].IniciarPersonaje();
                    }
                    for (int i = 0; i < hordas[horda].GetComponentsInChildren<MoverNiebla>().Length; i++)
                    {
                        hordas[horda].GetComponentsInChildren<MoverNiebla>()[i].IniciarNiebla();
                    }
                    for (int i = 0; i < hordas[horda].GetComponentsInChildren<MoverVagoneta>().Length; i++)
                    {
                        hordas[horda].GetComponentsInChildren<MoverVagoneta>()[i].IniciarNiebla();
                    }
                }
                horda++;
                StartCoroutine(GenerarHordas());
            }
            else
            {
                horda++;
                StopAllCoroutines();
            }
        }     
       
    }

    IEnumerator GenerarHordasV3()
    {
        if (!GameObject.FindGameObjectWithTag("ProtaTag").GetComponent<ProtaControl>().estoyMuerto)
        {
            if (horda < hordasV3.Length)
            {
                if (hordasV3[horda].activado)
                {
                    //GetComponentsInChildren<GeneradorEnemigosV2>()[horda].ActivarSimple(tiempoHorda[horda]);
                    yield return new WaitForSeconds(tiempoHorda[horda]);
                    if (!GetComponentsInChildren<GeneradorEnemigosV3>()[horda].omitirHorda)
                    {
                        GameObject.FindGameObjectWithTag("MenuPausa").GetComponentInChildren<AnimacionTextoHorda>().NumeroHorda((numHordasFinal + 1).ToString(), numHordas.ToString());
                        numHordasFinal++;
                    }

                    hordasIniciadas = true;

                    for (int i = 0; i < hordasV3[horda].GetComponentsInChildren<ControladorPosicion>().Length; i++)
                    {
                        hordasV3[horda].GetComponentsInChildren<ControladorPosicion>()[i].IniciarPersonaje(false);
                    }                
                }
                else
                {
                    while (hordasV3[horda].GetComponentsInChildren<ControladorPosicion>().Length != 0)
                        hordasV3[horda].GetComponentsInChildren<ControladorPosicion>()[0].gameObject.SetActive(false);
                }
                horda++;
                StartCoroutine(GenerarHordasV3());
            }
            else
            {
                horda++;
                StopAllCoroutines();
            }
        }

    }

    IEnumerator GenerarHordas2()
    {
        if (horda < hordas.Length)
        {
            yield return new WaitForSeconds(tiempoHorda[horda]);
            hordasIniciadas = true;

            for (int i = 0; i < hordas[horda].GetComponentsInChildren<GenericEnemyControl>().Length; i++)
            {
                hordas[horda].GetComponentsInChildren<GenericEnemyControl>()[i].IniciarPersonaje();
            }
            for (int i = 0; i < hordas[horda].GetComponentsInChildren<ParacaEnemyControl>().Length; i++)
            {
                hordas[horda].GetComponentsInChildren<ParacaEnemyControl>()[i].IniciarPersonaje();
            }
            for (int i = 0; i < hordas[horda].GetComponentsInChildren<VoladorEnemyControl>().Length; i++)
            {
                hordas[horda].GetComponentsInChildren<VoladorEnemyControl>()[i].IniciarPersonaje();
            }
            horda++;
            StartCoroutine(GenerarHordas());
        }
        else
        {
            horda++;
            StopAllCoroutines();
        }

    }

}
