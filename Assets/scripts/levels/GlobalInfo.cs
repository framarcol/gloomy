﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalInfo : MonoBehaviour {

    public Transform prota;
    public Transform torre;
    //public float DistanciaDelCentro;
  //  public bool frontal;

    public static Vector2 mainResolution = new Vector2(1280.0f, 720.0f);
    public static Vector2 mainAspectRatio = new Vector2(16.0f, 9.0f);

    private void Awake()
    {
        if(GameObject.FindGameObjectsWithTag("Enemigos").Length == 0)
        {
            Instantiate((GameObject)Resources.Load("Enemigos"));
        }
    }

    public static Vector2 GetAspectRatio()
    {
        Vector2 garAspect;
        float actualAspectRatio = Camera.main.aspect;
        if (actualAspectRatio == (16.0f / 9.0f))
        {
            garAspect = new Vector2(16.0f, 9.0f);
        }
        else if (actualAspectRatio == (4.0f / 3.0f))
        {
            garAspect = new Vector2(4.0f, 3.0f);
        }
        else if (actualAspectRatio == (5.0f / 4.0f))
        {
            garAspect = new Vector2(5.0f, 4.0f);
        }
        else if (actualAspectRatio == (16.0f / 10.0f))
        {
            garAspect = new Vector2(16.0f, 10.0f);
        }
        else
        {
            garAspect = new Vector2(-1.0f, -1.0f);
        }

        return garAspect;
    }

}
