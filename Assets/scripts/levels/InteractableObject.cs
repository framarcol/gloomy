﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractableObject : MonoBehaviour {

    public int energy;
    int energiaInicialObjetoInteractivo;
    public float speed;
    public bool barricada;
    public int ReduccionDanoGranada;
    public Rigidbody2D myRigidbody2D;
    bool esperando;
    public enum ReactorType { solidWall, traspasableWall, destructible, enemy, mina, dinamita, carbon, oro, superviviente, botonPorton };
    [Tooltip("This is the default behaviour this element has to react when receive a shoot:\n" +
        "- solid wall: stops the bullet.\n" +
        "- traspasable wall: don't stop the bullet (and do nothing).\n" + 
        "- others: reduce bullet and object energy.\n" +
        "This method should be overriden by enemies.")]
    public ReactorType reactorType;

    private void Start()
    {
        energiaInicialObjetoInteractivo = energy;
    }

    IEnumerator Esperar()
    {
        yield return new WaitForSeconds(0.5f);
        esperando = false;
    }

    //What should do the object if it receive a shoot. This is the default behavior: loose the same energy
    //of the bullet, and make the bullet loose energy (it returns the energy the bullet should loose)
    public virtual int ShootReactor (int bulletEnergy)
    {
        int tempEnergy=0;

        switch (reactorType)
        {
            case ReactorType.traspasableWall:
                tempEnergy = 0;
                break;
            case ReactorType.carbon:
                if (!esperando)
                {
                    esperando = true;
                    StartCoroutine(Esperar());

                    if (energy != 0)
                    {
                        if (energy == 1)
                        {
                            GetComponent<BoxCollider2D>().enabled = false;
                            GetComponent<VagonetaControlador>().SacarCarbon(1);
                            energy = 0;
                        }
                        else
                        {
                            GetComponent<AnimationsPlaylist>().PlayAnimation("carbon_small", 0, 10, 1);
                            GetComponent<VagonetaControlador>().SacarCarbon(2);
                            energy = 1;
                        }

                    }
                }
             
                break;
            case ReactorType.oro:
                if (!esperando)
                {
                    esperando = true;
                    StartCoroutine(Esperar());

                    if (energy != 0)
                    {
                        if (energy == 1)
                        {
                            GetComponent<VagonetaControlador>().oro = 0;
                            GetComponent<VagonetaControlador>().SacarOro(1);
                            GetComponent<BoxCollider2D>().enabled = false;
                            energy = 0;
                        }
                        else
                        {
                            GetComponent<AnimationsPlaylist>().PlayAnimation("gold_small", 0, 10, 1);
                            GetComponent<VagonetaControlador>().SacarOro(2);
                            GetComponent<VagonetaControlador>().oro /= 2;
                            energy = 1;
                        }

                    }
                }
               
                break;
            case ReactorType.dinamita:
                energy = 0;
                break;
            case ReactorType.botonPorton:
                energy -= bulletEnergy;
                tempEnergy = energy;
                if (energy <= 0)
                {
                    GetComponent<BoxCollider2D>().enabled = false;
                    GameObject.FindGameObjectWithTag("GeneradorHordasPorton").GetComponent<GeneradorHordasPorton>().IniciarHordas();
                }
                else
                {
                    float valor = ((float)energy / (float)energiaInicialObjetoInteractivo) -1;

                    GetComponentsInChildren<Image>()[1].color = new Color(1, 1, 1, valor * -1);
                }

                break;
            default:
                tempEnergy = energy;
                if(!barricada) energy -= bulletEnergy;
                else
                {
                    bulletEnergy /= ReduccionDanoGranada;
                    energy -= bulletEnergy;
                }
                break;
        }

        return tempEnergy;
    }
}
