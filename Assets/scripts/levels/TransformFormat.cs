﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformFormat : MonoBehaviour {

    //public GlobalInfo globalInfo;
    private Object[] Sprites;
    private static Vector2 actualAspectRatio;

	// Use this for initialization
	void Start () {
        actualAspectRatio = GlobalInfo.GetAspectRatio();

        Camera.main.orthographicSize = (GlobalInfo.mainResolution.y * (actualAspectRatio.y / GlobalInfo.mainAspectRatio.y) / 2) / 100;
        Sprites = FindObjectsOfType(typeof(GameObject));

        foreach (GameObject Sprit in Sprites)
        {
            if (Sprit.GetComponent<SpriteRenderer>() && !Sprit.transform.parent)
            {
                Sprit.transform.localScale = new Vector3(Sprit.transform.localScale.x * (actualAspectRatio.x / GlobalInfo.mainAspectRatio.x),
                                                            Sprit.transform.localScale.y * (actualAspectRatio.y / GlobalInfo.mainAspectRatio.y),
                                                            Sprit.transform.localScale.z);
                Sprit.transform.position = new Vector3(Sprit.transform.position.x * (actualAspectRatio.x / GlobalInfo.mainAspectRatio.x),
                                                        Sprit.transform.position.y * (actualAspectRatio.y / GlobalInfo.mainAspectRatio.y),
                                                        Sprit.transform.position.z);
            }
        }
	}

    public static Vector2 getTransVel (Vector2 Velocity)
    {
        actualAspectRatio = GlobalInfo.GetAspectRatio();
        return new Vector2(Velocity.x * (actualAspectRatio.x / 16f), Velocity.y * (actualAspectRatio.y / 9f));
    }
}
