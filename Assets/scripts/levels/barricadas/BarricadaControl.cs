﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DragonBones;

public class BarricadaControl : MonoBehaviour {

    public int energy;
    public UnityArmatureComponent armadura;
    [Tooltip("Indica cuántas barricadas diferentes se han animado (ojo, no cuántas animaciones hay, sino barricadas diferentes)")]
    public int numberOfBarricades;
    public GameObject explosion;

    private enum BarricadaStates { _beDragging, _bePlaced};
   // private BarricadaStates state;
    public int myStatus;   //Indica el estado de destrozo de la barricada tras ser emplazada en el esenario
    private int nBarricada;
    //Animation variables
    private AnimationsPlaylist animationsPlaylist;
    public int[] animationWhenEnergyLowers = new int[4];
    private bool playingAnimation;
    private Animator explosionAnimator;

    //Events
    public delegate void DamagedDelegate(int objectEnergy);
    public event DamagedDelegate DamagedObject;
    public delegate void ColapseObjectDelegate();
    public event ColapseObjectDelegate ColapseObject;

    // Use this for initialization
    void Start () {

        nBarricada = Random.Range(1, numberOfBarricades);
        if (armadura == null)
        {
            //Debug.Log("Armadura vacía");
            armadura = transform.GetComponent<UnityArmatureComponent>();
        }
        if (armadura != null)
        {
            armadura.armature.animation.GotoAndStopByTime("Barricada " + nBarricada + " - Estado-1", 0f);
        }
        if (animationsPlaylist == null)
        {
            animationsPlaylist = transform.GetComponent<AnimationsPlaylist>();
        }
        animationsPlaylist.DragonBonesEvent += DragonBonesEventListener;

        if (explosion != null)
        {
            explosionAnimator = explosion.GetComponent<Animator>();
        }

       // state = BarricadaStates._bePlaced;
        enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        //Tenemos que esperar a que todos los hijos salgan de cámara antes de eliminar el GameObject
        bool hijoVisible = false;
        int i = 0;
        while ((i < armadura.transform.childCount) && !hijoVisible)
        {
            hijoVisible = hijoVisible | armadura.transform.GetChild(i).GetComponent<Renderer>().isVisible;
            i++;
        }
        //Aqui sabemos si los hijos han desaparecido o no de pantalla
        if (!hijoVisible)
        {
            Destroy(gameObject);
        }
    }

    public void RecibirDano(int dano)
    {
        if (energy > 0)
        {
            if (!playingAnimation)
            {
                if ((energy > animationWhenEnergyLowers[myStatus]) && (energy - dano <= animationWhenEnergyLowers[myStatus]))
                {
                    PlayAndEvent();
                }
            }
            energy -= dano;
        }
    }


    //*************************************************************************************************************************
    //  EVENTOS
    //*************************************************************************************************************************
    private void DragonBonesEventListener(AnimationsPlaylist.DragonBonesEventType type, EventObject eventObject)
    {
        Debug.Log("EVENTO! tipo: " + type + ". Nombre: " + eventObject.name);

        if (type == AnimationsPlaylist.DragonBonesEventType.loopComplete)
        {
            if ((myStatus < animationWhenEnergyLowers.Length) && (energy <= animationWhenEnergyLowers[myStatus]))
            {
                PlayAndEvent();
            }
            else
            {
                playingAnimation = false;
            }
        }


        if (myStatus == 4)
        {
            if (type == AnimationsPlaylist.DragonBonesEventType.loopComplete)
            {
                if (ColapseObject != null)
                {
                    ColapseObject();
                }
                Explode();
                //explosion.SetActive(true);
                explosionAnimator.SetTrigger("Destruccion");
                GetComponent<BoxCollider2D>().enabled = false;
                enabled = true;
            }
        }
    }

    private void PlayAndEvent()
    {
        string animation;
        /*switch (myStatus)
        {
            case 0: animation = "daño torre 1"; break;
            case 1: animation = "Daño torre 2"; break;
            case 2: animation = "Torre daño 3"; break;
            default: animation = "Daño 4"; break;
        }*/
        animation = "Barricada " + nBarricada + " - Estado-" + (myStatus + 1);
        animationsPlaylist.PlayAnimation(animation, 0f, 1, 1f);
        playingAnimation = true;
        if (DamagedObject != null)
        {
            //DamagedTower(energy - dano);
            DamagedObject(animationWhenEnergyLowers[myStatus]);
        }
        explosionAnimator.SetTrigger("Impacto_grave");
        myStatus++;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log(collision.gameObject.layer + "   " + LayerMask.LayerToName(collision.gameObject.layer));
        if (collision.gameObject.layer == LayerMask.NameToLayer("scenary"))
        {
            GetComponent<Rigidbody2D>().isKinematic = true;
        }
    }

    //**************************************************************************************************************************
    //  EXPLOSION
    //**************************************************************************************************************************
    //************************************************************************************************************
    //  EXPLOSION
    //************************************************************************************************************
    protected void Explode()
    {
        /*foreach (string animName in armadura.armature.animation.animationNames)
        {
            armadura.armature.animation.Stop(animName);
        }*/
        armadura.armature.animation.timeScale = 0f;
        /*if (explosion != null)
            explosion.SetActive(true);*/

        Rigidbody2D tempRigidbody;
        for (int i = 0; i < armadura.transform.childCount; i++)
        {
            tempRigidbody = armadura.transform.GetChild(i).gameObject.AddComponent<Rigidbody2D>();
            tempRigidbody.mass = 200f;
            tempRigidbody.AddForce((armadura.transform.GetChild(i).position - transform.position) * 400, ForceMode2D.Impulse);
        }
    }
}
