﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugEnemyAnimationEvents : MonoBehaviour {

    private DebugEnemyControl debugEnemyControl;

	// Use this for initialization
	void Start () {
        debugEnemyControl = transform.GetComponentInParent<DebugEnemyControl>();	
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void EndHitAnimation()
    {
        debugEnemyControl.EndHitAnimation();
    }

    public void EndDeadAnimation()
    {
        debugEnemyControl.EndDeadAnimation();
    }
}
