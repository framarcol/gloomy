﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This script must be on the parent GameObject, because if not, it don't detect any collision at all!!!

public class DebugEnemyControl : InteractableObject
 {

    public enum EnemStates { esWalking, esClimbing, esKilling, esWaitingAnimation};
    [Space(15)]
    public EnemStates state;

    Transform myParentTransform;
    Vector3 tempVector3;

	// Use this for initialization
	void Start () {
        //myParentTransform = transform.parent.transform;
        myParentTransform = transform;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void FixedUpdate()
    {
        switch (state)
        {
            case EnemStates.esWalking:
                tempVector3 = myParentTransform.TransformDirection(Vector3.right) * speed;
                myRigidbody2D.velocity = new Vector2(tempVector3.x, myRigidbody2D.velocity.y);
                break;
            case EnemStates.esClimbing:
                myRigidbody2D.velocity = Vector2.up * speed;
                break;
        }
    }

    //If collides with tower, start climbing
    private void OnCollisionEnter2D (Collision2D collision)
    {
        //Debug.Log("He llegado a la torre");
        if (collision.gameObject.name == "tower")
        {
            state = EnemStates.esClimbing;
        }
    }

    //If collides with top of the tower, starts killing the prota
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log("He llegado a la cima de la torre: " + collision.gameObject.name);
        if (collision.gameObject.name == "tower")
        {
            state = EnemStates.esKilling;
            myRigidbody2D.velocity = Vector2.zero;
            myRigidbody2D.isKinematic = true;
        }
    }

    //************************************************************************************************************
    //  ANIMATION METHODS
    //************************************************************************************************************
    private void WaitForAnimation (string animation)
    {
        transform.GetComponentInChildren<Animator>().SetTrigger(animation);
        state = EnemStates.esWaitingAnimation;

    }

    public void EndHitAnimation ()
    {
        state = EnemStates.esWalking;
    }

    public void EndDeadAnimation()
    {
        Destroy(this.gameObject);
    }
    //************************************************************************************************************
    //  SHOOTS
    //************************************************************************************************************
    public override int ShootReactor (int bulletEnergy)
    {
        int tempEnergy = 0;

        if (energy > 0) //Initial check to prevent hitting when enem is dead
        {
            tempEnergy = energy;
            energy -= bulletEnergy; //substracting bullet energy to enemy
            //Triggering animation
            if (energy > 0)
            {
                WaitForAnimation("hit");
            }
            else
            {
                WaitForAnimation("dead");
            }
        }

        return tempEnergy;
    }
}
