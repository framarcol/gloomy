﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DragonBones;

public class EnemyNormalControl : InteractableObject {

    public float speedRunning;
    public enum EnemStates { esWalking, esWalkingToIdle,
                            esRunning, esRunningToIdle,
                            esIdle, esIdleToWalking, esIdleToRunning,
                            esIdleToClimbing, esClimbing, esClimbingToIdle, esKillingProta, esGoingEating, esEatingProta,
                            esShooted, esDying, esDead, esWaitingAnimation,
                            esAttackTower, esAttackBarricade, esShooted2
    };
    [Space(15)]
    public EnemStates state;
    public UnityArmatureComponent armadura;
    public AnimationsPlaylist animationsPlaylist;
    public GameObject explosion;

    protected EnemStates nextState;    //Next status after waiting for an animation to end
    protected int initEnergy;

    // Use this for initialization
    void Start () {
		if (myRigidbody2D == null)
        {
            myRigidbody2D = transform.GetComponent<Rigidbody2D>();
        }
        initEnergy = energy;
	}


    protected virtual void ChangeState(EnemStates newState) { }
    //************************************************************************************************************
    //  SHOOTS
    //************************************************************************************************************
    public override int ShootReactor(int bulletEnergy)
    {
        int tempEnergy = 0;

        if (GetComponent<GenericEnemyControl>() != null)
        {
            if (GetComponent<GenericEnemyControl>().disparable)
            {
                if (energy > 0)
                {
                    tempEnergy = energy;
                    energy -= bulletEnergy;

                    if (energy > 0)
                    {
                        if (bulletEnergy < GetComponent<GenericEnemyControl>().DanoRetroceso)
                        {
                            ChangeState(EnemStates.esShooted);
                        }
                        else if (bulletEnergy < GetComponent<GenericEnemyControl>().DanoMaximo)
                        {
                            if (GetComponentInChildren<BoxCollider2D>().enabled)
                            {
                                GetComponent<GenericEnemyControl>().reventado = false;
                                ChangeState(EnemStates.esShooted2);
                            }
                        }
                        else
                        {
                          //  Debug.Log("Estado 4");
                            if (GetComponentInChildren<BoxCollider2D>().enabled)
                            {
                                GetComponent<GenericEnemyControl>().reventado = true;
                                ChangeState(EnemStates.esShooted2);
                            }
                             
                        }
                    }
                    else
                    {
                    //    Debug.Log("Estado 5");
                        if (bulletEnergy < GetComponent<GenericEnemyControl>().DanoMaximo)
                        {

                            if (GetComponentInChildren<BoxCollider2D>().enabled)
                            {
                                GetComponent<GenericEnemyControl>().reventado = false;
                                ChangeState(EnemStates.esDying);
                            }
                            if (GetComponent<HuesudoControl>() != null) GetComponentInChildren<BoxCollider2D>().enabled = false;

                        }
                        else
                        {
                            if (GetComponentInChildren<BoxCollider2D>().enabled)
                            {
                                GetComponent<GenericEnemyControl>().reventado = true;
                                ChangeState(EnemStates.esDying);
                            }
                            if (GetComponent<HuesudoControl>() != null) GetComponentInChildren<BoxCollider2D>().enabled = false;

                        }

                    }
           
                }
            }
        }
        else if(GetComponent<VoladorEnemyControl>() != null)
        {
            if (GetComponent<VoladorEnemyControl>().disparable)
            {
                if (energy > 0)
                {
                    tempEnergy = energy;
                    energy -= bulletEnergy;

                    if (energy > 0)
                    {
                        if (bulletEnergy < GetComponent<VoladorEnemyControl>().DanoRetroceso)
                        {
                            ChangeState(EnemStates.esShooted2);
                        }              
                        else
                        {
                            ChangeState(EnemStates.esShooted);
                        }
                    }
                    else ChangeState(EnemStates.esDying);
                }
            }
        }
        else if (GetComponent<ParacaEnemyControl>() != null)
        {
            if (GetComponent<ParacaEnemyControl>().disparable)
            {
                if (energy > 0)
                {
                    tempEnergy = energy;
                    energy -= bulletEnergy;

                    if (energy > 0)
                    {
                        if (bulletEnergy < GetComponent<ParacaEnemyControl>().DanoRetroceso)
                        {
                            ChangeState(EnemStates.esShooted2);
                        }
                        else
                        {
                            ChangeState(EnemStates.esShooted);
                        }
                    }
                    else ChangeState(EnemStates.esDying);
                }
            }
        }
        else
        {
            if (energy > 0)
            {
                tempEnergy = energy;
                energy -= bulletEnergy;

                if (energy > 0) ChangeState(EnemStates.esShooted);
                else ChangeState(EnemStates.esDying);
            }
        }
            

        return tempEnergy;

    }

    //************************************************************************************************************
    //  EXPLOSION
    //************************************************************************************************************
    protected void Explode()
    {
        animationsPlaylist.StopAnimation();

        myRigidbody2D.velocity = Vector2.zero;
        myRigidbody2D.isKinematic = true;

        if (explosion != null)
        {
            explosion.transform.localEulerAngles = Vector3.forward * Random.Range(-180f, 180f);
            explosion.SetActive(true);
        }

        Rigidbody2D tempRigidbody;

        for (int i = 0; i < armadura.transform.childCount; i++)
        {
            tempRigidbody = armadura.transform.GetChild(i).gameObject.AddComponent<Rigidbody2D>();

            if(tempRigidbody != null)
            {
                tempRigidbody.mass = 200f;
                tempRigidbody.AddForce((armadura.transform.GetChild(i).position - transform.position) * 400, ForceMode2D.Impulse);
            }
        }

        StartCoroutine(EsperaMuerteFinal());
    }

    IEnumerator EsperaMuerteFinal()
    {
        yield return new WaitForSeconds(0.6f);
      //  explosion.SetActive(false);
        yield return new WaitForSeconds(0.7f);
        //Destroy(gameObject);
    }

    //************************************************************************************************************
    //  RESET
    //************************************************************************************************************
    public virtual void ResetEnemy()
    {
        armadura.armature.animation.timeScale = 1f;
        myRigidbody2D.isKinematic = true;
        for (int i = 0; i < armadura.transform.childCount; i++)
        {
            Destroy(armadura.transform.GetChild(i).gameObject.GetComponent<Rigidbody2D>());
        }
        energy = initEnergy;
    }
}
