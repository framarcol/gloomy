﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DragonBones;
using UnityEngine.UI;

public class GenericEnemyControl : EnemyNormalControl {

    [Tooltip ("El enemigo puede andar.")]
    public bool andar;
    [Tooltip("El enemigo puede correr")]
    public bool correr;
    [Tooltip("- true: el enemigo escala la torre y ataca al prota\n" +
        "- false: el enemigo ataca la torre.")]
    public bool atacaTorre;
    bool esPorton;

    [Space(10)]
    [Tooltip("Use this if armature was created looking to its left instead its right")]
    public bool flipX;
    public string ojoBoneName;
    public string NombreAtaque;
    bool flipInicial, flipInverso;
    float speedInicial, speedInverso;
    float speedRunningInicial, speedRunningInverso;

    public bool reventado;
    //public bool noRetrocede;

    public int poderAtaqueTorre;
    public int oro;
    public float porcentaje;
    public bool daRecompensa;
    int energiaTotal;
    bool hayBarra;
    public GameObject vidaUp;
 //   bool actualizaEstado;
    public int DanoRetroceso;
    public int DanoMaximo;
    public int DanoPorton;

    public GameObject prohibidoDisparar;

[Space(10)]
    Vector3 tempVector3;

    public GameObject oroUp;
    public bool disparable;

    public int idEnemigo;
   public bool segundoEstadoStandard;
    Vector2 coliInicial1, coliInicial2;

    Image barraSalud;
    //DragonBones.AnimationState animationState;

    GameObject levelControl;
    UnityEngine.Transform prota;
    UnityEngine.Transform torre;
    ControladorRecompensas recomp;
  //  AnimationSpeeds animationSpeeds;
    ControladorBarricada barricadaControl;
    ProtaControl protago;
    bool esperandoEventoEndPlayList = false;    //set if waiting for playing list ending event
    public bool iniciado;
    bool comprobarDireccion;
    public bool iniciarPersonajeAutomaticamente;
    public bool oroDado;
    bool unaVezDying;
    bool esperarUnaVezStandard;
    bool danadoPorPorton;

    float tiempoEsperaMuerte;

    GeneradorHordas generador;

    bool pinchando;

   // public float segundosPincho;

        // Standard: id 1
        // huesudo: id 2
        // Standard rompible: id 3
        // Fuerte: id 4
        // Cabezas: id 5
        // enano: id 6
        // loca: id 7

    int numPinchos;
    bool sinMovimiento;
    int originalSorting;
    string animCorrer;

    // Use this for initialization
    private void Awake()
    {
        originalSorting = GetComponentInChildren<UnityArmatureComponent>().sortingOrder;

        flipInicial = flipX;
        flipInverso = !flipX;

        speedInicial = speed;
        speedRunningInicial = speedRunning;

        speedInverso = speed * -1;
        speedRunningInverso = speedRunning * -1;


        Comienzo();
    }

    void Start()
    {
        if (GetComponentInChildren<Image>() == null)
        {
            hayBarra = false;
        }
        else
        {
            barraSalud = GetComponentInChildren<Image>();
            hayBarra = true;
            barraSalud.gameObject.SetActive(false);
        }
        
        Inicio();
    }

    void Comienzo()
    {
        animCorrer = "correr";
        oroDado = false;
        if (idEnemigo == 7)
        {
            int random = Random.Range(0, 3);

            switch (random)
            {
                case 1:
                    animCorrer += "_2";
                    break;
                case 2:
                    animCorrer += "_3";
                    break;
            }

        }

        // armadura.armature.cacheFrameRate = 24;
        disparable = true;

       /* if (!generador.V3)
        {
            if (GetComponentInParent<GeneradorEnemigosV2>() != null)
            {
                comprobarDireccion = GetComponentInParent<GeneradorEnemigosV2>().GetHaciaDerecha();
                if (comprobarDireccion)
                {
                    speedRunning *= -1;
                    speed *= -1;
                    if (flipX) flipX = false;
                    else flipX = true;
                }

            }
        }*/

        if (porcentaje == 0) porcentaje = 0.2f;

        if (Random.value < porcentaje) daRecompensa = true;
        else daRecompensa = false;

    }

    void Inicio()
    {
        energiaTotal = energy;

        if(hayBarra)
        barraSalud.gameObject.SetActive(false);

        animationsPlaylist.DragonBonesEvent += DragonBonesEventListener;
    
        armadura.armature.flipX = flipX;
        SetAngleY(transform.eulerAngles.y);
        initEnergy = energy;

        if (iniciarPersonajeAutomaticamente)
        {
            IniciarPersonaje();
        }

        prohibidoDisparar.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0);
    }

    public void IniciarPersonaje()
    {
        numPinchos = 0;
        if(idEnemigo != 5)
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        else
        {
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionY;
        }
        generador = GameObject.FindGameObjectWithTag("GeneradorHordas").GetComponent<GeneradorHordas>();
        levelControl = GameObject.FindGameObjectWithTag("GameController");
        prota = levelControl.GetComponent<GlobalInfo>().prota;
        torre = levelControl.GetComponent<GlobalInfo>().torre;
        recomp = levelControl.GetComponent<ControladorRecompensas>();
        protago = GameObject.FindGameObjectWithTag("ProtaTag").GetComponent<ProtaControl>();

        if (torre != null)
            torre.GetComponent<TowerControl>().EndColapseTower += OnColapseTower;
        prota.GetComponent<ProtaControl>().ProtaHitTheGround += OnProtaHitTheGround;
        prota.GetComponent<ProtaControl>().ProtaIsKilled += OnProtaIsKilled;

        GetComponentInChildren<UnityArmatureComponent>().sortingOrder = originalSorting;

        if (GetComponentInParent<GeneradorEnemigosV3>() != null)
        {
            comprobarDireccion = GetComponentInParent<GeneradorEnemigosV3>().GetHaciaDerecha();
            if (comprobarDireccion)
            {
                speedRunning = speedRunningInverso;
                speed = speedInverso;
                flipX = flipInverso;

                armadura.armature.flipX = flipX;
            }
            else
            {
                speedRunning = speedRunningInicial;
                speed = speedInicial;
                flipX = flipInicial;

                armadura.armature.flipX = flipX;
            }

        }

        if (prota.GetComponent<ProtaInfo>().energy > 0)
        {
            if (andar)
            {
                ChangeState(EnemStates.esWalking);
            }
            else if (correr)
            {
                ChangeState(EnemStates.esRunning);
            }
        }
        else
        {
            ChangeState(EnemStates.esIdle);
        }

        iniciado = true;
    }

    public void MatarPersonaje()
    {
        if (disparable && iniciado)
        {
            reventado = true;
            ChangeState(EnemStates.esDying);
            energy = -2;
        }
    }

    public void IniciarPersonajePorton()
    {
        esPorton = true;

        IniciarPersonaje();

        GetComponentInChildren<UnityArmatureComponent>().sortingOrder = 1;
    }


    public void MetodoDying()
    {
        if (daRecompensa)
        {
            if (!oroDado)
            {
                GameObject objetovida = Instantiate(oroUp, new Vector2(transform.position.x, transform.position.y), new Quaternion(0, 0, 0, 0), GameObject.FindGameObjectWithTag("InterfazW").transform);
                objetovida.transform.position = transform.position;
                objetovida.GetComponent<AnimacionOro>().EfectoOro(oro, transform.position);
                oroDado = true;
            }
        }

        StartCoroutine(DesactivarObjeto());

        ChangeState(EnemStates.esDead);

    }

    public void MetodoDying2()
    {
        if(torre!=null) torre.GetComponent<TowerControl>().EndColapseTower -= OnColapseTower;
        prota.GetComponent<ProtaControl>().ProtaHitTheGround -= OnProtaHitTheGround;
        prota.GetComponent<ProtaControl>().ProtaIsKilled -= OnProtaIsKilled;
        if (esperandoEventoEndPlayList)
        {
            animationsPlaylist.EndPlaylist -= OnEndAnimation;
            esperandoEventoEndPlayList = false;
        }
        GetComponentInChildren<BoxCollider2D>().enabled = false;

        if (idEnemigo != 3)
        {
            if (idEnemigo != 5)
                animationsPlaylist.PlayAnimation("reposo", 0, -1, 1);
        }
        else {
            animationsPlaylist.PlayAnimation("medio_reposo", 0, -1, 1);
        }
   
        if(idEnemigo != 3)
        {
            Explode();
        }
        else
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.down, ForceMode2D.Impulse);
        }

    }

    // Update is called once per frame
    void Update()
    {
        if(disparable == false && prohibidoDisparar != null)
        {
            prohibidoDisparar.SetActive(true);
        }
        else
        {
            if(prohibidoDisparar != null)
            {
                prohibidoDisparar.SetActive(false);
            }
        
        }


        if(idEnemigo == 3)
        {
            if(transform.localPosition.y < -100)
            {
                MatarPersonaje();
            }
        }


        if (iniciado)
        {
            if (hayBarra)
            {
                barraSalud.fillAmount = (float)energy / (float)energiaTotal;
            }

            if (protago.estoyMuerto)
            {
                ChangeState(EnemStates.esKillingProta);
            }


            switch (state)
            {
                case EnemStates.esWalking:
                    tempVector3 = transform.TransformDirection(Vector3.right) * speed;
                    myRigidbody2D.velocity = new Vector2(tempVector3.x, myRigidbody2D.velocity.y);
                    break;
                case EnemStates.esRunning:
                    if (!sinMovimiento)
                    {
                        if (!segundoEstadoStandard)
                        {
                                tempVector3 = transform.TransformDirection(Vector3.right) * speedRunning;
                                myRigidbody2D.velocity = new Vector2(tempVector3.x, myRigidbody2D.velocity.y);
                        }
                        else
                        {
                            if (esperarUnaVezStandard)
                            {
                                    tempVector3 = transform.TransformDirection(Vector3.right) * speedRunning;
                                    myRigidbody2D.velocity = new Vector2(tempVector3.x, myRigidbody2D.velocity.y);
                            }
                        }
                    }
                    break;
                case EnemStates.esClimbing:
                    myRigidbody2D.velocity = Vector2.up * speed;
                    break;           
                case EnemStates.esDying:
                    if (idEnemigo == 2 || (idEnemigo == 3))
                    {
                        if (segundoEstadoStandard)
                        {
                            if (!unaVezDying)
                            {
                                unaVezDying = true;
                                MetodoDying();
                            }
                        
                        }                                 
                    }
                    else
                    {
                        if (!unaVezDying)
                        {
                            unaVezDying = true;
                            MetodoDying();
                        }
                    }
                    break;
                case EnemStates.esWaitingAnimation:               
                    break;
                case EnemStates.esShooted2:
                  //  Debug.Log("adas");
                    if (segundoEstadoStandard) ChangeState(EnemStates.esRunning);
                    break;
            }
        }
        else
        {
            if (animationsPlaylist.IsPlayingAnimation())
            {
                animationsPlaylist.StopAnimation();
            }
        }
    }
    IEnumerator NoTeMuevas()
    {
        yield return new WaitForSeconds(0.5f);
        sinMovimiento = false;
    }

    IEnumerator DesactivarObjeto()
    {
      //  if (idEnemigo == 3) yield return new WaitForSeconds(0.3f);
      //  if (idEnemigo == 3) this.gameObject.GetComponent<UnityEngine.Transform>().position = new Vector2(1000, 1000);
        yield return new WaitForSeconds(1.1f);
        if (daRecompensa) recomp.SumarOro(oro);
        if(!generador.V3) this.gameObject.SetActive(false);
        else
        {
            ResetEnemy2();
        }
    }

    protected override void ChangeState(EnemStates newState)
    {
            List<AnimationsPlaylist.AnimationNode> lista = new List<AnimationsPlaylist.AnimationNode>();
            switch (newState)
            {
                case EnemStates.esIdle:
                    animationsPlaylist.PlayAnimation("win", 0.3f, -1, 1);
                    break;
                case EnemStates.esWalking:

                switch (idEnemigo)
                {
                    case 4:
                        int randon =/* Random.Range(1, 4)*/ 1;
                        if(randon == 1) lista.Add(new AnimationsPlaylist.AnimationNode("andar", 0f, -1, 1));
                        else if (randon == 2) lista.Add(new AnimationsPlaylist.AnimationNode("andar_" + randon, 0f, -1, 1 / 2));
                        break;
                    case 5:
                        lista.Add(new AnimationsPlaylist.AnimationNode("andar", 0f, -1, 1));
                        break;
                    default:
                        lista.Add(new AnimationsPlaylist.AnimationNode("reposo_andar", 0.1f, 1, 1));
                        lista.Add(new AnimationsPlaylist.AnimationNode("andar", 0f, -1, 1));
                        break;
                }

                   
                    animationsPlaylist.PlayAnimationList(lista);
                    break;
                case EnemStates.esRunning:
                if (!segundoEstadoStandard)
                {

                    lista.Add(new AnimationsPlaylist.AnimationNode("reposo_correr", 0.1f, 1, 1));

                    float aleator= 1 * Random.Range(0.6f, 1.5f);
                    int aleator2 = Random.Range(2, 5);

                    //  Debug.Log(aleator + "/" +  aleator2);

                    if (idEnemigo == 1 || idEnemigo == 3) {
                        lista.Add(new AnimationsPlaylist.AnimationNode("correr_" + aleator2, 0f, -1, aleator));
                    } 
                    else
                    {
                        if(idEnemigo ==6)
                        lista.Add(new AnimationsPlaylist.AnimationNode(animCorrer, 0f, -1, 2.5f));
                        else if(idEnemigo == 0)
                        {
                            lista.Add(new AnimationsPlaylist.AnimationNode(animCorrer, 0f, -1, 0.65f));
                        }
                        else
                        {
                            lista.Add(new AnimationsPlaylist.AnimationNode(animCorrer, 0f, -1, 1));
                        }
                    }

                    if (animationsPlaylist.currentAnimation != "muerte")
                    animationsPlaylist.PlayAnimationList(lista);
                }
                else
                {
                    lista.Add(new AnimationsPlaylist.AnimationNode("medio_reposo", 0.1f, 1, 1));
                    lista.Add(new AnimationsPlaylist.AnimationNode("medio_correr", 0f, -1, 1));
                    StartCoroutine(EsperarCaida(lista));
                }

                break;
                case EnemStates.esClimbing:
                    animationsPlaylist.PlayAnimation("win", 0.3f, -1, 1);
                    break;
                case EnemStates.esAttackTower:
                case EnemStates.esAttackBarricade:
                if (!segundoEstadoStandard)
                {
                    if(idEnemigo != 5)
                    {
                        lista.Add(new AnimationsPlaylist.AnimationNode(NombreAtaque, 0.3f, 1, 1));
                        lista.Add(new AnimationsPlaylist.AnimationNode(NombreAtaque, 0f, -1, 1));
                    }
                }
                else
                {
                    lista.Add(new AnimationsPlaylist.AnimationNode("medio_" + NombreAtaque, 0.3f, 1, 1));
                    lista.Add(new AnimationsPlaylist.AnimationNode("medio_" + NombreAtaque, 0f, -1, 1));
                }
                if(idEnemigo !=5)
                animationsPlaylist.PlayAnimationList(lista);
                    break;
                case EnemStates.esKillingProta:
                animationsPlaylist.PlayAnimation("win", 0f, 1, 1);
                break;
                case EnemStates.esEatingProta:
                animationsPlaylist.PlayAnimation("win", 0f, 1, 1);
                    break;
                case EnemStates.esShooted:
                if (hayBarra)
                    barraSalud.gameObject.SetActive(true);

                if (idEnemigo == 4)
                {
                    if (state != EnemStates.esAttackBarricade && state != EnemStates.esAttackTower && state != EnemStates.esShooted)
                    {
                        newState = EnemStates.esWalking;
                    }
                    else
                    {
                        if (state == EnemStates.esAttackBarricade)
                            newState = EnemStates.esAttackBarricade;
                        else if (state == EnemStates.esAttackTower)
                            newState = EnemStates.esAttackTower;
                        else newState = EnemStates.esAttackBarricade;
                    }
                    
                }
                else
                {
                    if(idEnemigo != 6)
                    {
                        newState = EnemStates.esRunning;
                    }
                    else
                    {
                        if (state != EnemStates.esAttackBarricade)
                        {
                            newState = EnemStates.esRunning;
                        }
                        else
                        {
                            newState = EnemStates.esAttackBarricade;
                        }
                    }                            
                }


                break;
            case EnemStates.esShooted2:

                if (hayBarra)
                    barraSalud.gameObject.SetActive(true);

                if(idEnemigo != 5)
                {
                    sinMovimiento = true;
                    StartCoroutine(NoTeMuevas());
                    if (!segundoEstadoStandard) {

                        if(idEnemigo != 0)
                        {
                            animationsPlaylist.PlayAnimation("recibir_impacto", 0, 1, 1);
                        }
                        else
                        {
                            animationsPlaylist.PlayAnimation("recibir_impacto", 0, 1, 0.75f);
                        }
           
                    } 
                    //else animationsPlaylist.PlayAnimation("medio_recibir_impacto", 0, 1,1);
                }

                if (state != EnemStates.esShooted2)
                {
                    myRigidbody2D.velocity = new Vector2(0f, myRigidbody2D.velocity.y);
                    nextState = state;
                }
                if (!esperandoEventoEndPlayList)
                {
                    animationsPlaylist.EndPlaylist += OnEndAnimation;
                    esperandoEventoEndPlayList = true;
                }
                break;
            case EnemStates.esDying:
                animationsPlaylist.StopAnimation();

                
                if(idEnemigo == 2)
                {
                 //   ChangeState(EnemStates.esWaitingAnimation);
                    GetComponentInChildren<BoxCollider2D>().enabled = false;
                    GetComponentInChildren<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePosition;
                    energy = initEnergy;
                    animationsPlaylist.PlayAnimation("muerte", 0, 1, 1);
                    GetComponent<HuesudoControl>().Resucitar(reventado);
                
                }
                else if(idEnemigo == 3 && !segundoEstadoStandard)
                {
                    if (reventado)
                    {
                        MetodoDying();
                        MetodoDying2();
                    }
                    else
                    {
                        energy = initEnergy / 2;
                        speedRunning = speedRunning / 2;
                        segundoEstadoStandard = true;
                        BoxCollider2D miBox = GetComponentInChildren<BoxCollider2D>();
                        miBox.enabled = false;
                        coliInicial1 = miBox.offset;
                        coliInicial2 = miBox.size;
                        miBox.offset = new Vector2(0.08f, -4.4f);
                        miBox.size = new Vector2(6.31f, 1.77f);

                        animationsPlaylist.PlayAnimation("medio_transicion", 0, 1, 1f);
                        ResetEnemy();
                        newState = EnemStates.esRunning;
                    }               
                }
                else if(idEnemigo == 5)
                {
                    MetodoDying();
                    MetodoDying2();
                }
                else
                {
                    MetodoDying();
                    MetodoDying2();
                }
                      
                break;
            case EnemStates.esDead:
                if (idEnemigo == 3) StartCoroutine(Teletransportar());
                break;
            }
     
            state = newState;
                  
    }

    IEnumerator Teletransportar()
    {
        yield return new WaitForSeconds(0.2f);
        transform.localPosition = new Vector2(1000, 1000);
    }

    IEnumerator EsperarMuerte()
    {
        yield return new WaitForSeconds(tiempoEsperaMuerte);
        MetodoDying();
        MetodoDying2();
    }

    IEnumerator EsperarCaida(List<AnimationsPlaylist.AnimationNode> lista)
    {
        yield return new WaitForSeconds(1);
        animationsPlaylist.PlayAnimationList(lista);
        yield return new WaitForSeconds(1);
        GetComponentInChildren<BoxCollider2D>().enabled = true;
        GetComponentInChildren<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        esperarUnaVezStandard = true;
    }

     //*******************************************************************************************************************
    //  TOWER INTERACTION
    //*******************************************************************************************************************
    //If collides with tower, start climbing
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Mina(Clone)")
        {
            collision.gameObject.GetComponent<ControladorMinas>().Explotar();
        }

        switch (collision.gameObject.tag)
        {
            case "tower":
                if(idEnemigo != 5)
                {
                    torre = collision.transform;

                    if (atacaTorre && (prota.GetComponent<ProtaInfo>().energy > 0))
                    {
                        nextState = EnemStates.esAttackTower;
                        ChangeState(EnemStates.esAttackTower);
                    }
                    else
                    {
                        nextState = EnemStates.esIdle;
                        ChangeState(EnemStates.esIdle);
                    }
                }
                else
                {
                    TowerControl towerControl = torre.GetComponent<TowerControl>();
                    towerControl.ShootReactor(poderAtaqueTorre);

                    energy = 0;

                    MetodoDying();
                    MetodoDying2();

                }
              
                break;

            case "Barricada":

                if (idEnemigo != 5) {

                    barricadaControl = collision.gameObject.GetComponent<ControladorBarricada>();
                    nextState = state;
                    ChangeState(EnemStates.esAttackBarricade);
                }
                else
                {
                    ControladorBarricada barricadaControla = collision.gameObject.GetComponent<ControladorBarricada>();

                    barricadaControla.RecibirDano(poderAtaqueTorre);

                    energy = 0;

                    MetodoDying();
                    MetodoDying2();
                }

                break;
            case "Superviviente":
                tiempoEsperaMuerte = 1.1f;
                Superviviente controladorSuper = collision.gameObject.GetComponent<Superviviente>();
                controladorSuper.Muerte(tiempoEsperaMuerte);
                    energy = 0;
                ChangeState(EnemStates.esAttackBarricade);
                StartCoroutine(EsperarMuerte());
                break;
        }
    }

    IEnumerator EsperarPinchar()
    {
        yield return new WaitForSeconds(IniciarJuego.shopData.GetArma(14).Cadencia[PlayerPrefs.GetInt(SaveGame.nivelesArmas[14])]);
        Pinchar();
    }

    void Pinchar()
    {
        GameObject objetovida = Instantiate(vidaUp, new Vector2(transform.position.x, transform.position.y + 0.2f), new Quaternion(0, 0, 0, 0), GameObject.FindGameObjectWithTag("InterfazW").transform);

        int danoPincho = IniciarJuego.shopData.GetArma(14).Dano[PlayerPrefs.GetInt(SaveGame.nivelesArmas[14])];

        objetovida.GetComponent<AnimacionRestaVida>().EfectoVida(danoPincho);
        ShootReactor(danoPincho);

        StopCoroutine(EsperarPinchar());
        pinchando = false;
       
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Pincho(Clone)")
        {
            if (!pinchando)
            {
                pinchando = true;
                StartCoroutine(EsperarPinchar());
            }
        
        }
    }


    private void OnTriggerEnter2D (Collider2D collider)
    {
        for (int i = 1; i < 20; i++)
        {
            if (collider.gameObject.name == "Atrezzo" + i)
            {
                disparable = false;
            }
        }

        if (esPorton && collider.gameObject.name == "BloqueoPorton")
        {
            disparable = false;
        }

        if (collider.gameObject.name == "ladder")
        {
            if (!atacaTorre)
            {
                nextState = EnemStates.esIdle;
                ChangeState(EnemStates.esIdle);
            }
        }

        if(collider.gameObject.name == "porton")
        {
            if (!danadoPorPorton)
            {
                GameObject objetovida = Instantiate(vidaUp, new Vector2(transform.position.x, transform.position.y + 0.2f), new Quaternion(0, 0, 0, 0), GameObject.FindGameObjectWithTag("InterfazW").transform);

                objetovida.GetComponent<AnimacionRestaVida>().EfectoVida(DanoPorton);

                danadoPorPorton = true;
                ShootReactor(DanoPorton);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        for (int i = 1; i < 20; i++)
        {
            if (collision.gameObject.name == "Atrezzo" + i)
            {
                disparable = true;
            }
        }

        if (esPorton && collision.gameObject.name == "BloqueoPorton")
        {
            disparable = true;
        }

        if (collision.gameObject.name == "ladderTop")
        {
            if (prota.transform.position.x > transform.position.x)
            {
                SetAngleY(0f);
            }
            else
            {
                SetAngleY(180f);
            }

            nextState = EnemStates.esKillingProta;
            ChangeState(EnemStates.esKillingProta);
            myRigidbody2D.velocity = Vector2.zero;
            myRigidbody2D.isKinematic = true;
        }
    }

    //****************************************************************************************************************
    //  Dragon Bones EVENTS LISTENERS
    //****************************************************************************************************************
    private void DragonBonesEventListener (AnimationsPlaylist.DragonBonesEventType type, EventObject eventObject)
    {
       // Debug.Log(eventObject.name);
        switch (state)
        {
            case EnemStates.esKillingProta:
                if (eventObject.name == "PUM")
                {
                    prota.GetComponent<ProtaControl>().RecibirDano(1);
                }
                if ((type == AnimationsPlaylist.DragonBonesEventType.loopComplete) && (prota.GetComponent<ProtaInfo>().energy <= 0))
                {
                    ChangeState(EnemStates.esEatingProta);
                }
                break;
            case EnemStates.esAttackTower:
                if (eventObject.name == "PUM")
                {
                    TowerControl towerControl = torre.GetComponent<TowerControl>();
                    towerControl.ShootReactor(poderAtaqueTorre);
                }
                break;
            case EnemStates.esAttackBarricade:
                if (eventObject.name == "PUM")
                {
                    if(barricadaControl != null)
                    {
                        int vida = barricadaControl.RecibirDano(poderAtaqueTorre);

                        if (vida <= 0)
                        {
                            if(idEnemigo != 4)
                            ChangeState(EnemStates.esRunning);
                            else  ChangeState(EnemStates.esWalking);
                        }                          
                    }
                    else
                    {
                        IniciarPersonaje();
                    }             
                }
                break;
            case EnemStates.esRunning:
                if (eventObject.name == "PUM")
                {
                    if (barricadaControl != null)
                    {
                        int vida = barricadaControl.RecibirDano(poderAtaqueTorre);
                        if (vida <= 0) ChangeState(EnemStates.esRunning);
                    }
                    else if (torre.GetComponent<TowerControl>() != null)
                    {
                        TowerControl towerControl = torre.GetComponent<TowerControl>();
                        towerControl.ShootReactor(poderAtaqueTorre);                      
                    }
                    else
                    {
                        IniciarPersonaje();
                    }
                }
                break;
        }
    }

    private void OnEndAnimation ()
    {
        ChangeState(nextState);
        animationsPlaylist.EndPlaylist -= OnEndAnimation;
        esperandoEventoEndPlayList = false;
    }

    //*****************************************************************************************************************
    //  OTHER METHODS
    //*****************************************************************************************************************
    private void SetAngleY (float n)
    {
        transform.eulerAngles = Vector3.up * n;

        if (idEnemigo != 5) {
      
            if (n > 90.0f)
            {
                armadura.armature.GetSlot(ojoBoneName)._setZorder(-1000);
            }
            else
            {
                armadura.armature.GetSlot(ojoBoneName)._setZorder(1000);
            }
        }
    
    }

    //*****************************************************************************************************************
    //  OTHER EVENTS LISTENERS
    //*****************************************************************************************************************
    private void OnCollapseBarricade ()
    {
        // barricadaControl.ColapseObject -= OnCollapseBarricade;
        ChangeState(nextState);
    }

    private void OnColapseTower ()
    {
        if (energy > 0)
        {
            switch (state)
            {
                 case EnemStates.esIdle:
                    ShootReactor(1);
                    break;
            }
        }
    }

    private void OnProtaHitTheGround ()
    {
        if (energy > 0)
        {
            if (prota.GetComponent<ProtaControl>().torsoPosition.x > transform.position.x)
            {
                SetAngleY(0f);
            }
            else
            {
                SetAngleY(180f);
            }

            ChangeState(EnemStates.esEatingProta);
        }
    }

    private void OnProtaIsKilled ()
    {
        if (energy > 0)
        {
            switch (state)
            {
                case EnemStates.esAttackTower:
                    ChangeState(EnemStates.esIdle);
                    break;            
            }
        }
    }

    public void AnimacionHuesudo()
    {
        animationsPlaylist.PlayAnimation("resurgir", 0, 1, 1);
    }

    //*****************************************************************************************************************
    //  RESET
    //*****************************************************************************************************************

    public override void ResetEnemy()
    {
        armadura.armature.animation.timeScale = 1f;
        if(idEnemigo != 3) GetComponentInChildren<BoxCollider2D>().enabled = true;
        else
        {
            GetComponentInChildren<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        }
        myRigidbody2D.isKinematic = false;
        for (int i = 0; i < armadura.transform.childCount; i++) Destroy(armadura.transform.GetChild(i).gameObject.GetComponent<Rigidbody2D>());
        explosion.SetActive(false);

        if(idEnemigo != 3) energy = initEnergy;

        if (prota.GetComponent<ProtaInfo>().energy > 0)
        {
            if (andar) ChangeState(EnemStates.esWalking);
            else if (correr) ChangeState(EnemStates.esRunning);
        }
        else ChangeState(EnemStates.esIdle);
        SetAngleY(transform.eulerAngles.y);


    }

    public void ResetEnemy2()
    {
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        armadura.armature.animation.timeScale = 1f;
        myRigidbody2D.isKinematic = false;
        GetComponentInChildren<BoxCollider2D>().enabled = true;
        SetAngleY(transform.eulerAngles.y);
        for (int i = 0; i < armadura.transform.childCount; i++) Destroy(armadura.transform.GetChild(i).gameObject.GetComponent<Rigidbody2D>());

        unaVezDying = false;
        iniciado = false;
        reventado = false;
        segundoEstadoStandard = false;

        animationsPlaylist.DragonBonesEvent -= DragonBonesEventListener;

        explosion.SetActive(false);

        energy = initEnergy;

        if (idEnemigo == 2)
        {
            GetComponent<HuesudoControl>().veces =0;
        }


        if (prota.GetComponent<ProtaInfo>().energy > 0)
        {
            if (andar) ChangeState(EnemStates.esWalking);
            else if (correr) ChangeState(EnemStates.esRunning);
        }
        else ChangeState(EnemStates.esIdle);

        transform.localPosition = new Vector2(20, 0);
        if(idEnemigo == 3)
        {
            speedRunning *= 2;
            BoxCollider2D miBox = GetComponentInChildren<BoxCollider2D>();
            miBox.offset = coliInicial1;
            miBox.size = coliInicial2;
            animationsPlaylist.PlayAnimation("reposo", 0, -1, 1);
        }
        Comienzo();
        Inicio();

        GetComponentInParent<ControladorPosicion>().gameObject.SetActive(false);
        transform.parent = GameObject.FindGameObjectWithTag("Enemigos").transform;
    }
}
