﻿using DragonBones;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GordiControl : InteractableObject {

    public enum EnemStates { esWalking, esClimbing, esKilling, esWaitingAnimation, esShooted, esDead, esWalkingShooted };
    [Space(15)]
    public EnemStates state;

    EnemStates nextState;    //Next status after waiting for an animation to end
    Vector3 tempVector3;

    //public Dictionary<string, DragonBonesData> dragonBonesInfo;
    //public DragonBonesData dbData;
    public UnityArmatureComponent armadura;

    // Use this for initialization
    void Start () {
        /*AnimationConfig tempAnimConfig = armadura.armature.animation.animationConfig;
        tempAnimConfig.fadeOutTime = 0.3f;
        armadura.armature.animation.PlayConfig(tempAnimConfig);*/

        // Load data.
        //UnityFactory.factory.LoadDragonBonesData("level/enemies/Gordi/Gordi_ske"); // DragonBones file path (without suffix)
        //UnityFactory.factory.LoadTextureAtlasData("level/enemies/Gordi/Gordi_tex"); //Texture atlas file path (without suffix) 
        // Create armature.
        //var armatureComponent = UnityFactory.factory.BuildArmatureComponent("Armature"); // Input armature name

        //armadura.armature.animation.GotoAndPlayByTime("Caminar", 0, 3);
    }
	
	// Update is called once per frame
	void Update () {
        switch (state)
        {
            case EnemStates.esWalking:
                tempVector3 = transform.TransformDirection(Vector3.right) * speed;
                myRigidbody2D.velocity = new Vector2(tempVector3.x, myRigidbody2D.velocity.y);
                break;
            case EnemStates.esClimbing:
                myRigidbody2D.velocity = Vector2.up * speed;
                break;
            case EnemStates.esWalkingShooted:
                tempVector3 = transform.TransformDirection(Vector3.right) * speed;
                myRigidbody2D.velocity = new Vector2(tempVector3.x, myRigidbody2D.velocity.y);
                if (!armadura.armature.animation.isPlaying)
                {
                    ChangeState(nextState);
                }
                break;
            case EnemStates.esKilling:
                //Tenemos que esperar a que todos los hijos salgan de cámara antes de eliminar el GameObject
                bool hijoVisible = false;
                int i = 0;
                while ((i < armadura.transform.childCount) && !hijoVisible)
                {
                    hijoVisible = hijoVisible | armadura.transform.GetChild(i).GetComponent<Renderer>().isVisible;
                    i++;
                }
                //Aqui sabemos si los hijos han desaparecido o no de pantalla
                if (!hijoVisible)
                {
                    ChangeState(EnemStates.esDead);
                }
                break;
            case EnemStates.esWaitingAnimation:
                if (!armadura.armature.animation.isPlaying)
                {
                    ChangeState(nextState);
                }
                break;
        }
    }

    //If collides with tower, start climbing
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log("He llegado a la torre");
        if (collision.gameObject.name == "tower")
        {
            state = EnemStates.esClimbing;
        }
    }

    //If collides with top of the tower, starts killing the prota
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log("He llegado a la cima de la torre: " + collision.gameObject.name);
        if (collision.gameObject.name == "tower")
        {
            state = EnemStates.esKilling;
            myRigidbody2D.velocity = Vector2.zero;
            myRigidbody2D.isKinematic = true;
        }
    }

    //************************************************************************************************************
    //  ANIMATION METHODS
    //************************************************************************************************************
    /*private void WaitForAnimation()
    {
        //transform.GetComponentInChildren<Animator>().SetTrigger(animation);
        state = EnemStates.esWaitingAnimation;

    }

    public void EndHitAnimation()
    {
        state = EnemStates.esWalking;
    }*/

    public void EndDeadAnimation()
    {
        Debug.Log("MUEROOO!!!");
        Destroy(this.gameObject);
    }
    //************************************************************************************************************
    //  SHOOTS
    //************************************************************************************************************
    public override int ShootReactor(int bulletEnergy)
    {
        int tempEnergy = 0;

        Debug.Log("Me han disparado");
        if (energy > 0) //Initial check to prevent hitting when enem is dead
        {
            tempEnergy = energy;
            energy -= bulletEnergy; //substracting bullet energy to enemy
            //Triggering animation
            if (energy > 0)
            {
                //Debug.Log("hitted!!");
                //WaitForAnimation();
                ChangeState(EnemStates.esWalkingShooted);
            }
            else
            {
                ChangeState(EnemStates.esKilling);
            }
        }

        return tempEnergy;
    }

    void ChangeState (EnemStates newState)
    {
        switch (newState)
        {
            case EnemStates.esWalking:
                armadura.armature.animation.Play("Caminar");
                state = EnemStates.esWalking;
                break;
            case EnemStates.esWalkingShooted:
                /*AnimationConfig tempAnimConfig = armadura.armature.animation.animationConfig;
                tempAnimConfig.fadeOutTime = 0.3f;
                armadura.armature.animation.PlayConfig(tempAnimConfig);*/

                armadura.armature.animation.GotoAndPlayByProgress("Caminar_daño", 0, 1);
                //Debug.Log("hitted!!");
                nextState = EnemStates.esWalking;
                state = EnemStates.esWalkingShooted;
                break;
            case EnemStates.esShooted:
                armadura.armature.animation.GotoAndPlayByProgress("Reposo_daño", 0, 1);
                if (state != EnemStates.esWaitingAnimation)
                {
                    nextState = state;
                }
                state = EnemStates.esWaitingAnimation;
                break;
            case EnemStates.esKilling:
                /*armadura.armature.animation.GotoAndStopByTime("muerte normal", 4.0f);
                //Debug.Log("hitted!!");
                nextState = EnemStates.esDead;
                state = EnemStates.esWaitingAnimation;*/
                state = EnemStates.esKilling;
                Explode();
                break;
            case EnemStates.esDead:
                EndDeadAnimation();
                break;
        }
    }

    void Explode ()
    {
        /*foreach (string animName in armadura.armature.animation.animationNames)
        {
            armadura.armature.animation.Stop(animName);
        }*/
        armadura.armature.animation.timeScale = 0f;
        //Destroy(armadura.GetComponent<UnityArmatureComponent>());

        speed = 0;
        Rigidbody tempRigidbody;
        for (int i = 0; i < armadura.transform.childCount; i++)
        {
            tempRigidbody = armadura.transform.GetChild(i).gameObject.AddComponent<Rigidbody>();
            tempRigidbody.mass = 200f;
            tempRigidbody.AddForce((armadura.transform.GetChild(i).position - transform.position) * 400, ForceMode.Impulse);
        }
    }
}
