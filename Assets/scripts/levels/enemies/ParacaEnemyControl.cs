﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DragonBones;
using UnityEngine.UI;

public class ParacaEnemyControl : EnemyNormalControl {

    [Space(10)]
    public string ojoBoneName;

    public int oro;
    public float porcentaje;
   public bool daRecompensa;
    public MeshRenderer imagenParachute;

    GameObject levelControl;
    UnityEngine.Transform prota;
    UnityEngine.Transform torre;
    public GameObject oroUp;
    public GameObject vidaUp;

    ControladorRecompensas recomp;
    public bool oroDado;

    private ParacaEnemyParacaidasControl paracaEnemyParacaidasControl;
    bool aLaDerechaDelProta;
    float distanciaAlProta = 0.5f;
    float alturaAlProta = 0;
    Vector2 destinoAtaque;
    bool esperandoEventoEndPlayList = false;    //set if waiting for playing list ending event
    bool iniciado;
    public bool impenetrable;
    bool orientacionCambiada;
    bool danadoPorPorton;
    public bool disparable;

    public GameObject prohibidoDisparar;

    bool ojoIzq;
    bool flipX;
    public int DanoRetroceso;
    public int DanoPorton;
    GameObject prota2;
    GeneradorHordas generador;

    public UnityEngine.Transform OjoPorOjo;

    bool supervivienteMuerto;

    int originalSorting;

    float tiempoEsperaMuerte;

    private void Awake()
    {
        originalSorting = GetComponentInChildren<UnityArmatureComponent>().sortingOrder;

        impenetrable = false;
        disparable = true;
    }

    void Start()
    {
        if (armadura == null)
        {
            armadura = transform.GetComponent<UnityArmatureComponent>();
        }
        if (animationsPlaylist == null)
        {
            animationsPlaylist = transform.GetComponent<AnimationsPlaylist>();
        }
        if (myRigidbody2D == null)
        {
            myRigidbody2D = transform.GetComponent<Rigidbody2D>();
        }

        initEnergy = energy;

        if (porcentaje == 0) porcentaje = 0.2f;

        if (Random.value < porcentaje)
        {
            daRecompensa = true;
        }
        else
        {
            daRecompensa = false;
        }
    }

    public void IniciarPersonaje()
    {
        impenetrable = false;
        orientacionCambiada = false;
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        generador = GameObject.FindGameObjectWithTag("GeneradorHordas").GetComponent<GeneradorHordas>();

        levelControl = GameObject.FindGameObjectWithTag("GameController");
        prota = levelControl.GetComponent<GlobalInfo>().prota;
        torre = levelControl.GetComponent<GlobalInfo>().torre;
        animationsPlaylist.DragonBonesEvent += DragonBonesEventListener;
        recomp = levelControl.GetComponent<ControladorRecompensas>();

        if (torre != null)
            torre.GetComponent<TowerControl>().DamagedTower += OnTowerDamaged;
        prota.GetComponent<ProtaControl>().ProtaHitTheGround += OnProtaHitTheGround;
        prota.GetComponent<ProtaControl>().ProtaIsKilled += OnProtaIsKilled;

        armadura.armature.flipX = true;

        distanciaAlProta = 0;
        paracaEnemyParacaidasControl = GetComponentInChildren<ParacaEnemyParacaidasControl>();
        aLaDerechaDelProta = GetALaDerechaDelProta();

        distanciaAlProta = distanciaAlProta * 1.15f;

        ProtaInfo protaInforma = GameObject.FindGameObjectWithTag("ProtaTag").GetComponent<ProtaInfo>();

        if (protaInforma.esCentro) alturaAlProta = 0.4f;
        else  alturaAlProta = 0.6f;


        destinoAtaque = GetDestination(distanciaAlProta, alturaAlProta);

        GetComponentInChildren<UnityArmatureComponent>().sortingOrder = originalSorting;
        ChangeState(EnemStates.esRunning);
        iniciado = true;
    }

    public void MatarPersonaje()
    {
        if (disparable && iniciado)
        {
            ChangeState(EnemStates.esDying);
            energy = -2;
        }
    }

    public void IniciarPersonajePorton()
    {
        IniciarPersonaje();

        GetComponentInChildren<UnityArmatureComponent>().sortingOrder = 1;

    }

    // Update is called once per frame
    void Update () {

        if (disparable == false && prohibidoDisparar != null)
        {
            prohibidoDisparar.SetActive(true);
        }
        else
        {
            prohibidoDisparar.SetActive(false);
        }

        if (iniciado)
        {        
            prota2 = GameObject.FindGameObjectWithTag("VictimasController");

            if(ojoIzq)
            imagenParachute.transform.position = new Vector3(imagenParachute.transform.position.x, imagenParachute.transform.position.y, -4);
            else imagenParachute.transform.position = new Vector3(imagenParachute.transform.position.x, imagenParachute.transform.position.y, 0);


            switch (state)
            {
                case EnemStates.esRunning:
                     if ((aLaDerechaDelProta && (transform.position.x > destinoAtaque.x)) ||
                        (!aLaDerechaDelProta && (transform.position.x < destinoAtaque.x)))
                    {
                        myRigidbody2D.velocity = (destinoAtaque - new Vector2(transform.position.x, transform.position.y)).normalized * speed;
                    }
                    else
                    {
                        myRigidbody2D.velocity = Vector2.zero;
                        destinoAtaque = GetDestination(distanciaAlProta, alturaAlProta);
                        ChangeState(EnemStates.esWalking);
                    }
                    break;
                case EnemStates.esWalking:
                    if (Vector2.Distance(transform.position, destinoAtaque) > 0.1f)
                    {
                        myRigidbody2D.velocity = (destinoAtaque - new Vector2(transform.position.x, transform.position.y)).normalized * speed;
                    }
                    else
                    {
                        myRigidbody2D.velocity = Vector2.zero;
                        ChangeState(EnemStates.esKillingProta);
                    }
                    break;
                case EnemStates.esGoingEating:
                    if (Vector2.Distance(transform.position, destinoAtaque) > 0.3f)
                    {
                        myRigidbody2D.velocity = (destinoAtaque - new Vector2(transform.position.x, transform.position.y)).normalized * speed;
                    }
                    else
                    {
                        myRigidbody2D.velocity = Vector2.zero;
                        ChangeState(EnemStates.esWalking);
                    }
                    break;
                case EnemStates.esDying:
                    impenetrable = true;
                    if (daRecompensa)
                    {
                        if (!oroDado)
                        {
                            GameObject objetovida = Instantiate(oroUp, transform.position, new Quaternion(0, 0, 0, 0), GameObject.FindGameObjectWithTag("InterfazW").transform);
                            objetovida.GetComponent<AnimacionOro>().EfectoOro(oro, transform.position);
                            oroDado = true;
                        }
                    }

                    if (animationsPlaylist.IsPlayingAnimation())
                    {
                         animationsPlaylist.StopAnimation();
                    }
                   
                    bool hijoVisible = false;
                    int i = 0;
                    while ((i < armadura.transform.childCount) && !hijoVisible)
                    {
                        hijoVisible = hijoVisible | armadura.transform.GetChild(i).GetComponent<Renderer>().isVisible;
                        i++;
                    }
                  
                    if (!hijoVisible)
                    {
                        ChangeState(EnemStates.esDead);
                    }
                    break;
            }
        }
        else
        {
            if (animationsPlaylist.IsPlayingAnimation())
            {
                animationsPlaylist.StopAnimation();
            }
        }
		
	}


    //*****************************************************************************************************************
    //  CHANGE STATE
    //*****************************************************************************************************************
    protected override void ChangeState(EnemStates newState)
    {
        List<AnimationsPlaylist.AnimationNode> lista = new List<AnimationsPlaylist.AnimationNode>();
        switch (newState)
        {
            case EnemStates.esIdle:
                myRigidbody2D.velocity = Vector2.zero;
                animationsPlaylist.PlayAnimation("correr", 0.3f, -1,1);
                break;
            case EnemStates.esRunning:
                if (destinoAtaque.x > transform.position.x)
                {
                    SetAngleY(0f);
                }
                else
                {
                    SetAngleY(180f);
                }
                animationsPlaylist.PlayAnimation("correr", 0.3f, -1, 1 * Random.Range(0.3f, 1.8f));
                break;
            case EnemStates.esKillingProta:
                if (!GameObject.FindGameObjectWithTag("ProtaTag").GetComponent<ProtaControl>().frontal)
                {        
                    paracaEnemyParacaidasControl.Release(speed);
                    lista.Add(new AnimationsPlaylist.AnimationNode("tocar_suelo", 0.1f, 1, 1));
                    lista.Add(new AnimationsPlaylist.AnimationNode("atacar", 0f, -1, 1));
                    animationsPlaylist.PlayAnimationList(lista);
                }
                else newState = EnemStates.esDying;

                break;
            case EnemStates.esWalking:
                if (destinoAtaque.x > transform.position.x)
                {
                    SetAngleY(0f);
                }
                else
                {
                    SetAngleY(180f);
                }
                animationsPlaylist.PlayAnimation("bajar_paracaidas", 0.3f, 1, 1);
                break;
            case EnemStates.esGoingEating:
                if (destinoAtaque.x > transform.position.x) SetAngleY(0f);
                else  SetAngleY(180f);
                animationsPlaylist.PlayAnimation("correr", 0.3f, -1, 1);
                break;
            case EnemStates.esEatingProta:
                animationsPlaylist.PlayAnimation("comer", 0f, 1,1);
                break;
            case EnemStates.esShooted2:
                if (disparable) newState = EnemStates.esRunning;
                break;
            case EnemStates.esShooted:
                if (disparable)
                {
                    switch (state)
                    {
                        case EnemStates.esKillingProta:
                            animationsPlaylist.PlayAnimation("recibir_impacto", 0.1f, 1, 1);
                            if (state != EnemStates.esShooted) nextState = state;
                            break;
                        default:
                            animationsPlaylist.PlayAnimation("recibir_impacto_paracaidas", 0.1f, 1, 1);
                            if (state != EnemStates.esShooted)
                            {
                                myRigidbody2D.velocity = Vector2.zero;
                                myRigidbody2D.AddForce((transform.position - prota.position).normalized * 20f, ForceMode2D.Impulse);
                                nextState = state;
                            }
                            break;
                    }
                    if (!esperandoEventoEndPlayList)
                    {
                        animationsPlaylist.EndPlaylist += OnEndAnimation;
                        esperandoEventoEndPlayList = true;
                    }
               
                }
                break;
            case EnemStates.esDying:

                StartCoroutine(DesactivarObjeto());

                if(torre != null)
                torre.GetComponent<TowerControl>().DamagedTower -= OnTowerDamaged;
                prota.GetComponent<ProtaControl>().ProtaHitTheGround -= OnProtaHitTheGround;
                prota.GetComponent<ProtaControl>().ProtaIsKilled -= OnProtaIsKilled;
                if (esperandoEventoEndPlayList)
                {
                    animationsPlaylist.EndPlaylist -= OnEndAnimation;
                    esperandoEventoEndPlayList = false;
                }
                paracaEnemyParacaidasControl.Release(speed);
                animationsPlaylist.StopAnimation();
                Explode();
                break;
            case EnemStates.esDead:
                if (oroDado) recomp.SumarOro(oro);
                break;
        }
        state = newState;
    }

    IEnumerator DesactivarObjeto()
    {
        yield return new WaitForSeconds(1.1f);
        if (daRecompensa) recomp.SumarOro(oro);
        if (!generador.V3) this.gameObject.SetActive(false);
        else ResetEnemy();
    }

    //****************************************************************************************************************
    //  Dragon Bones EVENTS LISTENERS
    //****************************************************************************************************************
    private void DragonBonesEventListener(AnimationsPlaylist.DragonBonesEventType type, EventObject eventObject)
    {
        switch (state)
        {
            case EnemStates.esKillingProta:
                if (eventObject.name == "PUM")
                {
                    prota.GetComponent<ProtaControl>().RecibirDano(1);
                }
                if ((type == AnimationsPlaylist.DragonBonesEventType.loopComplete) && (prota.GetComponent<ProtaInfo>().energy <= 0))
                {
                    ChangeState(EnemStates.esEatingProta);
                }
                break;
        }
    }

    private void OnEndAnimation()
    {
        //Debug.Log("Recibo evento, paso a " + nextState);
        ChangeState(nextState);
        animationsPlaylist.EndPlaylist -= OnEndAnimation;
        esperandoEventoEndPlayList = false;
    }

    //*****************************************************************************************************************
    //  OTHER EVENTS LISTENERS
    //*****************************************************************************************************************

    private void OnTowerDamaged (int tEnergy)
    {
        if (tEnergy <= 0) ChangeState(EnemStates.esIdle);
    }

    private void OnProtaHitTheGround ()
    {
        switch (state)
        {
            case EnemStates.esIdle:
            case EnemStates.esRunning:
            case EnemStates.esWalking:
                if (transform.position.x > prota.position.x)
                    aLaDerechaDelProta = true;
                else
                    aLaDerechaDelProta = false;

                BoxCollider2D protaBoxCollider2D = prota.GetComponent<BoxCollider2D>();
                destinoAtaque = GetDestination(distanciaAlProta, 0.15f);
                destinoAtaque += protaBoxCollider2D.offset;
                destinoAtaque.y -= protaBoxCollider2D.size.y;
                ChangeState(EnemStates.esGoingEating);
                break;
        }
    }

    private void OnProtaIsKilled ()
    {
        switch (state)
        {
            case EnemStates.esRunning:
            case EnemStates.esWalking:
                if (transform.position.x > prota.position.x)
                    aLaDerechaDelProta = true;
                else
                    aLaDerechaDelProta = false;

                destinoAtaque = GetDestination(distanciaAlProta * 5f, alturaAlProta);
                ChangeState(EnemStates.esRunning);
                break;
            case EnemStates.esKillingProta:
                ChangeState(EnemStates.esEatingProta);
                break;
        }
    }

    //*****************************************************************************************************************
    //  RESET
    //*****************************************************************************************************************

    public override void ResetEnemy()
    {
        iniciado = false;
        oroDado = false;

        if (porcentaje == 0) porcentaje = 0.2f;

        if (Random.value < porcentaje) daRecompensa = true;
        else daRecompensa = false;

        armadura.armature.animation.timeScale = 1f;
        for (int i = 0; i < armadura.transform.childCount; i++)
        {
            Destroy(armadura.transform.GetChild(i).gameObject.GetComponent<Rigidbody2D>());
        }
        explosion.SetActive(false);
        paracaEnemyParacaidasControl.ResetMe();

        energy = initEnergy;
        aLaDerechaDelProta = GetALaDerechaDelProta();
        destinoAtaque = GetDestination(distanciaAlProta * 5f, alturaAlProta);


        if (prota.GetComponent<ProtaInfo>().energy > 0) ChangeState(EnemStates.esRunning);
        else
        {
            if (prota.GetComponent<ProtaInfo>().state == ProtaControl.ProtaEstados.peCaer) OnProtaHitTheGround();
            else OnProtaIsKilled();
        }

        transform.localPosition = new Vector2(20, 0);
        GetComponentInParent<ControladorPosicion>().gameObject.SetActive(false);
        transform.parent = GameObject.FindGameObjectWithTag("Enemigos").transform;
    }

    //*****************************************************************************************************************
    //  OTHER METHODS
    //*****************************************************************************************************************
    private void SetAngleY(float n)
    {
        if (!orientacionCambiada)
        {

            orientacionCambiada = true;

            transform.eulerAngles = Vector3.up * n;
            if (n > 90.0f)
            {
                armadura.armature.GetSlot(ojoBoneName)._setZorder(-1000);
                ojoIzq = false;
            }
            else
            {
                armadura.armature.GetSlot(ojoBoneName)._setZorder(1000);
                ojoIzq = true;
            }
        }
       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        for (int i = 1; i < 20; i++)
        {
         //   Debug.Log(collision.gameObject.name);
            if (collision.gameObject.name == "Atrezzo" + i)
            {
              //  Debug.Log("hola");
                disparable = false;
            }
        }

        if (collision.gameObject.name == "porton")
        {
            if (!danadoPorPorton)
            {
                GameObject objetovida = Instantiate(vidaUp, new Vector2(transform.position.x, transform.position.y + 0.2f), new Quaternion(0, 0, 0, 0), GameObject.FindGameObjectWithTag("InterfazW").transform);

                objetovida.GetComponent<AnimacionRestaVida>().EfectoVida(DanoPorton);

                danadoPorPorton = true;
                ShootReactor(DanoPorton);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        for (int i = 1; i < 20; i++)
        {
            if (collision.gameObject.name == "Atrezzo" + i)
            {
                disparable = true;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<Superviviente>() != null && !supervivienteMuerto)
        {
            supervivienteMuerto = true;
            tiempoEsperaMuerte = 1.1f;
            Superviviente controladorSuper = collision.gameObject.GetComponent<Superviviente>();
            controladorSuper.Muerte(tiempoEsperaMuerte);
            energy = 0;
            //ChangeState(EnemStates.esAttackBarricade);
            //animationsPlaylist.PlayAnimation("comer", 0f, -1, animationSpeeds.comer);
            StartCoroutine(EsperarMuerte());
            //MetodoDying2();
        }
    }

    IEnumerator EsperarMuerte()
    {
        yield return new WaitForSeconds(tiempoEsperaMuerte);
        ChangeState(EnemStates.esDying);
        //Explode();
    }

    private Vector2 GetDestination (float howNear, float howHigh)
    {
        Vector2 destiny = new Vector2(0,0);
        if (torre != null) {
            destiny = prota.position;
        }
          
        else
        {
            prota2 = GameObject.FindGameObjectWithTag("VictimasController");
            destiny = new Vector2(prota2.transform.position.x+0.81f, prota2.transform.position.y-1.91f);
        }
           
        destiny.y += howHigh;
        if (aLaDerechaDelProta)
        {
            destiny.x += howNear;
        } else
        {
            destiny.x -= howNear;
        }

        return destiny;
    }

    private bool GetALaDerechaDelProta ()
    {
        bool i;
        if (transform.position.x > prota.position.x) i = true;
        else i = false;

        return i;
    }
}
