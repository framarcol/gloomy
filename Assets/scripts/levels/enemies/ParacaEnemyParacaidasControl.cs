﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DragonBones;

public class ParacaEnemyParacaidasControl : MonoBehaviour {

    public UnityArmatureComponent armadura;

    private UnityEngine.Transform oldParent;
    private Vector3 oldLocalPosition;
    private Rigidbody2D myRigidbody2D;
    private bool hijoVisible = false;
    private int i = 0;

    // Use this for initialization
    void Start () {

        if (armadura == null)
        {
            armadura = transform.GetComponent<UnityArmatureComponent>();
        }
        armadura.armature.flipX = true;

        if (myRigidbody2D == null)
        {
            myRigidbody2D = transform.GetComponent<Rigidbody2D>();
        }
        enabled = false;
        myRigidbody2D.isKinematic = true;
        oldParent = transform.parent;
        oldLocalPosition = transform.localPosition;
    }
	
	// Update is called once per frame
	void Update () {
        myRigidbody2D.AddForce(Vector2.up * 10f);
    }

    public void Flipear(bool flipX)
    {
        armadura.armature.flipX = flipX;
    }

    public void Release (float initialSpeed)
    {
        //Debug.Log("Soltando paracaidas: " + initialSpeed);
        transform.parent = null;
        myRigidbody2D.isKinematic = false;

        myRigidbody2D.velocity = transform.TransformDirection(Vector2.right) * initialSpeed*3;
        myRigidbody2D.AddForce(Vector2.up*6, ForceMode2D.Impulse);
        enabled = true;
    }

    public void ResetMe ()
    {
        transform.parent = oldParent;
        transform.localPosition = oldLocalPosition;
        myRigidbody2D.isKinematic = true;
        myRigidbody2D.velocity = Vector3.zero;
        enabled = false;
        gameObject.SetActive(true);
    } 

}
