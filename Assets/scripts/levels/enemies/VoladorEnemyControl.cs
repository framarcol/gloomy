﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DragonBones;

public class VoladorEnemyControl : EnemyNormalControl {

    //public UnityArmatureComponent armadura;
    //AnimationsPlaylist animationsPlaylist;
   // AnimationSpeeds animationSpeeds;
    BoxCollider2D myBoxCollider2D;

    public int oro;
    public float porcentaje;
    public bool daRecompensa;
    public GameObject OroUp;
    public GameObject vidaUp;

    ControladorRecompensas recomp;

    //public float speedRunning;
    [Space(10)]
    public string ojoBoneName;
    public int DistanciaMordida;
    public int DanoRetroceso;
    public int DanoPorton;

    GameObject levelControl;
    UnityEngine.Transform prota;
    UnityEngine.Transform prota2;
    UnityEngine.Transform torre;

    bool aLaDerechaDelProta;
   // public float distanciaAlProta;
    Vector2 destinoAtaque;
    float sinusTime;    //Variable to make enemy fly in a sinusoidal way
    bool esperandoEventoEndPlayList = false;    //set if waiting for playing list ending event
    bool comprobarDireccion;
    public bool iniciado;
    public bool oroDado;
    public bool impenetrable;
    public bool disparable;

    //enum EnemStates { esIdle, esRunning, esKillingProta, esGoingEating, esEatingProta, esShooted, esDying, esDead };
    //EnemStates state;
    //EnemStates nextState;
    public GameObject prohibidoDisparar;

    bool danadoPorPorton;
    bool colisionadoSuperviviente;

    int originalSorting;

    float tiempoEsperaMuerte;

    GeneradorHordas generador;
    string correr;

    private void Awake()
    {
        originalSorting = GetComponentInChildren<UnityArmatureComponent>().sortingOrder;
      
        impenetrable = false;
        disparable = true;
     /*   if (GetComponentInParent<GeneradorEnemigosV2>() != null)
        {
            comprobarDireccion = GetComponentInParent<GeneradorEnemigosV2>().GetHaciaDerecha();
            if (comprobarDireccion)
            {
                speedRunning *= -1;
            }

        }*/
    }

    private void Start()
    {
        if (armadura == null)
        {
            armadura = transform.GetComponent<UnityArmatureComponent>();
        }
        if (animationsPlaylist == null)
        {
            animationsPlaylist = transform.GetComponent<AnimationsPlaylist>();
        }
        if (myRigidbody2D == null)
        {
            myRigidbody2D = transform.GetComponent<Rigidbody2D>();
        }
        if (myBoxCollider2D == null)
        {
            myBoxCollider2D = transform.GetComponentInChildren<BoxCollider2D>();
        }
 
        initEnergy = energy;
    }

    public void IniciarPersonaje()
    {
        impenetrable = false;
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
        levelControl = GameObject.FindGameObjectWithTag("GameController");
        prota = levelControl.GetComponent<GlobalInfo>().prota;
        torre = levelControl.GetComponent<GlobalInfo>().torre;

        if (torre != null)
        {
            prota2 = prota;
        }
        else
        {
            prota2 = GameObject.FindGameObjectWithTag("VictimasController").GetComponentInChildren<RectTransform>();
        }


        recomp = levelControl.GetComponent<ControladorRecompensas>();
        animationsPlaylist.DragonBonesEvent += DragonBonesEventListener;
        if (torre != null)
            torre.GetComponent<TowerControl>().DamagedTower += OnTowerDamaged;

        prota.GetComponent<ProtaControl>().ProtaHitTheGround += OnProtaHitTheGround;
        prota.GetComponent<ProtaControl>().ProtaIsKilled += OnProtaIsKilled;

        aLaDerechaDelProta = GetALaDerechaDelProta();

        int random = Random.Range(0, 2);

        correr = "correr";

        if (random == 0)
        {
            correr += "_2";
        }

        GetComponentInChildren<UnityArmatureComponent>().sortingOrder = originalSorting;

        ChangeState(EnemStates.esRunning);
        
        if (porcentaje == 0) porcentaje = 0.2f;

        if (Random.value < porcentaje)
        {
            daRecompensa = true;
        }
        else
        {
            daRecompensa = false;
        }

        iniciado = true;

        prohibidoDisparar.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0);
    }

    public void MatarPersonaje()
    {
        if (disparable && iniciado)
        {
            ChangeState(EnemStates.esDying);
            energy = -2;
        }
    }

    public void IniciarPersonajePorton()
    {
        IniciarPersonaje();

        GetComponentInChildren<UnityArmatureComponent>().sortingOrder = 1;
    }

    // Update is called once per frame
    void Update () {

        if (disparable == false && prohibidoDisparar != null)
        {
            prohibidoDisparar.SetActive(true);
        }
        else
        {
            prohibidoDisparar.SetActive(false);
        }

        if (iniciado)
        {
            if(torre == null)
            prota2 = GameObject.FindGameObjectWithTag("VictimasController").GetComponentInChildren<RectTransform>();

            switch (state)
            {
                case EnemStates.esRunning:
                    sinusTime += Time.deltaTime;
                    //Debug.Log(Vector2.Distance(prota2.position, transform.position).ToString());
                    if (Vector2.Distance(prota2.position, transform.position) > DistanciaMordida)
                   // if ((aLaDerechaDelProta && (transform.position.x > destinoAtaque.x)) ||
                   //     (!aLaDerechaDelProta && (transform.position.x < destinoAtaque.x)))
                    {
                        myRigidbody2D.velocity = (destinoAtaque - new Vector2(transform.position.x, transform.position.y)).normalized * speed +
                                                 (Vector2.up * Mathf.Sin(sinusTime * 6.2f * 1) * 1f);
                    }
                    else
                    {
                        myRigidbody2D.velocity = Vector2.zero;
                        ChangeState(EnemStates.esKillingProta);
                    }
                    break;
                case EnemStates.esGoingEating:
                    sinusTime += Time.deltaTime;
                    if (Vector2.Distance(transform.position, destinoAtaque) > 0.1f)
                    {
                        myRigidbody2D.velocity = (destinoAtaque - new Vector2(transform.position.x, transform.position.y)).normalized * speed +
                                                 (Vector2.up * Mathf.Sin(sinusTime * 6.2f * 1) * 1f);
                        //If this enemy in near enough of the prota, we can do it to go through walls to eat him to avoid the tower
                        //colliding woth it and not allowing it to eat some delicious human flesh
                        if ((Vector2.Distance(transform.position, destinoAtaque) < 1.3f) && !myBoxCollider2D.isTrigger)
                        {
                            myBoxCollider2D.isTrigger = true;
                        }

                    }
                    else
                    {
                        myRigidbody2D.velocity = Vector2.zero;
                        if (!prota.GetComponent<ProtaControl>().frontal)
                            ChangeState(EnemStates.esEatingProta);
                        else
                        {
                            iniciado = false;
                        }
 
                    }
                    break;
                case EnemStates.esDying:
                    impenetrable = true;
                    if (daRecompensa)
                    {
                        //  GameObject objetoOro = Instantiate(vidaUp, shootPos, new Quaternion(0, 0, 0, 0), GameObject.FindGameObjectWithTag("InterfazW").transform);

                        if (daRecompensa)
                        {
                            if (!oroDado)
                            {
                                GameObject objetovida = Instantiate(OroUp, transform.position, new Quaternion(0, 0, 0, 0), GameObject.FindGameObjectWithTag("InterfazW").transform);

                                objetovida.GetComponent<AnimacionOro>().EfectoOro(oro, transform.position);
                                oroDado = true;
                             }
                        }
                    }         
                    
                    //Medida preventiva, por si DragonBones ha tardado en responder al stop
                    if (animationsPlaylist.IsPlayingAnimation())
                    {
                        //Debug.Log("Lanzadas contramedidas");
                        animationsPlaylist.StopAnimation();
                    }
                    //Tenemos que esperar a que todos los hijos salgan de cámara antes de eliminar el GameObject
                    bool hijoVisible = false;
                    int i = 0;
                    while ((i < armadura.transform.childCount) && !hijoVisible)
                    {
                        hijoVisible = hijoVisible | armadura.transform.GetChild(i).GetComponent<Renderer>().isVisible;
                        i++;
                    }
                    //Aqui sabemos si los hijos han desaparecido o no de pantalla
                    if (!hijoVisible)
                    {
                        ChangeState(EnemStates.esDead);
                    }
                    break;
            }
        }
        else
        {
            if (animationsPlaylist.IsPlayingAnimation())
            {
                animationsPlaylist.StopAnimation();
            }
        }

    }


    //*****************************************************************************************************************
    //  CHANGE STATE
    //*****************************************************************************************************************
    protected override void ChangeState(EnemStates newState)
    {
        //List<AnimationsPlaylist.AnimationNode> lista = new List<AnimationsPlaylist.AnimationNode>();
        switch (newState)
        {
            case EnemStates.esIdle:
                myRigidbody2D.velocity = Vector2.zero;
                animationsPlaylist.PlayAnimation("reposo", 0.3f, -1, 1);
                break;
            case EnemStates.esRunning:
                destinoAtaque = GetDestination(0);
                //Debug.Log(destinoAtaque);
                if (destinoAtaque.x > transform.position.x)
                {
                    SetAngleY(0f);
                }
                else
                {
                    SetAngleY(180f);
                }
                animationsPlaylist.PlayAnimation(correr, 0.3f, -1, 1);
                sinusTime = 0f;
                break;
            case EnemStates.esKillingProta:
           //     GameObject.FindGameObjectWithTag("ProtaTag").GetComponent<ProtaControl>().disparable = false;
                animationsPlaylist.PlayAnimation("atacar", 0f, -1, 1);
                break;
            case EnemStates.esGoingEating:
                if (destinoAtaque.x > transform.position.x)
                {
                    SetAngleY(0f);
                }
                else
                {
                    SetAngleY(180f);
                }
                animationsPlaylist.PlayAnimation(correr, 0.3f, -1,1);
                sinusTime = 0f;
                break;
            case EnemStates.esEatingProta:
                animationsPlaylist.PlayAnimation("comer", 0f, -1, 1);
                break;
            case EnemStates.esShooted2:
              newState = EnemStates.esRunning;
                break;
            case EnemStates.esShooted:
                if (disparable)
                {
                    animationsPlaylist.PlayAnimation("recibir_impacto", 0.1f, 1, 1.5f);
                    if (state != EnemStates.esShooted)
                    {
                        myRigidbody2D.velocity = Vector2.zero;
                        myRigidbody2D.AddForce((transform.position - prota2.position).normalized * 20f, ForceMode2D.Impulse);
                        nextState = state;
                    }
                    if (!esperandoEventoEndPlayList)
                    {
                        animationsPlaylist.EndPlaylist += OnEndAnimation;
                        esperandoEventoEndPlayList = true;
                    }
                }      
                break;
            case EnemStates.esDying:
                if (torre != null)
                    torre.GetComponent<TowerControl>().DamagedTower -= OnTowerDamaged;

                if (prota != null)
                {
                    prota.GetComponent<ProtaControl>().ProtaHitTheGround -= OnProtaHitTheGround;
                    prota.GetComponent<ProtaControl>().ProtaIsKilled -= OnProtaIsKilled;
                }
                if (esperandoEventoEndPlayList)
                {
                    animationsPlaylist.EndPlaylist -= OnEndAnimation;
                    esperandoEventoEndPlayList = false;
                }
                animationsPlaylist.StopAnimation();
                Explode();
                break;
            case EnemStates.esDead:        
                if (oroDado) recomp.SumarOro(oro);
                    ResetEnemy();
                break;
        }
        state = newState;
    }

    //****************************************************************************************************************
    //  Dragon Bones EVENTS LISTENERS
    //****************************************************************************************************************
    private void DragonBonesEventListener(AnimationsPlaylist.DragonBonesEventType type, EventObject eventObject)
    {
        if (iniciado)
        {
            switch (state)
            {
                case EnemStates.esKillingProta:
                    if (eventObject.name == "PUM")
                    {
                        prota.GetComponent<ProtaControl>().RecibirDano(1);
                    }
                    if ((type == AnimationsPlaylist.DragonBonesEventType.loopComplete) && (prota.GetComponent<ProtaInfo>().energy <= 0))
                    {
                        GameObject.FindGameObjectWithTag("ProtaTag").GetComponent<ProtaControl>().disparable = false;
                        destinoAtaque = GetDestination(0f);
                        destinoAtaque.y -= 0.45f;
                        ChangeState(EnemStates.esGoingEating);
                    }
                    break;
            }
        }
    }

    private void OnEndAnimation()
    {
        ChangeState(nextState);
        animationsPlaylist.EndPlaylist -= OnEndAnimation;
        esperandoEventoEndPlayList = false;
    }

    //*****************************************************************************************************************
    //  OTHER EVENTS LISTENERS
    //*****************************************************************************************************************

    private void OnTowerDamaged (int tEnergy)
    {
        if ((tEnergy <= 0) && (energy > 0))
        {
            ChangeState(EnemStates.esIdle);
        }
    }

    private void OnProtaHitTheGround ()
    {
        if (iniciado)
        {
            if (energy > 0)
            {
                BoxCollider2D protaBoxCollider2D = prota.GetComponent<BoxCollider2D>();
                if (transform.position.x > prota2.position.x + protaBoxCollider2D.offset.x)
                    aLaDerechaDelProta = true;
                else
                    aLaDerechaDelProta = false;

                destinoAtaque = GetDestination(0.5f);
                destinoAtaque += protaBoxCollider2D.offset;
                destinoAtaque.y -= protaBoxCollider2D.size.y;
                ChangeState(EnemStates.esGoingEating);
            }
        }     
    }

    private void OnProtaIsKilled ()
    {
        if (iniciado)
        {
            if (energy > 0)
            {
                if (transform.position.x > prota2.position.x)
                    aLaDerechaDelProta = true;
                else
                    aLaDerechaDelProta = false;

                destinoAtaque = GetDestination(0f);
                destinoAtaque.y -= 0.45f;
                ChangeState(EnemStates.esGoingEating);
                // GameObject.FindGameObjectWithTag("ArmaTag").GetComponent<MiarmaControla>().habilitado = false;
            }
        }
    }

    private void OnDestroy()
    {
        if (torre != null)
            torre.GetComponent<TowerControl>().DamagedTower -= OnTowerDamaged;

        if (prota != null)
        {
            prota.GetComponent<ProtaControl>().ProtaHitTheGround -= OnProtaHitTheGround;
            prota.GetComponent<ProtaControl>().ProtaIsKilled -= OnProtaIsKilled;
        }
    }

    //*****************************************************************************************************************
    //  RESET
    //*****************************************************************************************************************

    public override void ResetEnemy ()
    {
        oroDado = false;

        iniciado = false;

        armadura.armature.animation.timeScale = 1f;
        myRigidbody2D.isKinematic = false;
        for (int i = 0; i < armadura.transform.childCount; i++)
        {
            Destroy(armadura.transform.GetChild(i).gameObject.GetComponent<Rigidbody2D>());
        }
        explosion.SetActive(false);

        energy = initEnergy;
        aLaDerechaDelProta = GetALaDerechaDelProta();

        if (prota.GetComponent<ProtaInfo>().energy > 0)
        {
            ChangeState(EnemStates.esRunning);
        }
        else
        {
            if (prota.GetComponent<ProtaInfo>().state == ProtaControl.ProtaEstados.peCaer)
                OnProtaHitTheGround();
            else
                OnProtaIsKilled();
        }

        transform.localPosition = new Vector2(20, 0);
        GetComponentInParent<ControladorPosicion>().gameObject.SetActive(false);
        transform.parent = GameObject.FindGameObjectWithTag("Enemigos").transform;
    }

    //*****************************************************************************************************************
    //  OTHER METHODS
    //*****************************************************************************************************************
    private void SetAngleY(float n)
    {
        //Debug.Log("Slot: " + armadura.armature.GetSlot(ojoBoneName) + ",   Y: " + n);
        transform.eulerAngles = Vector3.up * n;
        if (n > 90.0f)
        {
            armadura.armature.GetSlot(ojoBoneName)._setZorder(-1000);
        }
        else
        {
            armadura.armature.GetSlot(ojoBoneName)._setZorder(1000);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.GetComponent<Superviviente>() != null && !colisionadoSuperviviente)
        {
            colisionadoSuperviviente = true;
            tiempoEsperaMuerte = 1.1f;
            Superviviente controladorSuper = collision.gameObject.GetComponent<Superviviente>();
            controladorSuper.Muerte(tiempoEsperaMuerte);
            energy = 0;
            animationsPlaylist.PlayAnimation("comer", 0f, -1, 1);
            StartCoroutine(EsperarMuerte());
        }
    }


    IEnumerator EsperarMuerte()
    {
        yield return new WaitForSeconds(tiempoEsperaMuerte);
        ChangeState(EnemStates.esDying);
        Explode();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        for (int i = 1; i < 20; i++)
        {
            if (collision.gameObject.name == "Atrezzo" + i)
            {
                disparable = false;
            }
        }

        if (collision.gameObject.name == "porton")
        {
            if (!danadoPorPorton)
            {
                GameObject objetovida = Instantiate(vidaUp, new Vector2(transform.position.x, transform.position.y + 0.2f), new Quaternion(0, 0, 0, 0), GameObject.FindGameObjectWithTag("InterfazW").transform);

                objetovida.GetComponent<AnimacionRestaVida>().EfectoVida(DanoPorton);

                danadoPorPorton = true;
                ShootReactor(DanoPorton);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        for (int i = 1; i < 20; i++)
        {
            if (collision.gameObject.name == "Atrezzo" + i)
            {
                disparable = true;
            }
        }
    }

    private Vector2 GetDestination (float howNear)
    {
        if(GetComponent<UnityEngine.Transform>() != null)
        {
            if(prota2 == null)
            {
                Start();
            }

            Vector2 destiny = prota2.position;

            if (aLaDerechaDelProta)
            {
                destiny.x += howNear;
            }
            else
            {
                destiny.x += howNear;
            }

            destiny.y -= 0.1f;

            return destiny;
        }
        return new Vector2(0,0);
    }

    private bool GetALaDerechaDelProta ()
    {
        bool i;
        if (transform.position.x > prota2.position.x)
        {
            i = true;
        }
        else
        {
            i = false;
        }

        return i;
    }
}
