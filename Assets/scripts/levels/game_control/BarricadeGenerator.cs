﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarricadeGenerator : MonoBehaviour {

    [Tooltip("Set here the prefab you want to generate for this level")]
    public GameObject barricadeType;
    public TouchControl touchControl;

    private GameObject barricadeGenerated;
    public Vector2 touchPosition;

	// Use this for initialization
	void Start () {
        if (touchControl == null)
            touchControl = GameObject.FindGameObjectWithTag("GameController").GetComponent<TouchControl>();
    }
	
	// Update is called once per frame
	void Update () {
        touchPosition = touchControl.GetTouchPosition();
        if ((touchPosition.x != -1) && (touchPosition.y != -1))
        {
            barricadeGenerated.transform.position = touchPosition;
        }
        else
        {
            barricadeGenerated.GetComponent<BarricadaControl>().enabled = true;
            barricadeGenerated.GetComponent<Rigidbody2D>().isKinematic = false;
            barricadeGenerated = null;
            this.enabled = false;
        }
	}

    public void GenerateBarricade ()
    {
        //Hay que comprobar que se tienen barricadas en ProtaInfo antes de llamarlo y deducir uno de ellos al generarlo ***************************************************
        touchPosition = touchControl.GetTouchPosition();
        //Debug.Log(touchPosition);
        if ((touchPosition.x != -1) && (touchPosition.y != -1))
        {
            barricadeGenerated = Instantiate(barricadeType, touchPosition, Quaternion.identity);
            this.enabled = true;
        }
    }
}
