﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BotonPropiedades : MonoBehaviour {

    public int huecosDisponibles;
    public int puntosNivel;
    public int nivel;
    public float tiempoNivel;
    public string[] armasObligatorias;


	void Start () {

        PlayerPrefs.SetInt("huecosNivel" + nivel, huecosDisponibles);
        PlayerPrefs.SetInt("puntosNivel" + nivel, puntosNivel);
        PlayerPrefs.DeleteKey("armasObligatorias" + nivel);
        string armas;
        for(int i = 0; i < armasObligatorias.Length; i++)
        {
            armas = PlayerPrefs.GetString("armasObligatorias" + nivel);
            PlayerPrefs.SetString("armasObligatorias" + nivel, armas + armasObligatorias[i]);
        }
    }

    public void CargaNivel()
    {
        //ControladorInterfaz.tiempoNivelDef = tiempoNivel;
        ControladorGeneralFases.fase = nivel;
        loadSC.nextScene = nivel + 3;
        SceneManager.LoadScene("LoadScreenArmas");
    }
	
}
