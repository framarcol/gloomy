﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControladorInterfaz : MonoBehaviour {

    Image barraProgresion;
    public int tiempoNivel, tiempoNivelDef;
    MenusEmergentes controladorJuego;
    public int tiempoInicio;
   // GeneradorHordas generaHordas;

    private void Update()
    {
        tiempoNivel = (int)(tiempoInicio + tiempoNivelDef - Time.time);
        if (tiempoNivel < 0) tiempoNivel = 0;
        // float porcentaje = tiempoNivel / tiempoNivelDef;

      //  if(generaHordas != null)
      //  GetComponentsInChildren<Text>()[1].text = generaHordas.numHordasFinal.ToString() + "/" + generaHordas.numHordas.ToString();
       // GetComponentsInChildren<Text>()[3].text = controladorJuego.GetComponentInChildren<ControladorRecompensas>().oroFinal.ToString();

     //   barraProgresion.fillAmount = porcentaje;
    }

    private void Start()
    {
       // generaHordas = GameObject.FindGameObjectWithTag("GeneradorHordas").GetComponent<GeneradorHordas>();
        controladorJuego = GameObject.FindGameObjectWithTag("GameController").GetComponent<MenusEmergentes>();
        if (IniciarJuego.shopData == null)
        {
            ProtaInfo.testPersonaje = true;
        }
        else
        {
            ProtaInfo.testPersonaje = false;
        }



      //  barraProgresion = GetComponentInChildren<Image>();
      //  tiempoNivel = 0;
      //  tiempoInicio = (int)Time.time;

       /* if (IniciarJuego.shopData.GetMundo(IniciarJuego.shopData.GetNivelByEscena(SceneManager.GetActiveScene().buildIndex), BotonesMenu.mundoSeleccionado) != null)
        {
            GetComponentsInChildren<Text>()[1].text = "Level: " + IniciarJuego.shopData.GetNivelByEscena(SceneManager.GetActiveScene().buildIndex).ToString();

        }
        else
        {
            GetComponentsInChildren<Text>()[1].text = "Level: 0";
        }*/




    //    if (IniciarJuego.shopData.GetMundo(IniciarJuego.shopData.GetNivelByEscena(SceneManager.GetActiveScene().buildIndex), BotonesMenu.mundoSeleccionado) != null)
    //        tiempoNivelDef = IniciarJuego.shopData.GetMundo(IniciarJuego.shopData.GetNivelByEscena(SceneManager.GetActiveScene().buildIndex), BotonesMenu.mundoSeleccionado).tiempoEscena;
    //    else
    //        tiempoNivelDef = 120;


    }

    public void PausarJ()
    {
        
        controladorJuego = GameObject.FindGameObjectWithTag("GameController").GetComponent<MenusEmergentes>();
        Time.timeScale = 0f;
        TouchControl touch = GameObject.FindGameObjectWithTag("GameController").GetComponent<TouchControl>();
        touch.pauseStatus = true;
        controladorJuego.Pausar();
    }
}
