﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Tapdaq;
public class ControladorSelectorArmas : MonoBehaviour {

    public Text txtTotalPuntos, txtSumaPuntos;

    public static int puntosSumados, totalPuntos, sumaArmas;
    public bool frontal;

    public static int[] armasSeleccinadas;
    public static int[] numArmasId;
    public Image[] imagenesArmas;
    public Sprite[] spritesArmas;
    public GameObject Tutorial;
    public Text txtChallenge;
    public Sprite gatling;

    public Transform CanvasArmas;

    public int numNivel;
    public int numMundo;

    public GameObject[] SeleccionArmas;
    public GameObject[] SeleccionArmasExtra;
    public GameObject[] textosCantidades;
    public Text[] txtGamePlay;
    public GameObject tuSeleccion;

    public GameObject[] DestacarArma;
    public GameObject[] DestacarArmaSecundaria;
    public GameObject DestacarPlay;
    ShopData.UpgradeItem miNivel;
    int armaDestacada;
    public Text txtSeleccionArmas;

    int contador, contador2;

    public AudioClip armaClick;

    public GameObject[] spriteNewArmas;

    public BoxCollider2D btnIzquierda, btnDerecha;
    public GameObject btnArmagedon, btnMas;

    private void Awake()
    {

     
#if UNITY_EDITOR
        SeleccionaIdiomas.ActualizaIdioma();
       // PlayerPrefs.DeleteAll();
#endif
    }

    void CrearZombiesCarga()
    {
        string[] nombres = new string[] { "EnemyStandard", "EnemyMidget", "EnemyMuscle", "EnemySkellington", "",
                        "EnemyLoca", "EnemyCabeza1", "" ,"EnemyColossus", "EnemyDragon", "EnemyStandardHalfZombie",
            "EnemyParachute", "WagonDynamiteS2",
                        "WagonDynamiteS5", "WagonDynamiteS7", "WagonCoalS2", "WagonCoalS5", "WagonGoldS2", "WagonGoldS6", "RandomwagonS2"};

        GameObject enemis = GameObject.FindGameObjectWithTag("Enemigos");

        int[] contadorEnemigos = new int[20];

        for (int i = 0; i < miNivel.numTipoEnemigos; i++)
        {
            switch (miNivel.idEnemigo[i])
            {
                case 0:
                    for (int j = 0; j < enemis.GetComponentsInChildren<GenericEnemyControl>().Length; j++)
                        if (enemis.GetComponentsInChildren<GenericEnemyControl>()[j].idEnemigo == 1) contadorEnemigos[0]++;
                    break;
                case 1:
                    for (int j = 0; j < enemis.GetComponentsInChildren<GenericEnemyControl>().Length; j++)
                        if (enemis.GetComponentsInChildren<GenericEnemyControl>()[j].idEnemigo == 6) contadorEnemigos[1]++;
                    break;
                case 2:
                    for (int j = 0; j < enemis.GetComponentsInChildren<GenericEnemyControl>().Length; j++)
                        if (enemis.GetComponentsInChildren<GenericEnemyControl>()[j].idEnemigo == 4) contadorEnemigos[2]++;
                    break;
                case 3:
                    for (int j = 0; j < enemis.GetComponentsInChildren<GenericEnemyControl>().Length; j++)
                        if (enemis.GetComponentsInChildren<GenericEnemyControl>()[j].idEnemigo == 2) contadorEnemigos[3]++;
                    break;
                case 5:
                    for (int j = 0; j < enemis.GetComponentsInChildren<GenericEnemyControl>().Length; j++)
                        if (enemis.GetComponentsInChildren<GenericEnemyControl>()[j].idEnemigo == 7) contadorEnemigos[5]++;
                    break;
                case 6:
                    for (int j = 0; j < enemis.GetComponentsInChildren<GenericEnemyControl>().Length; j++)
                        if (enemis.GetComponentsInChildren<GenericEnemyControl>()[j].idEnemigo == 5) contadorEnemigos[6]++;
                    break;
                case 8:
                    for (int j = 0; j < enemis.GetComponentsInChildren<GenericEnemyControl>().Length; j++)
                        if (enemis.GetComponentsInChildren<GenericEnemyControl>()[j].idEnemigo == 0) contadorEnemigos[8]++;
                    break;
                case 9:
                    contadorEnemigos[9] = enemis.GetComponentsInChildren<VoladorEnemyControl>().Length;
                    break;
                case 10:
                    for (int j = 0; j < enemis.GetComponentsInChildren<GenericEnemyControl>().Length; j++)
                        if (enemis.GetComponentsInChildren<GenericEnemyControl>()[j].idEnemigo == 3) contadorEnemigos[10]++;
                    break;
                case 11:
                    contadorEnemigos[11] = enemis.GetComponentsInChildren<ParacaEnemyControl>().Length;
                    break;
                case 12:
                    for (int j = 0; j < enemis.GetComponentsInChildren<VagonetaControlador>().Length; j++)
                        if (enemis.GetComponentsInChildren<VagonetaControlador>()[j].idVagoneta == 12) contadorEnemigos[12]++;
                    break;
                case 13:
                    for (int j = 0; j < enemis.GetComponentsInChildren<VagonetaControlador>().Length; j++)
                        if (enemis.GetComponentsInChildren<VagonetaControlador>()[j].idVagoneta == 13) contadorEnemigos[13]++;
                    break;
                case 14:
                    for (int j = 0; j < enemis.GetComponentsInChildren<VagonetaControlador>().Length; j++)
                        if (enemis.GetComponentsInChildren<VagonetaControlador>()[j].idVagoneta == 14) contadorEnemigos[14]++;
                    break;
                case 15:
                    for (int j = 0; j < enemis.GetComponentsInChildren<VagonetaControlador>().Length; j++)
                        if (enemis.GetComponentsInChildren<VagonetaControlador>()[j].idVagoneta == 15) contadorEnemigos[15]++;
                    break;
                case 16:
                    for (int j = 0; j < enemis.GetComponentsInChildren<VagonetaControlador>().Length; j++)
                        if (enemis.GetComponentsInChildren<VagonetaControlador>()[j].idVagoneta == 16) contadorEnemigos[16]++;
                    break;
                case 17:
                    for (int j = 0; j < enemis.GetComponentsInChildren<VagonetaControlador>().Length; j++)
                        if (enemis.GetComponentsInChildren<VagonetaControlador>()[j].idVagoneta == 17) contadorEnemigos[17]++;
                    break;
                case 18:
                    for (int j = 0; j < enemis.GetComponentsInChildren<VagonetaControlador>().Length; j++)
                        if (enemis.GetComponentsInChildren<VagonetaControlador>()[j].idVagoneta == 18) contadorEnemigos[18]++;
                    break;
                case 19:
                    for (int j = 0; j < enemis.GetComponentsInChildren<VagonetaControlador>().Length; j++)
                        if (enemis.GetComponentsInChildren<VagonetaControlador>()[j].idVagoneta == 19) contadorEnemigos[19]++;
                    break;
            }

        }

        for (int i = 0; i < miNivel.numTipoEnemigos; i++)
        {
            for (int j = contadorEnemigos[miNivel.idEnemigo[i]]; j < miNivel.cantidadEnemigos[i] / 2; j++)
            {
               GameObject objeto =  Instantiate((GameObject)Resources.Load(nombres[miNivel.idEnemigo[i]]), enemis.transform);
                objeto.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            }
        }
    }

    void Start () {

        armaDestacada = 0;
        contador = 0;
        contador2 = 0;

        if (IniciarJuego.shopData == null)
            IniciarJuego.Iniciar();

        if (SaveGame.armasDesbloqueadas == null)
            SaveGame.Iniciar();

        if (!PlayerPrefs.HasKey(SaveGame.armasDesbloqueadas[1]))
        {
            PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[1], 1);
        }
#if UNITY_EDITOR
        PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[1], 1);
        PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[2], 1);
        PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[3], 1);
        PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[4], 1);
      //  PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[5], 0);
        PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[6], 1);
        PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[7], 1);
        PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[11], 1);
        PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[12], 1);
        PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[13], 1);
        PlayerPrefs.SetInt(SaveGame.armasDesbloqueadas[14], 1);

        PlayerPrefs.SetInt(SaveGame.nivelesArmas[1], 2);
        PlayerPrefs.SetInt(SaveGame.nivelesArmas[2], 3);
        PlayerPrefs.SetInt(SaveGame.nivelesArmas[3], 2);
        PlayerPrefs.SetInt(SaveGame.nivelesArmas[4], 2);
        PlayerPrefs.SetInt(SaveGame.nivelesArmas[5], 0);
        PlayerPrefs.SetInt(SaveGame.nivelesArmas[6], 5);
        PlayerPrefs.SetInt(SaveGame.nivelesArmas[7], 0);
#endif

        armasSeleccinadas = new int[5];

        numArmasId = new int[20];

        if (BotonesMenu.nivelMundoSeleccionado == 0)
        {
            BotonesMenu.nivelMundoSeleccionado = numNivel;
            BotonesMenu.mundoSeleccionado = numMundo;
            loadSC.nextScene = IniciarJuego.shopData.GetEscenaByNivel(numNivel);                   
        }

        miNivel = IniciarJuego.shopData.GetMundo(BotonesMenu.nivelMundoSeleccionado, BotonesMenu.mundoSeleccionado);

     //   CrearZombiesCarga();

        if (frontal)
        {
            totalPuntos = 900;
        }
        else
        {
            totalPuntos = miNivel.PuntosNivel;
            txtTotalPuntos.text = SeleccionaIdiomas.gameText[0, 2] + ": " + (totalPuntos - puntosSumados).ToString();
        }

        for (int i = 0; i <SeleccionArmas.Length; i++) SeleccionArmas[i].SetActive(false);

        if (!frontal)
        {
            for (int i = 0; i < IniciarJuego.shopData.GetNumeroArmas(1) && i < 7; i++)
            {

                if (PlayerPrefs.GetInt(SaveGame.armasDesbloqueadas[i + 1]) == 1)
                {
                    ShopData.StandardShopItem miArma = IniciarJuego.shopData.GetArma(i + 1);
                    SeleccionArmas[i].SetActive(true);
                    //zumArma

                   int nivel = PlayerPrefs.GetInt(SaveGame.nivelesArmas[i + 1]);

                 //   if (nivel == 5) nivel = 4;

                    SeleccionArmas[i].GetComponentsInChildren<Image>()[1].sprite = miArma.itemThumbnail[nivel];
                    SeleccionArmas[i].GetComponentInChildren<Text>().text = miArma.Recursos.ToString() + " " + SeleccionaIdiomas.gameText[0, 7];
                }
                else
                {
                    ShopData.StandardShopItem miArma = IniciarJuego.shopData.GetArma(i + 1);
                    SeleccionArmas[i].SetActive(true);
                    //zumArma
                    SeleccionArmas[i].GetComponentsInChildren<Image>()[1].sprite = miArma.itemThumbnail[PlayerPrefs.GetInt(SaveGame.nivelesArmas[i + 1])];
                    SeleccionArmas[i].GetComponentsInChildren<Image>()[4].color = new Color(1, 1, 1, 1);
                    int[] tabla = IniciarJuego.shopData.GetArmaNivelDesbloqueado(i + 1);
                    tabla[0] += 1;
                    tabla[1] += 1;
                    if(tabla[0] != 1)
                    SeleccionArmas[i].GetComponentInChildren<Text>().text = SeleccionaIdiomas.gameText[0, 18] + " " + tabla[0] + " " + SeleccionaIdiomas.gameText[0, 19] + " " + tabla[1];
                    else
                    {
                        SeleccionArmas[i].GetComponentInChildren<Text>().text = PlayerPrefs.GetInt(SaveGame.fragmentosDesbloqueados[i+1])  + "/" + IniciarJuego.shopData.GetArma(i + 1).NumPiezas.ToString();
                    }
                    SeleccionArmas[i].GetComponent<Toggle>().interactable = false;
                }

            }

            for (int i = 0; i < SeleccionArmasExtra.Length; i++) SeleccionArmasExtra[i].SetActive(false);

            for (int i = 10; i < IniciarJuego.shopData.GetNumeroArmas(2) + 10 && i < 4 + 10; i++)
            {
                if (PlayerPrefs.GetInt(SaveGame.armasDesbloqueadas[i + 1]) == 1)
                {
                    ShopData.StandardShopItem miArma = IniciarJuego.shopData.GetArma(i + 1);
                    SeleccionArmasExtra[i - 10].SetActive(true);
                    //zumArma
                    int nivel = PlayerPrefs.GetInt(SaveGame.nivelesArmas[i + 1]);

                  //  if (nivel == 5) nivel = 4;

                    SeleccionArmasExtra[i - 10].GetComponentsInChildren<Image>()[1].sprite = miArma.itemThumbnail[nivel];
                    SeleccionArmasExtra[i - 10].GetComponentInChildren<Text>().text = miArma.Recursos.ToString() + " " + SeleccionaIdiomas.gameText[0, 7];
                }
                else
                {
                    ShopData.StandardShopItem miArma = IniciarJuego.shopData.GetArma(i + 1);
                    SeleccionArmasExtra[i - 10].SetActive(true);
                    //zumArma
                    SeleccionArmasExtra[i - 10].GetComponentsInChildren<Image>()[1].sprite = miArma.itemThumbnail[PlayerPrefs.GetInt(SaveGame.nivelesArmas[i + 1])];
                    SeleccionArmasExtra[i - 10].GetComponentsInChildren<Image>()[4].color = new Color(1, 1, 1, 1);
                    int[] tabla = IniciarJuego.shopData.GetArmaNivelDesbloqueado(i + 1);
                    tabla[0] += 1;
                    tabla[1] += 1;
                    SeleccionArmasExtra[i - 10].GetComponentInChildren<Text>().text = SeleccionaIdiomas.gameText[0, 18] + " " + tabla[0] + " " + SeleccionaIdiomas.gameText[0, 19] + " " + tabla[1];
                    SeleccionArmasExtra[i - 10].GetComponentsInChildren<Button>()[0].interactable = false;
                    SeleccionArmasExtra[i - 10].GetComponentsInChildren<Button>()[1].interactable = false;

                }
            }
        }
        else
        {
            ShopData.StandardShopItem miArma;
            miArma = IniciarJuego.shopData.GetArma(2);
            SeleccionArmas[0].GetComponent<ValorPuntosArmas>().idArma = 2;
            SeleccionArmas[0].SetActive(true);
            SeleccionArmas[0].GetComponentsInChildren<Image>()[1].sprite = gatling;
            SeleccionArmas[0].GetComponentInChildren<Text>().text = miArma.Recursos.ToString() + " " + SeleccionaIdiomas.gameText[0, 7];


            miArma = IniciarJuego.shopData.GetArma(5);
            SeleccionArmas[1].GetComponent<ValorPuntosArmas>().idArma = 5;
            SeleccionArmas[1].SetActive(true);
            SeleccionArmas[1].GetComponentsInChildren<Image>()[1].sprite = IniciarJuego.shopData.GetArma(8).itemThumbnail[0];
            SeleccionArmas[1].GetComponentInChildren<Text>().text = miArma.Recursos.ToString() + " " + SeleccionaIdiomas.gameText[0, 7];



            miArma = IniciarJuego.shopData.GetArma(4);
            SeleccionArmas[2].GetComponent<ValorPuntosArmas>().idArma = 4;
            SeleccionArmas[2].SetActive(true);
            SeleccionArmas[2].GetComponentsInChildren<Image>()[1].sprite = miArma.itemThumbnail[PlayerPrefs.GetInt(SaveGame.nivelesArmas[4 + 1])];
            SeleccionArmas[2].GetComponentInChildren<Text>().text = miArma.Recursos.ToString() + " " + SeleccionaIdiomas.gameText[0, 7];


        }

        if (!miNivel.esTutorial)
        {
            if (!frontal)
            {
                if (miNivel.ArmasObligatorias)
                {
                    //   btnArmagedon.gameObject.SetActive(false);
                    btnArmagedon.transform.localPosition = new Vector3(-20, -350);

                    btnMas.gameObject.SetActive(false);
                    btnDerecha.gameObject.SetActive(false);
                    btnIzquierda.gameObject.SetActive(false);

                    //  for (int i = 0; i < txtGamePlay.Length; i++) txtGamePlay[i].text = "";
                    for (int i = 0; i < SeleccionArmas.Length; i++)
                    {
                        CanvasArmas.position = new Vector2(1000, 1000);
                    //    SeleccionArmas[i].transform.position = new Vector2(1000, 1000);
                    }
           
                    for (int i = 0; i < SeleccionArmasExtra.Length; i++) SeleccionArmasExtra[i].transform.position = new Vector2(1000, 1000);


                    for (int i = 0; i < miNivel.NumArmasObligatorias; i++)
                    {
                        SeleccionArmas[miNivel.IdArmas[i] - 1].GetComponent<Toggle>().isOn = true;
                        SeleccionArmas[miNivel.IdArmas[i] - 1].GetComponent<Toggle>().interactable = false;
                    }

                    for (int i = 0; i < miNivel.numArmasSecundariasObligatorias; i++)
                    {
                        for (int j = 0; j < miNivel.IdArmasSecundariasCantidad[i]; j++)
                        {
                            SeleccionArmasExtra[miNivel.IdArmasSecundarias[i] - 1 - 10].GetComponentInChildren<Button>().onClick.Invoke();
                        }

                        SeleccionArmasExtra[miNivel.IdArmasSecundarias[i] - 1 - 10].GetComponentsInChildren<Button>()[0].interactable = false;
                        SeleccionArmasExtra[miNivel.IdArmasSecundarias[i] - 1 - 10].GetComponentsInChildren<Button>()[1].interactable = false;
                    }

                }
            }
            else
            {
                SeleccionArmas[0].GetComponent<Toggle>().isOn = true;
                SeleccionArmas[0].GetComponent<Toggle>().interactable = false;
            }

        }
        else
        {

            CanvasArmas.GetComponentInChildren<ScrollRect>().horizontal = false;
            btnDerecha.enabled = false;
            btnIzquierda.enabled = false;

            for(int i = 0; i < SeleccionArmas.Length; i++)
            {
                SeleccionArmas[i].GetComponent<Toggle>().interactable = false;
            }

            if(contador < miNivel.NumArmasObligatorias)
            {
                DestacarArma[miNivel.IdArmas[contador] - 1].SetActive(true);
                armaDestacada = miNivel.IdArmas[contador] - 1;
                SeleccionArmas[armaDestacada].GetComponent<Toggle>().interactable = true;
            }

            if(contador2 < miNivel.numArmasSecundariasObligatorias)
            {
                DestacarArmaSecundaria[miNivel.IdArmasSecundarias[contador] - 1].SetActive(true);
            }
        }

        if (miNivel.armasBloq)
        {
            for (int i = 0; i < miNivel.numArmasBloq; i++)
            {
                if(miNivel.idArmasBloq[i] < 10)
                SeleccionArmas[miNivel.idArmasBloq[i] - 1].GetComponent<Toggle>().interactable = false;
                else
                {
                    SeleccionArmasExtra[miNivel.idArmasBloq[i] - 1 - 10].GetComponentsInChildren<Button>()[0].interactable = false;
                    SeleccionArmasExtra[miNivel.idArmasBloq[i] - 1 - 10].GetComponentsInChildren<Button>()[1].interactable = false;
                }
            }
        }

        ShopData.UpgradeItem miNivelAnterior = IniciarJuego.shopData.GetMundo(BotonesMenu.nivelMundoSeleccionado-1, BotonesMenu.mundoSeleccionado);

        if(miNivel.itemId != 1)
        {
            if (miNivelAnterior.ArmaDesbloqueable && PlayerPrefs.GetInt(SaveGame.nivelesDesbloquedos[BotonesMenu.mundoSeleccionado - 1]) < BotonesMenu.nivelMundoSeleccionado)
            {
                spriteNewArmas[miNivelAnterior.idArmaDes - 1].SetActive(true);
            }
        }

    }

    public void DesactivarTutorial(int idArma)
    {
      //  GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[18]);

        if(DestacarArma[idArma - 1].activeSelf)
        {          
            DestacarArma[idArma - 1].SetActive(false);

            contador++;

            if (miNivel.esTutorial)
            {
                SeleccionArmas[idArma - 1].GetComponent<Toggle>().interactable = false;

                if (contador < miNivel.NumArmasObligatorias)
                {
                    DestacarArma[miNivel.IdArmas[contador] - 1].SetActive(true);
                    SeleccionArmas[miNivel.IdArmas[contador] - 1].GetComponent<Toggle>().interactable = true;
                    SeleccionArmas[miNivel.IdArmas[contador] - 2].GetComponent<Toggle>().interactable = false;
                }
                else if (contador2 < miNivel.numArmasSecundariasObligatorias)
                {
                    for (int i = 0; i < miNivel.numArmasSecundariasObligatorias; i++)
                    {
                        DestacarArmaSecundaria[miNivel.IdArmasSecundarias[i] - 1].SetActive(true);
                    }
                }
                else
                {
                    DestacarPlay.SetActive(true);
                }
            }
        }    
    }

    public void DesactivarTutorialSecundario(int idArma)
    {
        if (DestacarArmaSecundaria[idArma - 1].activeSelf)
        {
            DestacarArmaSecundaria[idArma - 1].SetActive(false);
            contador2++;

            if (miNivel.esTutorial)
            {
                if (contador2 < miNivel.numArmasSecundariasObligatorias)
                {
                    DestacarArma[miNivel.IdArmas[contador2] - 1].SetActive(true);
                }
                else
                {
                    DestacarPlay.SetActive(true);
                }
            }
        }

    }

    private void Update()
    {
        if(BotonesMenu.mundoSeleccionado == 1)
        {
            if (BotonesMenu.nivelMundoSeleccionado < 7)
            {
                btnMas.SetActive(false);
            }
        }

        
        txtTotalPuntos.text = SeleccionaIdiomas.gameText[0, 2] + ": " + (totalPuntos - puntosSumados).ToString();
        if (txtTotalPuntos.text == SeleccionaIdiomas.gameText[0, 2] + ": 0")
        {
            txtTotalPuntos.color = Color.red;
        }
        else
        {
            txtTotalPuntos.color = Color.white;
        }
    

        if (miNivel.ArmasObligatorias && !miNivel.esTutorial)
        {
            tuSeleccion.transform.position= new Vector2(-619.8f, 504.3f);
            txtTotalPuntos.text = "";
            for (int i = 0; i < txtGamePlay.Length; i++) txtGamePlay[i].text = "";
            txtSeleccionArmas.text = SeleccionaIdiomas.gameText[9,16];
            txtChallenge.text = SeleccionaIdiomas.gameText[9, 17];
        }

        for (int i = 0; i < textosCantidades.Length; i++) if(armasSeleccinadas[i] !=0) textosCantidades[i].GetComponent<Text>().text = "x" + numArmasId[armasSeleccinadas[i] - 1].ToString();

    }

    public void SumaPuntos(int idArma, bool esArma)
    {
        if (!esArma) idArma += 10;

        ShopData.StandardShopItem armaSelec = IniciarJuego.shopData.GetArma(idArma);

        int valor = armaSelec.Recursos;

         if(frontal && idArma == 5)
        {
            valor = 800;
        }
      //  Debug.Log(totalPuntos);

    //    Debug.Log((valor + puntosSumados).ToString() + " " + totalPuntos);

        if(valor + puntosSumados >= totalPuntos)
        {
          //  Debug.Log("Hola");
          //  GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[15]);
        }

        if(valor + puntosSumados <= totalPuntos)
        {
            puntosSumados += valor;

            if (!esArma && numArmasId[idArma-1] != 0) numArmasId[idArma-1]++;

            for (int i = 0; i < armasSeleccinadas.Length; i++)
            {
                if (numArmasId[idArma-1] == 0)
                {
                    if (armasSeleccinadas[i] == 0)
                    {
                        armasSeleccinadas[i] = idArma;

                        if (!esArma) numArmasId[idArma-1]++;

                        //zumArma
                        if (!frontal)
                        {
                            //  Debug.Log(PlayerPrefs.GetInt(SaveGame.nivelesArmas[armaSelec.itemId]));
                            int nivel = PlayerPrefs.GetInt(SaveGame.nivelesArmas[armaSelec.itemId]);

                           // if (nivel == 5) nivel = 4;

                            imagenesArmas[i].sprite = armaSelec.itemThumbnail[nivel];

                        }
                       
                        else
                        {
                            if(idArma == 2)
                            {
                                imagenesArmas[i].sprite = gatling;
                            }
                            else if(idArma == 5)
                            {
                                imagenesArmas[i].sprite = IniciarJuego.shopData.GetArma(8).itemThumbnail[0];
                            }
                            else
                            {
                                imagenesArmas[i].sprite = armaSelec.itemThumbnail[PlayerPrefs.GetInt(SaveGame.nivelesArmas[i])];

                            }
                        }

                        if (!esArma) textosCantidades[i].SetActive(true);
                        else textosCantidades[i].SetActive(false);

                        break;
                    }
                }
                else
                {
                    textosCantidades[i].GetComponent<Text>().text = "x" + numArmasId[idArma-1].ToString();
                }

            }
        }
    }

    public void RestaPuntos(int idArma, bool esArma)
    {
        if (!esArma) idArma += 10;
        int valor = IniciarJuego.shopData.GetArma(idArma).Recursos;

        if (frontal && idArma == 5)
        {
            valor = 800;
        }

        bool continua = true;

        if(esArma)puntosSumados -= valor;

        if (!esArma) {
            if(numArmasId[idArma -1] > 0)
            {
                continua = true;
                numArmasId[idArma - 1]--;
                puntosSumados -= valor;
            }
            else
            {
                continua = false;
            }        
        }

        if (continua)
        {
            for (int i = 0; i < armasSeleccinadas.Length; i++)
            {
                if (armasSeleccinadas[i] == idArma)
                {
                    if (numArmasId[idArma - 1] == 0)
                    {
                        armasSeleccinadas[i] = 0;
                        imagenesArmas[i].sprite = spritesArmas[4];
                        if (!esArma) textosCantidades[i].SetActive(false);
                    }
                    else
                    {
                        textosCantidades[i].GetComponent<Text>().text = "x" + numArmasId[idArma - 1].ToString();
                    }

                }

            }
        }     

    }


  

}
