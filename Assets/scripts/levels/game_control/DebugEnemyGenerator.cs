﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugEnemyGenerator : MonoBehaviour {

    public GameObject enemy;
    public GameObject enemy2;
    public GameObject enemy3;


    bool generarEnemigo;
    int cual;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        generarEnemigo = false;
        
        if (Input.touchCount > 0)   //Code for touchscreen
        {
            foreach (Touch toque in Input.touches)
            {
                if (toque.phase == TouchPhase.Began)
                {
                    generarEnemigo = true;
                    cual = 1;
                }
            }
        }
        else
        {
            //No touchscreen detected
            if (Input.GetKeyDown(KeyCode.Z))
            {
                generarEnemigo = true;
                cual = 0;
            }
            if (Input.GetKeyDown(KeyCode.X))
            {
                generarEnemigo = true;
                cual = 1;
            }
            if (Input.GetKeyDown(KeyCode.C))
            {
                generarEnemigo = true;
                cual = 2;
            }
        }

        if (generarEnemigo)
        {
            switch (cual)
            {
                case 0:
                    Instantiate(enemy, this.transform.position, this.transform.localRotation);
                    break;
                case 1:
                    Instantiate(enemy2, this.transform.position, this.transform.localRotation);
                    break;
                case 2:
                    Instantiate(enemy3, this.transform.position, this.transform.localRotation);
                    break;
            }
        }
	}
}
