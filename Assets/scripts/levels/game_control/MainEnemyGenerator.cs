﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainEnemyGenerator : MonoBehaviour {

    public PoolingSystem poolingSystem;
    public Transform[] enemyGenerators;
    public float intervaloTiempo;
    public float numEnemigos;
    public Transform posicionCreacion;
    int contador;
    /*public GameObject prefabEnanoMotosierra;
    public GameObject prefabHuesudo;
    public GameObject prefabNina;
    public GameObject prefabLoca;
    public GameObject prefabVolador;
    public GameObject prefabParaca;*/

    public enum EnemyTypes { _enEnanoMotosierra = 0, _enHuesudo = 4, _enNina = 1, _enLoca = 5, _enVolador = 2, _enParaca = 3 };
    public EnemyTypes enemigoSeleccionado;

	// Use this for initialization
	void Start () {
        contador = 0;
        if (poolingSystem == null)
            poolingSystem = FindObjectOfType<PoolingSystem>();

      //  StartCoroutine(GeneradorAutomatico());
	}

    public IEnumerator GeneradorAutomatico()
    {
        yield return new WaitForSeconds(intervaloTiempo);

        if (contador < numEnemigos)
        {
            contador++;
            GenerarEnemigo((int)enemigoSeleccionado);
        }
           
    }

    public void GenerarEnemigo(int enemigo)
    {
        switch (enemigo)
        {
            case 0:
                GenerarEnemigo(EnemyTypes._enEnanoMotosierra, posicionCreacion);
                break;
            case 1:
                GenerarEnemigo(EnemyTypes._enNina, posicionCreacion);
                break;
            case 2:
                GenerarEnemigo(EnemyTypes._enVolador, posicionCreacion);
                break;
            case 3:
                GenerarEnemigo(EnemyTypes._enParaca, posicionCreacion);
                break;
            case 4:
                GenerarEnemigo(EnemyTypes._enHuesudo, posicionCreacion);
                break;
            case 5: 
                GenerarEnemigo(EnemyTypes._enLoca, posicionCreacion);
                break;
        }

        StartCoroutine(GeneradorAutomatico());
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.Z))
            GenerarEnemigo(EnemyTypes._enEnanoMotosierra, enemyGenerators[0]);
        if (Input.GetKeyDown(KeyCode.X))
            GenerarEnemigo(EnemyTypes._enHuesudo, enemyGenerators[0]);
        if (Input.GetKeyDown(KeyCode.C))
            GenerarEnemigo(EnemyTypes._enNina, enemyGenerators[0]);
        if (Input.GetKeyDown(KeyCode.V))
            GenerarEnemigo(EnemyTypes._enLoca, enemyGenerators[0]);
        if (Input.GetKeyDown(KeyCode.B))
            GenerarEnemigo(EnemyTypes._enVolador, enemyGenerators[1]);
    }

    //Previous version, using INSTANTIATE
    /*public void GenerarEnemigo (EnemyTypes type, Transform generator)
    {
        switch (type)
        {
            case EnemyTypes._enEnanoMotosierra:
                Instantiate(prefabEnanoMotosierra, generator.position, generator.localRotation);
                break;
            case EnemyTypes._enHuesudo:
                Instantiate(prefabHuesudo, generator.position, generator.localRotation);
                break;
            case EnemyTypes._enNina:
                Instantiate(prefabNina, generator.position, generator.localRotation);
                break;
            case EnemyTypes._enLoca:
                Instantiate(prefabLoca, generator.position, generator.localRotation);
                break;
            case EnemyTypes._enVolador:
                Instantiate(prefabVolador, generator.position, generator.localRotation);
                break;
            case EnemyTypes._enParaca:
                Instantiate(prefabParaca, generator.position, generator.localRotation);
                break;
        }
    }*/

    public void GenerarEnemigo(EnemyTypes type, Transform generator)
    {
        switch (type)
        {
            case EnemyTypes._enEnanoMotosierra:
                poolingSystem.CreateEnemyFromTag("EnemyEnanoMotosierra", generator.position, generator.localRotation);
                break;
            case EnemyTypes._enHuesudo:
                poolingSystem.CreateEnemyFromTag("EnemyHuesudo", generator.position, generator.localRotation);
                break;
            case EnemyTypes._enNina:
                poolingSystem.CreateEnemyFromTag("EnemyNina", generator.position, generator.localRotation);
                break;
            case EnemyTypes._enLoca:
                poolingSystem.CreateEnemyFromTag("EnemyLoca", generator.position, generator.localRotation);
                break;
            case EnemyTypes._enVolador:
                poolingSystem.CreateEnemyFromTag("EnemyVolador", generator.position, generator.localRotation);
                break;
            case EnemyTypes._enParaca:
                poolingSystem.CreateEnemyFromTag("EnemyParaca", generator.position, generator.localRotation);
                break;
        }
    }

    

    public void GenerarEnano (Transform generator)
    {
        GenerarEnemigo(EnemyTypes._enEnanoMotosierra, generator);
        //contador++;
        //StartCoroutine(GeneradorAutomatico());
    }
    public void GenerarNina (Transform generator)
    {
        GenerarEnemigo(EnemyTypes._enNina, generator);
    }
    public void GenerarVolador(Transform generator)
    {
        GenerarEnemigo(EnemyTypes._enVolador, generator);
    }
    public void GenerarParaca(Transform generator)
    {
        GenerarEnemigo(EnemyTypes._enParaca, generator);
    }
    public void GenerarHuesudo(Transform generator)
    {
        GenerarEnemigo(EnemyTypes._enHuesudo, generator);
    }
}
