﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiarmaControla : MonoBehaviour {
    Transform weaponOut;
    public bool habilitado;

    public enum WeaponTypes { _wtGun, _wtRifle, _wtShotgun, _wtUzi, _wtSniper, _wtGrenadeLauncher, _wtGrenade, _wtBarricada, _wtMina, _wtPinchos, _wtLaser };
    public WeaponTypes currentWeapon = WeaponTypes._wtGun;

    private ProtaInfo protaInfo;
    private InterfazControl interfazControl;
    //Private variables
    Vector3 mousePosition;
    float angleRad;
    public float angleDeg;
    private float weaponDamage;
    private float weaponRadio;
    private float weaponAlcance;
    private float danoFinal;
    GameObject[] armaSeleccionada;
    float danoDistancia;
    public float factorCorreccion;
    public int[] casillasArmas;
    SpriteRenderer[] armasSprites;
  //  SpriteRenderer[] armasSpritesGlow;
    Sprite[] spritesOriginales;
    bool SpritesCargados;
    int armaPulsada;
    public GameObject[] explosionPistola;
    public GameObject[] explosionEscopeta;
    public GameObject[] explosionRifle;
    public GameObject[] explosionLaser;
    public GameObject[] explosionFranco;

    public AudioClip escopeta, pistola, metralleta, franco;

    GameObject[] temporizadores;
    public static float[] timer = new float[5];
    float[] segundos = new float[5];
    bool[] cambiarImagen = new bool[5];
    public static bool[] posicionado = new bool[4];
    public static bool[] Subposicion = new bool[8];
    int barTocada;
    ControladorSenalBarricada posicionesBarricada, posicionesPinchos;
    ControladorSenalMinas posicionesMina;
    public Transform preBarricada, preMina, prePinchos;
    public GameObject vidaUp;
    //temporal
    int numVeces = 0;
    GameObject armasImagenes, armasRestoImagenes, armasSpritesFinales;

    bool frontal;
    public Sprite gatling;

    public GameObject SpriteProhibido;

    public GameObject Protagonista, rotacion;

    private void Awake()
    {
        if (IniciarJuego.shopData != null)
            frontal = IniciarJuego.shopData.GetMundo(BotonesMenu.nivelMundoSeleccionado, BotonesMenu.mundoSeleccionado).frontal;

        if(IniciarJuego.shopData != null)
        {
            if (!frontal)
            {
                GameObject objetoObtener = GameObject.FindGameObjectWithTag("PosSec");
                posicionesBarricada = objetoObtener.GetComponentsInChildren<ControladorSenalBarricada>()[0];
                posicionesMina = objetoObtener.GetComponentInChildren<ControladorSenalMinas>();
                posicionesPinchos = objetoObtener.GetComponentsInChildren<ControladorSenalBarricada>()[1];

                posicionesBarricada.gameObject.SetActive(false);
                posicionesMina.gameObject.SetActive(false);
                posicionesPinchos.gameObject.SetActive(false);
            }
        }

        armasImagenes = GameObject.FindGameObjectWithTag("ArmasPrefab");
        armasRestoImagenes = GameObject.FindGameObjectWithTag("EstadosArmasTag");
        armasSpritesFinales = GameObject.FindGameObjectWithTag("ProtaTag");
        armasSprites = new SpriteRenderer[10];
        armaSeleccionada = new GameObject[5];
        temporizadores = new GameObject[5];

        for (int i = 0; i < armasSprites.Length  && i < armasSpritesFinales.GetComponent<ProtaInfo>().imagenes.Length; i++)
        {
            if (armasSpritesFinales.GetComponent<ProtaInfo>().imagenes[i] != null)
            {
                armasSprites[i] = armasSpritesFinales.GetComponent<ProtaInfo>().imagenes[i].GetComponent<SpriteRenderer>();
            }
            else
            {
                if(i < armasImagenes.GetComponentsInChildren<SpriteRenderer>().Length)
                armasSprites[i] = armasImagenes.GetComponentsInChildren<SpriteRenderer>()[i];
            }
  
        }

        for (int i = 0, j = 1; j < armaSeleccionada.Length+1; j++, i++)
        {
          
            if(armasRestoImagenes.GetComponentsInChildren<Button>()[i].name == "imgSelect"+j)
            armaSeleccionada[i] = armasRestoImagenes.GetComponentsInChildren<Button>()[i].gameObject;
        }

        for (int i = 5, j = 1; j < temporizadores.Length +1 ; j++, i++)
        {
            //Debug.Log(armasRestoImagenes.GetComponentsInChildren<Image>()[i].name);
            if (armasRestoImagenes.GetComponentsInChildren<Image>()[i].name == "imgTemp" + j)
            {
                temporizadores[j-1] = armasRestoImagenes.GetComponentsInChildren<Image>()[i].gameObject;          
            }
                    
        }

        for (int i = 0; i < temporizadores.Length; i++)
        {
            temporizadores[i].SetActive(false);
        }

        weaponOut = GetComponentInChildren<Transform>();
        casillasArmas = new int[20];
        
    }

    void Start()
    {

        for (int i = 0; i < posicionado.Length; i++) posicionado[i] = false;
        if (protaInfo == null)
            protaInfo = transform.parent.GetComponent<ProtaInfo>();

        bool siguiente = true, hayArma = false, SecundariaCargada = false;
        int secundaria = 0, casilleroSecundaria = 0;

        for (int i = 0; i < ControladorSelectorArmas.armasSeleccinadas.Length; i++)
        {
            if (siguiente)
            {
                switch (ControladorSelectorArmas.armasSeleccinadas[i])
                {
                    case 1:
                        SelectWeapon(WeaponTypes._wtGun, i);
                        hayArma = true;
                        siguiente = false;
                        break;
                    case 2:
                        SelectWeapon(WeaponTypes._wtRifle, i);
                        hayArma = true;
                        siguiente = false;
                        break;
                    case 3:
                        SelectWeapon(WeaponTypes._wtShotgun, i);
                        hayArma = true;
                        siguiente = false;
                        break;
                    case 4:
                        SelectWeapon(WeaponTypes._wtSniper, i);
                        hayArma = true;
                        siguiente = false;
                        break;
                    case 5:
                        SelectWeapon(WeaponTypes._wtGrenadeLauncher, i);
                        hayArma = true;
                        siguiente = false;
                        break;
                    case 6:
                        SelectWeapon(WeaponTypes._wtUzi, i);
                        hayArma = true;
                        siguiente = false;
                        break;
                    case 7:
                        SelectWeapon(WeaponTypes._wtLaser, i);
                        hayArma = true;
                        siguiente = false;
                        break;
                    default:
                        hayArma = false;
                        if (!SecundariaCargada)
                        {
                            secundaria = ControladorSelectorArmas.armasSeleccinadas[i];
                            casilleroSecundaria = i;
                            SecundariaCargada = true;
                        }           
                        siguiente =true;
                        break;
                }
            }
            else
            {              
                break;             
            }

        }

        if (!hayArma)
        {
            switch (secundaria)
            {
                case 11:
                    SelectWeapon(WeaponTypes._wtGun, casilleroSecundaria);
                    SelectWeapon(WeaponTypes._wtGrenade, casilleroSecundaria);
                    break;
                case 12:
                    SelectWeapon(WeaponTypes._wtGun, casilleroSecundaria);
                    SelectWeapon(WeaponTypes._wtBarricada, casilleroSecundaria);
                    break;
                case 13:
                    SelectWeapon(WeaponTypes._wtGun, casilleroSecundaria);
                    SelectWeapon(WeaponTypes._wtMina, casilleroSecundaria);
                    break;
                case 14:
                    SelectWeapon(WeaponTypes._wtGun, casilleroSecundaria);
                    SelectWeapon(WeaponTypes._wtPinchos, casilleroSecundaria);
                    break;
            }
        }
        habilitado = true;
    }

    void Update()
    {
       // for (int i = 0; i < Subposicion.Length; i++) Debug.Log(i.ToString() +  ": " + Subposicion[i].ToString());
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        angleRad = Mathf.Atan2(mousePosition.y - transform.position.y, mousePosition.x - transform.position.x);
        angleDeg = (180f / Mathf.PI) * angleRad;
        this.transform.rotation = Quaternion.Euler(0, 0, angleDeg);

        for (int i = 0; i < timer.Length; i++)
        {
            if (cambiarImagen[i])
                timer[i] += Time.deltaTime;
        }

        for (int i = 0; i < cambiarImagen.Length; i++)
        {
            if (timer[i] < segundos[i])
            {
                if (cambiarImagen[i]) temporizadores[i].SetActive(true);
                temporizadores[i].GetComponent<Image>().fillAmount = timer[i] / segundos[i];
            }
        }

        for (int i = 0; i < cambiarImagen.Length; i++)
        {
            if (cambiarImagen[i])
            {
                if (timer[i] >= segundos[i])
                {
                    temporizadores[i].GetComponent<Image>().fillAmount = 1;
                    if (cambiarImagen[i]) temporizadores[0].SetActive(false);
                    timer[i] = 0;
                    cambiarImagen[i] = false;
                }

            }
        }

    }

    public void ShootToFrontal(GameObject objetoGolpeado)
    {
        switch (currentWeapon)
        {
            case WeaponTypes._wtRifle:
                danoFinal = 10;
                break;
            case WeaponTypes._wtSniper:
                danoFinal = 150;
                 break;
        }

        if(objetoGolpeado != null)
        {
            InteractableObject objetoInteractivo;

            objetoInteractivo = objetoGolpeado.GetComponent<InteractableObject>();

           // Debug.Log(objetoInteractivo.gameObject.name);

            float tipoEnemigo = 0;

            if (objetoInteractivo.ShootReactor((int)danoFinal) > 0)
            {

                if (objetoInteractivo.GetComponent<GenericEnemyControl>() != null)
                {

                    if (objetoInteractivo.GetComponent<GenericEnemyControl>().idEnemigo == 4)
                    {
                        tipoEnemigo = 1;
                    }
                    else if(objetoInteractivo.GetComponent<GenericEnemyControl>().idEnemigo == 2)
                    {
                        tipoEnemigo = 1.3f;
                    }
                    else if (objetoInteractivo.GetComponent<GenericEnemyControl>().idEnemigo == 6)
                    {
                        tipoEnemigo = 1f;
                    }
                    else
                    {
                        tipoEnemigo = 0.2f;
                    }

                }
                if (objetoInteractivo.GetComponent<VoladorEnemyControl>() != null) tipoEnemigo = 1.2f;
                if (objetoInteractivo.GetComponent<ParacaEnemyControl>() != null) tipoEnemigo = 1.2f;
                float aleatorio = Random.Range(0, 0.3f);
                int aleatorio2 = Random.Range(0, 3);
                GameObject explode;
                switch (currentWeapon)
                {
                    case WeaponTypes._wtRifle:
                        explode = Instantiate(explosionRifle[aleatorio2], new Vector2(objetoInteractivo.transform.position.x + aleatorio, objetoInteractivo.transform.position.y + aleatorio + tipoEnemigo - 0.4f), Quaternion.identity);
                        break;
                    case WeaponTypes._wtSniper:
                        explode = Instantiate(explosionRifle[aleatorio2], objetoInteractivo.transform.position, Quaternion.identity);
                        break;
                }
            }

            Vector3 posicionBicho = objetoInteractivo.transform.position;

            if (danoFinal > 0 && objetoInteractivo.GetComponent<InteractableObject>().energy > 0)
            {
                    GameObject objetovida = Instantiate(vidaUp, new Vector2(posicionBicho.x, posicionBicho.y + tipoEnemigo), new Quaternion(0, 0, 0, 0), GameObject.FindGameObjectWithTag("InterfazW").transform);
                    objetovida.GetComponent<AnimacionRestaVida>().EfectoVida((int)danoFinal);
            }
        }

        if (currentWeapon != WeaponTypes._wtSniper && currentWeapon != WeaponTypes._wtPinchos && currentWeapon != WeaponTypes._wtMina && currentWeapon != WeaponTypes._wtBarricada && currentWeapon != WeaponTypes._wtRifle)
            ReduceWeaponAmmo(currentWeapon);

    }

    public void ReducirArma()
    {
        ReduceWeaponAmmo(currentWeapon);
    }

    public void MouseToShoot(Vector2 shootPos, GameObject objetoGolpeado)
    {
        if (habilitado && WeaponHasAmmo(currentWeapon))
        {
            if (!frontal)
            {
                angleRad = Mathf.Atan2(shootPos.y - transform.position.y, shootPos.x - transform.position.x);
                angleDeg = (180f / Mathf.PI) * angleRad;
                transform.parent.GetComponent<ProtaControl>().Disparar(angleDeg, shootPos);
            }
            else  transform.parent.GetComponent<ProtaControl>().DispararFrontal(objetoGolpeado, shootPos);

        }
        else
        {
            if (!WeaponHasAmmo(currentWeapon))
            {
                SpriteProhibido.SetActive(true);
               
                switch (currentWeapon)
                {
                    case WeaponTypes._wtGun:
                        SpriteProhibido.GetComponentsInChildren<SpriteRenderer>()[1].sprite = IniciarJuego.shopData.GetArma(1).itemHigh;
                        break;
                    case WeaponTypes._wtRifle:
                        SpriteProhibido.GetComponentsInChildren<SpriteRenderer>()[1].sprite = IniciarJuego.shopData.GetArma(2).itemHigh;
                        break;
                    case WeaponTypes._wtShotgun:
                        SpriteProhibido.GetComponentsInChildren<SpriteRenderer>()[1].sprite = IniciarJuego.shopData.GetArma(3).itemHigh;
                        break;
                    case WeaponTypes._wtUzi:
                        SpriteProhibido.GetComponentsInChildren<SpriteRenderer>()[1].sprite = IniciarJuego.shopData.GetArma(6).itemHigh;
                        break;
                    case WeaponTypes._wtLaser:
                        SpriteProhibido.GetComponentsInChildren<SpriteRenderer>()[1].sprite = IniciarJuego.shopData.GetArma(7).itemHigh;
                        break;
                    case WeaponTypes._wtSniper:
                        SpriteProhibido.GetComponentsInChildren<SpriteRenderer>()[1].sprite = IniciarJuego.shopData.GetArma(4).itemHigh;
                        break;
                    case WeaponTypes._wtGrenadeLauncher:
                        SpriteProhibido.GetComponentsInChildren<SpriteRenderer>()[1].sprite = IniciarJuego.shopData.GetArma(5).itemHigh;
                        break;
                    case WeaponTypes._wtGrenade:
                        SpriteProhibido.GetComponentsInChildren<SpriteRenderer>()[1].sprite = IniciarJuego.shopData.GetArma(11).itemHigh;
                        break;
                    case WeaponTypes._wtBarricada:
                        SpriteProhibido.GetComponentsInChildren<SpriteRenderer>()[1].sprite = IniciarJuego.shopData.GetArma(12).itemHigh;
                        break;
                    case WeaponTypes._wtMina:
                        SpriteProhibido.GetComponentsInChildren<SpriteRenderer>()[1].sprite = IniciarJuego.shopData.GetArma(13).itemHigh;
                        break;
                    case WeaponTypes._wtPinchos:
                        SpriteProhibido.GetComponentsInChildren<SpriteRenderer>()[1].sprite = IniciarJuego.shopData.GetArma(14).itemHigh;
                        break;
                }

            }
        }
    }

    public void ShootTo(Vector2 shootPos)
    {
        RaycastHit2D[] objetives = Physics2D.RaycastAll(weaponOut.position, shootPos - (Vector2)weaponOut.position, 15f);

        Debug.DrawRay(weaponOut.position, shootPos - (Vector2)weaponOut.position, Color.red, 0.5f);
        InteractableObject interactableObject;
        int numeroObjetivo = 0;
        
        numVeces++;
        Vector3 posicionBicho = new Vector3(0,0,0);

        if(objetives.Length > 0)
        {
            for(int i = 0; i < objetives.Length; i++)
            {
                interactableObject = objetives[i].transform.GetComponent<InteractableObject>();

                if (interactableObject != null)
                {
                    if ((interactableObject.reactorType == InteractableObject.ReactorType.enemy) ||
                        (interactableObject.reactorType == InteractableObject.ReactorType.destructible) ||
                        (interactableObject.reactorType == InteractableObject.ReactorType.oro) ||
                        (interactableObject.reactorType == InteractableObject.ReactorType.dinamita) ||
                        (interactableObject.reactorType == InteractableObject.ReactorType.carbon) ||
                  (interactableObject.reactorType == InteractableObject.ReactorType.botonPorton))
                    {
                        if (interactableObject.GetComponent<VagonetaControlador>() != null)
                            if (!interactableObject.GetComponent<VagonetaControlador>().disparable)
                            {
                                numeroObjetivo++;
                                continue;
                            }

                        if (interactableObject.GetComponent<GenericEnemyControl>() != null)
                            if (!interactableObject.GetComponent<GenericEnemyControl>().disparable)
                            {
                                numeroObjetivo++;
                                continue;
                            }
                        
                        if (interactableObject.GetComponent<VoladorEnemyControl>() != null)
                            if (!interactableObject.GetComponent<VoladorEnemyControl>().disparable)
                            {
                                numeroObjetivo++;
                                continue;
                            }

                        if (interactableObject.GetComponent<ParacaEnemyControl>() != null)
                        {

                            if (!interactableObject.GetComponent<ParacaEnemyControl>().disparable)
                            {
                                numeroObjetivo++;
                                continue;
                            }

                        }

                        if (interactableObject.GetComponent<ParacaEnemyControl>() != null)
                            if (interactableObject.GetComponent<ParacaEnemyControl>().impenetrable || interactableObject.GetComponent<ParacaEnemyControl>().energy <0)
                            {
                                numeroObjetivo++;
                                continue;
                            }

                        if (interactableObject.GetComponent<VoladorEnemyControl>() != null)
                            if (interactableObject.GetComponent<VoladorEnemyControl>().impenetrable)
                            {
                                numeroObjetivo++;
                                continue;
                            }

                        danoDistancia = Vector2.Distance(objetives[i].transform.position, weaponOut.position);
                        


                        posicionBicho = objetives[i].transform.position;
                        break;
                    }
                    else numeroObjetivo++;
                }

            }         
        }
        else
        {
            danoDistancia = 200;
        }

        if(rotacion != null)
        {
            if (shootPos.x < 0)
            {
                Protagonista.GetComponent<Transform>().localRotation = rotacion.GetComponent<Transform>().localRotation;
                danoDistancia -= 0.5f;
            }
            else
            {
                Protagonista.GetComponent<Transform>().localRotation = Quaternion.Euler(0, 0, 0);
            }
        }   

        danoDistancia -= factorCorreccion;

        if(weaponDamage == 0)
        {
            switch (currentWeapon)
            {
                case WeaponTypes._wtGun:
                    weaponDamage = protaInfo.Damage[1];
                    weaponAlcance = protaInfo.Alcance[1];
                    break;
                case WeaponTypes._wtRifle:
                    weaponDamage = protaInfo.Damage[2];
                    weaponAlcance = protaInfo.Alcance[2];
                    break;
                case WeaponTypes._wtShotgun:
                    weaponDamage = protaInfo.Damage[3];
                    weaponAlcance = protaInfo.Alcance[3];
                    break;
                case WeaponTypes._wtSniper:
                    weaponDamage = protaInfo.Damage[4];
                    weaponAlcance = protaInfo.Alcance[4];
                    break;
                case WeaponTypes._wtGrenadeLauncher:
                    weaponDamage = protaInfo.Damage[5];
                    weaponAlcance = protaInfo.Alcance[5];
                    break;
                case WeaponTypes._wtUzi:
                    weaponDamage = protaInfo.Damage[6];
                    weaponAlcance = protaInfo.Alcance[6];
                    break;
                case WeaponTypes._wtLaser:
                    weaponDamage = protaInfo.Damage[7];
                    weaponAlcance = protaInfo.Alcance[7];
                    break;
            }
        }
    
        if (weaponRadio == 0)
        {
            if(danoDistancia <= 12)
            {
                if (danoDistancia <= weaponAlcance)
                {
                    danoFinal = weaponDamage;
                }
                else if (danoDistancia >= weaponAlcance && danoDistancia <= weaponAlcance * 2)
                {
                    danoFinal = weaponDamage * 0.5f;
                }
                else if (danoDistancia >= weaponAlcance * 2 && danoDistancia <= weaponAlcance * 3)
                {
                    danoFinal = weaponDamage * 0.25f;
                }
                else
                {
                    danoFinal = 0;
                }
            }
            else
            {
                danoFinal = 0;
            }
           
        }
        else
        {
            danoFinal = weaponDamage;
        }

        int vidaEnemigo = 0;
        float tipoEnemigo = 0;
        float tipoEnemigoX = 0;

        switch (currentWeapon)
        {
            case WeaponTypes._wtGun:
                GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[1]);
                  break;
            case WeaponTypes._wtRifle:
                GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[2]);
                 break;
            case WeaponTypes._wtShotgun:
                GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[3]);
                break;
            case WeaponTypes._wtSniper:
                GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[4]);
                break;
            case WeaponTypes._wtUzi:
                GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[32]);
                break;
            case WeaponTypes._wtLaser:
                GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[33]);
                break;
        }
        int z = 0;
        foreach (RaycastHit2D obj in objetives)
        {
            interactableObject = obj.transform.GetComponent<InteractableObject>();

            if (interactableObject != null)
            {
                if ((interactableObject.reactorType == InteractableObject.ReactorType.enemy) ||
                   (interactableObject.reactorType == InteractableObject.ReactorType.destructible) ||
                  (interactableObject.reactorType == InteractableObject.ReactorType.oro) ||
                  (interactableObject.reactorType == InteractableObject.ReactorType.dinamita) ||
                  (interactableObject.reactorType == InteractableObject.ReactorType.carbon)  ||
                  (interactableObject.reactorType == InteractableObject.ReactorType.botonPorton))
                {
                    if (interactableObject.GetComponent<VagonetaControlador>() != null)
                        if (!interactableObject.GetComponent<VagonetaControlador>().disparable) continue;
                    if (interactableObject.GetComponent<GenericEnemyControl>() != null)
                        if (!interactableObject.GetComponent<GenericEnemyControl>().disparable) continue;
                    if (interactableObject.GetComponent<VoladorEnemyControl>() != null)
                        if (!interactableObject.GetComponent<VoladorEnemyControl>().disparable) continue;
                    if (interactableObject.GetComponent<ParacaEnemyControl>() != null)
                        if (!interactableObject.GetComponent<ParacaEnemyControl>().disparable) continue;

                    if (interactableObject.GetComponent<ParacaEnemyControl>() != null)
                        if (interactableObject.GetComponent<ParacaEnemyControl>().impenetrable) continue;
                    if (interactableObject.GetComponent<VoladorEnemyControl>() != null)
                        if (interactableObject.GetComponent<VoladorEnemyControl>().impenetrable) continue;
                    if (interactableObject.name == "Muro") continue;

                    if (objetives[numeroObjetivo].transform.gameObject.GetComponentInChildren<ParacaEnemyControl>() != null)
                    {
                        if (objetives[numeroObjetivo].transform.gameObject.GetComponentInChildren<ParacaEnemyControl>().energy < 1)
                        {
                            numeroObjetivo++;
                        }
                    }

                    if (interactableObject.ShootReactor((int)danoFinal) > 0)
                    {
                        if (interactableObject.GetComponent<GenericEnemyControl>() != null)
                        {
                            if (currentWeapon != WeaponTypes._wtLaser)
                            {
                                if (interactableObject.GetComponent<GenericEnemyControl>().idEnemigo == 4)
                                {
                                    tipoEnemigo = 1.8f;
                                    tipoEnemigoX = -0.4f;
                                }
                                else if (interactableObject.GetComponent<GenericEnemyControl>().idEnemigo == 2)
                                {
                                    tipoEnemigo = 1.3f;
                                }
                                else if (interactableObject.GetComponent<GenericEnemyControl>().idEnemigo == 3 &&
                                    interactableObject.GetComponent<GenericEnemyControl>().segundoEstadoStandard &&
                                      interactableObject.GetComponent<GenericEnemyControl>().energy < 2)
                                {
                                    tipoEnemigo = -0.3f;
                                    tipoEnemigoX = -0.4f;
                                }
                                else if (interactableObject.GetComponent<GenericEnemyControl>().idEnemigo == 7)
                                {
                                    tipoEnemigo = 1.5f;
                                }
                                else if (interactableObject.GetComponent<GenericEnemyControl>().idEnemigo == 6)
                                {
                                    tipoEnemigo = 1f;
                                }
                                else
                                {
                                    tipoEnemigo = 0.2f;
                                }
                            }
                            else{
                                switch (interactableObject.GetComponent<GenericEnemyControl>().idEnemigo)
                                {
                                    case 0:
                                        break;
                                    case 1:
                                        tipoEnemigo = 0;
                                        break;
                                    case 2:
                                        break;
                                    case 3:
                                        tipoEnemigo = 0;
                                        break;
                                    case 4:
                                        break;
                                    case 5:
                                        break;
                                    case 6:
                                        tipoEnemigo = 1f;
                                        break;
                                    case 7:
                                        break;
                                    case 8:
                                        break;
                                }

                            }
                           

                        }

                        if(currentWeapon != WeaponTypes._wtLaser)
                        {
                            if (interactableObject.GetComponent<VoladorEnemyControl>() != null) tipoEnemigo = 1.2f;
                            if (interactableObject.GetComponent<ParacaEnemyControl>() != null) tipoEnemigo = 0.7f;
                        }
                        else
                        {
                            if (interactableObject.GetComponent<VoladorEnemyControl>() != null) tipoEnemigo = 1.2f;
                            if (interactableObject.GetComponent<ParacaEnemyControl>() != null) tipoEnemigo = 1.2f;
                        }
                        float aleatorio = Random.Range(0, 0.3f);
                        int aleatorio2 = Random.Range(0, 3);
                        GameObject explode;

                        while(objetives[numeroObjetivo].transform.gameObject.GetComponentInChildren<GenericEnemyControl>() == null && numeroObjetivo < objetives.Length-1)
                        {
                            if (objetives[numeroObjetivo].transform.gameObject.GetComponentInChildren<ParacaEnemyControl>() != null) break;
                            if (objetives[numeroObjetivo].transform.gameObject.GetComponentInChildren<VoladorEnemyControl>() != null) break;
                            numeroObjetivo++;
                        }

                        switch (currentWeapon)
                        {
                            case WeaponTypes._wtGun:
                                explode = Instantiate(explosionPistola[aleatorio2], new Vector2(objetives[numeroObjetivo].transform.position.x + tipoEnemigoX +  + aleatorio, objetives[numeroObjetivo].transform.position.y + aleatorio + tipoEnemigo - 0.4f), Quaternion.identity);
                                break;
                            case WeaponTypes._wtRifle:
                                explode = Instantiate(explosionRifle[aleatorio2], new Vector2(objetives[numeroObjetivo].transform.position.x + tipoEnemigoX + aleatorio, objetives[numeroObjetivo].transform.position.y + aleatorio + tipoEnemigo - 0.4f), Quaternion.identity);
                                break;
                            case WeaponTypes._wtShotgun:
                                explode = Instantiate(explosionEscopeta[aleatorio2], new Vector2(objetives[numeroObjetivo].transform.position.x + tipoEnemigoX + aleatorio, objetives[numeroObjetivo].transform.position.y + aleatorio + tipoEnemigo - 0.4f), Quaternion.identity);
                                break;
                            case WeaponTypes._wtSniper:
                                explode = Instantiate(explosionRifle[aleatorio2], new Vector2(objetives[numeroObjetivo].transform.position.x + tipoEnemigoX + aleatorio, objetives[numeroObjetivo].transform.position.y + aleatorio + tipoEnemigo - 0.4f), Quaternion.identity);
                                break;
                            case WeaponTypes._wtUzi:
                                explode = Instantiate(explosionRifle[aleatorio2], new Vector2(objetives[numeroObjetivo].transform.position.x + tipoEnemigoX + aleatorio, objetives[numeroObjetivo].transform.position.y + aleatorio + tipoEnemigo - 0.4f), Quaternion.identity);
                                break;
                            case WeaponTypes._wtLaser:
                                explode = Instantiate(explosionLaser[0], new Vector2(objetives[numeroObjetivo].transform.position.x + tipoEnemigoX, objetives[numeroObjetivo].transform.position.y + tipoEnemigo - 0.4f), Quaternion.identity);
                                break;
                        }
                        if(currentWeapon != WeaponTypes._wtLaser)
                        break;
                        else
                        {
                            if (vidaEnemigo + danoFinal > 0)
                            {
                                GameObject objetovida = Instantiate(vidaUp, new Vector2(objetives[z].transform.position.x, objetives[numeroObjetivo].transform.position.y + tipoEnemigo), new Quaternion(0, 0, 0, 0), GameObject.FindGameObjectWithTag("InterfazW").transform);
                                objetovida.GetComponent<AnimacionRestaVida>().EfectoVida((int)danoFinal);
                            }
                        }
                    }
                    else
                    {     
                        danoFinal = 0;
                        if ((interactableObject.reactorType == InteractableObject.ReactorType.oro) ||
                             (interactableObject.reactorType == InteractableObject.ReactorType.dinamita) ||
                          (interactableObject.reactorType == InteractableObject.ReactorType.carbon))
                        {
                            break;
                        }  
                    }
                }
            }

            z++;
        }

        if(currentWeapon != WeaponTypes._wtLaser)
        {
            if (posicionBicho != new Vector3(0, 0, 0))
            {
                if (vidaEnemigo + danoFinal > 0)
                {
                    GameObject objetovida = Instantiate(vidaUp, new Vector2(posicionBicho.x, posicionBicho.y + tipoEnemigo), new Quaternion(0, 0, 0, 0), GameObject.FindGameObjectWithTag("InterfazW").transform);
                    objetovida.GetComponent<AnimacionRestaVida>().EfectoVida((int)danoFinal);
                }
            }
        }

        if (currentWeapon != WeaponTypes._wtLaser && currentWeapon != WeaponTypes._wtPinchos && currentWeapon != WeaponTypes._wtMina && currentWeapon != WeaponTypes._wtBarricada)
        ReduceWeaponAmmo(currentWeapon);
    }

    public void RPGGrenadeLaunch(Transform prefabGranadaRPG, Vector2 posLanzamientoGranada, Vector2 shootPosition)
    {
        angleRad = Mathf.Atan2(shootPosition.y - posLanzamientoGranada.y, shootPosition.x - posLanzamientoGranada.x);
        angleDeg = (180f / Mathf.PI) * angleRad;
        Transform temp = Instantiate(prefabGranadaRPG, posLanzamientoGranada, Quaternion.Euler(0f, 0f, angleDeg));
        temp.localScale = transform.parent.GetComponent<ProtaControl>().armadura.transform.localScale;

        ReduceWeaponAmmo(MiarmaControla.WeaponTypes._wtGrenadeLauncher);
    }

    public float GetCurrenWeaponDamage()
    {
        return weaponDamage;
    }

    public void CambiarDireccion(Vector2 shootPosition)
    {
        if (shootPosition.x < 0)
        {
            if (rotacion != null)
                Protagonista.GetComponent<Transform>().localRotation = rotacion.GetComponent<Transform>().localRotation;
        }
        else
        {
            if (rotacion != null)
                Protagonista.GetComponent<Transform>().localRotation = new Quaternion();
        }
    }

    public void GrenadeDrop(Transform prefabGranada, Vector2 posLanzamientoGranada, Vector2 shootPosition)
    {
        Transform temp = Instantiate(prefabGranada, posLanzamientoGranada, Quaternion.identity);
        temp.GetComponent<Granada>().Lanzar(temp.transform.position, shootPosition);
        ReduceWeaponAmmo(WeaponTypes._wtGrenade);
    }

    public void Barricada(int id)
    {
        if(protaInfo.Ammo[12] > 0 && !posicionado[id-1])
        {
            barTocada = id;
            SacarBarricada(preBarricada.transform);
        }
    }

    public void Mina(int id)
    {
        if (protaInfo.Ammo[13] > 0 && !Subposicion[id-1])
        {
            barTocada = id;
            SacarMina(preMina.transform);
        }
    }

    public void Pincho(int id)
    {
        if (protaInfo.Ammo[14] > 0 && !posicionado[id-1])
        {
            barTocada = id;
            SacarPinchos(prePinchos.transform);
        }
    }

    void SacarObjeto(int posicionF, float posX, Transform prefab, bool esCentro)
    {
        Debug.Log("D");
      //  Debug.Log("PosicionF: " + posicionF + "posX: " + posX);
        posicionado[posicionF] = true;

        Subposicion[posicionF * 2] = true;
        Subposicion[(posicionF * 2) + 1] = true;

        Transform temp = Instantiate(prefab, new Vector3(posX, -4, 0), Quaternion.identity);
        temp.transform.position = new Vector3(posX, -4, 0);
        if (esCentro) temp.GetComponent<BoxCollider2D>().offset = new Vector2(0, temp.GetComponent<BoxCollider2D>().offset.y);
        if(temp.GetComponent<ControladorBarricada>() != null)
        temp.GetComponent<ControladorBarricada>().Emerger(posicionF);
        if (temp.GetComponent<ControladorPinchos>() != null)
            temp.GetComponent<ControladorPinchos>().Emerger(posicionF);

        if (temp.GetComponent<ControladorBarricada>() != null)
            ReduceWeaponAmmo(WeaponTypes._wtBarricada);
        if (temp.GetComponent<ControladorPinchos>() != null)
            ReduceWeaponAmmo(WeaponTypes._wtPinchos);
    }

    void SacarObjeto(int posicionF, int subPosicion, float posX, Transform prefab, bool esCentro)
    {
        //Debug.Log(subPosicion);
        Subposicion[subPosicion] = true;
        posicionado[posicionF] = true;

        Transform temp = Instantiate(prefab, new Vector3(posX, -4, 0), Quaternion.identity);
        temp.transform.position = new Vector3(posX, -4, 0);
        if (esCentro) temp.GetComponent<BoxCollider2D>().offset = new Vector2(0, temp.GetComponent<BoxCollider2D>().offset.y);

        temp.GetComponent<ControladorMinas>().Emerger(posicionF, subPosicion);

        ReduceWeaponAmmo(WeaponTypes._wtMina);
    }

    public void SacarBarricada(Transform prefabBarricada)
    {
        if (protaInfo.esCentro == false)
        {
            if (barTocada == 1) SacarObjeto(0, -2.88f, prefabBarricada, false);
            if (barTocada == 2) SacarObjeto(1, -0.72f, prefabBarricada, false);
            if (barTocada == 3) SacarObjeto(2, 1.4f, prefabBarricada, false);
            if (barTocada == 4) SacarObjeto(3, 3.54f, prefabBarricada, false);
        }
        else
        {
            if (barTocada == 1) SacarObjeto(0, -3.53f, prefabBarricada, true);
            if (barTocada == 2) SacarObjeto(1, 2.05f, prefabBarricada, true);         
        }

    }

    public void SacarPinchos(Transform prefabPinchos)
    {
        if (protaInfo.esCentro == false)
        {
            if (barTocada == 1) SacarObjeto(0, -2.18f, prefabPinchos, false);
            if (barTocada == 2) SacarObjeto(1, -0.05f, prefabPinchos, false);
            if (barTocada == 3) SacarObjeto(2, 2.19f, prefabPinchos, false);
            if (barTocada == 4) SacarObjeto(3, 4.2f, prefabPinchos, false);
        }
        else
        {
            if (barTocada == 1) SacarObjeto(0, -2.83f, prefabPinchos, true);
            if (barTocada == 2) SacarObjeto(1, 2.75f, prefabPinchos, true);
        }


    }

    public void SacarMina(Transform prefabMina)
    {
        if (protaInfo.esCentro == false)
        {
            if (barTocada == 1) SacarObjeto(0, 0, -2.82f, prefabMina, false);
            if (barTocada == 2) SacarObjeto(0, 1, -1.86f, prefabMina, false);
            if (barTocada == 3) SacarObjeto(1, 2, -0.56f, prefabMina, false);
            if (barTocada == 4) SacarObjeto(1, 3, 0.42f, prefabMina, false);
            if (barTocada == 5) SacarObjeto(2, 4, 1.72f, prefabMina, false);
            if (barTocada == 6) SacarObjeto(2, 5, 2.76f, prefabMina, false);
            if (barTocada == 7) SacarObjeto(3, 6, 4.15f, prefabMina, false);
            if (barTocada == 8) SacarObjeto(3, 7, 5.17f, prefabMina, false);
        }
        else
        {
            Debug.Log(barTocada);
            if (barTocada == 1) SacarObjeto(0, 0, -3.63f, prefabMina, true);
            if (barTocada == 2) SacarObjeto(0, 1, -2.65f, prefabMina, true);
            if (barTocada == 3) SacarObjeto(1, 2, 2.48f, prefabMina, true);
            if (barTocada == 4) SacarObjeto(1, 3, 3.46f, prefabMina, true);
            if (barTocada == 5) SacarObjeto(2, 4, -4.61f, prefabMina, true);
            if (barTocada == 6) SacarObjeto(2, 5, 4.38f, prefabMina, true);
        }
    }


    void CargaSprites()
    {
        if (!SpritesCargados)
        {
            spritesOriginales = new Sprite[armasSprites.Length];

            for (int i = 0; i < spritesOriginales.Length; i++)
            {
                if(armasSprites[i] != null)
                spritesOriginales[i] = armasSprites[i].sprite;
            }

            SpritesCargados = true;
        }
    }
    //***************************************************************************************************************
    //  WEAPON CHANGE
    //***************************************************************************************************************
    public void CargaArmaPrincipal(int idCasillero, int casillero)
    {
        CargaSprites();
        weaponDamage = protaInfo.Damage[idCasillero];
        weaponAlcance = protaInfo.Alcance[idCasillero];
        weaponRadio = protaInfo.Radio[idCasillero];
        for (int i = 0; i < armaSeleccionada.Length; i++)
        {

            armasSprites[i].sprite = spritesOriginales[i];

            if (!frontal)
            {
                Color micolor = armasSprites[i + 5].color;
                armasSprites[i + 5].color = new Color(micolor.r, micolor.g, micolor.b, 0);
                armaSeleccionada[i].SetActive(false);
            }

            //  armasSprites[i].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);

        }
        //zumArma
        if (!frontal)
        {
            armasSprites[casillero].sprite = IniciarJuego.shopData.GetArma(idCasillero).itemThumbnail[PlayerPrefs.GetInt(SaveGame.nivelesArmas[idCasillero])];
            Color micolor = armasSprites[casillero + 5].color;
            armasSprites[casillero +5].color = new Color(micolor.r, micolor.g, micolor.b, 0.75f);
            armasSprites[casillero].transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
        }
        else
        {
            if (casillero != 0) {

                if(idCasillero != 5)
                armasSprites[casillero].sprite = IniciarJuego.shopData.GetArma(idCasillero).itemThumbnail[PlayerPrefs.GetInt(SaveGame.nivelesArmas[idCasillero])];
                else armasSprites[casillero].sprite = IniciarJuego.shopData.GetArma(8).itemThumbnail[0];
            }
            else armasSprites[casillero].sprite = gatling;
        }
        armaSeleccionada[casillero].SetActive(true);
        if (armaPulsada != idCasillero)
        {
            StartCoroutine(RetrasoRecarga(casillero));
            armaPulsada = idCasillero;
        }

        casillasArmas[idCasillero] = casillero;
    }

    public void CargarArmaSecundaria(int idCasillero, int casillero)
    {
        weaponDamage = protaInfo.Damage[idCasillero];
        weaponAlcance = protaInfo.Alcance[idCasillero];
        weaponRadio = protaInfo.Radio[idCasillero];

        for (int i = 0; i < armaSeleccionada.Length; i++)
        {
            if(spritesOriginales != null)
            armasSprites[i].sprite = spritesOriginales[i];

            Color micolor2 = armasSprites[i + 5].color;
            armasSprites[i + 5].color = new Color(micolor2.r, micolor2.g, micolor2.b, 0);
       //     armasSprites[i].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);


            armaSeleccionada[i].SetActive(false);
        }

        armaSeleccionada[casillero].SetActive(true);
        armaSeleccionada[casillero].GetComponent<Button>().interactable = false;

        //zumArma
        armasSprites[casillero].sprite = IniciarJuego.shopData.GetArma(idCasillero).itemThumbnail[PlayerPrefs.GetInt(SaveGame.nivelesArmas[idCasillero])];

        armasSprites[casillero].sprite = IniciarJuego.shopData.GetArma(idCasillero).itemThumbnail[PlayerPrefs.GetInt(SaveGame.nivelesArmas[idCasillero])];
        Color micolor = armasSprites[casillero + 5].color;
        armasSprites[casillero + 5].color = new Color(micolor.r, micolor.g, micolor.b, 0.75f);
        armasSprites[casillero].transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);

        casillasArmas[idCasillero] = casillero;
    }

    public void SelectWeapon(WeaponTypes newWeapon, int casillero)
    {
        currentWeapon = newWeapon;
        switch (currentWeapon)
        {
            case WeaponTypes._wtGun:
                CargaArmaPrincipal(1, casillero);
                break;
            case WeaponTypes._wtRifle:
                CargaArmaPrincipal(2, casillero);
                break;
            case WeaponTypes._wtShotgun:
                CargaArmaPrincipal(3, casillero);
                break;
            case WeaponTypes._wtUzi:
                CargaArmaPrincipal(6, casillero);
                break;
            case WeaponTypes._wtSniper:
                CargaArmaPrincipal(4, casillero);           
                break;
            case WeaponTypes._wtGrenadeLauncher:
                CargaArmaPrincipal(5, casillero);
                break;
            case WeaponTypes._wtLaser:
                CargaArmaPrincipal(7, casillero);
                break;
            case WeaponTypes._wtGrenade:
                CargarArmaSecundaria(11, casillero);
                break;
            case WeaponTypes._wtBarricada:
                CargarArmaSecundaria(12, casillero);
                break;
            case WeaponTypes._wtMina:
                CargarArmaSecundaria(13, casillero);
                break;
            case WeaponTypes._wtPinchos:
                CargarArmaSecundaria(14, casillero);
                break;
        }

        if (!frontal)
        {
            if (currentWeapon == WeaponTypes._wtBarricada)
            {
                posicionesBarricada.gameObject.SetActive(true);
            }
            else
            {
                posicionesBarricada.gameObject.SetActive(false);
            }

            if (currentWeapon == WeaponTypes._wtMina)
            {
                posicionesMina.gameObject.SetActive(true);
            }
            else
            {
                posicionesMina.gameObject.SetActive(false);
            }

            if (currentWeapon == WeaponTypes._wtPinchos)
            {
                posicionesPinchos.gameObject.SetActive(true);
            }
            else
            {
                posicionesPinchos.gameObject.SetActive(false);
            }
        }  

        transform.parent.GetComponent<ProtaControl>().CambiarArma();
    }

    //*************************************************************************************************************************
    //  OTRAS FUNCIONES
    //*************************************************************************************************************************
    private bool WeaponHasAmmo(WeaponTypes tipoArma)
    {
        bool tieneMunicion = false;

        if (frontal) return true;

        switch (tipoArma)
        {
            case WeaponTypes._wtGun:
                tieneMunicion = protaInfo.Ammo[1] > 0;
                break;
            case WeaponTypes._wtRifle:
                tieneMunicion = protaInfo.Ammo[2] > 0;
                break;
            case WeaponTypes._wtShotgun:
                tieneMunicion = protaInfo.Ammo[3] > 0;
                break;
            case WeaponTypes._wtUzi:
                tieneMunicion = protaInfo.Ammo[6] > 0;
                break;
            case WeaponTypes._wtSniper:
                tieneMunicion = protaInfo.Ammo[4]> 0;
                break;
            case WeaponTypes._wtGrenadeLauncher:
                tieneMunicion = protaInfo.Ammo[5] > 0;
                break;
            case WeaponTypes._wtLaser:
                tieneMunicion = protaInfo.Ammo[7] > 0;
                break;
            case WeaponTypes._wtGrenade:
                tieneMunicion = protaInfo.Ammo[11] > 0;
                break;
            case WeaponTypes._wtBarricada:
                tieneMunicion = protaInfo.Ammo[12] > 0;
                break;
            case WeaponTypes._wtMina:
                tieneMunicion = protaInfo.Ammo[13] > 0;
                break;
            case WeaponTypes._wtPinchos:
                tieneMunicion = protaInfo.Ammo[14] > 0;
                break;
        }

        return tieneMunicion;
    }

    IEnumerator RetrasoRecarga(int idArma)
    {
        armaSeleccionada[idArma].GetComponent<Button>().interactable = false;
        yield return new WaitForSeconds(0.5f);
        armaSeleccionada[idArma].GetComponent<Button>().interactable = true;
    }

    IEnumerator Temporizador(float segundos, int idArma, bool esSecundaria)
    {
        habilitado = false;
        yield return new WaitForSeconds(segundos);

        if (!esSecundaria)
        {
            if(frontal && idArma == 2)
            {
                protaInfo.Ammo[idArma] = 100;
            }
            else
            {
           //     Debug.Log(protaInfo.Capacidad[idArma]);
                protaInfo.Ammo[idArma] = protaInfo.Capacidad[idArma];
            }

        }

        habilitado = true;

    }

    IEnumerator TemporizadorRecargaAntes(float segundos, int idArma, bool esSecundaria)
    {
        habilitado = false;
        yield return new WaitForSeconds(segundos);

        if (!esSecundaria)
        {
            protaInfo.Ammo[idArma] = protaInfo.Capacidad[idArma];
        }
            

        habilitado = true;


    }

    public void RecargarArmaAntes(int idArma)
    {
       // Debug.Log("Que si recargo coñoi");
        float tiempo;
        float[] temporizador = new float[15];
        int[] temporizadorBalasTotal = new int[15];
        int[] temporizadorBalasCapacidad = new int[15];

        for(int i = 0; i < temporizador.Length; i++)
        {
            temporizador[i] = protaInfo.Recarga[i];
            temporizadorBalasTotal[i] = protaInfo.Ammo[i];
            temporizadorBalasCapacidad[i] = protaInfo.Capacidad[i];
        }

        if (temporizadorBalasCapacidad[ControladorSelectorArmas.armasSeleccinadas[idArma]] != temporizadorBalasTotal[ControladorSelectorArmas.armasSeleccinadas[idArma]])
        {
            protaInfo.Ammo[ControladorSelectorArmas.armasSeleccinadas[idArma]] = 0;
            tiempo = temporizador[ControladorSelectorArmas.armasSeleccinadas[idArma]];
            if (idArma < 10)
                StartCoroutine(TemporizadorRecargaAntes(tiempo, ControladorSelectorArmas.armasSeleccinadas[idArma], false));
            else StartCoroutine(TemporizadorRecargaAntes(tiempo, ControladorSelectorArmas.armasSeleccinadas[idArma], true));
            segundos[idArma] = tiempo;
            cambiarImagen[idArma] = true;
        }
    }

    public void ReducirBalaArma(int idArmarecudir)
    {

        protaInfo.Ammo[idArmarecudir]--;
        if (protaInfo.Ammo[idArmarecudir] <= 0)
        {
      //      Debug.Log(idArmarecudir);
            StartCoroutine(Temporizador(protaInfo.Recarga[idArmarecudir], idArmarecudir, false));
            for (int i = 0; i < ProtaInfo.posicionArmaFinal.Length; i++) if (ProtaInfo.posicionArmaFinal[i] == idArmarecudir)
                    segundos[casillasArmas[idArmarecudir]] = protaInfo.Recarga[idArmarecudir];
            cambiarImagen[casillasArmas[idArmarecudir]] = true;
        }
    }

    public void ReducirBalaSecundaria(int idArmarecudir)
    {
      //  Debug.Log("Reducir");
        protaInfo.Ammo[idArmarecudir]--;
     //   StartCoroutine(Temporizador(protaInfo.Cadencia[idArmarecudir], idArmarecudir, true));
    //    segundos[casillasArmas[idArmarecudir]] = protaInfo.Recarga[idArmarecudir];
    //    cambiarImagen[casillasArmas[idArmarecudir]] = true;
    }

    private void ReduceWeaponAmmo(WeaponTypes tipoArma)
    {
        switch (tipoArma)
        {
            case WeaponTypes._wtGun:
                ReducirBalaArma(1);
                break;
            case WeaponTypes._wtRifle:
                ReducirBalaArma(2);
                break;
            case WeaponTypes._wtShotgun:
                ReducirBalaArma(3);
                break;
            case WeaponTypes._wtUzi:
                ReducirBalaArma(6);
                break;
            case WeaponTypes._wtSniper:
                ReducirBalaArma(4);
                break;
            case WeaponTypes._wtGrenadeLauncher:
                ReducirBalaArma(5);
                break;
            case WeaponTypes._wtLaser:
                ReducirBalaArma(7);
                break;
            case WeaponTypes._wtGrenade:
                ReducirBalaSecundaria(11);
                break;
            case WeaponTypes._wtBarricada:
                ReducirBalaSecundaria(12);
                break;
            case WeaponTypes._wtMina:
                ReducirBalaSecundaria(13);
                break;
            case WeaponTypes._wtPinchos:
                ReducirBalaSecundaria(14);
                break;
        }
    }
}
