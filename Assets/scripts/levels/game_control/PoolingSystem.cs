﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PoolingSystem : MonoBehaviour {

    protected enum PrefabEstate { _peDisabledNew, _peDisabledOld, _peRunning}
    private class PreloadedEnemy
    {
        public GameObject prefab;
        public PrefabEstate created;

        public PreloadedEnemy (GameObject newPrefab)
        {
            prefab = newPrefab;
            created = PrefabEstate._peDisabledNew;
        }
    }

    [Tooltip("This number indicates how many enemies of every type will be pre-loaded")]
    public int maxEnemiesOfEach;
    [Tooltip("This list indicates what enemies you want for THIS SCENE. If you don't put a type of enemy, it will not generate in this level")]
    public GameObject[] levelEnemyPrefabList;
 ///   private string[] prefabPathList;
    private List<List<PreloadedEnemy>> listOfListsOfEnemies;
    public GameObject coleccionEnemigos;

    // Use this for initialization
    /*	void Start () {


            GameObject tempGameObject;
            List<PreloadedEnemy> tempLista;
            listOfListsOfEnemies = new List<List<PreloadedEnemy>>();
            prefabPathList = new string[levelEnemyPrefabList.Length];

            for (int i = 0; i < levelEnemyPrefabList.Length; i++)
            {

                //Generation of enemy
                tempLista = new List<PreloadedEnemy>();
                for (int j = 0; j < maxEnemiesOfEach; j++)
                {
                    tempGameObject = (GameObject)Instantiate(levelEnemyPrefabList[i],
                                                                new Vector3(-1000f, -1000f, 1f),
                                                                Quaternion.identity, transform);
                    tempGameObject.SetActive(false);
                    tempLista.Add(new PreloadedEnemy (tempGameObject));
                }

                listOfListsOfEnemies.Add(tempLista);
            }
        }*/

    void Start()
    {


        GameObject tempGameObject;
        List<PreloadedEnemy> tempLista;
        listOfListsOfEnemies = new List<List<PreloadedEnemy>>();
      //  prefabPathList = new string[levelEnemyPrefabList.Length];
        string nombreObjeto = transform.GetChild(0).name;
        tempLista = new List<PreloadedEnemy>();

        for (int i = 0; i < transform.childCount; i++)
        {

            //Generation of enemy

            tempGameObject = transform.GetChild(i).gameObject;
            tempGameObject.SetActive(false);
            if (nombreObjeto != tempGameObject.name) {
            
                listOfListsOfEnemies.Add(tempLista);
                tempLista = new List<PreloadedEnemy>();
                nombreObjeto = tempGameObject.name;

            }
            tempLista.Add(new PreloadedEnemy(tempGameObject));
 
        }
        //Adding last list
        listOfListsOfEnemies.Add(tempLista);

        DebugImprimirLista();
    }


    // Update is called once per frame
    void Update () {
		
	}


    /*private int FindPrefabFromTag (string searchTag)
    {
        //Debug.Log("Buscando tag: " + searchTag);
        int index = -1;
        int cont = 0;
        while ((index < 0) && (cont < levelEnemyPrefabList.Length))
        {
            //Debug.Log("Posicion: " + cont + "    Tag: " + levelEnemyPrefabList[cont].tag);
            if (levelEnemyPrefabList[cont].tag == searchTag)
                index = cont;
            cont++;
        }

        return index;
    }*/

    private int FindPrefabFromTag(string searchTag)
    //Busca el tag en la lista de listas. Supone que no están todos los prefabs de la lista. Es para la prueba de rendimiento.
    {
        //Debug.Log("Buscando tag: " + searchTag);
        int index = -1;
        int cont = 0;
        while ((index < 0) && (cont < listOfListsOfEnemies.Count))
        {
            //Debug.Log("Posicion: " + cont + "    Tag: " + levelEnemyPrefabList[cont].tag);
            if (listOfListsOfEnemies[cont][0].prefab.tag == searchTag)
                index = cont;
            cont++;
        }

        return index;
    }

    private int FindFirstFreeEnemy (int enemyTypeIndex)
    {
        int index = -1;
        int cont = 0;
        while ((index < 0) && (cont < listOfListsOfEnemies[enemyTypeIndex].Count))
        {
            if (listOfListsOfEnemies[enemyTypeIndex][cont].created != PrefabEstate._peRunning)
                index = cont;
            cont++;
        }

        return index;
    }


    //***********************************************************************************************************************
    //  PUBLIC METHODS
    //***********************************************************************************************************************
    public void CreateEnemyFromTag (string newEnemyTag, Vector3 position, Quaternion rotation)
    {
        //Primero, averiguamos la lista de qué enemigo queremos usar
        int enemyTypeIndex = FindPrefabFromTag(newEnemyTag);
        if (enemyTypeIndex >= 0)
        {
            //Ahora tenemos que buscar el primer enemigo libre
            int enemyIndex = FindFirstFreeEnemy(enemyTypeIndex);
            if (enemyIndex >= 0)
            {
                listOfListsOfEnemies[enemyTypeIndex][enemyIndex].prefab.transform.position = position;
                listOfListsOfEnemies[enemyTypeIndex][enemyIndex].prefab.transform.rotation = rotation;
                if (listOfListsOfEnemies[enemyTypeIndex][enemyIndex].created == PrefabEstate._peDisabledOld)
                    listOfListsOfEnemies[enemyTypeIndex][enemyIndex].prefab.GetComponent<EnemyNormalControl>().ResetEnemy();
                listOfListsOfEnemies[enemyTypeIndex][enemyIndex].prefab.SetActive(true);
                listOfListsOfEnemies[enemyTypeIndex][enemyIndex].created = PrefabEstate._peRunning;
            }
        }
        else
        {
            Debug.LogError("Tipo de enemigo no encontrado: " + newEnemyTag);
        }
    }

    public void ResetMe (GameObject me)
    //Enemies call this method to set himself as free in the pooling list
    {
        int enemyTypeIndex = FindPrefabFromTag(me.tag);
        int index = -1;
        int cont = 0;
        //Looking for the gameObject on the list
        while ((index < 0) && (cont < listOfListsOfEnemies[enemyTypeIndex].Count))
        {
            if (listOfListsOfEnemies[enemyTypeIndex][cont].prefab == me)
                listOfListsOfEnemies[enemyTypeIndex][cont].created = PrefabEstate._peDisabledOld;
            cont++;
        }
    }



    //********************************************************
    private void DebugImprimirLista ()
    {
        string data = "";
        Debug.Log("Numero de listas: " + listOfListsOfEnemies.Count);

        for (int i = 0; i < listOfListsOfEnemies.Count; i++)
        {
            if (listOfListsOfEnemies[i][0] != null)
                data += listOfListsOfEnemies[i][0].prefab.name + "\n";
            else
                data += "Nueva lista sin elemento: \n";

            Debug.Log("Numero de elementos de (" + i + "): " + listOfListsOfEnemies[i].Count);
            for (int j = 0; j < listOfListsOfEnemies[i].Count; j++)
            {
                if (listOfListsOfEnemies[i][j] != null)
                    data += "    Element: " + listOfListsOfEnemies[i][j].prefab.name + "\n";
                else
                    data += "    null \n";
            }
        }

        Debug.Log(data);
    }

}


