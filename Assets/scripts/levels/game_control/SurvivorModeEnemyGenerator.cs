﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SurvivorModeEnemyGenerator : MonoBehaviour {

    [Tooltip("Nivel de dificultad de la partida")]
    public int difficultyLevel;
    [Tooltip("Lista de prefabs de enemigos que se pueden generar en el nivel")]
    public GameObject[] enemiesPrefabs;
    [Tooltip("Lista de valores de dificultad para eliminar a cada enemigo")]
    public int[] enemiesDifficulty;

    private int calculatedDifficulty;   //Dificultad actual de la partida, calculada a partir del número de enemigos y su dureza
    private float timeToNextEnemy;      //Tiempo que falta antes de generar un nuevo enemigo
    private bool modoHorda;             //Si está activo, se van a generar muchos enemigos pequeños en poco tiempo
   // private int[] rangosProbabilidades; //Indica el valor máximo para generar a cierto enemigo. Si se supera, se generará el siguiente, que
                                        //es más duro. A mayor diferencia con el anterior, mayor posibilidad de que aparezca. Los rangos
                                        //se calculan en función de la dificultad del nivel, la dificultad calculada, y la dureza de cada
                                        //enemigo
   // private int thoughestEnemy;     //El valor del enemigo más duro
   // private int weakestEnemy;       //El valor del enemigo más débil

	// Use this for initialization
	void Start () {
        timeToNextEnemy = Random.Range(3f, 5f);
        calculatedDifficulty = 0;
        modoHorda = false;
    //    rangosProbabilidades = new int[enemiesPrefabs.Length];
    //    thoughestEnemy = enemiesDifficulty.Max();
     //   weakestEnemy = enemiesDifficulty.Min();
    }
	
	// Update is called once per frame
	void Update () {
        timeToNextEnemy -= Time.deltaTime;
        if (timeToNextEnemy <= 0f)
        {
            if (calculatedDifficulty < difficultyLevel)
                GenerateEnemy();
            else
                CalculaTiempoSigEnemigo();
        }
	}

    private void GenerateEnemy ()
    {
        if (!modoHorda)
        {

        }
        else
        {

        }
    }

    private void CalculaRangosNormales ()
    {
        //int probabilidad;
        for (int i = 0; i < enemiesPrefabs.Length; i++)
        {
            //A más dificultad, más probabilidades de que aparezcan enemigos más grandes
          ///  probabilidad = enemiesDifficulty[i];
            //
        }
    }

    private void CalculaTiempoSigEnemigo ()
    {
        float minTiempo, maxTiempo;
        minTiempo = 5f - (float)difficultyLevel / 10f;
        if (minTiempo < 0f)
            minTiempo = 0f;

        maxTiempo = 15f - (float)difficultyLevel / 5f;
        if (maxTiempo < 2f)
            maxTiempo = 2f;

        timeToNextEnemy = Random.Range(minTiempo, maxTiempo);
    }
}
