﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeControl : MonoBehaviour {

    public Animator pauseMenuAnimator;
    [Tooltip("Seconds that game stays in slow motion when selecting weapon.")]
    public float slowMotionTime;

    public delegate void PauseEventDelegate(bool pauseState);
    public event PauseEventDelegate pauseEvent;

    private float elapsedTime;
    private float timePercent;
    private bool waitingEndingAnimationPauseMenuOut;
    private FadeControl fadeControl;

    
	// Use this for initialization
	void Start () {
 //       if (fadeControl == null)
  //          fadeControl = gameObject.GetComponent<GlobalInfo>().fadeControl;
        enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (elapsedTime < 1f)
        {
            timePercent = 1 - Mathf.Cos(Mathf.PI * (elapsedTime / slowMotionTime)); //cos function from slow to fast
            Time.timeScale = Mathf.Lerp(0.1f, 1f, timePercent);

            elapsedTime += Time.deltaTime;
        }
        else
        {
            Time.timeScale = 1f;
            enabled = false;
        }
	}

    public void Pause ()
    {
       // Time.timeScale = 0.0f;
        enabled = false;
        if (pauseEvent != null)
            pauseEvent(true);
    }

    public void Unpause ()
    {
       // Time.timeScale = 1.0f;
        enabled = false;
        if (pauseEvent != null)
            pauseEvent(false);
    }

    //***********************************************************************************************************
    //  PAUSE AND RESUME BUTTONS
    //***********************************************************************************************************
    public void PauseButtonAction ()
    {
        if (Time.timeScale != 0f)
        {
            //Time.timeScale = 0f;
            fadeControl.FadeToColor(1f, new Color(0f, 0f, 0f, 0.55f), true);
            Pause();
            pauseMenuAnimator.SetTrigger("Menu_in");
        }
        /*else
        {
            //Time.timeScale = 1f;
            Unpause();
            pauseMenuAnimator.SetBool("menu_in", false);
        }*/
    }


    public void ResumeButtonAction ()
    {
        fadeControl.FadeToColor(1f, Color.clear, false);
        pauseMenuAnimator.SetTrigger("Menu_out");
        //And now we have to wait for end animation event
        waitingEndingAnimationPauseMenuOut = true;
    }

    public void ResumeButtonActionEndAnimation ()
    {
        if (waitingEndingAnimationPauseMenuOut)
        {
            Unpause();
            waitingEndingAnimationPauseMenuOut = false;
        }
    }

    //***********************************************************************************************************
    //  OTHER BUTTONS THAT CONTROL TIME
    //***********************************************************************************************************
    public void ChaosControl ()
    {
        elapsedTime = 0f;
        enabled = true;
    }
}
