﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchControl : MonoBehaviour {

    public static Vector2 touchPosition;
    public enum MouseFunction { _mfNone, _mfShoot, _mfWeaponSelect, _mfWeaponSelected };
    public MouseFunction mouseFunction;

    GameObject levelControl;
    GameObject objetoGolpeado;
    Transform prota;
    public bool pauseStatus;
    int casillero;

    void Start () {

        levelControl = GameObject.FindGameObjectWithTag("GameController");
        prota = levelControl.GetComponent<GlobalInfo>().prota;
        objetoGolpeado = null;

    }
	
	// Update is called once per frame
	void Update () {
        mouseFunction = MouseFunction._mfNone;
        if (Input.touchCount > 0)   //Code for touchscreen
        {
            foreach (Touch toque in Input.touches)
            {
                if (toque.phase == TouchPhase.Ended)
                {
                    touchPosition = Camera.main.ScreenToWorldPoint(toque.position);
                    ScreenTouched(Camera.main.ScreenToWorldPoint(toque.position));
                }

                if (toque.phase == TouchPhase.Moved)
                {
                    touchPosition = Camera.main.ScreenToWorldPoint(toque.position);
                    ScreenTouched(Camera.main.ScreenToWorldPoint(toque.position));
                }

                if (toque.phase == TouchPhase.Stationary)
                {
                    touchPosition = Camera.main.ScreenToWorldPoint(toque.position);
                    ScreenTouched(Camera.main.ScreenToWorldPoint(toque.position));
                }
            }
        }
        else
        {
            //No touchscreen detected
            if (Input.GetMouseButton(0))
            {
                touchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                ScreenTouched(Camera.main.ScreenToWorldPoint(Input.mousePosition));
            }
        }

       
    }

    private void ScreenTouched (Vector2 screenTouchedPosition)
    {
        if (!pauseStatus)   //Check if we are in pause mode
        {
            //Camera.main.ScreenPointToRay(touchPosition);
            RaycastHit2D hit = Physics2D.Raycast(screenTouchedPosition, Vector2.zero, 0f);
            if (hit)
            {
                //Debug.Log(hit.transform.name);
                switch (hit.transform.name)
                {
                    case "HitBox":
                        mouseFunction = MouseFunction._mfWeaponSelect;
                        break;
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                    case "6":
                    case "7":
                    case "11":
                    case "12":
                    case "13":
                    case "14":
                        mouseFunction = MouseFunction._mfWeaponSelected;
                        break;
                    case "CantTouchThis":   //This gameObject prevents to touch some screen zones
                        break;
                    default:
                        objetoGolpeado = hit.transform.gameObject;
                        DefaultAction();
                        break;
                }
            }
            else
            {
                DefaultAction();
            }


            switch (mouseFunction)
            {
                case MouseFunction._mfShoot:
                    prota.transform.GetComponentInChildren<MiarmaControla>().MouseToShoot(screenTouchedPosition, objetoGolpeado);
                    break;
                case MouseFunction._mfWeaponSelect:
                    break;
                case MouseFunction._mfWeaponSelected:
                    switch (hit.transform.name)
                    {
                        case "1":
                            for (int i = 0; i < ProtaInfo.posicionArmaFinal.Length; i++) if (ProtaInfo.posicionArmaFinal[i] == 1) casillero = i;
                            prota.GetComponentInChildren<MiarmaControla>().SelectWeapon(MiarmaControla.WeaponTypes._wtGun, casillero);
                            break;
                        case "2":
                            for (int i = 0; i < ProtaInfo.posicionArmaFinal.Length; i++) if (ProtaInfo.posicionArmaFinal[i] == 2) casillero = i;
                            prota.GetComponentInChildren<MiarmaControla>().SelectWeapon(MiarmaControla.WeaponTypes._wtRifle, casillero);
                            break;
                        case "3":
                            for (int i = 0; i < ProtaInfo.posicionArmaFinal.Length; i++) if (ProtaInfo.posicionArmaFinal[i] == 3) casillero = i;
                            prota.GetComponentInChildren<MiarmaControla>().SelectWeapon(MiarmaControla.WeaponTypes._wtShotgun, casillero);
                            break;
                        case "4":
                            for (int i = 0; i < ProtaInfo.posicionArmaFinal.Length; i++) if (ProtaInfo.posicionArmaFinal[i] == 4) casillero = i;
                            prota.GetComponentInChildren<MiarmaControla>().SelectWeapon(MiarmaControla.WeaponTypes._wtSniper, casillero);
                            break;
                        case "5":
                            for (int i = 0; i < ProtaInfo.posicionArmaFinal.Length; i++) if (ProtaInfo.posicionArmaFinal[i] == 5) casillero = i;
                            prota.GetComponentInChildren<MiarmaControla>().SelectWeapon(MiarmaControla.WeaponTypes._wtGrenadeLauncher,casillero);
                            break;                   
                        case "6":
                            for (int i = 0; i < ProtaInfo.posicionArmaFinal.Length; i++) if (ProtaInfo.posicionArmaFinal[i] == 6) casillero = i;
                            prota.GetComponentInChildren<MiarmaControla>().SelectWeapon(MiarmaControla.WeaponTypes._wtUzi, casillero);
                            break;
                        case "7":
                            for (int i = 0; i < ProtaInfo.posicionArmaFinal.Length; i++) if (ProtaInfo.posicionArmaFinal[i] == 7) casillero = i;
                            prota.GetComponentInChildren<MiarmaControla>().SelectWeapon(MiarmaControla.WeaponTypes._wtLaser, casillero);
                            break;
                        case "11":
                            for (int i = 0; i < ProtaInfo.posicionArmaFinal.Length; i++) if (ProtaInfo.posicionArmaFinal[i] == 11) casillero = i;
                            prota.GetComponentInChildren<MiarmaControla>().SelectWeapon(MiarmaControla.WeaponTypes._wtGrenade, casillero);
                            break;
                        case "12":
                            for (int i = 0; i < ProtaInfo.posicionArmaFinal.Length; i++) if (ProtaInfo.posicionArmaFinal[i] == 12) casillero = i;
                            prota.GetComponentInChildren<MiarmaControla>().SelectWeapon(MiarmaControla.WeaponTypes._wtBarricada, casillero);
                            break;
                        case "13":
                            for (int i = 0; i < ProtaInfo.posicionArmaFinal.Length; i++) if (ProtaInfo.posicionArmaFinal[i] == 13) casillero = i;
                            prota.GetComponentInChildren<MiarmaControla>().SelectWeapon(MiarmaControla.WeaponTypes._wtMina, casillero);
                            break;
                        case "14":
                            for (int i = 0; i < ProtaInfo.posicionArmaFinal.Length; i++) if (ProtaInfo.posicionArmaFinal[i] == 14) casillero = i;
                            prota.GetComponentInChildren<MiarmaControla>().SelectWeapon(MiarmaControla.WeaponTypes._wtPinchos, casillero);
                            break;
                    }
                    break;
            }
        }
    }

    private void DefaultAction ()
    {
        mouseFunction = MouseFunction._mfShoot;
    }

    public void OnPauseEvent (bool newPauseStatus)
    {
        pauseStatus = newPauseStatus;
        enabled = !newPauseStatus;
    }

    //********************************************************************************************************************
    //  PUBLIC METHODS
    //********************************************************************************************************************
    public Vector2 GetTouchPosition ()
    {
        Vector2 tempTouchPosition = new Vector2(-1, -1); ;
        if (Input.touchCount > 0)   //Code for touchscreen
        {
            foreach (Touch toque in Input.touches)
            {
                if ((toque.phase == TouchPhase.Moved) || (toque.phase == TouchPhase.Stationary))
                        tempTouchPosition = Camera.main.ScreenToWorldPoint(toque.position);
            }
        }
        else
        {
            //No touchscreen detected
            if (Input.GetMouseButton(0))
                tempTouchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }

        return tempTouchPosition;
    }
}
