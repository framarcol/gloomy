﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ValorPuntosArmas : MonoBehaviour {

    public int idArma;
    public bool esArma;
    public GameObject Controlador;
    bool noRestar;
    int valorArma;
    public Image[] imagenArma;
    bool yaEstoy;

    public AudioClip armaClick;
    public AudioClip armaCLickDes;

    public GameObject cartelInformativo;
    public Text textoInformativo;
    public GameObject modoOk;

    private void Start()
    {
        yaEstoy = false;
        if (IniciarJuego.shopData == null)
            IniciarJuego.Iniciar();

        if (esArma)
        {
            valorArma = IniciarJuego.shopData.GetArma(idArma).Recursos;
        }  
        else valorArma = IniciarJuego.shopData.GetArma(idArma + 10).Recursos;

    }

    public void OnValueChanged(bool activado)
    {
        if (activado)
        {
            Sumar(false);
        }
        else
        {
            Restar(false);
        }
    }

    public void SumarSecundaria()
    {

    }

    public void Sumar(bool boton)
    {
        //Suma Arma

        ShopData.UpgradeItem miNivel = IniciarJuego.shopData.GetMundo(BotonesMenu.nivelMundoSeleccionado, BotonesMenu.mundoSeleccionado);

        int contador = 0;
        string[] nombreArmas = new string[5];

        for(int i = 0; i < imagenArma.Length; i++)
        {
            if (imagenArma[i].sprite.name == "atlas_rifle_5") contador++;
            nombreArmas[i] = imagenArma[i].sprite.name;
        }

        if (contador <= 5 && contador > 0 || yaEstoy)
        {
            yaEstoy = true;

            if (!miNivel.esTutorial && boton)
            {
                if (ControladorSelectorArmas.puntosSumados + valorArma <= ControladorSelectorArmas.totalPuntos)
                {
                    GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[19]);
                    Controlador.GetComponent<ControladorSelectorArmas>().SumaPuntos(idArma, esArma);
                    if (esArma) ControladorSelectorArmas.sumaArmas++;
                    
                }
                else
                {
                    GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[15]);
                    noRestar = true;
                    if (!boton) gameObject.GetComponent<Toggle>().isOn = false;
                    cartelInformativo.SetActive(true);
                    textoInformativo.text = SeleccionaIdiomas.gameText[18, 3];
                    modoOk.SetActive(true);
                }
            }
            else if (!boton)
            {
               // if(idArma == 4) Debug.Log((ControladorSelectorArmas.puntosSumados + valorArma).ToString());
                if (ControladorSelectorArmas.puntosSumados + valorArma <= ControladorSelectorArmas.totalPuntos)
                {
                    GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[19]);
                    Controlador.GetComponent<ControladorSelectorArmas>().SumaPuntos(idArma, esArma);
                    if (esArma) ControladorSelectorArmas.sumaArmas++;
                }
                else
                {
                    GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[15]);
                    noRestar = true;
                    if (!boton) gameObject.GetComponent<Toggle>().isOn = false;

                }
            }
        }
    }

    public void Restar(bool boton)
    {

        if (boton)
        {
            noRestar = false;
        }
 
        if (noRestar)
        {
            noRestar = false;
            
            yaEstoy = false;
        }
        else
        {
           // if()
            GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[20]);
            //Debug.Log(ControladorSelectorArmas.numArmasId[idArma - 1]);

            if (esArma) if (ControladorSelectorArmas.numArmasId[idArma - 1] == 0) yaEstoy = false;
            else if(ControladorSelectorArmas.numArmasId[idArma + 10 - 1] == 0) yaEstoy = false;

            if (ControladorSelectorArmas.puntosSumados > 0)
            Controlador.GetComponent<ControladorSelectorArmas>().RestaPuntos(idArma, esArma);
            if (esArma) ControladorSelectorArmas.sumaArmas--;
        }
    }


}
