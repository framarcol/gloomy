﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponsMenuControl : MonoBehaviour {

    public Vector3 inactivePosition;
    public Vector3 activePosition;
    public bool active;
    [Space(10)]
    public Transform menuPanel;
    public GameObject[] weaponList;

    private GameObject hitBox;
    private TimeControl timeControl;
    private ProtaInfo protaInfo;

    public GameObject ChocqueColider;

	// Use this for initialization
	void Start () {
        if (hitBox == null)
            hitBox = transform.Find("HitBox").gameObject;
        if (timeControl == null)
            timeControl = GameObject.Find("LevelControl").GetComponent<TimeControl>();
        if (protaInfo == null)
            protaInfo = GameObject.FindGameObjectWithTag("GameController").GetComponent<GlobalInfo>().prota.GetComponent<ProtaInfo>();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ActivateMenu()
    {
        UpdateWeaponsWithAmmo();
        menuPanel.localPosition = activePosition;
        active = true;
        //hitBox.SetActive(false);
        hitBox.GetComponent<BoxCollider2D>().enabled = false;
        timeControl.ChaosControl();
    }

    public void DeactivateMenu()
    {
        menuPanel.localPosition = inactivePosition;
        active = false;
        //hitBox.SetActive(true);
        hitBox.GetComponent<BoxCollider2D>().enabled = true;
        timeControl.Unpause();
    }

    public void CambiarImagenArma(Sprite arma)
    {
        ChocqueColider.GetComponentInChildren<SpriteRenderer>().sprite = arma;
    }

    public void UpdateWeaponsWithAmmo ()
    {
      //  weaponList[1].SetActive(protaInfo.rifleAmmo > 0);
      //  weaponList[2].SetActive(protaInfo.shotgunAmmo > 0);
      //  weaponList[3].SetActive(protaInfo.uziAmmo > 0);
      //  weaponList[4].SetActive(protaInfo.sniperAmmo > 0);
//weaponList[5].SetActive(protaInfo.grenadeLauncherAmmo > 0);
      //  weaponList[6].SetActive(protaInfo.grenadesAmmo > 0);
    }
}
