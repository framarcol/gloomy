﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomTest : MonoBehaviour {

    [Range(-10f,10f)]
    public float cameraZoom = 3.6f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Camera.main.orthographicSize = cameraZoom;
	}
}
