﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventosMenuPausa : MonoBehaviour {

    private TimeControl timeControl;

	// Use this for initialization
	void Start () {
        if (timeControl == null)
            timeControl = GameObject.Find("LevelControl").GetComponent<TimeControl>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void EventoMenuFueraDePantalla ()
    {
        timeControl.ResumeButtonActionEndAnimation();
    }
}
