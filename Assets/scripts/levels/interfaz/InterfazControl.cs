﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfazControl : MonoBehaviour {

    public float protaX, protaY;

	void Start () {

        bool frontal = false;
        if (IniciarJuego.shopData != null)
            frontal = IniciarJuego.shopData.GetMundo(BotonesMenu.nivelMundoSeleccionado, BotonesMenu.mundoSeleccionado).frontal;

        if(!frontal)
        GetComponent<Transform>().position = new Vector2(protaX, protaY);
    }

    public void ChangeBullets (int newValue)
    {

    }

    public void ChangeBullets(int newValue, int idArma)
    {
    }


    public void ChangeEnergy(int newValue)
    {
        if (newValue < 0)
            newValue = 0;
    }

    public void ChangeBarricades(int newValue)
    {
        if (newValue < 0)
            newValue = 0;
    }

    public void ChangePoints(int newValue)
    {
        string tempString = "";
        if (newValue < 100000)
            tempString += "0";
        if (newValue < 10000)
            tempString += "0";
        if (newValue < 1000)
            tempString += "0";
        if (newValue < 100)
            tempString += "0";
        tempString += newValue.ToString();
    }

}
