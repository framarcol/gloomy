﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenusEmergentes : MonoBehaviour {


    public GameObject menuPausa;
    public GameObject generaHordas;
    //public GameObject interfaz;
    public GameObject ObjetoPausa, objetoConseguido, objetoMuerte;
    //public GameObject GeneradorHordas;
    public static bool activar;
    public static int diamantes;

    private void Start()
    {

        diamantes = PlayerPrefs.GetInt(SaveGame.diamantes);
      //  Debug.Log(diamantes);
    }

    private void Update()
    {
        if (activar)
        {
            Reanudar();
            activar = false;
        }
    }

    public void MostrarPausa()
    {
        objetoMuerte.SetActive(true);
        menuPausa.SetActive(true);
    }

    public void MostrarPausa2()
    {
        if (objetoMuerte.activeSelf) objetoMuerte.SetActive(false);
        objetoConseguido.SetActive(true);
        menuPausa.SetActive(true);
    }

    public void Pausar()
    {
        ObjetoPausa.SetActive(true);
     //   interfaz.SetActive(false);
    //  generaHordas.SetActive(false);
        menuPausa.SetActive(true);
    }

    public void Reanudar()
    {
       // GeneradorHordas.GetComponent<GeneradorHordas>().CheckHordas();
        ObjetoPausa.SetActive(false);
        objetoConseguido.SetActive(false);
        objetoMuerte.SetActive(false);
       // interfaz.SetActive(true);
        generaHordas.SetActive(true);
     //   menuPausa.SetActive(false);
    }
}
