﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmaFX : MonoBehaviour {

    public GameObject armaSprite;
    public Transform posCanon;
    private Animator animator;
    
	// Use this for initialization
	void Start () {
        RefreshWeaponPosition(MiarmaControla.WeaponTypes._wtGun);
        if (animator == null)
        {
            animator = GetComponentInChildren<Animator>();
        }
	}
	
	// Update is called once per frame
	void Update () {
        transform.localPosition = Vector3.Scale(armaSprite.transform.localPosition, armaSprite.transform.parent.transform.localScale) +
            armaSprite.transform.parent.transform.localPosition;
            //+ new Vector3(desplRight, desplUp, 0f);
        transform.rotation = armaSprite.transform.rotation;
	}

    public void RefreshWeaponPosition (MiarmaControla.WeaponTypes newWeapon)
    {
        switch (newWeapon)
        {
            case MiarmaControla.WeaponTypes._wtGun:
                posCanon.localPosition = new Vector3(armaSprite.GetComponent<MeshRenderer>().bounds.size.x / 2.0f,
                                                     armaSprite.GetComponent<MeshRenderer>().bounds.size.y * 3f / 8.0f,
                                                     0f);
                break;
            case MiarmaControla.WeaponTypes._wtRifle:
                posCanon.localPosition = new Vector3(armaSprite.GetComponent<MeshRenderer>().bounds.size.x / 2.0f,
                                                     armaSprite.GetComponent<MeshRenderer>().bounds.size.y * 2f / 6.0f,
                                                     0f);
                break;
            case MiarmaControla.WeaponTypes._wtShotgun:
                posCanon.localPosition = new Vector3(armaSprite.GetComponent<MeshRenderer>().bounds.size.x / 2.0f,
                                                     armaSprite.GetComponent<MeshRenderer>().bounds.size.y * 3f / 10.0f,
                                                     0f);
                break;
            case MiarmaControla.WeaponTypes._wtUzi:
                posCanon.localPosition = new Vector3(armaSprite.GetComponent<MeshRenderer>().bounds.size.x / 2.0f,
                                                     armaSprite.GetComponent<MeshRenderer>().bounds.size.y / 4.0f,
                                                     0f);
                break;
            case MiarmaControla.WeaponTypes._wtSniper:
                posCanon.localPosition = new Vector3(armaSprite.GetComponent<MeshRenderer>().bounds.size.x / 2.0f,
                                                     0f,
                                                     0f);
                break;
            case MiarmaControla.WeaponTypes._wtGrenadeLauncher:
                posCanon.localPosition = new Vector3(armaSprite.GetComponent<MeshRenderer>().bounds.size.x / 5.0f,
                                                     armaSprite.GetComponent<MeshRenderer>().bounds.size.y / 4.0f,
                                                     0f);
                break;
            default:
                posCanon.localPosition = new Vector3(armaSprite.GetComponent<MeshRenderer>().bounds.size.x / 2.0f,
                                                     armaSprite.GetComponent<MeshRenderer>().bounds.size.y / 2.0f,
                                                     0f);
                break;
        }
    }

    public void PlayFX (MiarmaControla.WeaponTypes newWeapon)
    {
        switch (newWeapon)
        {
            case MiarmaControla.WeaponTypes._wtGun:
                animator.SetTrigger("DisparoGun");
                break;
            case MiarmaControla.WeaponTypes._wtRifle:
                animator.SetTrigger("DisparoRifle");
                break;
            case MiarmaControla.WeaponTypes._wtShotgun:
                animator.SetTrigger("DisparoShotgun");
                break;
            case MiarmaControla.WeaponTypes._wtUzi:
                animator.SetTrigger("DisparoUzi");
                break;
            case MiarmaControla.WeaponTypes._wtSniper:
                animator.SetTrigger("DisparoSniper");
                break;
            case MiarmaControla.WeaponTypes._wtGrenadeLauncher:
                animator.SetTrigger("DisparoGrenadeLauncher");
                break;
            default:
                break;
        }
    }
}
