﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Granada : MonoBehaviour {

    public float tiempoDeVuelo;
    public float tiempoDetonacion;
    public GameObject explosion;
	
	// Update is called once per frame
	void Update () {
        tiempoDetonacion -= Time.deltaTime;
        if (tiempoDetonacion <= 0f)
        {
            // Destroy(gameObject);
            // Destroy(GetComponent<Rigidbody2D>());
            //  Destroy(GetComponent<CapsuleCollider2D>());
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            transform.eulerAngles = Vector3.forward * Random.Range(-45f, 45f);
            transform.Find("Sprite").gameObject.SetActive(false);
            transform.Find("Explosion").gameObject.SetActive(true);
            GetComponentInChildren<Granada>().enabled = false;
            GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[5]);
            explosion.SetActive(true);
        }
	}

    public void Lanzar (Vector2 origen, Vector2 destino)
    {
        gameObject.SetActive(true);
        GetComponent<Rigidbody2D>().AddForce(CalcularMejorVelocidadLanzamiento(origen, destino, tiempoDeVuelo), ForceMode2D.Impulse);
        GetComponent<Rigidbody2D>().AddTorque(Random.Range(-1f,-4f));
    }

    private Vector3 CalcularMejorVelocidadLanzamiento(Vector3 origen, Vector3 destino, float tiempoLlegada)
    {
        //Calculo de vectores al destino
        Vector3 alDestino = destino - origen;

        Vector3 alDestinoXZ = alDestino;

      //  Debug.Log(alDestino.x);

        if(alDestino.x >= 0)
        {
            if (alDestino.x > 0 && alDestino.y < -2.5f)
            {
                alDestinoXZ = new Vector3(alDestino.x + 2.2f, alDestino.y, alDestino.z);
            }

            if (alDestino.x > 3 && alDestino.y < -2.5f)
            {
                alDestinoXZ = new Vector3(alDestino.x + 1.5f, alDestino.y, alDestino.z);
            }

            if (alDestino.x > 5 && alDestino.y < -2.5f)
            {
                alDestinoXZ = new Vector3(alDestino.x + 1f, alDestino.y, alDestino.z);
            }

            if (alDestino.x > 7 && alDestino.y < -2.5f)
            {
                alDestinoXZ = new Vector3(alDestino.x + 0.8f, alDestino.y, alDestino.z);
            }
        }
        else
        {
            if (alDestino.x < -1.6f && alDestino.y < -2.5f)
            {
                alDestinoXZ = new Vector3(alDestino.x - 2f, alDestino.y, alDestino.z);
            }

            if (alDestino.x < -3f && alDestino.y < -2.5f)
            {
                alDestinoXZ = new Vector3(alDestino.x - 1.5f, alDestino.y, alDestino.z);
            }

            if (alDestino.x < -4.6f && alDestino.y < -2.5f)
            {
                alDestinoXZ = new Vector3(alDestino.x - 1f, alDestino.y, alDestino.z);
            }
        }
       


        alDestinoXZ.y = 0.0f;

        //Calculo de distancias recorridas
        float distY = alDestino.y;
        float distXZ = alDestinoXZ.magnitude;

        float v0y = (distY / tiempoLlegada) + (0.5f * Physics.gravity.magnitude * tiempoLlegada);
        float v0xz = distXZ / tiempoLlegada;

        //Y se crea el vector de velocidades final
        Vector3 velocidadFinal = alDestino.normalized;  //Vector unitario direccion al objetivo
        velocidadFinal = velocidadFinal * v0xz;         //Se multiplica por la velocidad horizontal para tener el vector
                                                        //horizontal completo
        velocidadFinal.y = v0y;                 //Se le suma la componente vertical

        return velocidadFinal;
    }
}
