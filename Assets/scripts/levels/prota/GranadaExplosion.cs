﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GranadaExplosion : MonoBehaviour {

    public float maxDamage;
    public float minDamage;
    public float maxSize;
    public float explosionTime;
    public Gradient colorRange;
    public int idArma;
    public GameObject vidaUp;
    bool ejecutado;

    public int oroFinal;

    private float timeElapsed;
    private SpriteRenderer spriteRenderer;
    private float tempTime;
    private CircleCollider2D circleCollider2D;


    GenericEnemyControl[] enemigosMuertos;
    ParacaEnemyControl[] parachutesMuertos;
    VoladorEnemyControl[] voladoresMuertos;

    public int contadorEnemigosNormales, contadorEnemigosVoladores;

    bool UnaVez;

	void OnEnable () {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        circleCollider2D = GetComponent<CircleCollider2D>();

        if(idArma != 40)
        {
            maxSize = IniciarJuego.shopData.GetArma(idArma).Ratio[PlayerPrefs.GetInt(SaveGame.nivelesArmas[idArma])];
            maxDamage = IniciarJuego.shopData.GetArma(idArma).Dano[PlayerPrefs.GetInt(SaveGame.nivelesArmas[idArma])];
        }
        else
        {
            circleCollider2D.enabled = true;
            spriteRenderer.enabled = true;
        }

        if (idArma == 11) maxSize *= 2;

        timeElapsed = 0;

    }
	
	void Update () {
        tempTime = timeElapsed / explosionTime;
        if (tempTime > 1f)
            tempTime = 1f;

        transform.localScale = Vector3.one * Mathf.Lerp(1f, maxSize, tempTime);
        spriteRenderer.color = colorRange.Evaluate(tempTime);

        if(contadorEnemigosNormales >= 2 && contadorEnemigosVoladores >= 1 || contadorEnemigosNormales >= 1 && contadorEnemigosVoladores >= 2)
        {
            if (!UnaVez)
            {
                GameObject objetovida = Instantiate((GameObject)Resources.Load("OroUpGranada"), new Vector2(transform.position.x, transform.position.y), new Quaternion(0, 0, 0, 0), GameObject.FindGameObjectWithTag("InterfazW").transform);
                objetovida.transform.position = transform.position;
                objetovida.GetComponent<AnimacionOro>().EfectoOro(oroFinal, transform.position);
                UnaVez = true;
            }
        }


        if (timeElapsed > explosionTime)
        {
            if (spriteRenderer.enabled)
                spriteRenderer.enabled = false;
            if (circleCollider2D.enabled)
                circleCollider2D.enabled = false;

            if (timeElapsed > 3.25f)
                if(GetComponentInParent<VagonetaControlador>() == null)
                Destroy(transform.parent.gameObject);
        }

        timeElapsed += Time.deltaTime;
	}



    private void OnTriggerEnter2D(Collider2D thisCollider)
    {
        InteractableObject interactableObject = thisCollider.GetComponentInParent<InteractableObject>();

        if (interactableObject != null)
        {
            if ((interactableObject.reactorType == InteractableObject.ReactorType.enemy) ||
                (interactableObject.reactorType == InteractableObject.ReactorType.destructible) || 
                (interactableObject.reactorType == InteractableObject.ReactorType.solidWall))
            {

                GenericEnemyControl enemigo = interactableObject.GetComponent<GenericEnemyControl>();
     
                if (enemigo != null)
                {
                    if (enemigo.iniciado && enemigo.disparable)
                    {
                        if (enemigo.energy < (int)Mathf.Lerp(maxDamage, minDamage, tempTime))
                            contadorEnemigosNormales++;
                        oroFinal += enemigo.oro;
                        GameObject objetovida = Instantiate(vidaUp, enemigo.transform.position, new Quaternion(0, 0, 0, 0), GameObject.FindGameObjectWithTag("InterfazW").transform);

                        objetovida.GetComponent<AnimacionRestaVida>().EfectoVida((int)Mathf.Lerp(maxDamage, minDamage, tempTime));

                        interactableObject.ShootReactor((int)Mathf.Lerp(maxDamage, minDamage, tempTime));
                    }
                }
                else
                {
                    ParacaEnemyControl enemigo2 = interactableObject.GetComponent<ParacaEnemyControl>();
                    VoladorEnemyControl enemigo3 = interactableObject.GetComponent<VoladorEnemyControl>();

                    if (enemigo2 != null)
                    {
                        if(enemigo2.energy < (int)Mathf.Lerp(maxDamage, minDamage, tempTime))
                        contadorEnemigosVoladores++;

                        oroFinal += enemigo2.oro;

                        GameObject objetovida = Instantiate(vidaUp, enemigo2.transform.position, new Quaternion(0, 0, 0, 0), GameObject.FindGameObjectWithTag("InterfazW").transform);

                        objetovida.GetComponent<AnimacionRestaVida>().EfectoVida((int)Mathf.Lerp(maxDamage, minDamage, tempTime));

                        interactableObject.ShootReactor((int)Mathf.Lerp(maxDamage, minDamage, tempTime));
                    }

                    if(enemigo3 != null)
                    {
                        if (enemigo3.energy < (int)Mathf.Lerp(maxDamage, minDamage, tempTime))
                            contadorEnemigosVoladores++;

                        oroFinal += enemigo3.oro;

                        GameObject objetovida = Instantiate(vidaUp, enemigo3.transform.position, new Quaternion(0, 0, 0, 0), GameObject.FindGameObjectWithTag("InterfazW").transform);

                        objetovida.GetComponent<AnimacionRestaVida>().EfectoVida((int)Mathf.Lerp(maxDamage, minDamage, tempTime));

                        interactableObject.ShootReactor((int)Mathf.Lerp(maxDamage, minDamage, tempTime));

                    }


                }
         
            }
        }
    }
}
