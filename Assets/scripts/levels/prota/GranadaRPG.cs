﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GranadaRPG : MonoBehaviour {

    public float speed;

    private SpriteRenderer spriteRenderer;
    private bool explotando;

    public GameObject Explosion, ExplosionV2;

    Text txtBalas;

    bool TransformUnaVez;
    Transform transTemporal;

    private void Update()
    {
        if (txtBalas != null)
        {
            int num = (int)Vector2.Distance(new Vector2(0, transform.position.y), new Vector2(0, txtBalas.transform.position.y + 1)) * 3;
            txtBalas.text = num.ToString() + " M";
        } 
    }

    // Use this for initialization
    void Start () {
        if(GameObject.FindGameObjectWithTag("DistanciaCohete") != null)
        txtBalas = GameObject.FindGameObjectWithTag("DistanciaCohete").GetComponent<Text>();
        if (GameObject.FindGameObjectWithTag("DistanciaCohete") != null)
            GetComponent<Rigidbody2D>().velocity = transform.TransformDirection(Vector2.down * speed);
        else
            GetComponent<Rigidbody2D>().velocity = transform.TransformDirection(Vector2.right * speed);
        spriteRenderer = transform.Find("Sprite").GetComponent<SpriteRenderer>();
        explotando = false;
    }
	
    private void OnTriggerEnter2D(Collider2D collision)
    {
            if (!explotando && IsCollisionable(collision))
            {
                Debug.Log(collision.name);      
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                transform.eulerAngles = Vector3.forward * Random.Range(-45f, 45f);
                transform.localScale = Vector3.one;
                spriteRenderer.gameObject.SetActive(false);
                GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[5]);
                Explosion.gameObject.SetActive(true);
                ExplosionV2.gameObject.SetActive(true);
                StartCoroutine(Explotar());
            }
    }


    IEnumerator Explotar()
    {
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }

    private bool IsCollisionable (Collider2D collision)
    {
        bool collisionable = false;
        if (collision.gameObject.layer == LayerMask.NameToLayer("enemies"))
        {
            collisionable = true;
        }
        else if (collision.gameObject.layer == LayerMask.NameToLayer("scenary"))
        {
            InteractableObject interactableObject = collision.gameObject.GetComponent<InteractableObject>();
            if (interactableObject != null)
            {
                if ((interactableObject.reactorType == InteractableObject.ReactorType.solidWall) ||
                    (interactableObject.reactorType == InteractableObject.ReactorType.destructible))
                {
                    collisionable = true;
                }
            
            }
        }

        return collisionable;
    }
}
