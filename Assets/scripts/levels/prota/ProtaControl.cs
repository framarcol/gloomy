﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DragonBones;
using UnityEngine.UI;

public class ProtaControl : MonoBehaviour {

    public ProtaInfo protaInfo;
    public UnityArmatureComponent armadura;
    public Vector2 torsoPosition;
    MiarmaControla armaControl;
    [Space(10)]
    public UnityEngine.Transform prefabGranada;
    public UnityEngine.Transform posLanzamientoGranada;
    public UnityEngine.Transform prefabBarricada;
    [Space(10)]
    public UnityEngine.Transform prefabGranadaBazooka;
    public bool disparable;
    public bool esCentro;
    public delegate void ProtaEventCallerDelegate ();
    public event ProtaEventCallerDelegate ProtaHitTheGround;
    public event ProtaEventCallerDelegate ProtaIsKilled;

    private AnimationsPlaylist animationsPlayList;
    private TowerControl towerControl;
   // private InterfazControl interfazControl;
    private bool cayendo = false;
    private Rigidbody2D protaRigidbody2D;
    private DragonBones.Transform torso;
    private Vector2 shootPosition;
    MenusEmergentes menuInfo;
    private string nombreArma; //String con el nombre del arma para hacer la composición del nombre de las animaciones
    public bool frontal;
    public Image barraGatling; /*barraGatlingCalentamiento*/
    public GameObject misil;

    public float frecuenciaGatlingMinima, frecuenciaGatlingMaxima;
    public  float calentamientoGatlingMaximo, calentamientoGatlingMinimo, frecuenciaBajadaCalentamiento;
    float calentamientoGatling, frecuenciaGatling;
    public float aumentoFrecenciaCuarto, aumentoFrecuenciaMitad, aumentoFrecuenciaResto;
    public UnityEngine.Transform aguja;
    public UnityEngine.Transform armaSalida;


    bool gatlingDisparando;
    bool UnavezfinGatling;
    bool moverPerIzq;

 //   int contadorProvisional =0;

    public bool estoyMuerto;

    public enum ProtaEstados { peReposo, peRecibirDano, peDesequilibrio, peInicioCaida, peCaer, peMorir, peWaitingAnimation, peGameOver,
                               peDisparando };
    ProtaEstados state;
    ProtaEstados nextState;

    private void Awake()
    {
        UnavezfinGatling = true;
        //frecuenciaGatlingMaxima = 0.6f;
        //frecuenciaGatlingMinima = 0.06f;
        frecuenciaGatling = frecuenciaGatlingMinima;

       // calentamientoGatlingMaximo = 0.4f;
       // calentamientoGatlingMinimo = 0f;

        calentamientoGatling = calentamientoGatlingMinimo;

        Time.timeScale = 1f;
        armaControl = GetComponentInChildren<MiarmaControla>();
        if (frontal)
        {
            FrecuenciaGatling();
            CalentamientoGatling();
        }
       
    }

    void Start () {
        
        menuInfo = GameObject.FindGameObjectWithTag("GameController").GetComponent<MenusEmergentes>();
        disparable = true;
		if (protaInfo == null)
        {
            protaInfo = transform.GetComponent<ProtaInfo>();
        }
        if (animationsPlayList == null)
        {
            animationsPlayList = transform.GetComponent<AnimationsPlaylist>();
            animationsPlayList.DragonBonesEvent += DragonBonesEventListener;
            animationsPlayList.EndPlaylist += DragonBonesEndPlayList;
        }

        if (towerControl == null && !frontal)
        {
            towerControl = GameObject.FindGameObjectWithTag("GameController").GetComponent<GlobalInfo>().torre.GetComponent<TowerControl>();
            if (towerControl != null)
            {
                towerControl.ColapseTower += OnColapseTower;
                towerControl.DamagedTower += OnDamagedTower;
            }
        }

        protaRigidbody2D = transform.GetComponent<Rigidbody2D>();
        torso = armadura.armature.GetBone("torso").boneData.transform;
        ChangeState(ProtaEstados.peReposo);
    }
	
	// Update is called once per frame
	void Update () {

        if(Input.touchCount > 0)
        {
            gatlingDisparando = true;
        }
        else
        {
            if (Input.GetMouseButton(0))
            {
                gatlingDisparando = true;
            }
            else
            {
                if (!UnavezfinGatling)
                {
                    UnavezfinGatling = true;
                    GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[30]);
                }
                gatlingDisparando = false;
            }
           
        }

       // Debug.Log(frecuenciaGatling);

        torsoPosition = new Vector2(torso.x, torso.y);
		switch (state)
        {
            case ProtaEstados.peCaer:
                if (protaRigidbody2D.velocity == Vector2.zero)
                {

                   // animationsPlayList.PlayAnimation("Caida_Muerte_" + nombreArma, 0.2f, 1, 1f);
                    nextState = ProtaEstados.peGameOver;
                    state = ProtaEstados.peWaitingAnimation;
                    protaInfo.energy = 0;
                    if (ProtaHitTheGround != null)
                        ProtaHitTheGround();
                }
                break;
        }

        if (moverPerIzq)
        {
            transform.Translate(Vector3.left * Time.deltaTime * (10));
        }

        if (frontal)
        {
            barraGatling.fillAmount = frecuenciaGatlingMaxima / frecuenciaGatling;

            aguja.rotation = Quaternion.Euler(new Vector3(0,0,((calentamientoGatling / calentamientoGatlingMaximo) * 180f) - 90f)*-1);

          //  barraGatlingCalentamiento.fillAmount = calentamientoGatling / calentamientoGatlingMaximo;

        }

        //  Debug.Log(gatlingDisparando);

    }

    IEnumerator AumentarCalentamientoGatling()
    {
        yield return new WaitForSeconds(0.05f);
     //   Debug.Log(gatlingDisparando);
        if (gatlingDisparando && frecuenciaGatling <= frecuenciaGatlingMaxima && calentamientoGatling < calentamientoGatlingMaximo)
        {
        //    Debug.Log(calentamientoGatling);
            calentamientoGatling += 0.01f;
            if (calentamientoGatling >= calentamientoGatlingMaximo)
            {
               // Debug.Log("da");
                protaInfo.Ammo[2] = 0;
            }
            
        }
        else
        {
            if(calentamientoGatling > calentamientoGatlingMinimo)
            {
                if(protaInfo.Ammo[2] == 0) calentamientoGatling = calentamientoGatling - (frecuenciaBajadaCalentamiento * 3);
                calentamientoGatling -= frecuenciaBajadaCalentamiento;
            }
            else
            {
                protaInfo.Ammo[2] = 100;
            }
        }

        CalentamientoGatling();
    }

    IEnumerator BajarFrecuenciaGatling()
    {
        yield return new WaitForSeconds(0.1f);
        if(frecuenciaGatling < frecuenciaGatlingMinima && !gatlingDisparando)
        {
            //Debug.Log("Subiendo");         
            frecuenciaGatling += 0.02f;
        }

        if (frecuenciaGatling < frecuenciaGatlingMinima && protaInfo.Ammo[2] == 0)
        {
            //Debug.Log("Subiendo");         
            frecuenciaGatling += 0.02f;
        }

        FrecuenciaGatling();
    }

    void CalentamientoGatling()
    {
        StartCoroutine(AumentarCalentamientoGatling());
    }

    void FrecuenciaGatling()
    {
        StartCoroutine(BajarFrecuenciaGatling());
    }

    private void ChangeState (ProtaEstados newState)
    {
        List<AnimationsPlaylist.AnimationNode> lista;
        switch (newState)
        {
            case ProtaEstados.peReposo:
               // gatlingDisparando = false;
                if (nombreArma != null)
                animationsPlayList.PlayAnimation("Reposo_" + nombreArma, 0.2f, -1, 1f);
                break;
            case ProtaEstados.peRecibirDano:
                animationsPlayList.PlayAnimation("Recibir_Daño_" + nombreArma, 0f, 1, 1f);
                nextState = ProtaEstados.peReposo;
                break;
            case ProtaEstados.peDesequilibrio:
                break;
            case ProtaEstados.peMorir:
                animationsPlayList.PlayAnimation("Muerte_" + nombreArma, 0.2f, 1, 1f);
                nextState = ProtaEstados.peGameOver;
                break;
            case ProtaEstados.peInicioCaida:
                armaControl.habilitado = false;
                cayendo = true;
                lista = new List<AnimationsPlaylist.AnimationNode>();
                //Debug.Log(nombreArma);
                lista.Add(new AnimationsPlaylist.AnimationNode("Desequilibrio_Caida_" + nombreArma, 0f, 1, 1f));
                lista.Add(new AnimationsPlaylist.AnimationNode("Caida", 0f, -1, 1f));
                animationsPlayList.PlayAnimationList(lista);
                estoyMuerto = true;
                StartCoroutine(MenuPausaEsperar(0.1f));
                break;
            case ProtaEstados.peCaer:
                animationsPlayList.PlayAnimation("Caida_" + nombreArma, 0.2f, -1, 1f);
                estoyMuerto = true;
                transform.GetComponent<BoxCollider2D>().enabled = false;
                protaRigidbody2D.isKinematic = false;
                protaRigidbody2D.AddForce(Vector2.down * 10f, ForceMode2D.Impulse);
                break;
            case ProtaEstados.peGameOver:
                estoyMuerto = true;
                StartCoroutine(MenuPausaEsperar(0.1f));
                break;
        }
        state = newState;
        protaInfo.state = newState;
    }

    //******************************************************************************************************************
    //  INTERACTION METHODS
    //******************************************************************************************************************

    IEnumerator MenuPausaEsperar(float segundos)
    {
        yield return new WaitForSeconds(segundos);
        menuInfo.MostrarPausa();
    }

    public void DispararFrontal(GameObject objetoAlcanzado, Vector2 shootPos)
    {
        state = ProtaEstados.peDisparando;
        nextState = ProtaEstados.peReposo;

       // gatlingDisparando = true;

      //  armaControl.habilitado = true;

        switch (GetCurrenWeapon())
        {
            case MiarmaControla.WeaponTypes._wtRifle:
                if (disparable)
                {
                    UnavezfinGatling = false;
                    GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[29]);
                    animationsPlayList.PlayAnimation("Disparo_Rifle_30", 0.1f, 1, 1);
                    disparable = false;
                    StartCoroutine(PausarArma(frecuenciaGatling));
                    if (frecuenciaGatling > frecuenciaGatlingMaxima)
                    {
                        //Debug.Log(frecuenciaGatling);
                        if (frecuenciaGatling > frecuenciaGatlingMinima - ((frecuenciaGatlingMinima - frecuenciaGatlingMaxima) / 4))
                        {
                            frecuenciaGatling -= aumentoFrecenciaCuarto;
                        }
                        else if (frecuenciaGatling > frecuenciaGatlingMinima - ((frecuenciaGatlingMinima - frecuenciaGatlingMaxima) / 2))
                        {
                            frecuenciaGatling -= aumentoFrecuenciaMitad;
                        }
                        else
                        {
                            frecuenciaGatling -= aumentoFrecuenciaResto;
                        }
                    }
                    else
                    {
                        frecuenciaGatling = frecuenciaGatlingMaxima;
                    }
                    armaControl.ShootToFrontal(objetoAlcanzado);
                }
                break;
            case MiarmaControla.WeaponTypes._wtSniper:
                if (disparable)
                {
                    animationsPlayList.PlayAnimation("Disparo_Franco_30", 0.1f, 1, 1);
                    disparable = false;
                    StartCoroutine(PausarArma(2f));
                    armaControl.ShootToFrontal(objetoAlcanzado);
                }
                break;
            case MiarmaControla.WeaponTypes._wtGrenadeLauncher:
                if (disparable && protaInfo.Ammo[5] >0)
                {
                    disparable = false;
                    StartCoroutine(PausarArma(IniciarJuego.shopData.GetArma(8).TiempoRecarga[0]));
                    Instantiate(misil, new Vector2(shootPos.x, 10), new Quaternion(0, 0, 0, 0), GameObject.FindGameObjectWithTag("InterfazW").transform);
                    armaControl.ReducirArma();
                }      
               break;
        }

      
    }

    public void Disparar (float angle, Vector2 position)
    {
        if (armaControl.currentWeapon != MiarmaControla.WeaponTypes._wtShotgun && armaControl.currentWeapon != MiarmaControla.WeaponTypes._wtLaser && armaControl.currentWeapon != MiarmaControla.WeaponTypes._wtGun && armaControl.currentWeapon != MiarmaControla.WeaponTypes._wtRifle && armaControl.currentWeapon != MiarmaControla.WeaponTypes._wtUzi)
            armaControl.habilitado = false;

        string animacionDisparo;

        if (armaControl.currentWeapon != MiarmaControla.WeaponTypes._wtRifle && armaControl.currentWeapon != MiarmaControla.WeaponTypes._wtUzi)
            animacionDisparo = "Disparo_" + nombreArma + "_";
        else
            animacionDisparo = "Disparo_" + nombreArma + "_2_";

        shootPosition = position;

        armaControl.CambiarDireccion(shootPosition);

        if (angle >= -15f)
            animacionDisparo += "0";
        else if ((angle < 15f) && (angle >= -45f))
            animacionDisparo += "30";
        else if ((angle < -45f) && (angle >= -75f))
            animacionDisparo += "60";
        else if ((angle < -75f) && (angle >= -90f))
            animacionDisparo += "60";
        else if ((angle < -75f) && (angle >= -90f))
            animacionDisparo += "60";
        else if ((angle < -90f) && (angle >= -134f))
            animacionDisparo += "60";
        else if ((angle < -134f) && (angle >= 142f))
            animacionDisparo += "60";
        else if ((angle < -142f) && (angle >= -163f))
            animacionDisparo += "30";
        else
            animacionDisparo += "0";    //Esto es temporal para probar cosas

        switch (armaControl.currentWeapon)
        {
            case MiarmaControla.WeaponTypes._wtGrenade:
                animacionDisparo = "Lanzamiento_granada";
                break;
        }

        switch (armaControl.currentWeapon)
        {
            case MiarmaControla.WeaponTypes._wtGun:
                if (disparable)
                {
                    animationsPlayList.PlayAnimation(animacionDisparo, 0.1f, 1, 1);
                    disparable = false;
                    StartCoroutine(PausarArma(protaInfo.Cadencia[1]));
                    armaControl.ShootTo(shootPosition);
                }
                break;
            case MiarmaControla.WeaponTypes._wtRifle:
                if (disparable)
                {
                    animationsPlayList.PlayAnimation(animacionDisparo, 0.1f, 1, 1);
                    disparable = false;
                    StartCoroutine(PausarArma(protaInfo.Cadencia[2]));
                    armaControl.ShootTo(shootPosition);
                }
                break;
            case MiarmaControla.WeaponTypes._wtUzi:
                if (disparable)
                {
                    animationsPlayList.PlayAnimation(animacionDisparo, 0.1f, 1, 1.2f );
                    disparable = false;
                    StartCoroutine(PausarArma(protaInfo.Cadencia[6]));
                    armaControl.ShootTo(shootPosition);
                }
                break;
            case MiarmaControla.WeaponTypes._wtLaser:
                if (disparable)
                {
                    animationsPlayList.PlayAnimation(animacionDisparo, 0.1f, 1, 8f);
                    disparable = false;
                    armaControl.ShootTo(shootPosition);
                    StartCoroutine(PausarArma(protaInfo.Cadencia[7]));
                    armaControl.ReducirBalaArma(7);
                }
                break;
            case MiarmaControla.WeaponTypes._wtShotgun:
                if (disparable)
                {
                    animationsPlayList.PlayAnimation(animacionDisparo, 0.1f, 1, protaInfo.Cadencia[3]);
                    disparable = false;
                    StartCoroutine(PausarArma(protaInfo.Cadencia[3]));
                    armaControl.ShootTo(shootPosition);
                }
                break;
            case MiarmaControla.WeaponTypes._wtSniper:
                if (disparable)
                {
                    animationsPlayList.PlayAnimation(animacionDisparo, 0.1f, 1, protaInfo.Cadencia[4]);
                }
                break;
            case MiarmaControla.WeaponTypes._wtGrenade:
                if (disparable)
                {
                    animationsPlayList.PlayAnimation(animacionDisparo, 0.1f, 1, protaInfo.Recarga[11]);
                }
                break;
            case MiarmaControla.WeaponTypes._wtBarricada:
                  animationsPlayList.PlayAnimation("Disparo_Mando", 0.1f, 1, 1.5f);
                break;
            case MiarmaControla.WeaponTypes._wtMina:
                    animationsPlayList.PlayAnimation("Disparo_Mando", 0.1f, 1, 1.5f);
                break;
            case MiarmaControla.WeaponTypes._wtPinchos:
                    animationsPlayList.PlayAnimation("Disparo_Mando", 0.1f, 1, 1.5f);
                break;

            default:
                animationsPlayList.PlayAnimation(animacionDisparo, 0.1f, 1, 1.1f);
                break;
        }   

        state = ProtaEstados.peDisparando;
        nextState = ProtaEstados.peReposo;
       // GetComponentInChildren<ArmaFX>().RefreshWeaponPosition(armaControl.currentWeapon);
    }


    IEnumerator PausarArma(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        disparable = true;
    }

    public void DeshabilitarDisparo()
    {
        disparable = false;
    }

    public void HabilitarDisparo(){

        disparable = true;
    }

    public void DeslizarPersonaje()
    {
        moverPerIzq = true;
    }
    
    public void RecibirDano (int dano)
    {
        if (!cayendo && (state != ProtaEstados.peRecibirDano))
        {
            protaInfo.energy -= dano;
            if (protaInfo.energy > 0)
            {
                if ((state == ProtaEstados.peDisparando) && !armaControl.habilitado)
                    armaControl.habilitado = true;

                ChangeState(ProtaEstados.peRecibirDano);
            }
            else
            {
                if (nextState != ProtaEstados.peGameOver)   //This IF prevents change animation while it is dying
                {
                    armaControl.habilitado = false;
                    if (ProtaIsKilled != null)
                    {
                        ProtaIsKilled();

                    }
                    ChangeState(ProtaEstados.peMorir);
                }
            }
        }
    }

    public MiarmaControla.WeaponTypes GetCurrenWeapon()
    {
        return armaControl.currentWeapon;
    }

    public void CambiarArma ()
    {
        
        if (!cayendo && (state != ProtaEstados.peMorir) && (state != ProtaEstados.peGameOver))
        {
            switch (armaControl.currentWeapon)
            {
                case MiarmaControla.WeaponTypes._wtGun:
                    nombreArma = "Pistola";
                    break;
                case MiarmaControla.WeaponTypes._wtRifle:
                    nombreArma = "Rifle";
                    break;
                case MiarmaControla.WeaponTypes._wtShotgun:
                    nombreArma = "Escopeta";
                    break;
                case MiarmaControla.WeaponTypes._wtUzi:
                    nombreArma = "Metralleta";
                    break;
                case MiarmaControla.WeaponTypes._wtLaser:
                    nombreArma = "Laser";
                    break;
                case MiarmaControla.WeaponTypes._wtSniper:
                    nombreArma = "Franco";
                    break;
                case MiarmaControla.WeaponTypes._wtGrenadeLauncher:
                    nombreArma = "Bazooka";
                    break;
                case MiarmaControla.WeaponTypes._wtMina:
                    nombreArma = "Mando";
                    break;
                case MiarmaControla.WeaponTypes._wtGrenade:
                    nombreArma = "Granada";
                    break;
                case MiarmaControla.WeaponTypes._wtBarricada:
                    nombreArma = "Mando";
                    break;
                case MiarmaControla.WeaponTypes._wtPinchos:
                    nombreArma = "Mando";
                    break;
            }
            armaControl.habilitado = true;

            if (animationsPlayList != null)             //Arma control puede llamar a este método antes de la inicialización del prota,
                ChangeState(ProtaEstados.peReposo);     //así evitamos un NullPointerException
        }
    }

    //*******************************************************************************************************************
    //  EVENTS METHODS
    //*******************************************************************************************************************

    private void DragonBonesEventListener(AnimationsPlaylist.DragonBonesEventType type, EventObject eventObject)
    {
       // Debug.Log(eventObject.name);

        switch (state)
        {
            case ProtaEstados.peDesequilibrio:
                armaControl.habilitado = true;
                ChangeState(ProtaEstados.peReposo);
                break;
            case ProtaEstados.peRecibirDano:
            case ProtaEstados.peCaer:
            case ProtaEstados.peMorir:
                if (type == AnimationsPlaylist.DragonBonesEventType.loopComplete)
                {
                    ChangeState(nextState);
                }
                break;
            case ProtaEstados.peDisparando:
                if (type == AnimationsPlaylist.DragonBonesEventType.loopComplete)
                {
                    armaControl.habilitado = true;
                    ChangeState(nextState);
                }
                break;
            case ProtaEstados.peWaitingAnimation:
                if (type == AnimationsPlaylist.DragonBonesEventType.loopComplete)
                {
                    ChangeState(nextState);
                }
                break;
        }

     //  Debug.Log(eventObject.name);

        if ((type == AnimationsPlaylist.DragonBonesEventType.frameEvent) && (eventObject.name == "PUM"))
        {
            if (armaControl.currentWeapon == MiarmaControla.WeaponTypes._wtGrenade)
            {
                if (esCentro)
                {
                  //  Debug.Log(shootPosition.x);
                    if (shootPosition.x > 0)
                    {
                        armaControl.GrenadeDrop(prefabGranada, posLanzamientoGranada.position, shootPosition);
                    }
                    else
                    {
                        armaControl.GrenadeDrop(prefabGranada, new Vector2(-0.3f, posLanzamientoGranada.position.y), shootPosition);
                    }
                }
                else
                {
                    armaControl.GrenadeDrop(prefabGranada, posLanzamientoGranada.position, shootPosition);
                }

            }
            else if (armaControl.currentWeapon == MiarmaControla.WeaponTypes._wtGrenadeLauncher)
            {
                armaControl.RPGGrenadeLaunch(prefabGranadaBazooka,armaSalida.position, shootPosition);
            }
            else
            {
                if(armaControl.currentWeapon != MiarmaControla.WeaponTypes._wtShotgun && armaControl.currentWeapon != MiarmaControla.WeaponTypes._wtLaser && armaControl.currentWeapon != MiarmaControla.WeaponTypes._wtGun && armaControl.currentWeapon != MiarmaControla.WeaponTypes._wtRifle && armaControl.currentWeapon != MiarmaControla.WeaponTypes._wtUzi)               
                {
                    armaControl.ShootTo(shootPosition);
                }
         
            }
        }
    }

    private void DragonBonesEndPlayList ()
    {
       switch (state)
        {
            case ProtaEstados.peInicioCaida:
                ChangeState(ProtaEstados.peCaer);
                break;
        }
    }

    private void OnDamagedTower (int towerEnergy)
    {
       // Debug.Log("Energia de la torre: " + towerEnergy);
        if (state != ProtaEstados.peMorir)
        {
            if (towerEnergy > 0)
            {
                if ((state == ProtaEstados.peDisparando) && !armaControl.habilitado)
                    armaControl.habilitado = true;

            }
            else
            {
                ChangeState(ProtaEstados.peInicioCaida);
            }
        }
    }

    private void OnColapseTower ()
    {
        animationsPlayList.PlayNextAnimation();
    }

    private void OnDestroy()
    {
        if (towerControl != null)
        {
            towerControl.ColapseTower -= OnColapseTower;
            towerControl.DamagedTower -= OnDamagedTower;
        }
    }
}
