﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ProtaInfo : MonoBehaviour {

    public int energy;
    public int barricadas;
    public int bombillas;
    public ProtaControl.ProtaEstados state;
    public static bool testPersonaje;
    public bool esCentro;
    public bool frontal;
    public Sprite gatlingDes;

    public GameObject[] imagenes;
    public Sprite[] spritesArmas;
    GameObject[] textosArmas;

    GameObject armasImagenes, armasRestoImagenes;

    public static int[] posicionArmaFinal;

    public bool[] EspecialArma = new bool [15];
    public int[] Ammo = new int[15];
    public float[] Damage = new float[15];
    public float[] Cadencia = new float[15];
    public float[] Alcance = new float[15];
    public float[] Recarga = new float[15];
    public float[] Radio = new float[15];
    public int[] Capacidad = new int[15];
    public bool posicionOcupada1;
    public bool posicionOcupada2;
    public bool posicionOcupada3;
    public bool posicionOcupada4;


    int[] municion;

    void Start()
    {
        ShopData.StandardShopItem[] armas = new ShopData.StandardShopItem[20];

        int i = 1;

        while(IniciarJuego.shopData.GetArma(i) != null)
        {
           armas[i] = IniciarJuego.shopData.GetArma(i);
            i++;
        }

        i = 11;

        while (IniciarJuego.shopData.GetArma(i) != null)
        {
            armas[i] = IniciarJuego.shopData.GetArma(i);
            i++;
        }

        if (testPersonaje)
        {
            for (int j = 0; j < Ammo.Length; j++)
            {
                if (PlayerPrefs.HasKey("pM"+j))
                    Ammo[j] = PlayerPrefs.GetInt("pM" + j);
                if (PlayerPrefs.HasKey("pD" + j))
                    Damage[j] = PlayerPrefs.GetInt("pD" + j);
                if (PlayerPrefs.HasKey("pC" + j))
                    Cadencia[j] = PlayerPrefs.GetInt("pC" + j);
                if (PlayerPrefs.HasKey("pA" + j))
                    Alcance[j] = PlayerPrefs.GetInt("pA" + j);
                if (PlayerPrefs.HasKey("pR" + j))
                    Recarga[j] = PlayerPrefs.GetInt("pR" + j);
                if (PlayerPrefs.HasKey("pRa" + j))
                    Radio[j] = PlayerPrefs.GetInt("pRa" + j);
                if (PlayerPrefs.HasKey("pCa" + j))
                    Capacidad[j] = PlayerPrefs.GetInt("pCa" + j);
            }    

        }
        else
        {
            if (SaveGame.nivelesArmas == null)
                SaveGame.Iniciar();

            for(int j = 1; j < 11; j++)
            {
                if (!frontal)
                {
                    if (!EspecialArma[j])
                    {
                        if (armas[j] != null)
                            Ammo[j] = armas[j].Capacidad[PlayerPrefs.GetInt(SaveGame.nivelesArmas[j])];
                    }
                    else
                    {
                        if (PlayerPrefs.HasKey("pM" + j))
                            Ammo[j] = PlayerPrefs.GetInt("pM" + j);
                    }
                }
                else{

                    switch (j)
                    {
                        case 4:
                            Ammo[j] = 5;
                            Capacidad[j] = 5;
                            break;
                        case 5:
                            Ammo[j] = IniciarJuego.shopData.GetArma(8).Capacidad[0];
                            Capacidad[j] = IniciarJuego.shopData.GetArma(8).Capacidad[0];
                            Recarga[j] = IniciarJuego.shopData.GetArma(8).TiempoRecarga[0];
                            break;
                        default:
                            if (armas[j] != null)
                                Ammo[j] = armas[j].Capacidad[PlayerPrefs.GetInt(SaveGame.nivelesArmas[j])];
                            break;

                    }
                }
               
            }

            for (int j = 11; j < EspecialArma.Length; j++)
            {
                Ammo[j] = ControladorSelectorArmas.numArmasId[j-1];
            }

            for (int j = 1; j < Damage.Length; j++)
            {
                if (armas[j] != null)
                {
                    Damage[j] = armas[j].Dano[PlayerPrefs.GetInt(SaveGame.nivelesArmas[j])];
                    Cadencia[j] = armas[j].Cadencia[PlayerPrefs.GetInt(SaveGame.nivelesArmas[j])];
                    Alcance[j] = armas[j].Alcance[PlayerPrefs.GetInt(SaveGame.nivelesArmas[j])];
                    Recarga[j] = armas[j].TiempoRecarga[PlayerPrefs.GetInt(SaveGame.nivelesArmas[j])];
                    if(!frontal && j != 5)
                    {
                        Capacidad[j] = armas[j].Capacidad[PlayerPrefs.GetInt(SaveGame.nivelesArmas[j])];
                    }
                    else
                    {
                        if(!frontal) Capacidad[j] = armas[j].Capacidad[PlayerPrefs.GetInt(SaveGame.nivelesArmas[j])];
                    }
                 
                    Radio[j] = armas[j].Ratio[PlayerPrefs.GetInt(SaveGame.nivelesArmas[j])];
                }
            }

        }
     //   Debug.Log(pistolCadencia);
     //   Cadencia[1] = 0.251f / Cadencia[1];
        Cadencia[3] = 0.91f / Cadencia[3];
        Cadencia[4] = 0.75f / Cadencia[4];

    }

    

    private void Awake()
    {
        if(IniciarJuego.shopData == null) testPersonaje = true;
        else testPersonaje = false;

        armasImagenes = GameObject.FindGameObjectWithTag("ArmasPrefab");
        armasRestoImagenes = GameObject.FindGameObjectWithTag("EstadosArmasTag");
        textosArmas = new GameObject[5];
        imagenes = new GameObject[10];

        for (int i = 0; i < imagenes.Length && i < armasImagenes.GetComponentsInChildren<SpriteRenderer>().Length; i++)
        {
            imagenes[i] = armasImagenes.GetComponentsInChildren<SpriteRenderer>()[i].gameObject;
        }

        for (int i = 0, j = 1; j < textosArmas.Length + 1; j++, i++)
        {
            if (armasRestoImagenes.GetComponentsInChildren<Text>()[i].name == "txtBalas" + j)
                textosArmas[i] = armasRestoImagenes.GetComponentsInChildren<Text>()[i].gameObject;
        }

#if UNITY_EDITOR

     //   PlayerPrefs.DeleteAll();

        if (testPersonaje)
        {
            if (IniciarJuego.shopData == null)
                IniciarJuego.Iniciar();

            BotonesMenu.mundoSeleccionado = IniciarJuego.shopData.GetMundoByEscena(SceneManager.GetActiveScene().buildIndex);
            BotonesMenu.nivelMundoSeleccionado = IniciarJuego.shopData.GetNivelByEscena(SceneManager.GetActiveScene().buildIndex);

      //      Debug.Log(IniciarJuego.shopData.GetNivelByEscena(SceneManager.GetActiveScene().buildIndex));

      //      Debug.Log(SceneManager.GetActiveScene().buildIndex);

       //     Debug.Log(BotonesMenu.nivelMundoSeleccionado + "  " + BotonesMenu.mundoSeleccionado);

            bool frontal = IniciarJuego.shopData.GetMundo(BotonesMenu.nivelMundoSeleccionado, BotonesMenu.mundoSeleccionado).frontal;

            loadSC.nextScene = SceneManager.GetActiveScene().buildIndex;

           // DebuSceneManager.GetActiveScene().buildIndex;

            //      Debug.Log(frontal);

            if (!frontal) SceneManager.LoadScene("LoadScreenArmas");
            else SceneManager.LoadScene("LoadScreenArmasFrontal");
        }

#endif


        posicionArmaFinal = new int[5];

        if (testPersonaje)
        {
            if (IniciarJuego.shopData == null)
                IniciarJuego.Iniciar();
           ControladorSelectorArmas.armasSeleccinadas = new int[5];
           ControladorSelectorArmas.armasSeleccinadas[0] = 1;
           ControladorSelectorArmas.armasSeleccinadas[1] = 3;
           ControladorSelectorArmas.armasSeleccinadas[2] = 2;
           ControladorSelectorArmas.armasSeleccinadas[3] = 4;
           ControladorSelectorArmas.armasSeleccinadas[4] = 5;
            
        }

        municion = new int[15];

        for (int i = 0; i < ControladorSelectorArmas.armasSeleccinadas.Length; i++)
        {
  
            if (ControladorSelectorArmas.armasSeleccinadas[i] != 0)
            {
                imagenes[i].name = ControladorSelectorArmas.armasSeleccinadas[i].ToString();


                if (!testPersonaje) {
                    //zumArma
                    if (!frontal)
                    {
                        imagenes[i].GetComponent<SpriteRenderer>().sprite = IniciarJuego.shopData.GetArma(ControladorSelectorArmas.armasSeleccinadas[i]).itemThumbnail2[PlayerPrefs.GetInt(SaveGame.nivelesArmas[ControladorSelectorArmas.armasSeleccinadas[i]])];
                    }
                    else
                    {
                        if (i != 0)
                        {
                            if(ControladorSelectorArmas.armasSeleccinadas[i] != 5)
                            imagenes[i].GetComponent<SpriteRenderer>().sprite = IniciarJuego.shopData.GetArma(ControladorSelectorArmas.armasSeleccinadas[i]).itemThumbnail2[PlayerPrefs.GetInt(SaveGame.nivelesArmas[ControladorSelectorArmas.armasSeleccinadas[i]])];
                            else imagenes[i].GetComponent<SpriteRenderer>().sprite = IniciarJuego.shopData.GetArma(8).itemThumbnail2[0];

                        }
                        else imagenes[i].GetComponent<SpriteRenderer>().sprite = gatlingDes;

                    }

                }
                else imagenes[i].GetComponent<SpriteRenderer>().sprite = spritesArmas[ControladorSelectorArmas.armasSeleccinadas[i] - 1];
                posicionArmaFinal[i] = ControladorSelectorArmas.armasSeleccinadas[i];
            }
            else
            {
                textosArmas[i].SetActive(false);
                imagenes[i].SetActive(false);
            }
        }
    }

    private void Update()
    {
        if (posicionOcupada1)
        {
            MiarmaControla.posicionado[0] = true;
            MiarmaControla.Subposicion[0] = true;
            MiarmaControla.Subposicion[1] = true;
        }

        if (posicionOcupada2)
        {
            MiarmaControla.posicionado[1] = true;
            MiarmaControla.Subposicion[2] = true;
            MiarmaControla.Subposicion[3] = true;
        }

        if (posicionOcupada3)
        {
            MiarmaControla.posicionado[2] = true;
            MiarmaControla.Subposicion[4] = true;
            MiarmaControla.Subposicion[5] = true;
        }

        if (posicionOcupada4)
        {
            MiarmaControla.posicionado[3] = true;
            MiarmaControla.Subposicion[6] = true;
            MiarmaControla.Subposicion[7] = true;
        }

        for (int i = 0; i < Ammo.Length; i++)  municion[i] = Ammo[i];

        for (int i = 0; i < ControladorSelectorArmas.armasSeleccinadas.Length; i++)
                textosArmas[i].GetComponent<Text>().text = municion[ControladorSelectorArmas.armasSeleccinadas[i]].ToString();
    }
}
