﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DragonBones;
using UnityEngine.UI;

public class TowerControl : InteractableObject {

    //public int energy;
   // public GameObject explosion;
    private AnimationsPlaylist animationPlayList;
    private int myStatus;
   // private bool playingAnimation;
    int lastStatus;
    public GameObject vidaUp;

    //Events
    public delegate void DamagedTowerDelegate(int towerEnergy);
    public event DamagedTowerDelegate DamagedTower;
    public delegate void ColapseTowerDelegate ();
    public event ColapseTowerDelegate ColapseTower;
    public event ColapseTowerDelegate EndColapseTower;
    Text barraSalud;
   // float numeroFinal;
//    int numeroFinalEntero;
    public float energiaInicial;
    bool[] animEject = new bool[5];

    bool moverTorreIzq;

    void Awake()
    {
        barraSalud = GetComponentInChildren<Text>();
    }

	// Use this for initialization
	void Start () {

        lastStatus = -1;
        myStatus = -1;
        animationPlayList = transform.GetComponent<AnimationsPlaylist>();
        animationPlayList.DragonBonesEvent += DragonBonesEventListener;
        energiaInicial = energy;

      // animationPlayList.PlayAnimation("daño torre 1", 0f, 1, 1f);

        //Debug.Log(animationPlayList.currentAnimation);
    }
	
	// Update is called once per frame
	void Update () {
       // numeroFinal = (energy / energiaInicial) * 100;
       // numeroFinalEntero = (int)numeroFinal;
        barraSalud.text = energy.ToString();

        if (moverTorreIzq)
        {
           transform.Translate(Vector3.left * Time.deltaTime * (10));
        }
    }

    public override int ShootReactor (int dano)
    {
        if (energy > 0)
        {
            energy -= dano;
            if (energy < 0) energy = 0;
            GameObject objetovida = Instantiate(vidaUp, new Vector2(barraSalud.transform.position.x, barraSalud.transform.position.y +0.8f), new Quaternion(0, 0, 0, 0), GameObject.FindGameObjectWithTag("InterfazW").transform);
            objetovida.GetComponent<AnimacionRestaVida>().EfectoVida((int)dano);
            PlayAndEvent();
        }

        return 0;
    }

    private void DragonBonesEventListener(AnimationsPlaylist.DragonBonesEventType type, EventObject eventObject)
    {
        if (myStatus == 3)
        {
            if ((type == AnimationsPlaylist.DragonBonesEventType.frameEvent) && (eventObject.name == "eventName"))
            {
                if (ColapseTower != null)
                {
                    ColapseTower();
                }
              //  explosion.GetComponent<Animator>().SetTrigger("Colapso");
            }
            else if (type == AnimationsPlaylist.DragonBonesEventType.loopComplete)
            {
                if (EndColapseTower != null)
                    EndColapseTower();
            }
        }
    }

    public void DeslizarTorre()
    {
        moverTorreIzq = true;
    }

    private void PlayAndEvent ()
    {
        string animation;

       // Debug.Log("Energia: " + energy + "Inicial: " + energiaInicial);
       
        if((float)energy /energiaInicial < 0.75f)
        {
            myStatus = 0;       
        }

        if ((float)energy / energiaInicial < 0.5f)
        {
            myStatus = 1;
        }

        if ((float)energy / energiaInicial < 0.25f)
        {
            myStatus = 2;
        }

        if ((float)energy / energiaInicial <= 0f)
        {
            myStatus = 3;
        }

        if ((float)energy / energiaInicial >= 0.75f) myStatus = -1;
        //Debug.Log("Estado: "+ myStatus);

        if (lastStatus != myStatus && myStatus != -1)
        {
            lastStatus = myStatus;
            switch (myStatus)
            {
                case 0:
                    animation = "vida_100-75";
                    animEject[0] = true;
                    break;
                case 1:
                    if(!animEject[0]) animation = "vida_100-50";
                    else animation = "vida_75-50";
                    animEject[0] = true;
                    animEject[1] = true;
                    break;
                case 2:
                    if (!animEject[0]) animation = "vida_100-50";
                    else if (!animEject[1]) animation = "vida_75-25";
                    else animation = "vida_50-25";
                    animEject[0] = true;
                    animEject[1] = true;
                    animEject[2] = true;
                    break;
                default:
                    if (!animEject[0]) animation = "vida_100-0";
                    else if (!animEject[1]) animation = "vida_75-0";
                    else if (!animEject[2]) animation = "vida_50-0";
                    else animation = "vida_25-0";
                    animEject[0] = true;
                    animEject[1] = true;
                    animEject[2] = true;
                    animEject[3] = true;
                    break;
            }

            animationPlayList.PlayAnimation(animation, 0f, 1, 1f);
          //  playingAnimation = true;

            if (DamagedTower != null)
            {
                DamagedTower(energy);
            }
            //explosion.GetComponent<Animator>().SetTrigger("Impacto");
        }
        
    }
}
