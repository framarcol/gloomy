﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class loadSC : MonoBehaviour {

    public static int nextScene;
    public GameObject Imagenes;
    AsyncOperation miCargaNivel;

    public GameObject cartelInformativo;
    public Text textoInformativo;
    public GameObject modoOk, modoOpcion;

    bool corutinaMenuPrincipal;

    public AudioClip backMenu;

    private void Start()
    {
      LoadSCSImple.esInicio = false;

       // Debug.Log(BotonesMenu.nivelMundoSeleccionado + BotonesMenu.mundoSeleccionado);

        if(BotonesMenu.nivelMundoSeleccionado == 1 && BotonesMenu.mundoSeleccionado == 1)
        {
            cartelInformativo.SetActive(true);
            textoInformativo.text = SeleccionaIdiomas.gameText[17, 1];
            modoOk.SetActive(true);
            modoOpcion.SetActive(false);
        }

        if (BotonesMenu.nivelMundoSeleccionado == 8 && BotonesMenu.mundoSeleccionado == 1)
        {
            cartelInformativo.SetActive(true);
            textoInformativo.text = SeleccionaIdiomas.gameText[17, 2];
            modoOk.SetActive(true);
            modoOpcion.SetActive(false);
        }

        if (BotonesMenu.nivelMundoSeleccionado == 5 && BotonesMenu.mundoSeleccionado == 2)
        {
            cartelInformativo.SetActive(true);
            textoInformativo.text = SeleccionaIdiomas.gameText[17, 3];
            modoOk.SetActive(true);
            modoOpcion.SetActive(false);
        }

    }

    public void EmpezarFrontal()
    {
        LoadSCSImple.esInicio = false;
        LoadSCSImple.esMenu = false;
        SceneManager.LoadSceneAsync("LoadScreen", LoadSceneMode.Single);
    }

    public void Empezar()
    {
#if UNITY_EDITOR
        LoadSCSImple.esInicio = false;
        LoadSCSImple.esMenu = false;
        SceneManager.LoadSceneAsync("LoadScreen", LoadSceneMode.Single);
#endif

        if (!IniciarJuego.shopData.GetMundo(BotonesMenu.nivelMundoSeleccionado, BotonesMenu.mundoSeleccionado).ArmasObligatorias)
        {
            if (ControladorSelectorArmas.totalPuntos - ControladorSelectorArmas.puntosSumados > ControladorSelectorArmas.totalPuntos / 2 || ControladorSelectorArmas.sumaArmas == 0)
            {
                cartelInformativo.SetActive(true);
                if (ControladorSelectorArmas.sumaArmas != 0)
                {
                    textoInformativo.text = SeleccionaIdiomas.gameText[0, 8];
                    modoOk.SetActive(false);
                    modoOpcion.SetActive(true);
                }
                else
                {
                    textoInformativo.text = SeleccionaIdiomas.gameText[0, 9];
                    modoOk.SetActive(true);
                    modoOpcion.SetActive(false);
                }


            }
            else
            {
                LoadSCSImple.esInicio = false;
                LoadSCSImple.esMenu = false;
                SceneManager.LoadSceneAsync("LoadScreen", LoadSceneMode.Single);
            }
        }
        else
        {
            LoadSCSImple.esInicio = false;
            LoadSCSImple.esMenu = false;
            SceneManager.LoadSceneAsync("LoadScreen", LoadSceneMode.Single);
        }

      
       
    }

    public void Si()
    {
        LoadSCSImple.esInicio = false;
        LoadSCSImple.esMenu = false;
        SceneManager.LoadSceneAsync("LoadScreen", LoadSceneMode.Single);
    }

    public void No()
    {
        cartelInformativo.SetActive(false);
    }

    public void MenuPrincipal()
    {
         StartCoroutine(Menu());
    }

    IEnumerator Menu()
    {
        if (!corutinaMenuPrincipal)
        {
            corutinaMenuPrincipal = true;
            GetComponent<AudioSource>().PlayOneShot(IniciarJuego.shopData.upgradeItems[0].upgradeItemList[0].audios[26]);
            yield return new WaitForSeconds(0.4f);
            BotonesMenu.idDEstino = 3;
            LoadSCSImple.esMenu = true;
            SceneManager.LoadSceneAsync("LoadScreen");
            corutinaMenuPrincipal = false;
        }
    }

}
