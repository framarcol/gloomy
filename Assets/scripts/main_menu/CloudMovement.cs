﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudMovement : MonoBehaviour {

    public float speed;
    RectTransform rectTransform;

	// Use this for initialization
	void Start () {
        rectTransform = GetComponent<RectTransform>();
        //Debug.Log("WIDHT: " + rectTransform.rect.width);
	}
	
	// Update is called once per frame
	void Update () {
        rectTransform.anchoredPosition = rectTransform.anchoredPosition + (Vector2.left * speed);
        //Debug.Log(rectTransform.anchoredPosition);
        if (rectTransform.anchoredPosition.x < -(640f + (rectTransform.rect.width / 2f)))
        {
            rectTransform.anchoredPosition = new Vector2(640f + (rectTransform.rect.width / 2f), rectTransform.anchoredPosition.y);
        }
	}
}
