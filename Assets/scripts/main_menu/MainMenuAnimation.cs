﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuAnimation : MonoBehaviour {

    public GameObject fade;
    public GameObject imagenTexto;
    public GameObject cartel;
    [Space (10)]
    public Button botonOptions;
    public Button botonMap;
    public Button botonExit;
    
    enum State {sFade, sFlyingText};
    State actualState;

	// Use this for initialization
	void Start () {
        botonOptions.interactable = false;
        botonMap.interactable = false;
        botonExit.interactable = false;
        //Initialy, we activate fade and it's state
        actualState = State.sFade;
        fade.GetComponent<FadeControl>().FadeOn(2.0f);
    }
	
	// Update is called once per frame
	void Update () {
        switch (actualState)
        {
            case State.sFade:   //Waiting for fade reachs 50% before launching flying text
                if (fade.GetComponent<Image>().color.a < 0.5)
                {
                    actualState = State.sFlyingText;
                    imagenTexto.GetComponent<MainMenuFlyingText>().Fly(1.0f, this.transform);       //Launch title name to the sign
                    enabled = false;
                }
                break;
            default:
                break;
        }
		
	}

    //**************************************************************************************************************
    //  Activation methods ordered by sequence
    //**************************************************************************************************************
    public void ActivateSign ()
    {
        //Making fliying text a child of the sign
        imagenTexto.transform.SetParent(cartel.transform);
        imagenTexto.transform.localRotation = Quaternion.identity;
        imagenTexto.transform.localPosition = new Vector3(imagenTexto.transform.localPosition.x, imagenTexto.transform.localPosition.y, 0.0f);
        //Animating both
        cartel.GetComponent<Animator>().SetTrigger("crash");
        //Wait some time before launching buttons
    }

    public void ActivateButtons ()
    {
        botonOptions.interactable = true;
        botonMap.interactable = true;
        botonExit.interactable = true;
    }

    public void DeactivateButtons()
    {
        botonOptions.interactable = false;
        botonMap.interactable = false;
        botonExit.interactable = false;
    }


    //**************************************************************************************************************
    //  Coroutines
    //**************************************************************************************************************
    /*IEnumerator WaitFadeOnFor(float percent)
    {
        
    }*/
}
