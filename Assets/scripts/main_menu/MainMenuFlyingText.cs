﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuFlyingText : MonoBehaviour {

    Transform menuControlTransform;
    
    bool nowFlying;
    float seconds;      //Animation time
    float timeElapsed;  //Time elapsed of the animation
    float scale;        //scale of text (flying towards effect)
    float posY;         //Vertical position of text (flying towards effect)
    float posYDest;

    // Use this for initialization
    void Start () {
        posYDest = this.GetComponent<RectTransform>().anchoredPosition.y;   //260.0f
        this.GetComponent<RectTransform>().localScale = new Vector3(25, 25, 1);
        this.GetComponent<RectTransform>().anchoredPosition = new Vector3(0.0f, 3000.0f, 0.0f);
        nowFlying = false;
        enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (nowFlying)
        {
            timeElapsed += Time.deltaTime;
            if (timeElapsed < seconds)
            {
                scale = Mathf.Lerp(25.0f, 1.0f, timeElapsed / seconds);
                posY = Mathf.Lerp(3000.0f, posYDest, timeElapsed / seconds);
                this.GetComponent<RectTransform>().localScale = new Vector3(scale, scale, 1.0f);
                this.GetComponent<RectTransform>().anchoredPosition = new Vector3(0.0f, posY, 0.0f);
            }
            else
            {
                timeElapsed = seconds;
                this.GetComponent<RectTransform>().localScale = Vector3.one;
                this.GetComponent<RectTransform>().anchoredPosition = new Vector3(0.0f, posYDest, 0.0f);
                //Calling MenuControl for continue animation
                menuControlTransform.GetComponent<MainMenuAnimation>().ActivateSign();
                menuControlTransform.GetComponent<MainMenuAnimation>().ActivateButtons();
                enabled = false;
            }
        }
    }

    public void Fly (float flySeconds, Transform whoToCall)
    {
        menuControlTransform = whoToCall;
        seconds = flySeconds;
        timeElapsed = 0.0f;
        scale = 25.0f;
        posY = 2600.0f;
        nowFlying = true;
        enabled = true;
        //StartCoroutine(Flying(seconds));
    }
}
