﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuLaunchMapMenu : MonoBehaviour {

    enum MenuStatus { msNull, msWaiting,
                      msOpening1, msOpening2, msOpening3, msOpening4,
                      msRunning,
                      msClosing1, msClosing2, msClosing3, msClosing4,
                      msClosed
    };         //indicate the status of the menu

    public Transform fondo;
    public Transform menuElements;
    public Transform foregroundTransition;
    public Transform fade;
    [Space(10)]
    public Transform mapMenu;

    MenuStatus menuStatus;
    MenuStatus nextStatus;
    float waitingTime;

    // Use this for initialization
    void Start () {
        menuStatus = MenuStatus.msNull;
        nextStatus = MenuStatus.msNull;
        enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        switch (menuStatus)
        {
            case MenuStatus.msWaiting:
                if (waitingTime > 0.0f)
                {
                    waitingTime -= Time.deltaTime;
                }
                else
                {
                    menuStatus = nextStatus;
                }
                break;
            case MenuStatus.msOpening1: //Initial down scroll
                MapMenuOpening();
                break;
            case MenuStatus.msOpening2: //Scrolling and starting to fade off
                fade.GetComponent<FadeControl>().FadeOff(1.0f);
                MyWaitForSeconds(1.0f, MenuStatus.msOpening3);
                break;
            case MenuStatus.msOpening3: //initial scrolling map
                mapMenu.GetComponent<Animator>().SetTrigger("up");
                MyWaitForSeconds(0.5f, MenuStatus.msOpening4);
                //MyWaitForSeconds(1.0f, MenuStatus.msRunning);
                break;
            case MenuStatus.msOpening4: //Scrolling and starting to fade on
                fade.GetComponent<FadeControl>().FadeOn(1.0f);
                MyWaitForSeconds(1.0f, MenuStatus.msRunning);
                break;

            case MenuStatus.msRunning:
                //Debug.Log("Ahora haremos Fade ON con el mapa puesto");
                menuElements.gameObject.SetActive(false);
                enabled = false;
                break;

            case MenuStatus.msClosing1: //initial scrolling map
                mapMenu.GetComponent<Animator>().SetTrigger("down");
                MyWaitForSeconds(1.0f, MenuStatus.msClosing2);
                break;
            case MenuStatus.msClosing2: //Scrolling and starting to fade off
                fade.GetComponent<FadeControl>().FadeOff(1.0f);
                MyWaitForSeconds(1.0f, MenuStatus.msClosing3);
                break;
            case MenuStatus.msClosing3: //Main menu down scroll
                fade.GetComponent<FadeControl>().FadeOn(1.0f);
                MapMenuClosing();
                break;
            /*case MenuStatus.msClosing4: //Scrolling and starting to fade off
                fade.GetComponent<FadeControl>().FadeOn(1.0f);
                MyWaitForSeconds(1.0f, MenuStatus.msClosed);
                break;*/

            case MenuStatus.msClosed:   //Closing the menu
                //Debug.Log("El menu de mapa ha terminado, se queda a la espera");
                enabled = false;
                mapMenu.gameObject.SetActive(false);
                break;
        }
    }

    //*****************************************************************************************************************
    //  MAP MENU METHODS
    //*****************************************************************************************************************
    void MapMenuOpening()
    {
        fondo.GetComponent<Animator>().SetTrigger("TransitionUp");
        menuElements.GetComponent<Animator>().SetTrigger("TransitionUp");
        foregroundTransition.GetComponent<Animator>().SetTrigger("in");
        MyWaitForSeconds(1.0f, MenuStatus.msOpening2);
        
    }

    void MapMenuClosing()
    {
        menuElements.gameObject.SetActive(true);
        fondo.GetComponent<Animator>().SetTrigger("TransitionDown");
        menuElements.GetComponent<Animator>().SetTrigger("TransitionDown");
        foregroundTransition.GetComponent<Animator>().SetTrigger("out");
        //MyWaitForSeconds(1.0f, MenuStatus.msClosing4);
        MyWaitForSeconds(2.0f, MenuStatus.msClosed);

    }

    void MyWaitForSeconds (float segundos, MenuStatus sigEstado)
    {
        nextStatus = sigEstado;
        waitingTime = segundos + Time.deltaTime;
        menuStatus = MenuStatus.msWaiting;
    }

    //*****************************************************************************************************************
    //  PUBLIC MENU ACTIVATIONS
    //*****************************************************************************************************************
    
    public void ActivateLevelMenu()
    {
        //Debug.Log("Activando menu");
        mapMenu.gameObject.SetActive(true);
        menuStatus = MenuStatus.msOpening1;
        enabled = true;
    }

    public void DeactivateLevelMenu()
    {
        //Debug.Log("Activando menu");
        menuStatus = MenuStatus.msClosing1;
        enabled = true;
    }
}
