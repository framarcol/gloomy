﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuLaunchOptionsMenu : MonoBehaviour {

    public Transform optionsMenu;
    public MainMenuAnimation mainMenuAnimation;
    [Space(10)]
    public Button[] buttons;

    enum MenuStatus { msNull, msWaiting, msEndOpening, msEndClosing };
    MenuStatus menuStatus;
    MenuStatus nextStatus;
    float waitingTime;

    // Use this for initialization
    void Start () {
        buttons = optionsMenu.GetComponentsInChildren<Button>();
        menuStatus = MenuStatus.msNull;
        nextStatus = MenuStatus.msNull;
        enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        switch (menuStatus)
        {
            case MenuStatus.msWaiting:
                if (waitingTime > 0.0f)
                {
                    waitingTime -= Time.deltaTime;
                }
                else
                {
                    menuStatus = nextStatus;
                }
                break;
            case MenuStatus.msEndOpening:
                ActivateButtons();
                enabled = false;
                break;
            case MenuStatus.msEndClosing:
                mainMenuAnimation.ActivateButtons();
                optionsMenu.gameObject.SetActive(false);
                enabled = false;
                break;
        }
    }

    //*****************************************************************************************************************
    //  MAP MENU METHODS
    //*****************************************************************************************************************
    void MyWaitForSeconds(float segundos, MenuStatus sigEstado)
    {
        nextStatus = sigEstado;
        waitingTime = segundos + Time.deltaTime;
        menuStatus = MenuStatus.msWaiting;
    }

    //  Buttons activation/deactivation
    void ActivateButtons ()
    {
        foreach (Button tempButton in buttons)
        {
            tempButton.interactable = true;
        }
    }

    void DeactivateButtons ()
    {
        foreach (Button tempButton in buttons)
        {
            tempButton.interactable = false;
        }
    }

    //*****************************************************************************************************************
    //  PUBLIC MENU ACTIVATIONS
    //*****************************************************************************************************************
    public void ActivateOptionsMenu ()
    {
        optionsMenu.gameObject.SetActive(true);
        mainMenuAnimation.DeactivateButtons();
        optionsMenu.GetComponent<Animator>().SetTrigger("down");
        enabled = true;
        MyWaitForSeconds(1.5f, MenuStatus.msEndOpening);
    }

    public void DeactivateOptionsMenu ()
    {
        DeactivateButtons();
        optionsMenu.GetComponent<Animator>().SetTrigger("up");
        enabled = true;
        MyWaitForSeconds(1.5f, MenuStatus.msEndClosing);
    }
}
