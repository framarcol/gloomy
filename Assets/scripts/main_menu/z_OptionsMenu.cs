﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsMenu : MonoBehaviour {

    enum MenuType { mtOptionsMenu, mtLevelMenu, mtExitMenu};    //Indicate what menu are interacting to
    enum MenuStatus { msOpening1, msOpening2, msRunning, msClosing};         //indicate the status of the menu
    public Transform fondo;
    public Transform menuElements;
    public Transform foregroundTransition;
    public Transform fade;
    [Space (10)]
    [Tooltip ("Sets the time that takes animation of growing a tomb to show it's menu")]
    

    MenuType menuType;
    MenuStatus menuStatus;

 //   float animationPercent;

	// Use this for initialization
	void Start () {
        foregroundTransition.GetComponent<RectTransform>().anchoredPosition = Vector3.up * -360.0f;
        enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		switch (menuType)
        {
            case MenuType.mtOptionsMenu:
                switch (menuStatus)
                {
                    case MenuStatus.msOpening1:
                        break;
                }
            break;

            case MenuType.mtLevelMenu:
                switch (menuStatus)
                {
                    case MenuStatus.msOpening1:
                        MapMenuOpening();
                        enabled = false;
                        break;
                    case MenuStatus.msOpening2:
                        fade.GetComponent<FadeControl>().FadeOff(1.0f);
                        menuStatus = MenuStatus.msRunning;
                        break;
                    case MenuStatus.msRunning:
                        Debug.Log("Ahora haremos Fade ON con el mapa puesto");
                        enabled = false;
                        break;
                }
                break;
        }
	}


    //*****************************************************************************************************************
    //  OPTIONS MENU METHODS
    //*****************************************************************************************************************
    void OptionsMenuOpening ()
    {
        
    }

    //*****************************************************************************************************************
    //  MAP MENU METHODS
    //*****************************************************************************************************************
    void MapMenuOpening()
    {
        fondo.GetComponent<Animator>().SetTrigger("TransitionUp");
        menuElements.GetComponent<Animator>().SetTrigger("TransitionUp");
        foregroundTransition.GetComponent<Animator>().SetTrigger("in");
        menuStatus = MenuStatus.msOpening2;
        MyWaitForSeconds(1.0f);
    }

    void MyWaitForSeconds (float segundos)
    {
        float cont = 0.0f;
        while (cont < segundos)
        {
            cont += Time.deltaTime;
            Debug.Log("Tiempo: " + cont);
        }
        enabled = true;
    }
    //*****************************************************************************************************************
    //  PUBLIC MENU ACTIVATIONS
    //*****************************************************************************************************************
    public void ActivateOptionsMenu ()
    {
        menuType = MenuType.mtOptionsMenu;
        menuStatus = MenuStatus.msOpening1;
     //   animationPercent = 1.0f;
        enabled = true;
    }

    public void ActivateLevelMenu()
    {
        Debug.Log("Activando menu");
        menuType = MenuType.mtLevelMenu;
        menuStatus = MenuStatus.msOpening1;
     //   animationPercent = 1.0f;
        enabled = true;
    }
}
